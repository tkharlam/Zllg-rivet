#include "num2str.C"
//decay types
enum {Z2EEG=1,Z2MUMUG=2};
int DecayTypeSwitch=0;
// JetSwitch
// 1 - no jet selection
// 2 - njets==0
// 3 - njets>0
int JetSwitch=1;
// NPhotonSwitch
// 0 - no photoncounter selection
// n>0 - number of photons photoncounter==n
int NPhotonSwitch=0;
// DressSwitch for leptons
// 1 - dressed
// 2 - undressed
enum {DRESSED=1,UNDRESSED=2};
int DressSwitch=DRESSED;
// SimReweightSwitch
enum {ExternalFunc=1,Sim2PowhegPythia8=2,Sim2Sherpa224=3,Sim2PowhegPythia8RecoReweighted=4};
int PTZrewightSwitch=Sim2PowhegPythia8RecoReweighted;


enum {PowhegPythia8=1,Sherpa_14=2,Sherpa_221=3,AlpgenPythia=4,AlpgenJimmy=5,Sh_224=6,PP8_OneGamma=7};
int SimSwitch=PowhegPythia8;

// dir for printing pictures if dir NULL -> no printing
char *picdir="pics/simcmp/";
//  char *picdir=NULL;
string picname;
// "" if DressSwitch=DRESSED
// "undr_" if DressSwitch=UNDRESSED
string dresspicprefix;
string dresspostfix;
string DecayTypePostfix;

// histograms bins
int nbinsdR=12;
double binsdR[13]={0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.8, 3.2};
//  hdR1 = new TH1D("hdR1","dR",nbinsdR, binsdR);//ndR,ldR,udR);
int nbinsQp=41;
double binsQp[42]={4 , 6 , 8 , 10 , 12 , 14 , 16 , 18 , 20 , 22 , 24 , 26 , 28 , 30 , 32 , 34 , 36 , 38 , 40 , 42 , 44 , 46 , 48 , 50 , 52 , 54 , 56 , 58 , 60 , 62 , 64 , 66 , 68 , 70 , 72 , 74 , 76 , 78 , 80 , 82 , 84 , 90};
//  hQpw1 = new TH1D("hQpw1","Qp",nbinsQp, binsQp);//,nQp,lQp,uQp);
int nbinsPtG=16;
double binsPtG[17]={15, 17, 19, 21, 23,
		    25, 27, 29, 31, 33,
		    35, 37, 39, 41, 43,
		    45, 47};
// hptgammaw1 = new TH1D("hptgammaw1","ptgamma",nbinsPtG, binsPtG);//nptgamma,lptgamma,uptgamma);


// 20 бинов от 0 до 100 ГэВ
int nbinsPtZ=30;
double ubinPtZ=500.0;
double binsPtZ[31]= {  0,  2,  4,  6,  8,
		      10, 12, 14, 16, 18,
		      20, 25, 30, 35, 40,
		      45, 50, 55, 60, 65,
		      70, 75, 80, 90,100,
		     120,140,160,180,200,500};

enum {ReweightArray3Itr=1,ReweightArray1Itr=2};
int ReweightArraySwitch=ReweightArray1Itr;


// Для случая с дрессингом
// 3Itr -- three itteration
// Pt(Z) truth (отобранное в наших условиях по трус переменным) для мюонов
double ContentMuDressedPtZ_3Itr[30]= {1.09317  , 1.01701  , 0.968494 , 0.9822  , 1.05613  ,
				 0.978688 , 0.985235 , 1.01889  , 0.97329 , 0.997565 ,
				 1.00533  , 1.02044  , 0.981396 , 0.950034, 0.954854 ,
				 0.923743 , 1.08644  , 1.03262  , 0.994037, 0.840088 ,
				 0.896884 , 1.03971  , 0.977783 , 1.08985 , 0.96077  ,
				 1.19963  , 0.939939 , 1.08956  , 1.65591 , 0.987845};

// Pt(Z) truth для электронов
double ContentElDressedPtZ_3Itr[30]= { 1.08199 , 0.95467  , 1.03667  , 1.0165  , 0.996994 ,
				  1.00404 , 1.05117  , 0.934127 , 1.17716 , 0.920042 ,
				  0.968685, 1.02714  , 0.935541 , 0.895698, 1.04242  ,
				  0.913568, 0.945391 , 0.928024 , 1.01868 , 0.844705 ,
				  1.18525 , 0.953043 , 1.0987   , 0.961145, 0.953945 ,
				  1.11977 , 1.03245  , 1.57122  , 1.03664 , 0.808055};

// using dressed distribution (should be replaced)
// Для случая с андрессингом
// Pt(Z) truth (отобранное в наших условиях по трус переменным) для мюонов
double ContentMuBarePtZ_3Itr[30]= {1.09196  , 1.0169   , 0.969387 , 0.982626 , 1.0549   ,
			      0.97875  , 0.985662 , 1.01842  , 0.97359  , 0.997784 ,
			      1.00519  , 1.01999  , 0.981063 , 0.950106 , 0.954883 ,
			      0.925401 , 1.0863   , 1.03089  , 0.990899 , 0.841789 ,
			      0.899961 , 1.03968  , 0.978407 , 1.0894   , 0.962056 ,
			      1.19932  , 0.939351 , 1.09744  , 1.65106  , 0.988889};

// Pt(Z) truth для электронов
double ContentElBarePtZ_3Itr[30]= {  1.08235  , 0.952996 , 1.03658  , 1.01709 , 0.996527 ,
				   1.00392  , 1.05196  , 0.933823 , 1.18413 , 0.915849 ,
				   0.967957 , 1.02914  , 0.932143 , 0.895722, 1.04538  ,
				   0.910218 , 0.944843 , 0.931464 , 1.01728 , 0.84967  ,
				   1.19509  , 0.946933 , 1.10571  , 0.95032 , 0.958541 ,
				   1.12637  , 1.04183  , 1.60904  , 0.911253, 0.807161};

// Для случая с дрессингом
// 1Itr -- one itteration
// Pt(Z) truth (отобранное в наших условиях по трус переменным) для мюонов
double ContentMuDressedPtZ_1Itr[30]= {1.06889 , 1.02529 , 0.979593 , 0.992623 , 1.01575 ,
				      0.987408 , 0.997001 , 0.991554 , 0.99103 , 1.01009 ,
				      0.999973 , 1.00612 , 0.986776 , 0.957795 , 0.950745 ,
				      0.971172 , 1.04391 , 1.04291 , 0.969995 , 0.887968 ,
				      0.935707 , 1.04372 , 1.03567 , 1.03622 , 1.00032 ,
				      1.12982 , 0.868235 , 1.01601 , 1.33309 , 1.10575 };

// Pt(Z) truth для электронов
double ContentElDressedPtZ_1Itr[30]= { 1.05299 , 0.984693 , 1.02788 , 1.02093 , 1.01029 ,
				       1.00191 , 1.02651 , 0.984897 , 1.09722 , 0.950445 ,
				       0.973569 , 1.00955 , 0.939813 , 0.918978 , 1.00935 ,
				       0.933198 , 0.923157 , 0.942577 , 0.968736 , 0.914777 ,
				       1.11954 , 0.968367 , 1.06407 , 0.971669 , 0.929001 ,
				       1.07311 , 1.06407 , 1.39193 , 0.937898 , 0.752143 };

// using dressed distribution (should be replaced)
// Для случая с андрессингом
// Pt(Z) truth (отобранное в наших условиях по трус переменным) для мюонов
double ContentMuBarePtZ_1Itr[30]= {1.06889 , 1.02529 , 0.979593 , 0.992623 , 1.01575 ,
				   0.987408 , 0.997001 , 0.991554 , 0.99103 , 1.01009 ,
				   0.999973 , 1.00612 , 0.986776 , 0.957795 , 0.950745 ,
				   0.971172 , 1.04391 , 1.04291 , 0.969995 , 0.887968 ,
				   0.935707 , 1.04372 , 1.03567 , 1.03622 , 1.00032 ,
				   1.12982 , 0.868235 , 1.01601 , 1.33309 , 1.10575 };

// Pt(Z) truth для электронов
double ContentElBarePtZ_1Itr[30]= { 1.05299 , 0.984693 , 1.02788 , 1.02093 , 1.01029 ,
				    1.00191 , 1.02651 , 0.984897 , 1.09722 , 0.950445 ,
				    0.973569 , 1.00955 , 0.939813 , 0.918978 , 1.00935 ,
				    0.933198 , 0.923157 , 0.942577 , 0.968736 , 0.914777 ,
				    1.11954 , 0.968367 , 1.06407 , 0.971669 , 0.929001 ,
				    1.07311 , 1.06407 , 1.39193 , 0.937898 , 0.752143 };


// before pt(Z) true vs reconstruction for powheg+photos rewighting
// int nbinsPtZ=43;
// double ubinPtZ=900.0;
// double binsPtZ[44]= {0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 
// 		     20.0, 22.5, 25.0, 27.5, 30.0, 33.0, 36.0, 39.0, 42.0, 45.0, 
// 		     48.0, 51.0, 54.0, 57.0, 61.0, 65.0, 70.0, 75.0, 80.0, 85.0, 
// 		     95.0, 105.0, 125.0, 150.0, 175.0, 200.0, 250.0, 300.0, 350.0, 400.0, 
// 		     470.0, 550.0, 650.0, 900.0 }; 


// Data fore reweighting from docs/reweight/PtZall.C
double p9030_d27x1y4_xval[43] = { 1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0, 
				19.0, 21.25, 23.75, 26.25, 28.75, 31.5, 34.5, 37.5, 40.5, 43.5, 
				46.5, 49.5, 52.5, 55.5, 59.0, 63.0, 67.5, 72.5, 77.5, 82.5, 
				90.0, 100.0, 115.0, 137.5, 162.5, 187.5, 225.0, 275.0, 325.0, 375.0, 
				435.0, 510.0, 600.0, 775.0 };
double p9030_d27x1y4_xerrminus[43] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 
				     1.0, 1.25, 1.25, 1.25, 1.25, 1.5, 1.5, 1.5, 1.5, 1.5, 
				     1.5, 1.5, 1.5, 1.5, 2.0, 2.0, 2.5, 2.5, 2.5, 2.5, 
				     5.0, 5.0, 10.0, 12.5, 12.5, 12.5, 25.0, 25.0, 25.0, 25.0, 
				     35.0, 40.0, 50.0, 125.0 };
double p9030_d27x1y4_xerrplus[43] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 
				    1.0, 1.25, 1.25, 1.25, 1.25, 1.5, 1.5, 1.5, 1.5, 1.5, 
				    1.5, 1.5, 1.5, 1.5, 2.0, 2.0, 2.5, 2.5, 2.5, 2.5, 
				    5.0, 5.0, 10.0, 12.5, 12.5, 12.5, 25.0, 25.0, 25.0, 25.0, 
				    35.0, 40.0, 50.0, 125.0 };
double p9030_d27x1y4_yval[43] = { 0.02626, 0.05428, 0.05525, 0.04851, 0.04085, 0.0342, 0.02878, 0.02443, 0.02077, 
				0.01781, 0.01503, 0.01266, 0.01068, 0.009064, 0.007709, 0.006541, 0.005487, 0.004728, 0.004086, 
				0.003541, 0.003027, 0.00266, 0.002322, 0.002036, 0.001683, 0.001417, 0.001144, 9.545E-4, 7.721E-4, 
				5.771E-4, 4.044E-4, 2.562E-4, 1.337E-4, 7.115E-5, 3.744E-5, 1.762E-5, 6.756E-6, 2.735E-6, 1.228E-6, 
				5.711E-7, 2.118E-7, 8.54E-8, 7.0E-9 };


TFile *FT1,*FT2,*FT6,*FT7;
TFile *FTS1,*FTS2;
TTree *t1,*t2,*t6,*t7,*tundr1,*tundr2,*tundr6,*tundr7;
TChain *t3,*t4,*t5,*tundr3,*tundr4,*tundr5;
TTree *ts1,*ts2;
TH1D *hptZ0,*hptZ1,*hptZ2,*hptZ3,*hptZ4,*hptZ5,*hptZ6,*hptZ7;
TH1D *hptZw1,*hptZw2,*hptZw3,*hptZw4,*hptZw5,*hptZw6,*hptZw7;
TH1D *hptZdiv12,*hptZdiv13,*hptZdiv14,*hptZdiv15,*hptZdiv16,*hptZdiv17;
TH1D *hptZdiv61,*hptZdiv62,*hptZdiv63,*hptZdiv64,*hptZdiv65,*hptZdiv67;
TH1D *hptZdiv01,*hptZdiv02,*hptZdiv03,*hptZdiv04,*hptZdiv05,*hptZdiv06,*hptZdiv07;
TH1D *hptZr12,*hptZr13,*hptZr14,*hptZr15,*hptZr16,*hptZr17;
TH1D *hptZr61,*hptZr62,*hptZr63,*hptZr64,*hptZr65,*hptZr67;
TH1D *hptZr01,*hptZr02,*hptZr03,*hptZr04,*hptZr05,*hptZr06,*hptZr07;
TH1D *hptgamma1,*hptgamma2,*hptgamma3,*hptgamma4,*hptgamma5,*hptgamma6,*hptgamma7;
TH1D *hptgammaw1,*hptgammaw2,*hptgammaw3,*hptgammaw4,*hptgammaw5,*hptgammaw6,*hptgammaw7;
TH1D *hQp0,*hQp1,*hQp2,*hQp3,*hQp4,*hQp5,*hQp6,*hQp7;
TH1D *hQm0,*hQm1,*hQm2,*hQm3,*hQm4,*hQm5,*hQm6,*hQm7;
TH1D *hQpvsQm0,*hQpvsQm1,*hQpvsQm2,*hQpvsQm3,*hQpvsQm4,*hQpvsQm5,*hQpvsQm6,*hQpvsQm7;
TH1D *hQpBvsR0,*hQpBvsR1,*hQpBvsR2,*hQpBvsR3,*hQpBvsR4,*hQpBvsR5,*hQpBvsR6,*hQpBvsR7;
TH1D *hptgammaBvsR0,*hptgammaBvsR1,*hptgammaBvsR2,*hptgammaBvsR3,*hptgammaBvsR4,*hptgammaBvsR5,*hptgammaBvsR6,*hptgammaBvsR7;
TH1D *hptZBvsR0,*hptZBvsR1,*hptZBvsR2,*hptZBvsR3,*hptZBvsR4,*hptZBvsR5,*hptZBvsR6,*hptZBvsR7;
TH1D *hQpDressed,*hQpUnDressed,*hQpDrVsUn;
TH1D *hQpPTZcut,*hQpNoPTZcut,*hQpPTZVsNoPTZcut;
TH1D *hQpPTZcutDressed,*hQpNoPTZcutDressed,*hQpPTZVsNoPTZcutDressed;
TH1D *hQpt0,*hQpt1,*hQpt2,*hQpt3,*hQpt4,*hQpt5,*hQpt6,*hQpt7;
TH1D *hQpd0,*hQpd1,*hQpd2,*hQpd3,*hQpd4,*hQpd5,*hQpd6,*hQpd7;
TH1D *hQpw1,*hQpw2,*hQpw3,*hQpw4,*hQpw5,*hQpw6,*hQpw7;
TH1D *hdR0,*hdR1,*hdR2,*hdR3,*hdR4,*hdR5,*hdR6,*hdR7;
TH1D *hdRt0,*hdRt1,*hdRt2,*hdRt3,*hdRt4,*hdRt5,*hdRt6,*hdRt7;
TH1D *hQcl0,*hQcl1,*hQcl2,*hQcl3,*hQcl4,*hQcl5,*hQcl6,*hQcl7;
TH1D *hQclt0,*hQclt1,*hQclt2,*hQclt3,*hQclt4,*hQclt5,*hQclt6,*hQclt7;
TH1D *hQpEr1,*hQpEr2;
TH1D *hdREr1,*hdREr2;
TH1D *hQclEr1,*hQclEr2;
TH1D *hdRw1,*hdRw2,*hdRw3,*hdRw4,*hdRw5,*hdRw6,*hdRw7;
TH1D *hQpr12,*hQpr13,*hQpr32,*hQpr14,*hQpr15,*hQpr16,*hQpr17,*hQpr62,*hQpr63,*hQpr1,*hQpr2,*hQpr3,*hQpr4,*hQpr5,*hQpr6,*hQpr7;
TH1D *hQptr12,*hQptr13,*hQptr32,*hQptr14,*hQptr1,*hQptr2,*hQptr3,*hQptr4,*hQptr5,*hQptr6,*hQptr7;
TH1D *hQptd1,*hQptd2,*hQptd3,*hQptd4,*hQptd5,*hQptd6,*hQptd7;
TH1D *hQprw12,*hQprw13,*hQprw32,*hQprw14,*hQprw15,*hQprw16,*hQprw17,*hQprw62,*hQprw63;
TH1D *hdRr12,*hdRr13,*hdRr32,*hdRr14,*hdRr15,*hdRr16,*hdRr17,*hdRr62,*hdRr63
TH1D *hdRrw12,*hdRrw13,*hdRrw32,*hdRrw14,*hdRrw15,*hdRrw16,*hdRrw17,*hdRrw62,*hdRrw63;
TH1D *hptgammar12,*hptgammar13,*hptgammar32,*hptgammar14,*hptgammar15,*hptgammar16,*hptgammar17,*hptgammar62,*hptgammar63;
TH1D *hptgammarw12,*hptgammarw13,*hptgammarw32,*hptgammarw14,*hptgammarw15,*hptgammarw16,*hptgammarw17,*hptgammarw62,*hptgammarw63;



Int_t nptZ, lptZ, uptZ, uptZw;
Int_t nptgamma, lptgamma, uptgamma;
Int_t nQp, lQp, uQp;
Int_t ndR, ldR, udR;
Int_t nQcl, lQcl, uQcl;
Double_t N0,N1,N2,N3,N4,N5,N6,N7;
Double_t Nt0,Nt1,Nt2,Nt3,Nt4,Nt5,Nt6,Nt7;

double M,Qp,Qm,ptZ,etaZ,dR,M2L;
double ptplus,ptminus,ptgamma;
double etaplus,etaminus,etagamma;
double weight,Wfile;
int njets,photoncounter;

TCanvas *cQp,*cQpw,*cQpr,*cQprw,*cptZ,*cptZr,*cptZw,*cdR,*cdRw,*cdRr,*cdRrw,*cQpvsQm,*cQpAndQm,*cDrvsUn,*cDrAndUn,*cQpBvsR,*cQpBAndR,*cdRBvsR,*cdRBAndR,*cptgamma,*cptgammar,*cptgammarw,*cptgammaBvsR,*cptgammaBAndR;

string curstr,dirstr,filestr;
string Keefilestr,Kmmfilestr;

void init() {
  gStyle->SetOptStat(0); 
  gStyle->SetOptFit(1); 
  gStyle->SetOptTitle(0);
}

// simulation comparision
int diff() {
  int result1=1,result2=1;
  result1=diff(Z2EEG);
  result2=diff(Z2MUMUG);
  return result1*result2;
}

int diff(int decaytype) {
  return diff(decaytype,JetSwitch);
}
int diffForSim(int decaytype,int simtype) {
  return diff(decaytype,1,simtype);
}

int diff(int decaytype,int jetswitch) {
  return diff(decaytype,jetswitch,1);
}


//undressed leptons
int diffundr(int decaytype,int jetswitch,int simtype) {
  int oldDressSwitch=DressSwitch;
  DressSwitch=UNDRESSED;
  diff(decaytype,jetswitch,simtype)
  DressSwitch=oldDressSwitch;
}
//dressed leptons
int diffundr(int decaytype,int jetswitch,int simtype) {
  int oldDressSwitch=DressSwitch;
  DressSwitch=DRESSED;
  diff(decaytype,jetswitch,simtype)
  DressSwitch=oldDressSwitch;
}

int diff(int decaytype,int jetswitch,int simtype) {
  int oldJetSwitch=JetSwitch;
  JetSwitch=jetswitch;
  int oldSimSwitch=SimSwitch;
  SimSwitch=simtype;
  init();
  switch (decaytype){
  case Z2EEG:
    DecayTypeSwitch=Z2EEG;
    open_files_eeg();
    makeptZhist();
    histreweight();
    makepics(Z2EEG);
    close_chains();
    break;
  case Z2MUMUG:
    DecayTypeSwitch=Z2MUMUG;
    open_files_mmg();
    makeptZhist();
    histreweight();
    makepics(Z2MUMUG);
    close_chains();
    break;
  default:
    JetSwitch=oldJetSwitch;
    SimSwitch=oldSimSwitch;
    return 0;
  }
  JetSwitch=oldJetSwitch;
  SimSwitch=oldSimSwitch;
  return 1;
}

int open_files_mmg() {
  FT1=(TFile *)gROOT->FindObject("Rivet-tree-PowhegPythia8ZcorrZmmg");
  if (!FT1) FT1 = new TFile("data/mc12_8TeV.207321.PowhegPythia8_AU2CT10_Zmumu_2Lepton1Photon_ZCorr.evgen.EVNT.e5239.root","Rivet-tree-PowhegPythia8ZcorrZmmg","");
  t1=(TTree *)FT1->Get("tmmg");
  tundr1=(TTree *)FT1->Get("tmmgundr");

  FT2=(TFile *)gROOT->FindObject("Rivet-tree-mc12-Sherpa-Zmmg");
  if (!FT2) FT2 =  new TFile("data/mc12_8TeV.145162.Sherpa_CT10_mumugammaPt10.evgen.EVNT.e1434.root","Rivet-tree-mc12-Sherpa-Zmmg","");
  t2=(TTree *)FT2->Get("tmmg");
  tundr2=(TTree *)FT2->Get("tmmgundr");

  //  FT3 = new TFile("data/Rivet-tree-mc15-Sherpa-Zmmg.root","hist-mc15-Sherpa-Zmmg","");
  //  t3=(TTree *)FT3->Get("tmmg");

  t3 = new TChain("tmmg");

  t3->Add("data/mc15_8TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.evgen.EVNT.e5375.root");

  tundr3 = new TChain("tmmgundr");

  tundr3->Add("data/mc15_8TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.evgen.EVNT.e5375.root");
  
  // AlpgenPythia
  t4 = new TChain("tmmg");
  t4->Add("data/mc12_8TeV.147113.AlpgenPythia_Auto_P2011C_ZmumuNp0.evgen.EVNT.e1880.root");
  t4->Add("data/mc12_8TeV.147114.AlpgenPythia_Auto_P2011C_ZmumuNp1.evgen.EVNT.e1880.root");
  t4->Add("data/mc12_8TeV.147115.AlpgenPythia_Auto_P2011C_ZmumuNp2.evgen.EVNT.e1880.root");
  t4->Add("data/mc12_8TeV.147116.AlpgenPythia_Auto_P2011C_ZmumuNp3.evgen.EVNT.e1880.root");
  t4->Add("data/mc12_8TeV.147117.AlpgenPythia_Auto_P2011C_ZmumuNp4.evgen.EVNT.e1880.root");
  t4->Add("data/mc12_8TeV.147118.AlpgenPythia_Auto_P2011C_ZmumuNp5incl.evgen.EVNT.e1880.root");

  tundr4 = new TChain("tmmgundr");
  tundr4->Add("data/mc12_8TeV.147113.AlpgenPythia_Auto_P2011C_ZmumuNp0.evgen.EVNT.e1880.root");
  tundr4->Add("data/mc12_8TeV.147114.AlpgenPythia_Auto_P2011C_ZmumuNp1.evgen.EVNT.e1880.root");
  tundr4->Add("data/mc12_8TeV.147115.AlpgenPythia_Auto_P2011C_ZmumuNp2.evgen.EVNT.e1880.root");
  tundr4->Add("data/mc12_8TeV.147116.AlpgenPythia_Auto_P2011C_ZmumuNp3.evgen.EVNT.e1880.root");
  tundr4->Add("data/mc12_8TeV.147117.AlpgenPythia_Auto_P2011C_ZmumuNp4.evgen.EVNT.e1880.root");
  tundr4->Add("data/mc12_8TeV.147118.AlpgenPythia_Auto_P2011C_ZmumuNp5incl.evgen.EVNT.e1880.root");
  
  // AlpgenJimmy
  t5 = new TChain("tmmg");
  t5->Add("data/mc12_8TeV.107660.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp0.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107661.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp1.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107662.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp2.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107663.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp3.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107664.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp4.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107665.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp5.evgen.EVNT.e1571.root");

  tundr5 = new TChain("tmmgundr");
  tundr5->Add("data/mc12_8TeV.107660.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp0.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107661.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp1.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107662.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp2.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107663.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp3.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107664.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp4.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107665.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp5.evgen.EVNT.e1571.root");

  // Sherpa 2.24
  FT6=(TFile *)gROOT->FindObject("Rivet-tree-mc15_8TeV.364388.Sh_224_NN30NNLO_mumugamma");
  if (!FT6) FT6 = new TFile("data/mc15_8TeV.364388.Sh_224_NN30NNLO_mumugamma.evgen.EVNT.e6994_tid15777202_00.root","Rivet-tree-mc15_8TeV.364388.Sh_224_NN30NNLO_mumugamma","");
  t6=(TTree *)FT6->Get("tmmg");
  tundr6=(TTree *)FT6->Get("tmmgundr");

  // PowhegPythia8+PHOTOS (One #gamma only)
  // PowhegPythia8 Only one photon
  FT7=(TFile *)gROOT->FindObject("Rivet-tree-Zee-2Lepton1Photon-PP8_OneGamma");
  if (!FT7) FT7 = new TFile("data/mc12_8TeV.129252.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon_ZCorr_Mod.evgen.EVNT.e7027.root","Rivet-tree-Zee-2Lepton1Photon--PP8_OneGamma","");
  t7=(TTree *)FT7->Get("teeglikemmg");
  tundr7=(TTree *)FT7->Get("teeglikemmgundr");
  return 1;
}

int close_files() {
  FT1->Close();
  FT2->Close();
  FT6->Close();
  FT7->Close();    
  return 1;
}


int open_files_eeg() {
  FT1=(TFile *)gROOT->FindObject("Rivet-tree-PowhegPythia8ZcorrZeeg");
  if (!FT1) FT1 = new TFile("data/mc12_8TeV.207320.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon_ZCorr.evgen.EVNT.e5239.root","Rivet-tree-PowhegPythia8ZcorrZeeg","");
  t1=(TTree *)FT1->Get("teeg");
  tundr1=(TTree *)FT1->Get("teegundr");
  
  FT2=(TFile *)gROOT->FindObject("Rivet-tree-mc12-Sherpa-Zeeg");
  if (!FT2) FT2 =  new TFile("data/mc12_8TeV.145161.Sherpa_CT10_eegammaPt10.evgen.EVNT.e1434.root","Rivet-tree-mc12-Sherpa-Zeeg","");
  t2=(TTree *)FT2->Get("teeg");
  tundr2=(TTree *)FT2->Get("teegundr");

  //  FT3 = new TFile("data/Rivet-tree-mc15-Sherpa-Zeeg.root","hist-mc15-Sherpa-Zeeg","");
  //t3=(TTree *)FT3->Get("teeg");

  t3 = new TChain("teeg");
  t3->Add("data/mc15_8TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.evgen.EVNT.e5345.root");
  t3->Add("data/mc15_8TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5345.root");
  t3->Add("data/mc15_8TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.evgen.EVNT.e5345.root");
  t3->Add("data/mc15_8TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.evgen.EVNT.e5375.root");
  t3->Add("data/mc15_8TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.evgen.EVNT.e5320.root");
  t3->Add("data/mc15_8TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.evgen.EVNT.e5320.root");

  tundr3 = new TChain("teegundr");
  tundr3->Add("data/mc15_8TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.evgen.EVNT.e5345.root");
  tundr3->Add("data/mc15_8TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5345.root");
  tundr3->Add("data/mc15_8TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.evgen.EVNT.e5345.root");
  tundr3->Add("data/mc15_8TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.evgen.EVNT.e5375.root");
  tundr3->Add("data/mc15_8TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.evgen.EVNT.e5320.root");
  tundr3->Add("data/mc15_8TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.evgen.EVNT.e5320.root");

  // AlpgenPythia
  t4 = new TChain("teeg");

  t4->Add("data/mc12_8TeV.147105.AlpgenPythia_Auto_P2011C_ZeeNp0.evgen.EVNT.e1879.root");
  t4->Add("data/mc12_8TeV.147106.AlpgenPythia_Auto_P2011C_ZeeNp1.evgen.EVNT.e1879.root");
  t4->Add("data/mc12_8TeV.147107.AlpgenPythia_Auto_P2011C_ZeeNp2.evgen.EVNT.e1879.root");
  t4->Add("data/mc12_8TeV.147108.AlpgenPythia_Auto_P2011C_ZeeNp3.evgen.EVNT.e1879.root");
  t4->Add("data/mc12_8TeV.147109.AlpgenPythia_Auto_P2011C_ZeeNp4.evgen.EVNT.e1879.root");
  t4->Add("data/mc12_8TeV.147110.AlpgenPythia_Auto_P2011C_ZeeNp5incl.evgen.EVNT.e1879.root");

  tundr4 = new TChain("teegundr");

  tundr4->Add("data/mc12_8TeV.147105.AlpgenPythia_Auto_P2011C_ZeeNp0.evgen.EVNT.e1879.root");
  tundr4->Add("data/mc12_8TeV.147106.AlpgenPythia_Auto_P2011C_ZeeNp1.evgen.EVNT.e1879.root");
  tundr4->Add("data/mc12_8TeV.147107.AlpgenPythia_Auto_P2011C_ZeeNp2.evgen.EVNT.e1879.root");
  tundr4->Add("data/mc12_8TeV.147108.AlpgenPythia_Auto_P2011C_ZeeNp3.evgen.EVNT.e1879.root");
  tundr4->Add("data/mc12_8TeV.147109.AlpgenPythia_Auto_P2011C_ZeeNp4.evgen.EVNT.e1879.root");
  tundr4->Add("data/mc12_8TeV.147110.AlpgenPythia_Auto_P2011C_ZeeNp5incl.evgen.EVNT.e1879.root");


  // AlpgenJimmy
  t5 = new TChain("teeg");
  t5->Add("data/mc12_8TeV.107650.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp0.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107651.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp1.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107652.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp2.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107653.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp3.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107654.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp4.evgen.EVNT.e1571.root");
  t5->Add("data/mc12_8TeV.107655.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp5.evgen.EVNT.e1571.root");

  tundr5 = new TChain("teegundr");
  tundr5->Add("data/mc12_8TeV.107650.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp0.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107651.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp1.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107652.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp2.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107653.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp3.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107654.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp4.evgen.EVNT.e1571.root");
  tundr5->Add("data/mc12_8TeV.107655.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp5.evgen.EVNT.e1571.root");

  // Sherpa 2.24
  FT6=(TFile *)gROOT->FindObject("Rivet-tree-mc15_8TeV.364387.Sh_224_NN30NNLO_eegamma");
  if (!FT6) FT6 = new TFile("data/mc15_8TeV.364387.Sh_224_NN30NNLO_eegamma.evgen.EVNT.e6994_tid15777197_00.root","Rivet-tree-mc15_8TeV.364387.Sh_224_NN30NNLO_eegamma","");
  t6=(TTree *)FT6->Get("teeg");
  tundr6=(TTree *)FT6->Get("teegundr");

  // PowhegPythia8 Only one photon
  FT7=(TFile *)gROOT->FindObject("Rivet-tree-Zee-2Lepton1Photon-PP8_OneGamma");
  if (!FT7) FT7 = new TFile("data/user.tkharlam.mc12_8TeV.129250.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon.ZCorr.Mod.test6_EXT1.root","Rivet-tree-Zee-2Lepton1Photon-PP8_OneGamma","");
  t7=(TTree *)FT7->Get("teeg");
  tundr7=(TTree *)FT7->Get("teegundr");

  // FT7=(TFile *)gROOT->FindObject("Rivet-tree-Zee-2Lepton1Photon-PP8_OneGamma");
  // if (!FT7) FT7 = new TFile("data/mc12_8TeV.129252.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon_ZCorr_Mod.evgen.EVNT.e7027.root","Rivet-tree-Zee-2Lepton1Photon--PP8_OneGamma","");
  // t7=(TTree *)FT7->Get("teeg");
  // tundr7=(TTree *)FT7->Get("teegundr");
  
  return 1;
}

int close_chains() {
  delete t3;
  delete t4;
  delete t5;
  delete tundr3;
  delete tundr4;
  delete tundr5;
}



int makeptZhist(){
  TTree *tlocal1,*tlocal2,*tlocal6,*tlocal7;
  TChain *tlocal3,*tlocal4,*tlocal5;
  string addselectstr;
  switch (DressSwitch) {
  case DRESSED:
    tlocal1=t1;
    tlocal2=t2;
    tlocal3=t3;
    tlocal4=t4;
    tlocal5=t5;
    tlocal6=t6;
    tlocal7=t7;
    break;
  case UNDRESSED:
    tlocal1=tundr1;
    tlocal2=tundr2;
    tlocal3=tundr3;
    tlocal4=tundr4;
    tlocal5=tundr5;
    tlocal6=tundr6;
    tlocal7=tundr7;
    break;
  }

  switch(JetSwitch) {
  case 1:  // 1 - no jet selection
    addselectstr="(isSelected==1)";
    break;
  case 2: // 2 - njets==0
    addselectstr="(njets==0)";
    break;
  case 3: // 3 - njets>0
    addselectstr="(njets>0)";
    break;
  default:
    cout << "Unknown JetSwitch="<< JetSwitch << endl;
    return -1;
  }
  if (NPhotonSwitch>0)
    addselectstr="("+addselectstr+"&&(photoncounter=="+str(NPhotonSwitch)+"))";
    
    
  string cutstr,cutstrNoPTZcut;
  nptZ=80;lptZ=0.;uptZ=900.;uptZw=900.;
  nptgamma=80;lptgamma=0.;uptgamma=80.;
  nQp=50;lQp=0.;uQp=100.;
  ndR=30;ldR=0.;udR=3.0;

  
  hptZ1 = new TH1D("hptZ1","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZ1->Sumw2();
  cutstr=addselectstr+"*W*Wfile";
  tlocal1->Draw("ptZ>>hptZ1",cutstr.c_str());
  N1=hptZ1->Integral(1,nbinsPtZ);
  cout << "N1="<< N1 << endl;

  // truth reweighting for p_T(Z) reconstruction agreement for POWHEG+PHOTOS
  hptZ0 = new TH1D("hptZ0","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZ0->Sumw2();
  for (Int_t i=1;i<=nbinsPtZ;i++) {
    Double_t w,cont,cont_err;
    if (DressSwitch==DRESSED) {
      if (DecayTypeSwitch==Z2EEG) {
	if (ReweightArraySwitch==ReweightArray3Itr) 
	  w=ContentElDressedPtZ_3Itr[i-1];
	else if (ReweightArraySwitch==ReweightArray1Itr) 
	  w=ContentElDressedPtZ_1Itr[i-1];
      }
      else if (DecayTypeSwitch==Z2MUMUG) {
	if (ReweightArraySwitch==ReweightArray3Itr) 
	  w=ContentMuDressedPtZ_3Itr[i-1];
	else if (ReweightArraySwitch==ReweightArray1Itr) 
	  w=ContentMuDressedPtZ_1Itr[i-1];
      }
    }
    else if (DressSwitch==UNDRESSED) {
      if (DecayTypeSwitch==Z2EEG) {
	if (ReweightArraySwitch==ReweightArray3Itr) 
	  w=ContentElBarePtZ_3Itr[i-1];	
	else if (ReweightArraySwitch==ReweightArray1Itr) 
	  w=ContentElBarePtZ_1Itr[i-1];	
      }
      else if (DecayTypeSwitch==Z2MUMUG)  {
	if (ReweightArraySwitch==ReweightArray3Itr) 
	  w=ContentMuBarePtZ_3Itr[i-1];	
	else if (ReweightArraySwitch==ReweightArray1Itr) 
	  w=ContentMuBarePtZ_1Itr[i-1];	
      }
    }
    cont=hptZ1->GetBinContent(i)*w;
    hptZ0->SetBinContent(i,cont);
    cont_err=hptZ1->GetBinError(i)*w;
    hptZ0->SetBinError(i,cont_err);
  }

  N0=hptZ0->Integral(1,nbinsPtZ);
  cout << "N0="<< N0 << endl;
  
  hptZw1 = new TH1D("hptZw1","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZw1->Sumw2();

  hptgamma1 = new TH1D("hptgamma1","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgamma1->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal1->Draw("ptg>>hptgamma1",cutstr.c_str());

  hptgammaw1 = new TH1D("hptgammaw1","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgammaw1->Sumw2();
    
  hQp1 = new TH1D("hQp1","Qp",nbinsQp, binsQp);
  hQp1->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal1->Draw("Qp>>hQp1",cutstr.c_str());

  hQm1 = new TH1D("hQm1","Qm",nbinsQp, binsQp);
  hQm1->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal1->Draw("Qm>>hQm1",cutstr.c_str());

  hQpvsQm1=(TH1D *)hQp1->Clone("hQpvsQm1");
  hQpvsQm1->Divide(hQm1);
  
  hQpw1 = new TH1D("hQpw1","Qp",nbinsQp, binsQp);
  hQpw1->Sumw2();

  hdR1 = new TH1D("hdR1","dR",nbinsdR, binsdR);
  hdR1->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal1->Draw("dR>>hdR1",cutstr.c_str());

  hdRw1 = new TH1D("hdRw1","dR",nbinsdR, binsdR);
  hdRw1->Sumw2();


  tlocal1->SetBranchAddress("M",&M);
  tlocal1->SetBranchAddress("photoncounter",&photoncounter);
  tlocal1->SetBranchAddress("Qp",&Qp);
  tlocal1->SetBranchAddress("Qm",&Qm);
  tlocal1->SetBranchAddress("ptZ",&ptZ);
  tlocal1->SetBranchAddress("etaZ",&etaZ);
  tlocal1->SetBranchAddress("dR",&dR);
  tlocal1->SetBranchAddress("M2L",&M2L);
  tlocal1->SetBranchAddress("ptp",&ptplus);
  tlocal1->SetBranchAddress("ptm",&ptminus);
  tlocal1->SetBranchAddress("ptg",&ptgamma);
  tlocal1->SetBranchAddress("etap",&etaplus);
  tlocal1->SetBranchAddress("etam",&etaminus);
  tlocal1->SetBranchAddress("etag",&etagamma);
  tlocal1->SetBranchAddress("W",&weight);
  tlocal1->SetBranchAddress("Wfile",&Wfile);
  tlocal1->SetBranchAddress("njets",&njets);

  hptZ2 = new TH1D("hptZ2","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZ2->Sumw2();
  cutstr=addselectstr+"*W*Wfile";  
  tlocal2->Draw("ptZ>>hptZ2",cutstr.c_str());
  N2=hptZ2->Integral(1,nbinsPtZ);  
  cout << "N2="<< N2 << endl;

  hptZw2 = new TH1D("hptZw2","ptZ",nbinsPtZ,binsPtZ);
  hptZw2->Sumw2();

  hptgamma2 = new TH1D("hptgamma2","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgamma2->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal2->Draw("ptg>>hptgamma2",cutstr.c_str());

  hptgammaw2 = new TH1D("hptgammaw2","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgammaw2->Sumw2();
  
  hQp2 = new TH1D("hQp2","Qp",nbinsQp, binsQp);
  hQp2->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal2->Draw("Qp>>hQp2",cutstr.c_str());

  hQm2 = new TH1D("hQm2","Qm",nbinsQp, binsQp);
  hQm2->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal2->Draw("Qm>>hQm2",cutstr.c_str());

  hQpvsQm2=(TH1D *)hQp2->Clone("hQpvsQm2");
  hQpvsQm2->Divide(hQm2);
  
  hQpw2 = new TH1D("hQpw2","Qp",nbinsQp, binsQp);
  hQpw2->Sumw2();

  hdR2 = new TH1D("hdR2","dR",nbinsdR, binsdR);
  hdR2->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal2->Draw("dR>>hdR2",cutstr.c_str());

  hdRw2 = new TH1D("hdRw2","dR",nbinsdR, binsdR);
  hdRw2->Sumw2();

  tlocal2->SetBranchAddress("M",&M);
  tlocal2->SetBranchAddress("photoncounter",&photoncounter);
  tlocal2->SetBranchAddress("Qp",&Qp);
  tlocal2->SetBranchAddress("Qm",&Qm);
  tlocal2->SetBranchAddress("ptZ",&ptZ);
  tlocal2->SetBranchAddress("etaZ",&etaZ);
  tlocal2->SetBranchAddress("dR",&dR);
  tlocal2->SetBranchAddress("M2L",&M2L);
  tlocal2->SetBranchAddress("ptp",&ptplus);
  tlocal2->SetBranchAddress("ptm",&ptminus);
  tlocal2->SetBranchAddress("ptg",&ptgamma);
  tlocal2->SetBranchAddress("etap",&etaplus);
  tlocal2->SetBranchAddress("etam",&etaminus);
  tlocal2->SetBranchAddress("etag",&etagamma);
  tlocal2->SetBranchAddress("W",&weight);
  tlocal2->SetBranchAddress("Wfile",&Wfile);
  tlocal2->SetBranchAddress("njets",&njets);

  hptZ3 = new TH1D("hptZ3","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZ3->Sumw2();
  cutstr=addselectstr+"*W*Wfile";
  tlocal3->Draw("ptZ>>hptZ3",cutstr.c_str());
  N3=hptZ3->Integral(1,nbinsPtZ);  
  cout << "N3="<< N3 << endl;

  hptZw3 = new TH1D("hptZw3","ptZ",nbinsPtZ,binsPtZ);
  hptZw3->Sumw2();

  hptgamma3 = new TH1D("hptgamma3","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgamma3->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal3->Draw("ptg>>hptgamma3",cutstr.c_str());

  hptgammaw3 = new TH1D("hptgammaw3","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgammaw3->Sumw2();
  
  hQp3 = new TH1D("hQp3","Qp",nbinsQp, binsQp);
  hQp3->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal3->Draw("Qp>>hQp3",cutstr.c_str());

  hQm3 = new TH1D("hQm3","Qm",nbinsQp, binsQp);
  hQm3->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal3->Draw("Qm>>hQm3",cutstr.c_str());

  hQpvsQm3=(TH1D *)hQp3->Clone("hQpvsQm3");
  hQpvsQm3->Divide(hQm3);
  
  hQpw3 = new TH1D("hQpw3","Qp",nbinsQp, binsQp);
  hQpw3->Sumw2();

  hdR3 = new TH1D("hdR3","dR",nbinsdR, binsdR);
  hdR3->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal3->Draw("dR>>hdR3",cutstr.c_str());

  hdRw3 = new TH1D("hdRw3","dR",nbinsdR, binsdR);
  hdRw3->Sumw2();

  tlocal3->SetBranchAddress("M",&M);
  tlocal3->SetBranchAddress("photoncounter",&photoncounter);
  tlocal3->SetBranchAddress("Qp",&Qp);
  tlocal3->SetBranchAddress("Qm",&Qm);
  tlocal3->SetBranchAddress("ptZ",&ptZ);
  tlocal3->SetBranchAddress("etaZ",&etaZ);
  tlocal3->SetBranchAddress("dR",&dR);
  tlocal3->SetBranchAddress("M2L",&M2L);
  tlocal3->SetBranchAddress("ptp",&ptplus);
  tlocal3->SetBranchAddress("ptm",&ptminus);
  tlocal3->SetBranchAddress("ptg",&ptgamma);
  tlocal3->SetBranchAddress("etap",&etaplus);
  tlocal3->SetBranchAddress("etam",&etaminus);
  tlocal3->SetBranchAddress("etag",&etagamma);
  tlocal3->SetBranchAddress("W",&weight);
  tlocal3->SetBranchAddress("Wfile",&Wfile);
  tlocal3->SetBranchAddress("njets",&njets);

  
  hptZ4 = new TH1D("hptZ4","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZ4->Sumw2();
  cutstr=addselectstr+"*W*Wfile";  
  tlocal4->Draw("ptZ>>hptZ4",cutstr.c_str());
  N4=hptZ4->Integral(1,nbinsPtZ);  
  cout << "N4="<< N4 << endl;

  hptZw4 = new TH1D("hptZw4","ptZ",nbinsPtZ,binsPtZ);
  hptZw4->Sumw2();

  hptgamma4 = new TH1D("hptgamma4","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgamma4->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  cutstr=addselectstr+"*W*Wfile";  
  tlocal4->Draw("ptg>>hptgamma4",cutstr.c_str());

  hptgammaw4 = new TH1D("hptgammaw4","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgammaw4->Sumw2();
  
  hQp4 = new TH1D("hQp4","Qp",nbinsQp, binsQp);
  hQp4->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal4->Draw("Qp>>hQp4",cutstr.c_str());

  hQm4 = new TH1D("hQm4","Qm",nbinsQp, binsQp);
  hQm4->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal4->Draw("Qm>>hQm4",cutstr.c_str());

  hQpvsQm4=(TH1D *)hQp4->Clone("hQpvsQm4");
  hQpvsQm4->Divide(hQm4);  
  
  hQpw4 = new TH1D("hQpw4","Qp",nbinsQp, binsQp);
  hQpw4->Sumw2();

  hdR4 = new TH1D("hdR4","dR",nbinsdR, binsdR);
  hdR4->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal4->Draw("dR>>hdR4",cutstr.c_str());

  hdRw4 = new TH1D("hdRw4","dR",nbinsdR, binsdR);
  hdRw4->Sumw2();

  tlocal4->SetBranchAddress("M",&M);
  tlocal4->SetBranchAddress("photoncounter",&photoncounter);
  tlocal4->SetBranchAddress("Qp",&Qp);
  tlocal4->SetBranchAddress("Qm",&Qm);
  tlocal4->SetBranchAddress("ptZ",&ptZ);
  tlocal4->SetBranchAddress("etaZ",&etaZ);
  tlocal4->SetBranchAddress("dR",&dR);
  tlocal4->SetBranchAddress("M2L",&M2L);
  tlocal4->SetBranchAddress("ptp",&ptplus);
  tlocal4->SetBranchAddress("ptm",&ptminus);
  tlocal4->SetBranchAddress("ptg",&ptgamma);
  tlocal4->SetBranchAddress("etap",&etaplus);
  tlocal4->SetBranchAddress("etam",&etaminus);
  tlocal4->SetBranchAddress("etag",&etagamma);
  tlocal4->SetBranchAddress("W",&weight);
  tlocal4->SetBranchAddress("Wfile",&Wfile);
  tlocal4->SetBranchAddress("njets",&njets);

  hptZ5 = new TH1D("hptZ5","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZ5->Sumw2();
  cutstr=addselectstr+"*W*Wfile";  
  tlocal5->Draw("ptZ>>hptZ5",cutstr.c_str());
  N5=hptZ5->Integral(1,nbinsPtZ);  
  cout << "N5="<< N5 << endl;

  hptZw5 = new TH1D("hptZw5","ptZ",nbinsPtZ,binsPtZ);
  hptZw5->Sumw2();

  hptgamma5 = new TH1D("hptgamma5","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgamma5->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal5->Draw("ptg>>hptgamma5",cutstr.c_str());

  hptgammaw5 = new TH1D("hptgammaw5","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgammaw5->Sumw2();


  hQp5 = new TH1D("hQp5","Qp",nbinsQp, binsQp);
  hQp5->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal5->Draw("Qp>>hQp5",cutstr.c_str());

  hQm5 = new TH1D("hQm5","Qm",nbinsQp, binsQp);
  hQm5->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal5->Draw("Qm>>hQm5",cutstr.c_str());

  hQpvsQm5=(TH1D *)hQp5->Clone("hQpvsQm5");
  hQpvsQm5->Divide(hQm5);  
  
  hQpw5 = new TH1D("hQpw5","Qp",nbinsQp, binsQp);
  hQpw5->Sumw2();

  hdR5 = new TH1D("hdR5","dR",nbinsdR, binsdR);
  hdR5->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal5->Draw("dR>>hdR5",cutstr.c_str());

  hdRw5 = new TH1D("hdRw5","dR",nbinsdR, binsdR);
  hdRw5->Sumw2();

  tlocal5->SetBranchAddress("M",&M);
  tlocal5->SetBranchAddress("photoncounter",&photoncounter);
  tlocal5->SetBranchAddress("Qp",&Qp);
  tlocal5->SetBranchAddress("Qm",&Qm);
  tlocal5->SetBranchAddress("ptZ",&ptZ);
  tlocal5->SetBranchAddress("etaZ",&etaZ);
  tlocal5->SetBranchAddress("dR",&dR);
  tlocal5->SetBranchAddress("M2L",&M2L);
  tlocal5->SetBranchAddress("ptp",&ptplus);
  tlocal5->SetBranchAddress("ptm",&ptminus);
  tlocal5->SetBranchAddress("ptg",&ptgamma);
  tlocal5->SetBranchAddress("etap",&etaplus);
  tlocal5->SetBranchAddress("etam",&etaminus);
  tlocal5->SetBranchAddress("etag",&etagamma);
  tlocal5->SetBranchAddress("W",&weight);
  tlocal5->SetBranchAddress("Wfile",&Wfile);
  tlocal5->SetBranchAddress("njets",&njets);

  hptZ6 = new TH1D("hptZ6","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZ6->Sumw2();
  cutstr=addselectstr+"*W*Wfile";  
  tlocal6->Draw("ptZ>>hptZ6",cutstr.c_str());
  N6=hptZ6->Integral(1,nbinsPtZ);  
  cout << "N6="<< N6 << endl;
  
  hptZw6 = new TH1D("hptZw6","ptZ",nbinsPtZ,binsPtZ);
  hptZw6->Sumw2();

  hptgamma6 = new TH1D("hptgamma6","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgamma6->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal6->Draw("ptg>>hptgamma6",cutstr.c_str());

  hptgammaw6 = new TH1D("hptgammaw6","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgammaw6->Sumw2();


  hQp6 = new TH1D("hQp6","Qp",nbinsQp, binsQp);
  hQp6->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal6->Draw("Qp>>hQp6",cutstr.c_str());

  hQm6 = new TH1D("hQm6","Qm",nbinsQp, binsQp);
  hQm6->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal6->Draw("Qm>>hQm6",cutstr.c_str());

  hQpvsQm6=(TH1D *)hQp6->Clone("hQpvsQm6");
  hQpvsQm6->Divide(hQm6);  
  
  hQpw6 = new TH1D("hQpw6","Qp",nbinsQp, binsQp);
  hQpw6->Sumw2();

  hdR6 = new TH1D("hdR6","dR",nbinsdR, binsdR);
  hdR6->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal6->Draw("dR>>hdR6",cutstr.c_str());

  hdRw6 = new TH1D("hdRw6","dR",nbinsdR, binsdR);
  hdRw6->Sumw2();

  tlocal6->SetBranchAddress("M",&M);
  tlocal6->SetBranchAddress("photoncounter",&photoncounter);
  tlocal6->SetBranchAddress("Qp",&Qp);
  tlocal6->SetBranchAddress("Qm",&Qm);
  tlocal6->SetBranchAddress("ptZ",&ptZ);
  tlocal6->SetBranchAddress("etaZ",&etaZ);
  tlocal6->SetBranchAddress("dR",&dR);
  tlocal6->SetBranchAddress("M2L",&M2L);
  tlocal6->SetBranchAddress("ptp",&ptplus);
  tlocal6->SetBranchAddress("ptm",&ptminus);
  tlocal6->SetBranchAddress("ptg",&ptgamma);
  tlocal6->SetBranchAddress("etap",&etaplus);
  tlocal6->SetBranchAddress("etam",&etaminus);
  tlocal6->SetBranchAddress("etag",&etagamma);
  tlocal6->SetBranchAddress("W",&weight);
  tlocal6->SetBranchAddress("Wfile",&Wfile);
  tlocal6->SetBranchAddress("njets",&njets);

  hptZ7 = new TH1D("hptZ7","p_{T}(Z)",nbinsPtZ,binsPtZ);
  hptZ7->Sumw2();
  cutstr=addselectstr+"*W*Wfile";  
  tlocal7->Draw("ptZ>>hptZ7",cutstr.c_str());
  N7=hptZ7->Integral(1,nbinsPtZ);   
  cout << "N7="<< N7 << endl;
  
  hptZw7 = new TH1D("hptZw7","ptZ",nbinsPtZ,binsPtZ);
  hptZw7->Sumw2();

  hptgamma7 = new TH1D("hptgamma7","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgamma7->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal7->Draw("ptg>>hptgamma7",cutstr.c_str());
    
  hptgammaw7 = new TH1D("hptgammaw7","p_{T}(#gamma)",nbinsPtG, binsPtG);
  hptgammaw7->Sumw2();

    
  hQp7 = new TH1D("hQp7","Qp",nbinsQp, binsQp);
  hQp7->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal7->Draw("Qp>>hQp7",cutstr.c_str());

  hQm7 = new TH1D("hQm7","Qm",nbinsQp, binsQp);
  hQm7->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal7->Draw("Qm>>hQm7",cutstr.c_str());

  hQpvsQm7=(TH1D *)hQp7->Clone("hQpvsQm7");
  hQpvsQm7->Divide(hQm7);  
  
  hQpw7 = new TH1D("hQpw7","Qp",nbinsQp, binsQp);
  hQpw7->Sumw2();

  hdR7 = new TH1D("hdR7","dR",nbinsdR, binsdR);
  hdR7->Sumw2();
  cutstr="((ptZ<"+str(uptZ)+")&&"+addselectstr+")*W*Wfile";
  tlocal7->Draw("dR>>hdR7",cutstr.c_str());
  
  hdRw7 = new TH1D("hdRw7","dR",nbinsdR, binsdR);
  hdRw7->Sumw2();

  tlocal7->SetBranchAddress("M",&M);
  tlocal7->SetBranchAddress("photoncounter",&photoncounter);
  tlocal7->SetBranchAddress("Qp",&Qp);
  tlocal7->SetBranchAddress("Qm",&Qm);
  tlocal7->SetBranchAddress("ptZ",&ptZ);
  tlocal7->SetBranchAddress("etaZ",&etaZ);
  tlocal7->SetBranchAddress("dR",&dR);
  tlocal7->SetBranchAddress("M2L",&M2L);
  tlocal7->SetBranchAddress("ptp",&ptplus);
  tlocal7->SetBranchAddress("ptm",&ptminus);
  tlocal7->SetBranchAddress("ptg",&ptgamma);
  tlocal7->SetBranchAddress("etap",&etaplus);
  tlocal7->SetBranchAddress("etam",&etaminus);
  tlocal7->SetBranchAddress("etag",&etagamma);
  tlocal7->SetBranchAddress("W",&weight);
  tlocal7->SetBranchAddress("Wfile",&Wfile);
  tlocal7->SetBranchAddress("njets",&njets);
  
  // for dressed/undressed comparison
  hQpDressed = new TH1D("hQpDressed","Qp",nbinsQp, binsQp);
  hQpDressed->Sumw2();
  hQpUnDressed = new TH1D("hQpUnDressed","Qp",nbinsQp, binsQp);
  hQpUnDressed->Sumw2();
  // for p_T(Z) cut/no cut comparison
  hQpPTZcut = new TH1D("hQpPTZcut","Qp",nbinsQp, binsQp);
  hQpPTZcut->Sumw2();
  hQpNoPTZcut = new TH1D("hQpNoPTZcut","Qp",nbinsQp, binsQp);
  hQpNoPTZcut->Sumw2();
  hQpPTZcutDressed = new TH1D("hQpPTZcutDressed","Qp",nbinsQp, binsQp);
  hQpPTZcutDressed->Sumw2();
  hQpNoPTZcutDressed = new TH1D("hQpNoPTZcutDressed","Qp",nbinsQp, binsQp);
  hQpNoPTZcutDressed->Sumw2();
  //cuts
  cutstr="((ptZ<"+str(ubinPtZ)+")&&"+addselectstr+")*W*Wfile";
  cutstrNoPTZcut="("+addselectstr+")*W*Wfile";
  switch(SimSwitch) {
  case PowhegPythia8:
    t1->Draw("Qp>>hQpDressed",cutstr.c_str());
    tundr1->Draw("Qp>>hQpUnDressed",cutstr.c_str());
    t1->Draw("Qp>>hQpPTZcutDressed",cutstr.c_str());
    t1->Draw("Qp>>hQpNoPTZcutDressed",cutstrNoPTZcut.c_str());
    tundr1->Draw("Qp>>hQpPTZcut",cutstr.c_str());
    tundr1->Draw("Qp>>hQpNoPTZcut",cutstrNoPTZcut.c_str());
    break;
  case Sherpa_14:
    t2->Draw("Qp>>hQpDressed",cutstr.c_str());
    tundr2->Draw("Qp>>hQpUnDressed",cutstr.c_str());
    t2->Draw("Qp>>hQpPTZcutDressed",cutstr.c_str());
    t2->Draw("Qp>>hQpNoPTZcutDressed",cutstrNoPTZcut.c_str());
    tundr2->Draw("Qp>>hQpPTZcut",cutstr.c_str());
    tundr2->Draw("Qp>>hQpNoPTZcut",cutstrNoPTZcut.c_str());
    break;
  case Sherpa_221:
    t3->Draw("Qp>>hQpDressed",cutstr.c_str());
    tundr3->Draw("Qp>>hQpUnDressed",cutstr.c_str());
    t3->Draw("Qp>>hQpPTZcutDressed",cutstr.c_str());
    t3->Draw("Qp>>hQpNoPTZcutDressed",cutstrNoPTZcut.c_str());
    tundr3->Draw("Qp>>hQpPTZcut",cutstr.c_str());
    tundr3->Draw("Qp>>hQpNoPTZcut",cutstrNoPTZcut.c_str());
    break;
  case AlpgenPythia:
    t4->Draw("Qp>>hQpDressed",cutstr.c_str());
    tundr4->Draw("Qp>>hQpUnDressed",cutstr.c_str());
    t4->Draw("Qp>>hQpPTZcutDressed",cutstr.c_str());
    t4->Draw("Qp>>hQpNoPTZcutDressed",cutstrNoPTZcut.c_str());
    tundr4->Draw("Qp>>hQpPTZcut",cutstr.c_str());
    tundr4->Draw("Qp>>hQpNoPTZcut",cutstrNoPTZcut.c_str());
    break;
  case AlpgenJimmy:
    t5->Draw("Qp>>hQpDressed",cutstr.c_str());
    tundr5->Draw("Qp>>hQpUnDressed",cutstr.c_str());
    t5->Draw("Qp>>hQpPTZcutDressed",cutstr.c_str());
    t5->Draw("Qp>>hQpNoPTZcutDressed",cutstrNoPTZcut.c_str());
    tundr5->Draw("Qp>>hQpPTZcut",cutstr.c_str());
    tundr5->Draw("Qp>>hQpNoPTZcut",cutstrNoPTZcut.c_str());
    break;
  case Sh_224:
    t6->Draw("Qp>>hQpDressed",cutstr.c_str());
    tundr6->Draw("Qp>>hQpUnDressed",cutstr.c_str());
    t6->Draw("Qp>>hQpPTZcutDressed",cutstr.c_str());
    t6->Draw("Qp>>hQpNoPTZcutDressed",cutstrNoPTZcut.c_str());
    tundr6->Draw("Qp>>hQpPTZcut",cutstr.c_str());
    tundr6->Draw("Qp>>hQpNoPTZcut",cutstrNoPTZcut.c_str());
    break;
  case PP8_OneGamma:
    t7->Draw("Qp>>hQpDressed",cutstr.c_str());
    tundr7->Draw("Qp>>hQpUnDressed",cutstr.c_str());
    t7->Draw("Qp>>hQpPTZcutDressed",cutstr.c_str());
    t7->Draw("Qp>>hQpNoPTZcutDressed",cutstrNoPTZcut.c_str());
    tundr7->Draw("Qp>>hQpPTZcut",cutstr.c_str());
    tundr7->Draw("Qp>>hQpNoPTZcut",cutstrNoPTZcut.c_str());
    break;
  default:
    cout << "Wrong SimSwitch="<< SimSwitch << endl;
    return 0;
  }
  hQpDrVsUn=(TH1D *)hQpDressed->Clone("hQpDrVsUn");
  hQpDrVsUn->Divide(hQpUnDressed);

  hQpPTZVsNoPTZcutDressed=(TH1D *)hQpPTZcutDressed->Clone("hQpPTZVsNoPTZcutDressed");
  hQpPTZVsNoPTZcutDressed->Divide(hQpNoPTZcutDressed);
  setDiffErrors(hQpPTZVsNoPTZcutDressed,hQpPTZcutDressed,hQpNoPTZcutDressed);
  hQpPTZVsNoPTZcut=(TH1D *)hQpPTZcut->Clone("hQpPTZVsNoPTZcut");
  hQpPTZVsNoPTZcut->Divide(hQpNoPTZcut);
  setDiffErrors(hQpPTZVsNoPTZcut,hQpPTZcut,hQpNoPTZcut);
  
  hptZdiv12=(TH1D *)hptZ1->Clone("hptZdiv12");
  hptZdiv12->Divide(hptZ2);
  hptZr12=(TH1D *)hptZ2->Clone("hptZr12");
  hptZr12->Divide(hptZ1);
  hptZr12->Scale((1./N2)/(1./N1));
  hptZdiv13=(TH1D *)hptZ1->Clone("hptZdiv13");
  hptZdiv13->Divide(hptZ3);
  hptZr13=(TH1D *)hptZ3->Clone("hptZr13");
  hptZr13->Divide(hptZ1);
  hptZr13->Scale((1./N3)/(1./N1));
  hptZdiv14=(TH1D *)hptZ1->Clone("hptZdiv14");
  hptZdiv14->Divide(hptZ4);
  hptZr14=(TH1D *)hptZ4->Clone("hptZr14");
  hptZr14->Divide(hptZ1);
  hptZr14->Scale((1./N4)/(1./N1));
  hptZdiv15=(TH1D *)hptZ1->Clone("hptZdiv15");
  hptZdiv15->Divide(hptZ5);
  hptZr15=(TH1D *)hptZ5->Clone("hptZr15");
  hptZr15->Divide(hptZ1);
  hptZr15->Scale((1./N5)/(1./N1));
  hptZdiv16=(TH1D *)hptZ1->Clone("hptZdiv16");
  hptZdiv16->Divide(hptZ6);
  hptZr16=(TH1D *)hptZ6->Clone("hptZr16");
  hptZr16->Divide(hptZ1);
  hptZr16->Scale((1./N6)/(1./N1));
  hptZdiv17=(TH1D *)hptZ1->Clone("hptZdiv17");
  hptZdiv17->Divide(hptZ7);
  hptZr17=(TH1D *)hptZ7->Clone("hptZr17");
  hptZr17->Divide(hptZ1);
  hptZr17->Scale((1./N7)/(1./N1));

  hptZdiv61=(TH1D *)hptZ6->Clone("hptZdiv61");
  hptZdiv61->Divide(hptZ1);
  hptZr61=(TH1D *)hptZ1->Clone("hptZr61");
  hptZr61->Divide(hptZ6);
  hptZr61->Scale((1./N1)/(1./N6));
  hptZdiv62=(TH1D *)hptZ6->Clone("hptZdiv62");
  hptZdiv62->Divide(hptZ2);
  hptZr62=(TH1D *)hptZ2->Clone("hptZr62");
  hptZr62->Divide(hptZ6);
  hptZr62->Scale((1./N2)/(1./N6));
  hptZdiv63=(TH1D *)hptZ6->Clone("hptZdiv63");
  hptZdiv63->Divide(hptZ3);
  hptZr63=(TH1D *)hptZ3->Clone("hptZr63");
  hptZr63->Divide(hptZ6);
  hptZr63->Scale((1./N3)/(1./N6));
  hptZdiv64=(TH1D *)hptZ6->Clone("hptZdiv64");
  hptZdiv64->Divide(hptZ4);
  hptZr64=(TH1D *)hptZ4->Clone("hptZr64");
  hptZr64->Divide(hptZ6);
  hptZr64->Scale((1./N4)/(1./N6));
  hptZdiv65=(TH1D *)hptZ6->Clone("hptZdiv65");
  hptZdiv65->Divide(hptZ5);
  hptZr65=(TH1D *)hptZ5->Clone("hptZr65");
  hptZr65->Divide(hptZ6);
  hptZr65->Scale((1./N5)/(1./N6));
  hptZdiv67=(TH1D *)hptZ6->Clone("hptZdiv67");
  hptZdiv67->Divide(hptZ7);
  hptZr67=(TH1D *)hptZ7->Clone("hptZr67");
  hptZr67->Divide(hptZ6);
  hptZr67->Scale((1./N7)/(1./N6));

  hptZdiv01=(TH1D *)hptZ0->Clone("hptZdiv01");
  hptZdiv01->Divide(hptZ1);
  hptZr01=(TH1D *)hptZ1->Clone("hptZr01");
  hptZr01->Divide(hptZ0);
  // hptZr01->Scale((1./N1)/(1./N0));
  hptZdiv02=(TH1D *)hptZ0->Clone("hptZdiv02");
  hptZdiv02->Divide(hptZ2);
  hptZr02=(TH1D *)hptZ2->Clone("hptZr02");
  hptZr02->Divide(hptZ0);
  // hptZr02->Scale((1./N2)/(1./N0));
  hptZdiv03=(TH1D *)hptZ0->Clone("hptZdiv03");
  hptZdiv03->Divide(hptZ3);
  hptZr03=(TH1D *)hptZ3->Clone("hptZr03");
  hptZr03->Divide(hptZ0);
  // hptZr03->Scale((1./N3)/(1./N0));
  hptZdiv04=(TH1D *)hptZ0->Clone("hptZdiv04");
  hptZdiv04->Divide(hptZ4);
  hptZr04=(TH1D *)hptZ4->Clone("hptZr04");
  hptZr04->Divide(hptZ0);
  // hptZr04->Scale((1./N4)/(1./N0));
  hptZdiv05=(TH1D *)hptZ0->Clone("hptZdiv05");
  hptZdiv05->Divide(hptZ5);
  hptZr05=(TH1D *)hptZ5->Clone("hptZr05");
  hptZr05->Divide(hptZ0);
  // hptZr05->Scale((1./N5)/(1./N0));
  hptZdiv06=(TH1D *)hptZ0->Clone("hptZdiv06");
  hptZdiv06->Divide(hptZ6);
  hptZr06=(TH1D *)hptZ6->Clone("hptZr06");
  hptZr06->Divide(hptZ0);
  // hptZr06->Scale((1./N6)/(1./N0));
  hptZdiv07=(TH1D *)hptZ0->Clone("hptZdiv07");
  hptZdiv07->Divide(hptZ7);
  hptZr07=(TH1D *)hptZ7->Clone("hptZr07");
  hptZr07->Divide(hptZ0);
  // hptZr07->Scale((1./N7)/(1./N0));
  
  hQpr12=(TH1D *)hQp2->Clone("hQpr12");
  hQpr12->Divide(hQp1);
  hQpr12->Scale((1./N2)/(1./N1));
  hQpr13=(TH1D *)hQp3->Clone("hQpr13");
  hQpr13->Divide(hQp1);
  hQpr13->Scale((1./N3)/(1./N1));
  hQpr32=(TH1D *)hQp2->Clone("hQpr32");
  hQpr32->Divide(hQp3);
  hQpr32->Scale((1./N2)/(1./N3));
  hQpr14=(TH1D *)hQp4->Clone("hQpr14");
  hQpr14->Divide(hQp1);
  hQpr14->Scale((1./N4)/(1./N1));
  hQpr15=(TH1D *)hQp5->Clone("hQpr15");
  hQpr15->Divide(hQp1);
  hQpr15->Scale((1./N5)/(1./N1));
  hQpr16=(TH1D *)hQp6->Clone("hQpr16");
  hQpr16->Divide(hQp1);
  hQpr16->Scale((1./N6)/(1./N1));
  hQpr17=(TH1D *)hQp7->Clone("hQpr17");
  hQpr17->Divide(hQp1);
  hQpr17->Scale((1./N7)/(1./N1));
  hQpr62=(TH1D *)hQp2->Clone("hQpr62");
  hQpr62->Divide(hQp6);
  hQpr62->Scale((1./N2)/(1./N6));
  hQpr63=(TH1D *)hQp3->Clone("hQpr63");
  hQpr63->Divide(hQp6);
  hQpr63->Scale((1./N3)/(1./N6));

   
  hdRr12=(TH1D *)hdR2->Clone("hdRr12");
  hdRr12->Divide(hdR1);
  hdRr12->Scale((1./N2)/(1./N1));
  hdRr13=(TH1D *)hdR3->Clone("hdRr13");
  hdRr13->Divide(hdR1);
  hdRr13->Scale((1./N3)/(1./N1));
  hdRr32=(TH1D *)hdR2->Clone("hdRr32");
  hdRr32->Divide(hdR3);
  hdRr32->Scale((1./N2)/(1./N3));
  hdRr14=(TH1D *)hdR4->Clone("hdRr14");
  hdRr14->Divide(hdR1);
  hdRr14->Scale((1./N4)/(1./N1));
  hdRr15=(TH1D *)hdR5->Clone("hdRr15");
  hdRr15->Divide(hdR1);
  hdRr15->Scale((1./N5)/(1./N1));
  hdRr16=(TH1D *)hdR6->Clone("hdRr16");
  hdRr16->Divide(hdR1);
  hdRr16->Scale((1./N6)/(1./N1));
  hdRr17=(TH1D *)hdR7->Clone("hdRr17");
  hdRr17->Divide(hdR1);
  hdRr17->Scale((1./N7)/(1./N1));
  hdRr62=(TH1D *)hdR2->Clone("hdRr62");
  hdRr62->Divide(hdR6);
  hdRr62->Scale((1./N2)/(1./N6));
  hdRr63=(TH1D *)hdR3->Clone("hdRr63");
  hdRr63->Divide(hdR6);
  hdRr63->Scale((1./N3)/(1./N6));

  hptgammar12=(TH1D *)hptgamma2->Clone("hptgammar12");
  hptgammar12->Divide(hptgamma1);
  hptgammar12->Scale((1./N2)/(1./N1));
  hptgammar13=(TH1D *)hptgamma3->Clone("hptgammar13");
  hptgammar13->Divide(hptgamma1);
  hptgammar13->Scale((1./N3)/(1./N1));
  hptgammar32=(TH1D *)hptgamma2->Clone("hptgammar32");
  hptgammar32->Divide(hptgamma3);
  hptgammar32->Scale((1./N2)/(1./N3));
  hptgammar14=(TH1D *)hptgamma4->Clone("hptgammar14");
  hptgammar14->Divide(hptgamma1);
  hptgammar14->Scale((1./N4)/(1./N1));
  hptgammar15=(TH1D *)hptgamma5->Clone("hptgammar15");
  hptgammar15->Divide(hptgamma1);
  hptgammar15->Scale((1./N5)/(1./N1));
  hptgammar16=(TH1D *)hptgamma6->Clone("hptgammar16");
  hptgammar16->Divide(hptgamma1);
  hptgammar16->Scale((1./N6)/(1./N1));
  hptgammar17=(TH1D *)hptgamma7->Clone("hptgammar17");
  hptgammar17->Divide(hptgamma1);
  hptgammar17->Scale((1./N7)/(1./N1));
  hptgammar62=(TH1D *)hptgamma2->Clone("hptgammar62");
  hptgammar62->Divide(hptgamma6);
  hptgammar62->Scale((1./N2)/(1./N6));
  hptgammar63=(TH1D *)hptgamma3->Clone("hptgammar63");
  hptgammar63->Divide(hptgamma6);
  hptgammar63->Scale((1./N3)/(1./N6));
  
  return 1;
}

int makepics(int decaytype) {
  return makepics(decaytype,1);
}

int makepics(int decaytype) {
  TH1D *hQp,*hQm,*hQpvsQm;
  TH1D *hQpw,*hQpBvsR;
  TH1D *hdR,*hdRw,*hdRBvsR;
  TH1D *hptgamma,*hptgammaw,*hptgammaBvsR;
  TH1D *hptZ,*hptZw,*hptZBvsR;
  TH1D *hDressed,*hUnDressed,*hDrVsUn;
  gStyle->SetOptTitle(1);
  switch (DressSwitch) {
  case DRESSED:
    dresspicprefix="";
    break;
  case UNDRESSED:
    dresspicprefix="undr_";
    break;
  }

  //BEGIN for Dressed/Undressed hist 
  hDrVsUn= (TH1D *) hQpDrVsUn->Clone("hQpDrVsUn_draw");
  hDressed= (TH1D *) hQpDressed->Clone("hQpDressed_draw");
  hUnDressed= (TH1D *) hQpUnDressed->Clone("hQpUnDressed_draw");
  switch (SimSwitch){
  case PowhegPythia8:
    switch (decaytype){
    case Z2EEG:
      hDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hUnDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hDrVsUn->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hUnDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hDrVsUn->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case Sherpa_14:
    switch (decaytype){
    case Z2EEG:
      hDressed->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hUnDressed->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hDrVsUn->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      break;
    case Z2MUMUG:
      hDressed->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hUnDressed->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hDrVsUn->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      break;
    }
    break;
  case Sherpa_221:
    switch (decaytype){
    case Z2EEG:
      hDressed->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hUnDressed->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hDrVsUn->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hDressed->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hUnDressed->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hDrVsUn->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case AlpgenPythia:
    switch (decaytype){
    case Z2EEG:
      hDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hUnDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hDrVsUn->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hUnDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hDrVsUn->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case AlpgenJimmy:
    switch (decaytype){
    case Z2EEG:
      hDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hUnDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hDrVsUn->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hUnDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hDrVsUn->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case Sh_224:
    switch (decaytype){
    case Z2EEG:
      hDressed->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hUnDressed->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hDrVsUn->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hDressed->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hUnDressed->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hDrVsUn->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case PP8_OneGamma:
    switch (decaytype){
    case Z2EEG:
      hDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hUnDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hDrVsUn->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hUnDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hDrVsUn->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  }

  cDrvsUn=(TCanvas *)gROOT->FindObject("cDrvsUn");
  if (!cDrvsUn)  cDrvsUn=new TCanvas("cDrvsUn","cDrvsUn",800.,600.);
  cDrvsUn->Clear();
  cDrvsUn->cd();
  cDrvsUn->SetGridy();
  cDrvsUn->SetGridx();
  switch (decaytype){
  case Z2EEG:
    hDrVsUn->GetXaxis()->SetTitle("M_{inv}(e^{+}#gamma), GeV");
    hDrVsUn->GetYaxis()->SetTitle("Dressed/Bare");
    hDrVsUn->GetYaxis()->SetTitleOffset(0.8);
    break;
  case Z2MUMUG:
    hDrVsUn->GetXaxis()->SetTitle("M_{inv}(#mu^{+}#gamma), GeV");
    hDrVsUn->GetYaxis()->SetTitle("Dressed/Bare");
    hDrVsUn->GetYaxis()->SetTitleOffset(0.8);
    break;
  }
  // gStyle->SetOptStat(1); 
  // gStyle->SetOptFit(1);
  hDrVsUn->GetYaxis()->SetRangeUser(0.4,1.6);
  TF1 *lDrvsUn = new TF1("lDrvsUn","[0]", 0, 1);
  lDrvsUn->SetParName(0,"C");
  hDrVsUn->Fit(lDrvsUn,"","",5.,90.);
  hDrVsUn->Draw("ep");

  // gStyle->SetOptStat(0); 

  cDrAndUn=(TCanvas *)gROOT->FindObject("cDrAndUn");
  if (!cDrAndUn)  cDrAndUn=new TCanvas("cDrAndUn","cDrAndUn",800.,600.);
  cDrAndUn->Clear();
  cDrAndUn->cd();

  TLegend *LDrAndUn = new TLegend(0.68,0.7,0.89,0.84);
  switch (decaytype){
  case Z2EEG:
    hDressed->GetXaxis()->SetTitle("M_{inv} (e^{+}#gamma), GeV");
    hUnDressed->GetXaxis()->SetTitle("M_{inv} (e^{+}#gamma), GeV");
    LDrAndUn->AddEntry(hDressed,"Dressed leptons ","LEP");
    LDrAndUn->AddEntry(hUnDressed,"Bare leptons ","LEP");
    break;
  case Z2MUMUG:
    hDressed->GetXaxis()->SetTitle("M_{inv} (#mu^{+}#gamma), GeV");
    hUnDressed->GetXaxis()->SetTitle("M_{inv} (#mu^{+}#gamma), GeV");
    LDrAndUn->AddEntry(hDressed,"Dressed leptons ","LEP");
    LDrAndUn->AddEntry(hUnDressed,"Bare leptons ","LEP");
    break;
  }
  hDressed->SetMarkerColor(kRed);
  hDressed->SetLineColor(kRed);
  hDressed->Draw("ep");
  hUnDressed->SetMarkerColor(kBlue);
  hUnDressed->SetLineColor(kBlue);
  hUnDressed->Draw("ep same");

  LDrAndUn->Draw();

  if (picdir!=NULL) {    
    picname=string(picdir)+"DrVsBare/"+"QpDrAndBare-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cDrAndUn->Print(picname.c_str());
    picname=string(picdir)+"DrVsBare/"+"QpDrVsBare-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cDrvsUn->Print(picname.c_str());
  }
  // return 1;
  //END DressedVsUndressed
 // BEGIN_DressedVsUndressed   

  //BEGIN for PTZcut/NoPTZcut hist 
  hPTZVsNoPTZcut= (TH1D *) hQpPTZVsNoPTZcut->Clone("hQpPTZVsNoPTZcut_draw");
  hPTZcut= (TH1D *) hQpPTZcut->Clone("hQpPTZcut_draw");
  hNoPTZcut= (TH1D *) hQpNoPTZcut->Clone("hQpNoPTZcut_draw");
  switch (SimSwitch){
  case PowhegPythia8:
    switch (decaytype){
    case Z2EEG:
      hPTZcut->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcut->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcut->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcut->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case Sherpa_14:
    switch (decaytype){
    case Z2EEG:
      hPTZcut->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hNoPTZcut->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hPTZVsNoPTZcut->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      break;
    case Z2MUMUG:
      hPTZcut->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hNoPTZcut->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hPTZVsNoPTZcut->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      break;
    }
    break;
  case Sherpa_221:
    switch (decaytype){
    case Z2EEG:
      hPTZcut->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcut->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcut->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcut->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case AlpgenPythia:
    switch (decaytype){
    case Z2EEG:
      hPTZcut->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcut->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcut->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcut->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case AlpgenJimmy:
    switch (decaytype){
    case Z2EEG:
      hPTZcut->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcut->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcut->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcut->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case Sh_224:
    switch (decaytype){
    case Z2EEG:
      hPTZcut->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcut->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcut->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcut->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcut->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case PP8_OneGamma:
    switch (decaytype){
    case Z2EEG:
      hPTZcut->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcut->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcut->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcut->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcut->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcut->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  }

  cPTZVsNoPTZcut=(TCanvas *)gROOT->FindObject("cPTZVsNoPTZcut");
  if (!cPTZVsNoPTZcut)  cPTZVsNoPTZcut=new TCanvas("cPTZVsNoPTZcut","cPTZVsNoPTZcut",800.,600.);
  cPTZVsNoPTZcut->Clear();
  cPTZVsNoPTZcut->cd();
  cPTZVsNoPTZcut->SetGridy();
  cPTZVsNoPTZcut->SetGridx();
  switch (decaytype){
  case Z2EEG:
    hPTZVsNoPTZcut->GetXaxis()->SetTitle("M_{inv}(e^{+}#gamma), GeV");
    hPTZVsNoPTZcut->GetYaxis()->SetTitle("PTZcut/NoPTZcut");
    hPTZVsNoPTZcut->GetYaxis()->SetTitleOffset(1.1);
    break;
  case Z2MUMUG:
    hPTZVsNoPTZcut->GetXaxis()->SetTitle("M_{inv}(#mu^{+}#gamma), GeV");
    hPTZVsNoPTZcut->GetYaxis()->SetTitle("PTZcut/NoPTZcut");
    hPTZVsNoPTZcut->GetYaxis()->SetTitleOffset(1.1);
    break;
  }
  // gStyle->SetOptStat(1); 
  // gStyle->SetOptFit(1);
  hPTZVsNoPTZcut->GetYaxis()->SetRangeUser(0.9,1.1);
  TF1 *lPTZVsNoPTZcut = new TF1("lPTZVsNoPTZcut","[0]", 0, 1);
  lPTZVsNoPTZcut->SetParName(0,"C");
  hPTZVsNoPTZcut->Fit(lPTZVsNoPTZcut,"","",5.,90.);
  hPTZVsNoPTZcut->Draw("ep");

  // gStyle->SetOptStat(0); 

  cDrAndUn=(TCanvas *)gROOT->FindObject("cDrAndUn");
  if (!cDrAndUn)  cDrAndUn=new TCanvas("cDrAndUn","cDrAndUn",800.,600.);
  cDrAndUn->Clear();
  cDrAndUn->cd();

  TLegend *LDrAndUn = new TLegend(0.68,0.7,0.89,0.84);
  switch (decaytype){
  case Z2EEG:
    hPTZcut->GetXaxis()->SetTitle("M_{inv} (e^{+}#gamma), GeV");
    hNoPTZcut->GetXaxis()->SetTitle("M_{inv} (e^{+}#gamma), GeV");
    LDrAndUn->AddEntry(hPTZcut,"PTZcut leptons ","LEP");
    LDrAndUn->AddEntry(hNoPTZcut,"NoPTZcut leptons ","LEP");
    break;
  case Z2MUMUG:
    hPTZcut->GetXaxis()->SetTitle("M_{inv} (#mu^{+}#gamma), GeV");
    hNoPTZcut->GetXaxis()->SetTitle("M_{inv} (#mu^{+}#gamma), GeV");
    LDrAndUn->AddEntry(hPTZcut,"PTZcut leptons ","LEP");
    LDrAndUn->AddEntry(hNoPTZcut,"NoPTZcut leptons ","LEP");
    break;
  }
  hPTZcut->SetMarkerColor(kRed);
  hPTZcut->SetLineColor(kRed);
  hPTZcut->Draw("ep");
  hNoPTZcut->SetMarkerColor(kBlue);
  hNoPTZcut->SetLineColor(kBlue);
  hNoPTZcut->Draw("ep same");

  LDrAndUn->Draw();

  if (picdir!=NULL) {    
    picname=string(picdir)+"PTZcut/PTZAndNoPTZcut-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cDrAndUn->Print(picname.c_str());
    picname=string(picdir)+"PTZcut/PTZVsNoPTZcut-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cPTZVsNoPTZcut->Print(picname.c_str());
  }
  // return 1;
  //END PTZVsNoPTZcut

  //BEGIN for PTZcutDressed/NoPTZcutDressed hist 
  hPTZVsNoPTZcutDressed= (TH1D *) hQpPTZVsNoPTZcutDressed->Clone("hQpPTZVsNoPTZcutDressed_draw");
  hPTZcutDressed= (TH1D *) hQpPTZcutDressed->Clone("hQpPTZcutDressed_draw");
  hNoPTZcutDressed= (TH1D *) hQpNoPTZcutDressed->Clone("hQpNoPTZcutDressed_draw");
  switch (SimSwitch){
  case PowhegPythia8:
    switch (decaytype){
    case Z2EEG:
      hPTZcutDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcutDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcutDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcutDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case Sherpa_14:
    switch (decaytype){
    case Z2EEG:
      hPTZcutDressed->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hNoPTZcutDressed->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hPTZVsNoPTZcutDressed->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      break;
    case Z2MUMUG:
      hPTZcutDressed->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hNoPTZcutDressed->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hPTZVsNoPTZcutDressed->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      break;
    }
    break;
  case Sherpa_221:
    switch (decaytype){
    case Z2EEG:
      hPTZcutDressed->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcutDressed->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcutDressed->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcutDressed->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case AlpgenPythia:
    switch (decaytype){
    case Z2EEG:
      hPTZcutDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcutDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcutDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcutDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case AlpgenJimmy:
    switch (decaytype){
    case Z2EEG:
      hPTZcutDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcutDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcutDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcutDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case Sh_224:
    switch (decaytype){
    case Z2EEG:
      hPTZcutDressed->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcutDressed->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcutDressed->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcutDressed->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case PP8_OneGamma:
    switch (decaytype){
    case Z2EEG:
      hPTZcutDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hNoPTZcutDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hPTZcutDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hNoPTZcutDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hPTZVsNoPTZcutDressed->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  }

  cPTZVsNoPTZcutDressed=(TCanvas *)gROOT->FindObject("cPTZVsNoPTZcutDressed");
  if (!cPTZVsNoPTZcutDressed)  cPTZVsNoPTZcutDressed=new TCanvas("cPTZVsNoPTZcutDressed","cPTZVsNoPTZcutDressed",800.,600.);
  cPTZVsNoPTZcutDressed->Clear();
  cPTZVsNoPTZcutDressed->cd();
  cPTZVsNoPTZcutDressed->SetGridy();
  cPTZVsNoPTZcutDressed->SetGridx();
  switch (decaytype){
  case Z2EEG:
    hPTZVsNoPTZcutDressed->GetXaxis()->SetTitle("M_{inv}(e^{+}#gamma), GeV");
    hPTZVsNoPTZcutDressed->GetYaxis()->SetTitle("PTZcut/NoPTZcut (for Dressed electrons)");
    hPTZVsNoPTZcutDressed->GetYaxis()->SetTitleOffset(1.1);
    break;
  case Z2MUMUG:
    hPTZVsNoPTZcutDressed->GetXaxis()->SetTitle("M_{inv}(#mu^{+}#gamma), GeV");
    hPTZVsNoPTZcutDressed->GetYaxis()->SetTitle("PTZcut/NoPTZcut (for Dressed muons)");
    hPTZVsNoPTZcutDressed->GetYaxis()->SetTitleOffset(1.1);
    break;
  }
  // gStyle->SetOptStat(1); 
  // gStyle->SetOptFit(1);
  hPTZVsNoPTZcutDressed->GetYaxis()->SetRangeUser(0.8,1.2);
  TF1 *lPTZVsNoPTZcutDressed = new TF1("lPTZVsNoPTZcutDressed","[0]", 0, 1);
  lPTZVsNoPTZcutDressed->SetParName(0,"C");
  hPTZVsNoPTZcutDressed->Fit(lPTZVsNoPTZcutDressed,"","",5.,90.);
  hPTZVsNoPTZcutDressed->Draw("ep");

  // gStyle->SetOptStat(0); 

  cDrAndUn=(TCanvas *)gROOT->FindObject("cDrAndUn");
  if (!cDrAndUn)  cDrAndUn=new TCanvas("cDrAndUn","cDrAndUn",800.,600.);
  cDrAndUn->Clear();
  cDrAndUn->cd();

  TLegend *LDrAndUn = new TLegend(0.68,0.7,0.89,0.84);
  switch (decaytype){
  case Z2EEG:
    hPTZcutDressed->GetXaxis()->SetTitle("M_{inv} (e^{+}#gamma), GeV");
    hNoPTZcutDressed->GetXaxis()->SetTitle("M_{inv} (e^{+}#gamma), GeV");
    LDrAndUn->AddEntry(hPTZcutDressed,"PTZcutDressed leptons ","LEP");
    LDrAndUn->AddEntry(hNoPTZcutDressed,"NoPTZcutDressed leptons ","LEP");
    break;
  case Z2MUMUG:
    hPTZcutDressed->GetXaxis()->SetTitle("M_{inv} (#mu^{+}#gamma), GeV");
    hNoPTZcutDressed->GetXaxis()->SetTitle("M_{inv} (#mu^{+}#gamma), GeV");
    LDrAndUn->AddEntry(hPTZcutDressed,"PTZcutDressed leptons ","LEP");
    LDrAndUn->AddEntry(hNoPTZcutDressed,"NoPTZcutDressed leptons ","LEP");
    break;
  }
  hPTZcutDressed->SetMarkerColor(kRed);
  hPTZcutDressed->SetLineColor(kRed);
  hPTZcutDressed->Draw("ep");
  hNoPTZcutDressed->SetMarkerColor(kBlue);
  hNoPTZcutDressed->SetLineColor(kBlue);
  hNoPTZcutDressed->Draw("ep same");

  LDrAndUn->Draw();

  if (picdir!=NULL) {    
    picname=string(picdir)+"PTZcut/PTZAndNoPTZcutDressed-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cDrAndUn->Print(picname.c_str());
    picname=string(picdir)+"PTZcut/PTZVsNoPTZcutDressed-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cPTZVsNoPTZcutDressed->Print(picname.c_str());
  }
  // return 1;
  //END PTZVsNoPTZcutDressed
  
  //BEGIN for QpvsQm hist 
  switch (SimSwitch){
  case PowhegPythia8:
    hQpvsQm= (TH1D *) hQpvsQm1->Clone("hQpvsQm1_draw");
    hQp= (TH1D *) hQp1->Clone("hQp1_draw");
    hQm= (TH1D *) hQm1->Clone("hQm1_draw");
    hQpw= (TH1D *) hQpw1->Clone("hQpw1_draw");
    hQpBvsR= (TH1D *) hQpBvsR1->Clone("hQpBvsR1_draw");
    hdR= (TH1D *) hdR1->Clone("hdR1_draw");
    hdRw= (TH1D *) hdRw1->Clone("hdRw1_draw");
    hdRBvsR= (TH1D *) hdRBvsR1->Clone("hdRBvsR1_draw");
    hptgamma= (TH1D *) hptgamma1->Clone("hptgamma1_draw");
    hptgammaw= (TH1D *) hptgammaw1->Clone("hptgammaw1_draw");
    hptgammaBvsR= (TH1D *) hptgammaBvsR1->Clone("hptgammaBvsR1_draw");
    hptZ= (TH1D *) hptZ1->Clone("hptZ1_draw");
    hptZw= (TH1D *) hptZw1->Clone("hptZw1_draw");
    hptZBvsR= (TH1D *) hptZBvsR1->Clone("hptZBvsR1_draw");
    switch (decaytype){
    case Z2EEG:
      hQp->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hQm->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hQpvsQm->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hQpw->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hQpBvsR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hdR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hdRw->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hdRBvsR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hptgamma->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaw->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaBvsR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hptZ->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hptZw->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      hptZBvsR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hQp->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQm->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpvsQm->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpw->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpBvsR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRw->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRBvsR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgamma->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaw->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaBvsR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZ->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZw->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZBvsR->SetTitle("Powheg+Pythia8+PHOTOS_ZCorr (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case Sherpa_14:
    hQpvsQm= (TH1D *) hQpvsQm2->Clone("hQpvsQm2_draw");
    hQp= (TH1D *) hQp2->Clone("hQp2_draw");
    hQm= (TH1D *) hQm2->Clone("hQm2_draw");
    hQpw= (TH1D *) hQpw2->Clone("hQpw2_draw");
    hQpBvsR= (TH1D *) hQpBvsR2->Clone("hQpBvsR2_draw");
    hdR= (TH1D *) hdR2->Clone("hdR2_draw");
    hdRw= (TH1D *) hdRw2->Clone("hdRw2_draw");
    hdRBvsR= (TH1D *) hdRBvsR2->Clone("hdRBvsR2_draw");
    hptgamma= (TH1D *) hptgamma2->Clone("hptgamma2_draw");
    hptgammaw= (TH1D *) hptgammaw2->Clone("hptgammaw2_draw");
    hptgammaBvsR= (TH1D *) hptgammaBvsR2->Clone("hptgammaBvsR2_draw");
    hptZ= (TH1D *) hptZ2->Clone("hptZ2_draw");
    hptZw= (TH1D *) hptZw2->Clone("hptZw2_draw");
    hptZBvsR= (TH1D *) hptZBvsR2->Clone("hptZBvsR2_draw");
    switch (decaytype){
    case Z2EEG:
      hQp->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hQm->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hQpvsQm->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hQpw->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hQpBvsR->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hdR->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hdRw->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hdRBvsR->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hptgamma->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hptgammaw->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hptgammaBvsR->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hptZ->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hptZw->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      hptZBvsR->SetTitle("Sherpa 1.4 (Z #rightarrow e^{#plus}e^{#minus}#gamma)");
      break;
    case Z2MUMUG:
      hQp->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hQm->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hQpvsQm->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hQpw->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hQpBvsR->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hdR->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hdRw->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hdRBvsR->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hptgamma->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hptgammaw->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hptgammaBvsR->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hptZ->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hptZw->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      hptZBvsR->SetTitle("Sherpa 1.4 (Z #rightarrow #mu^{#plus}#mu^{#minus}#gamma)");
      break;
    }
    break;
  case Sherpa_221:
    hQpvsQm= (TH1D *) hQpvsQm3->Clone("hQpvsQm3_draw");
    hQp= (TH1D *) hQp3->Clone("hQp3_draw");
    hQm= (TH1D *) hQm3->Clone("hQm3_draw");
    hQpw= (TH1D *) hQpw3->Clone("hQpw3_draw");
    hQpBvsR= (TH1D *) hQpBvsR3->Clone("hQpBvsR3_draw");
    hdR= (TH1D *) hdR3->Clone("hdR3_draw");
    hdRw= (TH1D *) hdRw3->Clone("hdRw3_draw");
    hdRBvsR= (TH1D *) hdRBvsR3->Clone("hdRBvsR3_draw");
    hptgamma= (TH1D *) hptgamma3->Clone("hptgamma3_draw");
    hptgammaw= (TH1D *) hptgammaw3->Clone("hptgammaw3_draw");
    hptgammaBvsR= (TH1D *) hptgammaBvsR3->Clone("hptgammaBvsR3_draw");
    hptZ= (TH1D *) hptZ3->Clone("hptZ3_draw");
    hptZw= (TH1D *) hptZw3->Clone("hptZw3_draw");
    hptZBvsR= (TH1D *) hptZBvsR3->Clone("hptZBvsR3_draw");
    switch (decaytype){
    case Z2EEG:
      hQp->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hQm->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hQpvsQm->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hQpw->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hQpBvsR->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hdR->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hdRw->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hdRBvsR->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hptgamma->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaw->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaBvsR->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hptZ->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hptZw->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      hptZBvsR->SetTitle("Sherpa 2.2 (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hQp->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQm->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpvsQm->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpBvsR->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdR->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRw->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRBvsR->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgamma->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaw->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaBvsR->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZ->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZw->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZBvsR->SetTitle("Sherpa 2.2 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case AlpgenPythia:
    hQpvsQm= (TH1D *) hQpvsQm4->Clone("hQpvsQm4_draw");
    hQp= (TH1D *) hQp4->Clone("hQp4_draw");
    hQm= (TH1D *) hQm4->Clone("hQm4_draw");
    hQpw= (TH1D *) hQpw4->Clone("hQpw4_draw");
    hQpBvsR= (TH1D *) hQpBvsR4->Clone("hQp4_draw");
    hdR= (TH1D *) hdR4->Clone("hdR4_draw");
    hdRw= (TH1D *) hdRw4->Clone("hdR4_draw");
    hdRBvsR= (TH1D *) hdRBvsR4->Clone("hdR4_draw");
    hptgamma= (TH1D *) hptgamma4->Clone("hptgamma4_draw");
    hptgammaw= (TH1D *) hptgammaw4->Clone("hptgamma4_draw");
    hptgammaBvsR= (TH1D *) hptgammaBvsR4->Clone("hptgamma4_draw");
    hptZ= (TH1D *) hptZ4->Clone("hptZ4_draw");
    hptZw= (TH1D *) hptZw4->Clone("hptZ4_draw");
    hptZBvsR= (TH1D *) hptZBvsR4->Clone("hptZ4_draw");
    switch (decaytype){
    case Z2EEG:
      hQp->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hQm->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hQpvsQm->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hQpw->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hQpBvsR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hdR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hdRw->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hdRBvsR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptgamma->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaw->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaBvsR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptZ->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptZw->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptZBvsR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hQp->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQm->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpvsQm->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpw->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpBvsR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRw->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRBvsR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgamma->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaw->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaBvsR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZ->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZw->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZBvsR->SetTitle("Alpgen+Pythia+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case AlpgenJimmy:
    hQpvsQm= (TH1D *) hQpvsQm5->Clone("hQpvsQm5_draw");
    hQp= (TH1D *) hQp5->Clone("hQp5_draw");
    hQm= (TH1D *) hQm5->Clone("hQm5_draw");
    hQpw= (TH1D *) hQpw5->Clone("hQpw5_draw");
    hQpBvsR= (TH1D *) hQpBvsR5->Clone("hQpBvsR5_draw");
    hdR= (TH1D *) hdR5->Clone("hdR5_draw");
    hdRw= (TH1D *) hdRw5->Clone("hdRw5_draw");
    hdRBvsR= (TH1D *) hdRBvsR5->Clone("hdRBvsR5_draw");
    hptgamma= (TH1D *) hptgamma5->Clone("hptgamma5_draw");
    hptgammaw= (TH1D *) hptgammaw5->Clone("hptgammaw5_draw");
    hptgammaBvsR= (TH1D *) hptgammaBvsR5->Clone("hptgammaBvsR5_draw");
    hptZ= (TH1D *) hptZ5->Clone("hptZ5_draw");
    hptZw= (TH1D *) hptZw5->Clone("hptZw5_draw");
    hptZBvsR= (TH1D *) hptZBvsR5->Clone("hptZBvsR5_draw");
    switch (decaytype){
    case Z2EEG:
      hQp->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hQm->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hQpvsQm->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hQpw->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hQpBvsR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hdR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hdRw->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hdRBvsR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptgamma->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaw->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaBvsR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptZ->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptZw->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      hptZBvsR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hQp->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQm->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpvsQm->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpw->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpBvsR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRw->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRBvsR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgamma->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaw->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaBvsR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZ->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZw->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZBvsR->SetTitle("Alpgen+Jimmy+PHOTOS (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case Sh_224:
    hQpvsQm= (TH1D *) hQpvsQm6->Clone("hQpvsQm6_draw");
    hQp= (TH1D *) hQp6->Clone("hQp6_draw");
    hQm= (TH1D *) hQm6->Clone("hQm6_draw");
    hQpw= (TH1D *) hQpw6->Clone("hQpw6_draw");
    hQpBvsR= (TH1D *) hQpBvsR6->Clone("hQpBvsR6_draw");
    hdR= (TH1D *) hdR6->Clone("hdR6_draw");
    hdRw= (TH1D *) hdRw6->Clone("hdRw6_draw");
    hdRBvsR= (TH1D *) hdRBvsR6->Clone("hdRBvsR6_draw");
    hptgamma= (TH1D *) hptgamma6->Clone("hptgamma6_draw");
    hptgammaw= (TH1D *) hptgammaw6->Clone("hptgammaw6_draw");
    hptgammaBvsR= (TH1D *) hptgammaBvsR6->Clone("hptgammaBvsR6_draw");
    hptZ= (TH1D *) hptZ6->Clone("hptZ6_draw");
    hptZw= (TH1D *) hptZw6->Clone("hptZw6_draw");
    hptZBvsR= (TH1D *) hptZBvsR6->Clone("hptZBvsR6_draw");
    switch (decaytype){
    case Z2EEG:
      hQp->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hQm->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hQpvsQm->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hQpw->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hQpBvsR->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hdR->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hdRw->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hdRBvsR->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hptgamma->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaw->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaBvsR->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hptZ->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hptZw->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      hptZBvsR->SetTitle("Sherpa 2.24 (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hQp->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQm->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpvsQm->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpw->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpBvsR->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdR->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRw->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRBvsR->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgamma->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaw->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaBvsR->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZ->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZw->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZBvsR->SetTitle("Sherpa 2.24 (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  case PP8_OneGamma:
    hQpvsQm= (TH1D *) hQpvsQm7->Clone("hQpvsQm7_draw");
    hQp= (TH1D *) hQp7->Clone("hQp7_draw");
    hQm= (TH1D *) hQm7->Clone("hQm7_draw");
    hQpw= (TH1D *) hQpw7->Clone("hQpw7_draw");
    hQpBvsR= (TH1D *) hQpBvsR7->Clone("hQpBvsR7_draw");
    hdR= (TH1D *) hdR7->Clone("hdR7_draw");
    hdRw= (TH1D *) hdRw7->Clone("hdRw7_draw");
    hdRBvsR= (TH1D *) hdRBvsR7->Clone("hdRBvsR7_draw");
    hptgamma= (TH1D *) hptgamma7->Clone("hptgamma7_draw");
    hptgammaw= (TH1D *) hptgammaw7->Clone("hptgammaw7_draw");
    hptgammaBvsR= (TH1D *) hptgammaBvsR7->Clone("hptgammaBvsR7_draw");
    hptZ= (TH1D *) hptZ7->Clone("hptZ7_draw");
    hptZw= (TH1D *) hptZw7->Clone("hptZw7_draw");
    hptZBvsR= (TH1D *) hptZBvsR7->Clone("hptZBvsR7_draw");
    switch (decaytype){
    case Z2EEG:
      hQp->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hQm->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hQpvsQm->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hQpw->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hQpBvsR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hdR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hdRw->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hdRBvsR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hptgamma->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaw->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hptgammaBvsR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hptZ->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hptZw->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      hptZBvsR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow e^{#plus}e^{#minus})");
      break;
    case Z2MUMUG:
      hQp->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQm->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpvsQm->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpw->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hQpBvsR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRw->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hdRBvsR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgamma->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaw->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptgammaBvsR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZ->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZw->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      hptZBvsR->SetTitle("PowhegPythia8+PHOTOS (One #gamma only) (Z #rightarrow #mu^{#plus}#mu^{#minus})");
      break;
    }
    break;
  }

  cQpvsQm=(TCanvas *)gROOT->FindObject("cQpvsQm");
  if (!cQpvsQm)  cQpvsQm=new TCanvas("cQpvsQm","cQpvsQm",800.,600.);
  cQpvsQm->Clear();
  cQpvsQm->cd();
  cQpvsQm->SetGridy();
  cQpvsQm->SetGridx();
  switch (decaytype){
  case Z2EEG:
    hQpvsQm->GetXaxis()->SetTitle("M_{inv}(e#gamma), GeV");
    hQpvsQm->GetYaxis()->SetTitle("Qp/Qm");
    hQpvsQm->GetYaxis()->SetTitleOffset(0.8);
    break;
  case Z2MUMUG:
    hQpvsQm->GetXaxis()->SetTitle("M_{inv}(#mu#gamma), GeV");
    hQpvsQm->GetYaxis()->SetTitle("Qp/Qm");
    hQpvsQm->GetYaxis()->SetTitleOffset(0.8);
    break;
  }
  // gStyle->SetOptStat(1); 
  // gStyle->SetOptFit(1);
  hQpvsQm->GetYaxis()->SetRangeUser(0.8,1.2);
  TF1 *lQpvsQm = new TF1("lQpvsQm","[0]", 0, 1);
  lQpvsQm->SetParName(0,"C");
  hQpvsQm->Fit(lQpvsQm,"","",5.,90.);
  hQpvsQm->Draw("ep");

  // gStyle->SetOptStat(0); 

  cQpAndQm=(TCanvas *)gROOT->FindObject("cQpAndQm");
  if (!cQpAndQm)  cQpAndQm=new TCanvas("cQpAndQm","cQpAndQm",800.,600.);
  cQpAndQm->Clear();
  cQpAndQm->cd();

  TLegend *LQpAndQm = new TLegend(0.68,0.7,0.89,0.84);
  switch (decaytype){
  case Z2EEG:
    hQp->GetXaxis()->SetTitle("M_{inv} (e^{#pm}#gamma), GeV");
    hQm->GetXaxis()->SetTitle("M_{inv} (e^{#pm}#gamma), GeV");
    LQpAndQm->AddEntry(hQp,"Qp M_{inv}(e^{#plus}#gamma) ","LEP");
    LQpAndQm->AddEntry(hQm,"Qm M_{inv}(e^{#minus}#gamma) ","LEP");
    break;
  case Z2MUMUG:
    hQp->GetXaxis()->SetTitle("M_{inv} (#mu^{#pm}#gamma), GeV");
    hQm->GetXaxis()->SetTitle("M_{inv} (#mu^{#pm}#gamma), GeV");
    LQpAndQm->AddEntry(hQp,"Qp M_{inv}(#mu^{#plus}#gamma) ","LEP");
    LQpAndQm->AddEntry(hQm,"Qm M_{inv}(#mu^{#minus}#gamma) ","LEP");
    break;
  }
  hQp->SetMarkerColor(kRed);
  hQp->SetLineColor(kRed);
  hQp->Draw("ep");
  hQm->SetMarkerColor(kBlue);
  hQm->SetLineColor(kBlue);
  hQm->Draw("ep same");

  LQpAndQm->Draw();

  if (picdir!=NULL) {    
    picname=string(picdir)+"QpVsQm/"+dresspicprefix+"QpAndQm-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cQpAndQm->Print(picname.c_str());
    picname=string(picdir)+"QpVsQm/"+dresspicprefix+"QpVsQm-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cQpvsQm->Print(picname.c_str());
  }
  // gStyle->SetOptTitle(0);

  //END for QpvsQm hist 

  //BEGIN for QpBvsR hist 
  cQpBvsR=(TCanvas *)gROOT->FindObject("cQpBvsR");
  if (!cQpBvsR)  cQpBvsR=new TCanvas("cQpBvsR","cQpBvsR",800.,600.);
  cQpBvsR->Clear();
  cQpBvsR->cd();
  cQpBvsR->SetGridy();
  cQpBvsR->SetGridx();
  switch (decaytype){
  case Z2EEG:
    hQpBvsR->GetXaxis()->SetTitle("M_{inv}(e^{+}#gamma), GeV");
    hQpBvsR->GetYaxis()->SetTitle("Base/p_{T}(Z) reweighted ");
    hQpBvsR->GetYaxis()->SetTitleOffset(1.);
    break;
  case Z2MUMUG:
    hQpBvsR->GetXaxis()->SetTitle("M_{inv}(#mu^{+}#gamma), GeV");
    hQpBvsR->GetYaxis()->SetTitle("Base/p_{T}(Z) reweighted ");
    hQpBvsR->GetYaxis()->SetTitleOffset(1.);
    break;
  }
  // gStyle->SetOptStat(1); 
  // gStyle->SetOptFit(1);
  hQpBvsR->GetYaxis()->SetRangeUser(0.8,1.2);
  TF1 *lQpBvsR = new TF1("lQpBvsR","[0]", 0, 1);
  lQpBvsR->SetParName(0,"C");
  hQpBvsR->Fit(lQpBvsR,"","",5.,90.);
  hQpBvsR->Draw("ep");

  // gStyle->SetOptStat(0); 

  cQpBAndR=(TCanvas *)gROOT->FindObject("cQpBAndR");
  if (!cQpBAndR)  cQpBAndR=new TCanvas("cQpBAndR","cQpBAndR",800.,600.);
  cQpBAndR->Clear();
  cQpBAndR->cd();

  TLegend *LQpBAndR = new TLegend(0.68,0.7,0.89,0.84);
  switch (decaytype){
  case Z2EEG:
    hQp->GetXaxis()->SetTitle("M_{inv} (e^{+}#gamma), GeV");
    hQpw->GetXaxis()->SetTitle("M_{inv} (e^{+}#gamma), GeV");
    LQpBAndR->AddEntry(hQp,"Base Qp hist ","LEP");
    LQpBAndR->AddEntry(hQpw,"p_{T}(Z) reweighted ","LEP");
    break;
  case Z2MUMUG:
    hQp->GetXaxis()->SetTitle("M_{inv} (#mu^{+}#gamma), GeV");
    hQpw->GetXaxis()->SetTitle("M_{inv} (#mu^{+}#gamma), GeV");
    LQpBAndR->AddEntry(hQp,"Base Qp hist ","LEP");
    LQpBAndR->AddEntry(hQpw,"p_{T}(Z) reweighted ","LEP");
    break;
  }
  hQp->SetMarkerColor(kRed);
  hQp->SetLineColor(kRed);
  hQp->Draw("ep");
  hQpw->SetMarkerColor(kBlue);
  hQpw->SetLineColor(kBlue);
  hQpw->Draw("ep same");

  LQpBAndR->Draw();

  if (picdir!=NULL) {    
    picname=string(picdir)+"BaseVsReweight/"+dresspicprefix+"QpBAndR-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cQpBAndR->Print(picname.c_str());
    picname=string(picdir)+"BaseVsReweight/"+dresspicprefix+"QpBvsR-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cQpBvsR->Print(picname.c_str());
  }
  //END for QpBvsR hist 

  //BEGIN for dRBvsR hist 
  cdRBvsR=(TCanvas *)gROOT->FindObject("cdRBvsR");
  if (!cdRBvsR)  cdRBvsR=new TCanvas("cdRBvsR","cdRBvsR",800.,600.);
  cdRBvsR->Clear();
  cdRBvsR->cd();
  cdRBvsR->SetGridy();
  cdRBvsR->SetGridx();
  switch (decaytype){
  case Z2EEG:
    hdRBvsR->GetXaxis()->SetTitle("dR");
    hdRBvsR->GetYaxis()->SetTitle("Base/p_{T}(Z) reweighted ");
    hdRBvsR->GetYaxis()->SetTitleOffset(1.);
    break;
  case Z2MUMUG:
    hdRBvsR->GetXaxis()->SetTitle("dR");
    hdRBvsR->GetYaxis()->SetTitle("Base/p_{T}(Z) reweighted ");
    hdRBvsR->GetYaxis()->SetTitleOffset(1.);
    break;
  }
  // gStyle->SetOptStat(1); 
  // gStyle->SetOptFit(1);
  hdRBvsR->GetYaxis()->SetRangeUser(0.8,1.2);
  TF1 *ldRBvsR = new TF1("ldRBvsR","[0]", 0, 1);
  ldRBvsR->SetParName(0,"C");
  hdRBvsR->Fit(ldRBvsR,"","",0.4,3.);
  hdRBvsR->Draw("ep");

  // gStyle->SetOptStat(0); 

  cdRBAndR=(TCanvas *)gROOT->FindObject("cdRBAndR");
  if (!cdRBAndR)  cdRBAndR=new TCanvas("cdRBAndR","cdRBAndR",800.,600.);
  cdRBAndR->Clear();
  cdRBAndR->cd();

  TLegend *LdRBAndR = new TLegend(0.68,0.7,0.89,0.84);
  switch (decaytype){
  case Z2EEG:
    hdR->GetXaxis()->SetTitle("dR");
    hdRw->GetXaxis()->SetTitle("dR");
    LdRBAndR->AddEntry(hdR,"Base dR hist ","LEP");
    LdRBAndR->AddEntry(hdRw,"p_{T}(Z) reweighted ","LEP");
    break;
  case Z2MUMUG:
    hdR->GetXaxis()->SetTitle("dR");
    hdRw->GetXaxis()->SetTitle("dR");
    LdRBAndR->AddEntry(hdR,"Base dR hist ","LEP");
    LdRBAndR->AddEntry(hdRw,"p_{T}(Z) reweighted ","LEP");
    break;
  }
  hdR->SetMarkerColor(kRed);
  hdR->SetLineColor(kRed);
  hdR->Draw("ep");
  hdRw->SetMarkerColor(kBlue);
  hdRw->SetLineColor(kBlue);
  hdRw->Draw("ep same");

  LdRBAndR->Draw();

  if (picdir!=NULL) {    
    picname=string(picdir)+"BaseVsReweight/"+dresspicprefix+"dRBAndR-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cdRBAndR->Print(picname.c_str());
    picname=string(picdir)+"BaseVsReweight/"+dresspicprefix+"dRBvsR-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cdRBvsR->Print(picname.c_str());
  }
  // return 0;
  // end dRBvsR hist

  //BEGIN for ptgammaBvsR hist 
  cptgammaBvsR=(TCanvas *)gROOT->FindObject("cptgammaBvsR");
  if (!cptgammaBvsR)  cptgammaBvsR=new TCanvas("cptgammaBvsR","cptgammaBvsR",800.,600.);
  cptgammaBvsR->Clear();
  cptgammaBvsR->cd();
  cptgammaBvsR->SetGridy();
  cptgammaBvsR->SetGridx();
  switch (decaytype){
  case Z2EEG:
    hptgammaBvsR->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
    hptgammaBvsR->GetYaxis()->SetTitle("Base/p_{T}(Z) reweighted ");
    hptgammaBvsR->GetYaxis()->SetTitleOffset(1.);
    break;
  case Z2MUMUG:
    hptgammaBvsR->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
    hptgammaBvsR->GetYaxis()->SetTitle("Base/p_{T}(Z) reweighted ");
    hptgammaBvsR->GetYaxis()->SetTitleOffset(1.);
    break;
  }
  // gStyle->SetOptStat(1); 
  // gStyle->SetOptFit(1);
  hptgammaBvsR->GetYaxis()->SetRangeUser(0.8,1.2);
  TF1 *lptgammaBvsR = new TF1("lptgammaBvsR","[0]", 0, 1);
  lptgammaBvsR->SetParName(0,"C");
  hptgammaBvsR->Fit(lptgammaBvsR,"","",5.,90.);
  hptgammaBvsR->Draw("ep");

  // gStyle->SetOptStat(0); 

  cptgammaBAndR=(TCanvas *)gROOT->FindObject("cptgammaBAndR");
  if (!cptgammaBAndR)  cptgammaBAndR=new TCanvas("cptgammaBAndR","cptgammaBAndR",800.,600.);
  cptgammaBAndR->Clear();
  cptgammaBAndR->cd();

  TLegend *LptgammaBAndR = new TLegend(0.68,0.7,0.89,0.84);
  switch (decaytype){
  case Z2EEG:
    hptgamma->GetXaxis()->SetTitle("p_{T} (#gamma), GeV");
    hptgammaw->GetXaxis()->SetTitle("p_{T} (#gamma), GeV");
    LptgammaBAndR->AddEntry(hptgamma,"Base p_{T}(#gamma) hist ","LEP");
    LptgammaBAndR->AddEntry(hptgammaw,"p_{T}(Z) reweighted ","LEP");
    break;
  case Z2MUMUG:
    hptgamma->GetXaxis()->SetTitle("p_{T} (#gamma), GeV");
    hptgammaw->GetXaxis()->SetTitle("p_{T} (#gamma), GeV");
    LptgammaBAndR->AddEntry(hptgamma,"Base p_{T}(#gamma) hist ","LEP");
    LptgammaBAndR->AddEntry(hptgammaw,"p_{T}(Z) reweighted ","LEP");
    break;
  }
  hptgamma->SetMarkerColor(kRed);
  hptgamma->SetLineColor(kRed);
  hptgamma->Draw("ep");
  hptgammaw->SetMarkerColor(kBlue);
  hptgammaw->SetLineColor(kBlue);
  hptgammaw->Draw("ep same");

  LptgammaBAndR->Draw();

  if (picdir!=NULL) {    
    picname=string(picdir)+"BaseVsReweight/"+dresspicprefix+"ptgammaBAndR-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cptgammaBAndR->Print(picname.c_str());
    picname=string(picdir)+"BaseVsReweight/"+dresspicprefix+"ptgammaBvsR-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cptgammaBvsR->Print(picname.c_str());
  }
  //END for ptgammaBvsR hist 


  //BEGIN for ptZBvsR hist 
  cptZBvsR=(TCanvas *)gROOT->FindObject("cptZBvsR");
  if (!cptZBvsR)  cptZBvsR=new TCanvas("cptZBvsR","cptZBvsR",800.,600.);
  cptZBvsR->Clear();
  cptZBvsR->cd();
  cptZBvsR->SetLogx();
  cptZBvsR->SetGridy();
  cptZBvsR->SetGridx();
  switch (decaytype){
  case Z2EEG:
    hptZBvsR->GetXaxis()->SetTitle("p_{T}(Z), GeV");
    hptZBvsR->GetYaxis()->SetTitle("Base/p_{T}(Z) reweighted ");
    hptZBvsR->GetYaxis()->SetTitleOffset(1.);
    break;
  case Z2MUMUG:
    hptZBvsR->GetXaxis()->SetTitle("p_{T}(Z), GeV");
    hptZBvsR->GetYaxis()->SetTitle("Base/p_{T}(Z) reweighted ");
    hptZBvsR->GetYaxis()->SetTitleOffset(1.);
    break;
  }
  // gStyle->SetOptStat(1); 
  // gStyle->SetOptFit(1);
  // hptZBvsR->GetYaxis()->SetRangeUser(0.8,1.2);
  // TF1 *lptZBvsR = new TF1("lptZBvsR","[0]", 0, 1);
  // lptZBvsR->SetParName(0,"C");
  // hptZBvsR->Fit(lptZBvsR,"","",5.,90.);
  hptZBvsR->Draw("ep");

  // gStyle->SetOptStat(0); 

  cptZBAndR=(TCanvas *)gROOT->FindObject("cptZBAndR");
  if (!cptZBAndR)  cptZBAndR=new TCanvas("cptZBAndR","cptZBAndR",800.,600.);
  cptZBAndR->Clear();
  cptZBAndR->cd();
  cptZBAndR->SetLogx();

  TLegend *LptZBAndR = new TLegend(0.68,0.7,0.89,0.84);
  switch (decaytype){
  case Z2EEG:
    hptZ->GetXaxis()->SetTitle("p_{T} (Z), GeV");
    hptZw->GetXaxis()->SetTitle("p_{T} (Z), GeV");
    LptZBAndR->AddEntry(hptZ,"Base p_{T}(Z) hist ","LEP");
    LptZBAndR->AddEntry(hptZw,"p_{T}(Z) reweighted ","LEP");
    break;
  case Z2MUMUG:
    hptZ->GetXaxis()->SetTitle("p_{T} (Z), GeV");
    hptZw->GetXaxis()->SetTitle("p_{T} (Z), GeV");
    LptZBAndR->AddEntry(hptZ,"Base p_{T}(Z) hist ","LEP");
    LptZBAndR->AddEntry(hptZw,"p_{T}(Z) reweighted ","LEP");
    break;
  }
  hptZ->SetMarkerColor(kRed);
  hptZ->SetLineColor(kRed);
  hptZ->Draw("ep");
  hptZw->SetMarkerColor(kBlue);
  hptZw->SetLineColor(kBlue);
  hptZw->Draw("ep same");

  LptZBAndR->Draw();

  if (picdir!=NULL) {    
    picname=string(picdir)+"BaseVsReweight/"+dresspicprefix+"ptZBAndR-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cptZBAndR->Print(picname.c_str());
    picname=string(picdir)+"BaseVsReweight/"+dresspicprefix+"ptZBvsR-"+str(decaytype)+"-"+str(SimSwitch)+".eps";
    cptZBvsR->Print(picname.c_str());
  }
  //END for ptZBvsR hist 
  
  // general settings
  int Smakepics=1;
  int Smakehist=1;
  switch (decaytype){
  case Z2EEG:
    dirstr="pics/simcmp/ee/"+dresspicprefix;
    break;
  case Z2MUMUG:
    dirstr="pics/simcmp/mumu/"+dresspicprefix;
    break;
  default:
    return 0;
  }

  TH1D *h1,*h2,*h3,*h4,*h5,*h6,*h7,*h8,*h9;
  TF1 *f7;
  cptZ=(TCanvas *)gROOT->FindObject("cptZ");
  if (!cptZ)  cptZ=new TCanvas("cptZ","cptZ",1000.,600.);
  cptZ->Clear();
  cptZ->cd();
  cptZ->SetLogx();
  h1= (TH1D *) hptZ1->Clone("hptZ1_draw");
  h2= (TH1D *) hptZ2->Clone("hptZ2_draw");
  h3= (TH1D *) hptZ3->Clone("hptZ3_draw");
  h4= (TH1D *) hptZ4->Clone("hptZ4_draw");
  h5= (TH1D *) hptZ5->Clone("hptZ5_draw");
  h6= (TH1D *) hptZ6->Clone("hptZ6_draw");
  h7= (TH1D *) hptZ7->Clone("hptZ7_draw");
  h1->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h1->SetMarkerStyle(22);
  h1->SetMarkerColor(kBlack);
  h1->SetLineColor(kBlack);
  h1->Scale(1./N1);
  h2->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);
  h2->SetLineColor(kRed);
  h2->Scale(1./N2);
  h3->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(kGreen);
  h3->SetLineColor(kGreen);
  h3->Scale(1./N3);
  h4->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kBlue);
  h4->SetLineColor(kBlue);
  h4->Scale(1./N4);
  h5->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h5->SetMarkerStyle(29);
  h5->SetMarkerColor(kOrange);
  h5->SetLineColor(kOrange);
  h5->Scale(1./N5);
  h6->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kPink);
  h6->SetLineColor(kPink);
  h6->Scale(1./N6);
  h7->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kSpring);
  h7->SetLineColor(kSpring);
  h7->Scale(1./N7);
  
  h2->SetTitle("p_{T}(Z), Normalized");
  h2->Draw();
  h3->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  h1->Draw("same");
  TLegend *LptZ = new TLegend(0.5,0.5,0.8,0.8);
  LptZ->AddEntry(h1,"PowhegPythia8","PE");
  LptZ->AddEntry(h2,"Sherpa 1.4","PE");
  LptZ->AddEntry(h3,"Sherpa 2.21","PE");
  LptZ->AddEntry(h4,"AlpgenPythia","PE");
  LptZ->AddEntry(h5,"AlpgenJimmy","PE");
  LptZ->AddEntry(h6,"Sherpa 2.24","PE");
  //  LptZ->AddEntry(h7,"PowhegPythia8 (One #gamma)","PE");
  LptZ->Draw();
  filestr=dirstr+"hptZ.eps";
  if (Smakepics==1) cptZ->Print(filestr.c_str());


  cptZw=(TCanvas *)gROOT->FindObject("cptZw");
  if (!cptZw)  cptZw=new TCanvas("cptZw","cptZw",1000.,600.);
  cptZw->Clear();
  cptZw->cd();
  h1= (TH1D *) hptZw1->Clone("hptZw1_draw");
  h2= (TH1D *) hptZw2->Clone("hptZw2_draw");
  h3= (TH1D *) hptZw3->Clone("hptZw3_draw");
  h4= (TH1D *) hptZw4->Clone("hptZw4_draw");
  h5= (TH1D *) hptZw5->Clone("hptZw5_draw");
  h6= (TH1D *) hptZw6->Clone("hptZw6_draw");
  h7= (TH1D *) hptZw7->Clone("hptZw7_draw");
  h1->SetTitle("p_{T}(Z), Normalized");
  h1->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h1->SetMarkerStyle(22);
  h1->SetMarkerColor(kBlack);
  h1->SetLineColor(kBlack);
  h1->Scale(1./N1);
  h2->SetTitle("p_{T}(Z), Normalized");
  h2->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);
  h2->SetLineColor(kRed);
  h2->Scale(1./N2);
  h3->SetTitle("p_{T}(Z), Normalized");
  h3->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(kGreen);
  h3->SetLineColor(kGreen);
  h3->Scale(1./N3);
  h4->SetTitle("p_{T}(Z), Normalized");
  h4->GetXaxis()->SetTitle("p_{T}, GeV");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kBlue);
  h4->SetLineColor(kBlue);
  h4->Scale(1./N4);
  h5->SetTitle("p_{T}(Z), Normalized");
  h5->GetXaxis()->SetTitle("p_{T}, GeV");
  h5->SetMarkerStyle(29);
  h5->SetMarkerColor(kOrange);
  h5->SetLineColor(kOrange);
  h5->Scale(1./N5);
  h6->SetTitle("p_{T}(Z), Normalized");
  h6->GetXaxis()->SetTitle("p_{T}, GeV");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kPink);
  h6->SetLineColor(kPink);
  h6->Scale(1./N6);
  h7->SetTitle("p_{T}(Z), Normalized");
  h7->GetXaxis()->SetTitle("p_{T}, GeV");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kSpring);
  h7->SetLineColor(kSpring);
  h7->Scale(1./N7);

  h2->Draw();
  h3->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  h1->Draw("same");
  TLegend *LptZw = new TLegend(0.5,0.5,0.8,0.8);
  LptZw->AddEntry(h1,"PowhegPythia8","PE");
  LptZw->AddEntry(h2,"Sherpa 1.4","PE");
  LptZw->AddEntry(h3,"Sherpa 2.21","PE");
  LptZw->AddEntry(h4,"AlpgenPythia","PE");
  LptZw->AddEntry(h5,"AlpgenJimmy","PE");
  LptZw->AddEntry(h6,"Sherpa 2.24","PE");
  //  LptZ->AddEntry(h7,"PowhegPythia8 (One #gamma)","PE");
  LptZw->Draw();
  filestr=dirstr+"hptZw.eps";
  if (Smakepics==1) cptZw->Print(filestr.c_str());


  cQp=(TCanvas *)gROOT->FindObject("cQp");
  if (!cQp)  cQp=new TCanvas("cQp","cQp",1000.,600.);
  cQp->Clear();
  cQp->cd();
  h1= (TH1D *)hQp1->Clone("hQp1_draw");
  h2= (TH1D *)hQp2->Clone("hQp2_draw");
  h3= (TH1D *)hQp3->Clone("hQp3_draw");
  h4= (TH1D *)hQp4->Clone("hQp4_draw");
  h5= (TH1D *)hQp5->Clone("hQp5_draw");
  h6= (TH1D *)hQp6->Clone("hQp6_draw");
  h7= (TH1D *)hQp7->Clone("hQp7_draw");
  h1->GetXaxis()->SetTitle("Qp, GeV");
  h1->SetMarkerStyle(22);
  h1->SetMarkerColor(kBlack);
  h1->SetLineColor(kBlack);
  h1->Scale(1./N1);
  h2->GetXaxis()->SetTitle("Qp, GeV");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);
  h2->SetLineColor(kRed);
  h2->Scale(1./N2);
  h3->GetXaxis()->SetTitle("Qp, GeV");
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(kGreen);
  h3->SetLineColor(kGreen);
  h3->Scale(1./N3);
  h4->GetXaxis()->SetTitle("Qp, GeV");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kBlue);
  h4->SetLineColor(kBlue);
  h4->Scale(1./N4);
  h5->GetXaxis()->SetTitle("Qp, GeV");
  h5->SetMarkerStyle(29);
  h5->SetMarkerColor(kOrange);
  h5->SetLineColor(kOrange);
  h5->Scale(1./N5);
  h6->GetXaxis()->SetTitle("Qp, GeV");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kPink);
  h6->SetLineColor(kPink);
  h6->Scale(1./N6);
  h7->GetXaxis()->SetTitle("Qp, GeV");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kSpring);
  h7->SetLineColor(kSpring);
  h7->Scale(1./N7);

  h2->SetTitle("Qp, Normalized");
  h2->Draw();
  h3->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  h1->Draw("same");
  TLegend *LQp = new TLegend(0.6,0.6,0.9,0.9);
  LQp->AddEntry(h1,"PowhegPythia8","PE");
  LQp->AddEntry(h2,"Sherpa 1.4","PE");
  LQp->AddEntry(h3,"Sherpa 2.21","PE");
  LQp->AddEntry(h4,"AlpgenPythia","PE");
  LQp->AddEntry(h5,"AlpgenJimmy","PE");
  LQp->AddEntry(h6,"Sherpa 2.24","PE");
  //  LQp->AddEntry(h7,"PowhegPythia8 (One #gamma)","PE");
  LQp->Draw();
  filestr=dirstr+"hQp.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());

  cQpw=(TCanvas *)gROOT->FindObject("cQpw");
  if (!cQpw)  cQpw=new TCanvas("cQpw","cQpw",1000.,600.);
  cQpw->Clear();
  cQpw->cd();
  h1= (TH1D *)hQpw1->Clone("hQpw1_draw");
  h2= (TH1D *)hQpw2->Clone("hQpw2_draw");
  h3= (TH1D *)hQpw3->Clone("hQpw3_draw");
  h4= (TH1D *)hQpw4->Clone("hQpw4_draw");
  h5= (TH1D *)hQpw5->Clone("hQpw5_draw");
  h6= (TH1D *)hQpw6->Clone("hQpw6_draw");
  h7= (TH1D *)hQpw7->Clone("hQpw7_draw");
  h1->GetXaxis()->SetTitle("Qp, GeV");
  h1->SetMarkerStyle(22);
  h1->SetMarkerColor(kBlack);
  h1->SetLineColor(kBlack);
  h1->Scale(1./N1);
  h2->GetXaxis()->SetTitle("Qp, GeV");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);
  h2->SetLineColor(kRed);
  h2->Scale(1./N2);
  h3->GetXaxis()->SetTitle("Qp, GeV");
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(kGreen);
  h3->SetLineColor(kGreen);
  h3->Scale(1./N3);
  h4->GetXaxis()->SetTitle("Qp, GeV");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kBlue);
  h4->SetLineColor(kBlue);
  h4->Scale(1./N4);
  h5->GetXaxis()->SetTitle("Qp, GeV");
  h5->SetMarkerStyle(29);
  h5->SetMarkerColor(kOrange);
  h5->SetLineColor(kOrange);
  h5->Scale(1./N5);
  h6->GetXaxis()->SetTitle("Qp, GeV");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kPink);
  h6->SetLineColor(kPink);
  h6->Scale(1./N6);
  h7->GetXaxis()->SetTitle("Qp, GeV");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kSpring);
  h7->SetLineColor(kSpring);
  h7->Scale(1./N7);

  h2->SetTitle("Qp, Normalized");
  h2->Draw();
  h3->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  h1->Draw("same");
  TLegend *LQpw = new TLegend(0.6,0.6,0.9,0.9);
  LQpw->AddEntry(h1,"PowhegPythia8","PE");
  LQpw->AddEntry(h2,"Sherpa 1.4","PE");
  LQpw->AddEntry(h3,"Sherpa 2.21","PE");
  LQpw->AddEntry(h4,"AlpgenPythia","PE");
  LQpw->AddEntry(h5,"AlpgenJimmy","PE");
  LQpw->AddEntry(h6,"Sherpa 2.24","PE");
  //  LQpw->AddEntry(h7,"PowhegPythia8 (One #gamma)","PE");
  LQpw->Draw();
  filestr=dirstr+"hQpw.eps";
  if (Smakepics==1) cQpw->Print(filestr.c_str());

  //dR
  cdR=(TCanvas *)gROOT->FindObject("cdR");
  if (!cdR)  cdR=new TCanvas("cdR","cdR",1000.,600.);
  cdR->Clear();
  cdR->cd();
  h1= (TH1D *)hdR1->Clone("hdR1_draw");
  h2= (TH1D *)hdR2->Clone("hdR2_draw");
  h3= (TH1D *)hdR3->Clone("hdR3_draw");
  h4= (TH1D *)hdR4->Clone("hdR4_draw");
  h5= (TH1D *)hdR5->Clone("hdR5_draw");
  h6= (TH1D *)hdR6->Clone("hdR6_draw");
  h7= (TH1D *)hdR7->Clone("hdR7_draw");
  h1->GetXaxis()->SetTitle("dR");
  h1->SetMarkerStyle(22);
  h1->SetMarkerColor(kBlack);
  h1->SetLineColor(kBlack);
  h1->Scale(1./N1);
  h2->GetXaxis()->SetTitle("dR");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);
  h2->SetLineColor(kRed);
  h2->Scale(1./N2);
  h3->GetXaxis()->SetTitle("dR");
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(kGreen);
  h3->SetLineColor(kGreen);
  h3->Scale(1./N3);
  h4->GetXaxis()->SetTitle("dR");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kBlue);
  h4->SetLineColor(kBlue);
  h4->Scale(1./N4);
  h5->GetXaxis()->SetTitle("dR");
  h5->SetMarkerStyle(29);
  h5->SetMarkerColor(kOrange);
  h5->SetLineColor(kOrange);
  h5->Scale(1./N5);
  h6->GetXaxis()->SetTitle("dR");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kPink);
  h6->SetLineColor(kPink);
  h6->Scale(1./N6);
  h7->GetXaxis()->SetTitle("dR");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kSpring);
  h7->SetLineColor(kSpring);
  h7->Scale(1./N7);

  h2->SetTitle("dR, Normalized");
  h2->Draw();
  h3->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  h1->Draw("same");
  TLegend *LdR = new TLegend(0.5,0.5,0.8,0.8);
  LdR->AddEntry(h1,"PowhegPythia8","PE");
  LdR->AddEntry(h2,"Sherpa 1.4","PE");
  LdR->AddEntry(h3,"Sherpa 2.21","PE");
  LdR->AddEntry(h4,"AlpgenPythia","PE");
  LdR->AddEntry(h5,"AlpgenJimmy","PE");
  LdR->AddEntry(h6,"Sherpa 2.24","PE");
  //  LdR->AddEntry(h7,"PowhegPythia8 (One #gamma)","PE");
  LdR->Draw();
  filestr=dirstr+"hdR.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());

  cdRw=(TCanvas *)gROOT->FindObject("cdRw");
  if (!cdRw)  cdRw=new TCanvas("cdRw","cdRw",1000.,600.);
  cdRw->Clear();
  cdRw->cd();
  h1= (TH1D *)hdRw1->Clone("hdRw1_draw");
  h2= (TH1D *)hdRw2->Clone("hdRw2_draw");
  h3= (TH1D *)hdRw3->Clone("hdRw3_draw");
  h4= (TH1D *)hdRw4->Clone("hdRw4_draw");
  h5= (TH1D *)hdRw5->Clone("hdRw5_draw");
  h6= (TH1D *)hdRw6->Clone("hdRw6_draw");
  h7= (TH1D *)hdRw7->Clone("hdRw7_draw");
  h1->GetXaxis()->SetTitle("dR");
  h1->SetMarkerStyle(22);
  h1->SetMarkerColor(kBlack);
  h1->SetLineColor(kBlack);
  h1->Scale(1./N1);
  h2->GetXaxis()->SetTitle("dR");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);
  h2->SetLineColor(kRed);
  h2->Scale(1./N2);
  h3->GetXaxis()->SetTitle("dR");
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(kGreen);
  h3->SetLineColor(kGreen);
  h3->Scale(1./N3);
  h4->GetXaxis()->SetTitle("dR");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kBlue);
  h4->SetLineColor(kBlue);
  h4->Scale(1./N4);
  h5->GetXaxis()->SetTitle("dR");
  h5->SetMarkerStyle(29);
  h5->SetMarkerColor(kOrange);
  h5->SetLineColor(kOrange);
  h5->Scale(1./N5);
  h6->GetXaxis()->SetTitle("dR");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kPink);
  h6->SetLineColor(kPink);
  h6->Scale(1./N6);
  h7->GetXaxis()->SetTitle("dR");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kSpring);
  h7->SetLineColor(kSpring);
  h7->Scale(1./N7);

  h2->SetTitle("dR, Normalized");
  h2->Draw();
  h3->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  h1->Draw("same");
  TLegend *LdRw = new TLegend(0.6,0.6,0.9,0.9);
  LdRw->AddEntry(h1,"PowhegPythia8","PE");
  LdRw->AddEntry(h2,"Sherpa 1.4","PE");
  LdRw->AddEntry(h3,"Sherpa 2.21","PE");
  LdRw->AddEntry(h4,"AlpgenPythia","PE");
  LdRw->AddEntry(h5,"AlpgenJimmy","PE");
  LdRw->AddEntry(h6,"Sherpa 2.24","PE");
  //  LdRw->AddEntry(h7,"PowhegPythia8 (One #gamma)","PE");
  LdRw->Draw();
  filestr=dirstr+"hdRw.eps";
  if (Smakepics==1) cdRw->Print(filestr.c_str());

  //ptgamma
  cptgamma=(TCanvas *)gROOT->FindObject("cptgamma");
  if (!cptgamma)  cptgamma=new TCanvas("cptgamma","cptgamma",1000.,600.);
  cptgamma->Clear();
  cptgamma->cd();
  h1= (TH1D *)hptgamma1->Clone("hptgamma1_draw");
  h2= (TH1D *)hptgamma2->Clone("hptgamma2_draw");
  h3= (TH1D *)hptgamma3->Clone("hptgamma3_draw");
  h4= (TH1D *)hptgamma4->Clone("hptgamma4_draw");
  h5= (TH1D *)hptgamma5->Clone("hptgamma5_draw");
  h6= (TH1D *)hptgamma6->Clone("hptgamma6_draw");
  h7= (TH1D *)hptgamma7->Clone("hptgamma7_draw");
  h1->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h1->SetMarkerStyle(22);
  h1->SetMarkerColor(kBlack);
  h1->SetLineColor(kBlack);
  h1->Scale(1./N1);
  h2->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);
  h2->SetLineColor(kRed);
  h2->Scale(1./N2);
  h3->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(kGreen);
  h3->SetLineColor(kGreen);
  h3->Scale(1./N3);
  h4->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kBlue);
  h4->SetLineColor(kBlue);
  h4->Scale(1./N4);
  h5->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h5->SetMarkerStyle(29);
  h5->SetMarkerColor(kOrange);
  h5->SetLineColor(kOrange);
  h5->Scale(1./N5);
  h6->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kPink);
  h6->SetLineColor(kPink);
  h6->Scale(1./N6);
  h7->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kSpring);
  h7->SetLineColor(kSpring);
  h7->Scale(1./N7);

  h2->SetTitle("p_{T}(#gamma), Normalized");
  h2->Draw();
  h3->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  h1->Draw("same");
  TLegend *Lptgamma = new TLegend(0.5,0.5,0.8,0.8);
  Lptgamma->AddEntry(h1,"PowhegPythia8","PE");
  Lptgamma->AddEntry(h2,"Sherpa 1.4","PE");
  Lptgamma->AddEntry(h3,"Sherpa 2.21","PE");
  Lptgamma->AddEntry(h4,"AlpgenPythia","PE");
  Lptgamma->AddEntry(h5,"AlpgenJimmy","PE");
  Lptgamma->AddEntry(h6,"Sherpa 2.24","PE");
  //  Lptgamma->AddEntry(h7,"PowhegPythia8 (One #gamma)","PE");
  Lptgamma->Draw();
  filestr=dirstr+"hptgamma.eps";
  if (Smakepics==1) cptgamma->Print(filestr.c_str());

  cptgammaw=(TCanvas *)gROOT->FindObject("cptgammaw");
  if (!cptgammaw)  cptgammaw=new TCanvas("cptgammaw","cptgammaw",1000.,600.);
  cptgammaw->Clear();
  cptgammaw->cd();
  h1= (TH1D *)hptgammaw1->Clone("hptgammaw1_draw");
  h2= (TH1D *)hptgammaw2->Clone("hptgammaw2_draw");
  h3= (TH1D *)hptgammaw3->Clone("hptgammaw3_draw");
  h4= (TH1D *)hptgammaw4->Clone("hptgammaw4_draw");
  h5= (TH1D *)hptgammaw5->Clone("hptgammaw5_draw");
  h6= (TH1D *)hptgammaw6->Clone("hptgammaw6_draw");
  h7= (TH1D *)hptgammaw7->Clone("hptgammaw7_draw");
  h1->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h1->SetMarkerStyle(22);
  h1->SetMarkerColor(kBlack);
  h1->SetLineColor(kBlack);
  h1->Scale(1./N1);
  h2->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);
  h2->SetLineColor(kRed);
  h2->Scale(1./N2);
  h3->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h3->SetMarkerStyle(21);
  h3->SetMarkerColor(kGreen);
  h3->SetLineColor(kGreen);
  h3->Scale(1./N3);
  h4->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kBlue);
  h4->SetLineColor(kBlue);
  h4->Scale(1./N4);
  h5->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h5->SetMarkerStyle(29);
  h5->SetMarkerColor(kOrange);
  h5->SetLineColor(kOrange);
  h5->Scale(1./N5);
  h6->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kPink);
  h6->SetLineColor(kPink);
  h6->Scale(1./N6);
  h7->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kSpring);
  h7->SetLineColor(kSpring);
  h7->Scale(1./N7);

  h2->SetTitle("p_{T}(#gamma), Normalized");
  h2->Draw();
  h3->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  h1->Draw("same");
  TLegend *Lptgammaw = new TLegend(0.6,0.6,0.9,0.9);
  Lptgammaw->AddEntry(h1,"PowhegPythia8","PE");
  Lptgammaw->AddEntry(h2,"Sherpa 1.4","PE");
  Lptgammaw->AddEntry(h3,"Sherpa 2.21","PE");
  Lptgammaw->AddEntry(h4,"AlpgenPythia","PE");
  Lptgammaw->AddEntry(h5,"AlpgenJimmy","PE");
  Lptgammaw->AddEntry(h6,"Sherpa 2.24","PE");
  //  Lptgammaw->AddEntry(h7,"PowhegPythia8 (One #gamma)","PE");
  Lptgammaw->Draw();
  filestr=dirstr+"hptgammaw.eps";
  if (Smakepics==1) cptgammaw->Print(filestr.c_str());
  
  cptZr=(TCanvas *)gROOT->FindObject("cptZr");
  if (!cptZr)  cptZr=new TCanvas("cptZr","cptZr",1000.,600.);
  cptZr->Clear();
  cptZr->SetGridy();
  cptZr->SetLogx();
  cptZr->cd();
  h1= (TH1D *)hptZr12->Clone("hptZr12_draw");
  h2= (TH1D *)hptZr13->Clone("hptZr13_draw");
  h4= (TH1D *)hptZr14->Clone("hptZr14_draw");
  h5= (TH1D *)hptZr15->Clone("hptZr15_draw");
  h6= (TH1D *)hptZr16->Clone("hptZr16_draw");
  h7= (TH1D *)hptZr17->Clone("hptZr17_draw");
  h8= (TH1D *)hptZr62->Clone("hptZr62_draw");
  h9= (TH1D *)hptZr63->Clone("hptZr63_draw");
  h1->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h1->SetMarkerStyle(20);
  h1->SetMarkerColor(kRed);
  h1->SetLineColor(kRed);
  // h2->Scale(1./N2);
  h2->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h2->SetMarkerStyle(21);
  h2->SetMarkerColor(kGreen);
  h2->SetLineColor(kGreen);
  // h4->Scale(1./N4);
  h4->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kOrange);
  h4->SetLineColor(kOrange);
  // h5->Scale(1./N5);
  h5->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h5->SetMarkerStyle(4);
  h5->SetMarkerColor(kSpring);
  h5->SetLineColor(kSpring);
  // h6->Scale(1./N6);
  h6->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kBlue);
  h6->SetLineColor(kBlue);
  // h7->Scale(1./N7);
  h7->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kBlack);
  h7->SetLineColor(kBlack);

  h8->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h8->SetMarkerStyle(20);
  h8->SetMarkerColor(kRed);
  h8->SetLineColor(kRed);

  h9->GetXaxis()->SetTitle("p_{T}(Z), GeV");
  h9->SetMarkerStyle(21);
  h9->SetMarkerColor(kBlue);
  h9->SetLineColor(kBlue);

  // All sims 2 PowhegPythia_Zcorr
  h2->GetYaxis()->SetRangeUser(0.7,1.5);
  h2->SetTitle("p_{T}(Z), Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  TLegend *LptZr = new TLegend(0.35,0.6,0.65,0.9);
  LptZr->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LptZr->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LptZr->AddEntry(h4,"AlpgenPythia/PowhegPythia8","PE");
  LptZr->AddEntry(h5,"AlpgenJimmy/PowhegPythia8","PE");
  LptZr->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LptZr->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LptZr->Draw();
  filestr=dirstr+"hptZr.eps";
  if (Smakepics==1) cptZr->Print(filestr.c_str());

  // All sim without alpgen vs PowhegPythia_Zcorr
  cptZr->Clear();
  cptZr->SetGridy();
  cptZr->SetLogx();
  cptZr->cd();
  h2->GetYaxis()->SetRangeUser(0.5,2.);
  h2->SetTitle("p_{T}(Z), Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  LptZr->Clear();
  LptZr->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LptZr->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LptZr->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LptZr->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LptZr->Draw();
  filestr=dirstr+"hptZr236v1.eps";
  if (Smakepics==1) cptZr->Print(filestr.c_str());

  // All PowhegPythia_OneGamma vs PowhegPythia_Zcorr
  cptZr->Clear();
  cptZr->SetGridy();
  cptZr->SetLogx();
  cptZr->cd();
  h7->GetYaxis()->SetRangeUser(0.8,1.2);
  h7->SetTitle("p_{T}(Z), Normalized ratios");
  f7 = new TF1("fptZ7","[0]", 0, 1);
  f7->SetParName(0,"C");
  h7->Fit(f7,"","",0.,900.);
  h7->Draw("ep");
  TLegend *LptZr7v1 = new TLegend(0.2,0.7,0.5,0.85);
  LptZr7v1->Clear();
  LptZr7v1->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LptZr7v1->Draw();
  filestr=dirstr+"hptZr7v1.eps";
  if (Smakepics==1) cptZr->Print(filestr.c_str());

  //Sherpa only sims
  cptZr->Clear();
  cptZr->SetGridy();
  cptZr->SetLogx();
  cptZr->cd();
  h8->GetYaxis()->SetRangeUser(0.5,2.);
  h8->SetTitle("p_{T}(Z), Normalized ratios");
  h8->Draw();
  h9->Draw("same");
  LptZr->Clear();
  LptZr->AddEntry(h8,"Sherpa 1.4/Sherpa 2.24","PE");
  LptZr->AddEntry(h9,"Sherpa 2.21/Sherpa 2.24","PE");
  LptZr->Draw();
  filestr=dirstr+"hptZr23v6.eps";
  if (Smakepics==1) cptZr->Print(filestr.c_str());

  
  cQpr=(TCanvas *)gROOT->FindObject("cQpr");
  if (!cQpr)  cQpr=new TCanvas("cQpr","cQpr",1000.,600.);
  cQpr->Clear();
  cQpr->SetGridy();
  cQpr->cd();
  h1= (TH1D *)hQpr12->Clone("hQpr12_draw");
  h2= (TH1D *)hQpr13->Clone("hQpr13_draw");
  h3= (TH1D *)hQpr32->Clone("hQpr32_draw");
  h4= (TH1D *)hQpr14->Clone("hQpr14_draw");
  h5= (TH1D *)hQpr15->Clone("hQpr15_draw");
  h6= (TH1D *)hQpr16->Clone("hQpr16_draw");
  h7= (TH1D *)hQpr17->Clone("hQpr17_draw");
  h8= (TH1D *)hQpr62->Clone("hQpr62_draw");
  h9= (TH1D *)hQpr63->Clone("hQpr63_draw");
  h1->GetXaxis()->SetTitle("Qp, GeV");
  h1->SetMarkerStyle(20);
  h1->SetMarkerColor(kRed);
  h1->SetLineColor(kRed);
  // h2->Scale(1./N2);
  h2->GetXaxis()->SetTitle("Qp, GeV");
  h2->SetMarkerStyle(21);
  h2->SetMarkerColor(kGreen);
  h2->SetLineColor(kGreen);
  // h3->Scale(1./N3);
  h3->GetXaxis()->SetTitle("Qp, GeV");
  h3->SetMarkerStyle(25);
  h3->SetMarkerColor(kOrange);
  h3->SetLineColor(kOrange);
  // h4->Scale(1./N4);
  h4->GetXaxis()->SetTitle("Qp");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kOrange);
  h4->SetLineColor(kOrange);
  // h5->Scale(1./N5);
  h5->GetXaxis()->SetTitle("Qp");
  h5->SetMarkerStyle(4);
  h5->SetMarkerColor(kSpring);
  h5->SetLineColor(kSpring);
  // h6->Scale(1./N6);
  h6->GetXaxis()->SetTitle("Qp");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kBlue);
  h6->SetLineColor(kBlue);
  // h7->Scale(1./N7);
  h7->GetXaxis()->SetTitle("Qp");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kBlack);
  h7->SetLineColor(kBlack);

  h8->GetXaxis()->SetTitle("Qp, GeV");
  h8->SetMarkerStyle(20);
  h8->SetMarkerColor(kRed);
  h8->SetLineColor(kRed);

  h9->GetXaxis()->SetTitle("Qp, GeV");
  h9->SetMarkerStyle(21);
  h9->SetMarkerColor(kBlue);
  h9->SetLineColor(kBlue);

  // All sims 2 PowhegPythia_Zcorr
  h2->GetYaxis()->SetRangeUser(0.7,1.5);
  h2->SetTitle("Qp, Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  TLegend *LQpr = new TLegend(0.35,0.6,0.65,0.9);
  LQpr->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LQpr->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LQpr->AddEntry(h4,"AlpgenPythia/PowhegPythia8","PE");
  LQpr->AddEntry(h5,"AlpgenJimmy/PowhegPythia8","PE");
  LQpr->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LQpr->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LQpr->Draw();
  filestr=dirstr+"hQpr.eps";
  if (Smakepics==1) cQpr->Print(filestr.c_str());

  
  // All sim without alpgen vs PowhegPythia_Zcorr
  cQpr->Clear();
  cQpr->SetGridy();
  cQpr->cd();
  h2->GetYaxis()->SetRangeUser(0.7,1.5);
  h2->SetTitle("Qp, Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  LQpr->Clear();
  LQpr->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LQpr->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LQpr->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LQpr->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LQpr->Draw();
  filestr=dirstr+"hQpr236v1.eps";
  if (Smakepics==1) cQpr->Print(filestr.c_str());

  // All PowhegPythia_OneGamma vs PowhegPythia_Zcorr
  cQpr->Clear();
  cQpr->SetGridy();
  cQpr->cd();
  h7->GetYaxis()->SetRangeUser(0.8,1.2);
  h7->SetTitle("Qp, Normalized ratios");
  f7 = new TF1("fQp7","[0]", 0, 1);
  f7->SetParName(0,"C");
  h7->Fit(f7,"","",5.,90.);
  h7->Draw("ep");
  TLegend *LQpr7v1 = new TLegend(0.2,0.7,0.5,0.85);
  LQpr7v1->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LQpr7v1->Draw();
  filestr=dirstr+"hQpr7v1.eps";
  if (Smakepics==1) cQpr->Print(filestr.c_str());
  
  //Sherpa only sims
  cQpr->Clear();
  cQpr->SetGridy();
  cQpr->cd();
  h8->GetYaxis()->SetRangeUser(0.7,1.5);
  h8->SetTitle("Qp, Normalized ratios");
  h8->Draw();
  h9->Draw("same");
  LQpr->Clear();
  LQpr->AddEntry(h8,"Sherpa 1.4/Sherpa 2.24","PE");
  LQpr->AddEntry(h9,"Sherpa 2.21/Sherpa 2.24","PE");
  LQpr->Draw();
  filestr=dirstr+"hQpr23v6.eps";
  if (Smakepics==1) cQpr->Print(filestr.c_str());

  cQprw=(TCanvas *)gROOT->FindObject("cQprw");
  if (!cQprw)  cQprw=new TCanvas("cQprw","cQprw",1000.,600.);
  cQprw->Clear();
  cQprw->SetGridy();
  cQprw->cd();
  h1= (TH1D *)hQprw12->Clone("hQprw12_draw");
  h2= (TH1D *)hQprw13->Clone("hQprw13_draw");
  h3= (TH1D *)hQprw32->Clone("hQprw32_draw");
  h4= (TH1D *)hQprw14->Clone("hQprw14_draw");
  h5= (TH1D *)hQprw15->Clone("hQprw15_draw");
  h6= (TH1D *)hQprw16->Clone("hQprw16_draw");
  h7= (TH1D *)hQprw17->Clone("hQprw17_draw");
  h8= (TH1D *)hQprw62->Clone("hQprw62_draw");
  h9= (TH1D *)hQprw63->Clone("hQprw63_draw");
  h1->GetXaxis()->SetTitle("Qp, GeV");
  h1->SetMarkerStyle(20);
  h1->SetMarkerColor(kRed);
  h1->SetLineColor(kRed);
  // h2->Scale(1./N2);
  h2->GetXaxis()->SetTitle("Qp, GeV");
  h2->SetMarkerStyle(21);
  h2->SetMarkerColor(kGreen);
  h2->SetLineColor(kGreen);
  // h3->Scale(1./N3);
  h3->GetXaxis()->SetTitle("Qp, GeV");
  h3->SetMarkerStyle(25);
  h3->SetMarkerColor(kOrange);
  h3->SetLineColor(kOrange);
  // h4->Scale(1./N4);
  h4->GetXaxis()->SetTitle("Qp");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kOrange);
  h4->SetLineColor(kOrange);
  // h5->Scale(1./N5);
  h5->GetXaxis()->SetTitle("Qp");
  h5->SetMarkerStyle(4);
  h5->SetMarkerColor(kSpring);
  h5->SetLineColor(kSpring);
  // h6->Scale(1./N6);
  h6->GetXaxis()->SetTitle("Qp");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kBlue);
  h6->SetLineColor(kBlue);
  // h7->Scale(1./N7);
  h7->GetXaxis()->SetTitle("Qp");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kBlack);
  h7->SetLineColor(kBlack);

  h8->GetXaxis()->SetTitle("Qp");
  h8->SetMarkerStyle(20);
  h8->SetMarkerColor(kRed);
  h8->SetLineColor(kRed);

  h9->GetXaxis()->SetTitle("Qp");
  h9->SetMarkerStyle(21);
  h9->SetMarkerColor(kBlue);
  h9->SetLineColor(kBlue);

  // All sims 2 PowhegPythia_Zcorr
  h2->GetYaxis()->SetRangeUser(0.7,1.5);
  h2->SetTitle("Qp, Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  TLegend *LQprw = new TLegend(0.35,0.6,0.65,0.9);
  LQprw->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LQprw->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LQprw->AddEntry(h4,"AlpgenPythia/PowhegPythia8","PE");
  LQprw->AddEntry(h5,"AlpgenJimmy/PowhegPythia8","PE");
  LQprw->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LQprw->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LQprw->Draw();
  filestr=dirstr+"hQprw.eps";
  if (Smakepics==1) cQprw->Print(filestr.c_str());

  
  // All sim without alpgen vs PowhegPythia_Zcorr
  cQprw->Clear();
  cQprw->SetGridy();
  cQprw->cd();
  h2->GetYaxis()->SetRangeUser(0.7,1.5);
  h2->SetTitle("Qp, Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  LQprw->Clear();
  LQprw->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LQprw->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LQprw->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LQprw->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LQprw->Draw();
  filestr=dirstr+"hQprw236v1.eps";
  if (Smakepics==1) cQprw->Print(filestr.c_str());

  // All PowhegPythia_OneGamma vs PowhegPythia_Zcorr
  cQprw->Clear();
  cQprw->SetGridy();
  cQprw->cd();
  h7->GetYaxis()->SetRangeUser(0.8,1.2);
  h7->SetTitle("Qp, Normalized ratios");
  f7 = new TF1("fQpw7","[0]", 0, 1);
  f7->SetParName(0,"C");
  h7->Fit(f7,"","",5.,90.);
  h7->Draw("ep");
  TLegend *LQprw7v1 = new TLegend(0.2,0.7,0.5,0.85);
  LQprw7v1->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LQprw7v1->Draw();
  filestr=dirstr+"hQprw7v1.eps";
  if (Smakepics==1) cQprw->Print(filestr.c_str());
  
  //Sherpa only sims
  cQprw->Clear();
  cQprw->SetGridy();
  cQprw->cd();
  h8->GetYaxis()->SetRangeUser(0.7,1.5);
  h8->SetTitle("Qp, Normalized ratios");
  h8->Draw();
  h9->Draw("same");
  LQprw->Clear();
  LQprw->AddEntry(h8,"Sherpa 1.4/Sherpa 2.24","PE");
  LQprw->AddEntry(h9,"Sherpa 2.21/Sherpa 2.24","PE");
  LQprw->Draw();
  filestr=dirstr+"hQprw23v6.eps";
  if (Smakepics==1) cQprw->Print(filestr.c_str());
  
  

  cdRr=(TCanvas *)gROOT->FindObject("cdRr");
  if (!cdRr)  cdRr=new TCanvas("cdRr","cdRr",1000.,600.);
  cdRr->Clear();
  cdRr->SetGridy();
  cdRr->cd();
  h1= (TH1D *)hdRr12->Clone("hdRr12_draw");
  h2= (TH1D *)hdRr13->Clone("hdRr13_draw");
  h3= (TH1D *)hdRr32->Clone("hdRr32_draw");
  h4= (TH1D *)hdRr14->Clone("hdRr14_draw");
  h5= (TH1D *)hdRr15->Clone("hdRr15_draw");
  h6= (TH1D *)hdRr16->Clone("hdRr16_draw");
  h7= (TH1D *)hdRr17->Clone("hdRr17_draw");
  h8= (TH1D *)hdRr62->Clone("hdRr62_draw");
  h9= (TH1D *)hdRr63->Clone("hdRr63_draw");  
  h1->GetXaxis()->SetTitle("dR");
  h1->SetMarkerStyle(20);
  h1->SetMarkerColor(kRed);
  h1->SetLineColor(kRed);
  // h2->Scale(1./N2);
  h2->GetXaxis()->SetTitle("dR");
  h2->SetMarkerStyle(21);
  h2->SetMarkerColor(kGreen);
  h2->SetLineColor(kGreen);
  // h3->Scale(1./N3);
  h3->GetXaxis()->SetTitle("dR");
  h3->SetMarkerStyle(25);
  h3->SetMarkerColor(kOrange);
  h3->SetLineColor(kOrange);
  // h4->Scale(1./N4);
  h4->GetXaxis()->SetTitle("dR");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kOrange);
  h4->SetLineColor(kOrange);
  // h5->Scale(1./N5);
  h5->GetXaxis()->SetTitle("dR");
  h5->SetMarkerStyle(4);
  h5->SetMarkerColor(kSpring);
  h5->SetLineColor(kSpring);
  // h6->Scale(1./N6);
  h6->GetXaxis()->SetTitle("dR");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kBlue);
  h6->SetLineColor(kBlue);
  // h7->Scale(1./N7);
  h7->GetXaxis()->SetTitle("dR");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kBlack);
  h7->SetLineColor(kBlack);

  h8->GetXaxis()->SetTitle("dR");
  h8->SetMarkerStyle(20);
  h8->SetMarkerColor(kRed);
  h8->SetLineColor(kRed);

  h9->GetXaxis()->SetTitle("dR");
  h9->SetMarkerStyle(21);
  h9->SetMarkerColor(kBlue);
  h9->SetLineColor(kBlue);
  

  h2->SetTitle("dR, Normalized ratios");
  h2->GetYaxis()->SetRangeUser(0.6,2.0);
  h2->Draw();
  h1->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  TLegend *LdRr = new TLegend(0.3,0.6,0.6,0.9);
  LdRr->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LdRr->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LdRr->AddEntry(h4,"AlpgenPythia/PowhegPythia8","PE");
  LdRr->AddEntry(h5,"AlpgenJimmy/PowhegPythia8","PE");
  LdRr->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LdRr->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LdRr->Draw();
  filestr=dirstr+"hdRr.eps";
  if (Smakepics==1) cdRr->Print(filestr.c_str());

  // All sim without alpgen vs PowhegPythia_Zcorr
  cdRr->Clear();
  cdRr->SetGridy();
  cdRr->cd();
  h2->GetYaxis()->SetRangeUser(0.6,2.0);
  h2->SetTitle("dR, Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  LdRr->Clear();
  LdRr->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LdRr->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LdRr->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LdRr->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LdRr->Draw();
  filestr=dirstr+"hdRr236v1.eps";
  if (Smakepics==1) cdRr->Print(filestr.c_str());

  // All PowhegPythia_OneGamma vs PowhegPythia_Zcorr
  cdRr->Clear();
  cdRr->SetGridy();
  cdRr->cd();
  h7->GetYaxis()->SetRangeUser(0.8,1.2);
  h7->SetTitle("dR, Normalized ratios");
  f7 = new TF1("fdR7","[0]", 0, 1);
  f7->SetParName(0,"C");
  h7->Fit(f7,"","",0.4,3.2);
  h7->Draw("ep");
  TLegend *LdRr7v1 = new TLegend(0.2,0.7,0.5,0.85);  
  LdRr7v1->Clear();
  LdRr7v1->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LdRr7v1->Draw();
  filestr=dirstr+"hdRr7v1.eps";
  if (Smakepics==1) cdRr->Print(filestr.c_str());
  
  //Sherpa only sims
  cdRr->Clear();
  cdRr->SetGridy();
  cdRr->cd();
  h8->GetYaxis()->SetRangeUser(0.6,2.0);
  h8->SetTitle("dR, Normalized ratios");
  h8->Draw();
  h9->Draw("same");
  LdRr->Clear();
  LdRr->AddEntry(h8,"Sherpa 1.4/Sherpa 2.24","PE");
  LdRr->AddEntry(h9,"Sherpa 2.21/Sherpa 2.24","PE");
  LdRr->Draw();
  filestr=dirstr+"hdRr23v6.eps";
  if (Smakepics==1) cdRr->Print(filestr.c_str());
  

  cdRrw=(TCanvas *)gROOT->FindObject("cdRrw");
  if (!cdRrw)  cdRrw=new TCanvas("cdRrw","cdRrw",1000.,600.);
  cdRrw->Clear();
  cdRrw->SetGridy();
  cdRrw->cd();
  h1= (TH1D *)hdRrw12->Clone("hdRrw12_draw");
  h2= (TH1D *)hdRrw13->Clone("hdRrw13_draw");
  h3= (TH1D *)hdRrw32->Clone("hdRrw32_draw");
  h4= (TH1D *)hdRrw14->Clone("hdRrw14_draw");
  h5= (TH1D *)hdRrw15->Clone("hdRrw15_draw");
  h6= (TH1D *)hdRrw16->Clone("hdRrw16_draw");
  h7= (TH1D *)hdRrw17->Clone("hdRrw17_draw");
  h8= (TH1D *)hdRrw62->Clone("hdRrw62_draw");
  h9= (TH1D *)hdRrw63->Clone("hdRrw63_draw");  
  h1->GetXaxis()->SetTitle("dR");
  h1->SetMarkerStyle(20);
  h1->SetMarkerColor(kRed);
  h1->SetLineColor(kRed);
  // h2->Scale(1./N2);
  h2->GetXaxis()->SetTitle("dR");
  h2->SetMarkerStyle(21);
  h2->SetMarkerColor(kGreen);
  h2->SetLineColor(kGreen);
  // h3->Scale(1./N3);
  h3->GetXaxis()->SetTitle("dR");
  h3->SetMarkerStyle(25);
  h3->SetMarkerColor(kOrange);
  h3->SetLineColor(kOrange);
  // h4->Scale(1./N4);
  h4->GetXaxis()->SetTitle("dR");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kOrange);
  h4->SetLineColor(kOrange);
  // h5->Scale(1./N5);
  h5->GetXaxis()->SetTitle("dR");
  h5->SetMarkerStyle(4);
  h5->SetMarkerColor(kSpring);
  h5->SetLineColor(kSpring);
  // h6->Scale(1./N6);
  h6->GetXaxis()->SetTitle("dR");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kBlue);
  h6->SetLineColor(kBlue);
  // h7->Scale(1./N7);
  h7->GetXaxis()->SetTitle("dR");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kBlack);
  h7->SetLineColor(kBlack);

  h8->GetXaxis()->SetTitle("dR");
  h8->SetMarkerStyle(20);
  h8->SetMarkerColor(kRed);
  h8->SetLineColor(kRed);

  h9->GetXaxis()->SetTitle("dR");
  h9->SetMarkerStyle(21);
  h9->SetMarkerColor(kBlue);
  h9->SetLineColor(kBlue);
  

  h2->SetTitle("dR, Normalized ratios");
  h2->GetYaxis()->SetRangeUser(0.,2.0);
  h2->Draw();
  h1->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  TLegend *LdRrw = new TLegend(0.3,0.6,0.6,0.9);
  LdRrw->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LdRrw->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LdRrw->AddEntry(h4,"AlpgenPythia/PowhegPythia8","PE");
  LdRrw->AddEntry(h5,"AlpgenJimmy/PowhegPythia8","PE");
  LdRrw->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LdRrw->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LdRrw->Draw();
  filestr=dirstr+"hdRrw.eps";
  if (Smakepics==1) cdRrw->Print(filestr.c_str());

  // All sim without alpgen vs PowhegPythia_Zcorr
  cdRrw->Clear();
  cdRrw->SetGridy();
  cdRrw->cd();
  h2->GetYaxis()->SetRangeUser(0.6,2.0);
  h2->SetTitle("dR, Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  LdRrw->Clear();
  LdRrw->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  LdRrw->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  LdRrw->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  LdRrw->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LdRrw->Draw();
  filestr=dirstr+"hdRrw236v1.eps";
  if (Smakepics==1) cdRrw->Print(filestr.c_str());

  // All PowhegPythia_OneGamma vs PowhegPythia_Zcorr
  cdRrw->Clear();
  cdRrw->SetGridy();
  cdRrw->cd();
  h7->GetYaxis()->SetRangeUser(0.8,1.2);
  h7->SetTitle("dR, Normalized ratios");
  f7 = new TF1("fdRw7","[0]", 0, 1);
  f7->SetParName(0,"C");
  h7->Fit(f7,"","",0.4,3.2);
  h7->Draw("ep");
  TLegend *LdRrw7v1 = new TLegend(0.2,0.7,0.5,0.85);  
  LdRrw7v1->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  LdRrw7v1->Draw();
  filestr=dirstr+"hdRrw7v1.eps";
  if (Smakepics==1) cdRrw->Print(filestr.c_str());
  
  //Sherpa only sims
  cdRrw->Clear();
  cdRrw->SetGridy();
  cdRrw->cd();
  h8->GetYaxis()->SetRangeUser(0.6,2.0);
  h8->SetTitle("dR, Normalized ratios");
  h8->Draw();
  h9->Draw("same");
  LdRrw->Clear();
  LdRrw->AddEntry(h8,"Sherpa 1.4/Sherpa 2.24","PE");
  LdRrw->AddEntry(h9,"Sherpa 2.21/Sherpa 2.24","PE");
  LdRrw->Draw();
  filestr=dirstr+"hdRrw23v6.eps";
  if (Smakepics==1) cdRrw->Print(filestr.c_str());
  
  cptgammar=(TCanvas *)gROOT->FindObject("cptgammar");
  if (!cptgammar)  cptgammar=new TCanvas("cptgammar","cptgammar",1000.,600.);
  cptgammar->Clear();
  cptgammar->SetGridy();
  cptgammar->cd();
  h1= (TH1D *)hptgammar12->Clone("hptgammar12_draw");
  h2= (TH1D *)hptgammar13->Clone("hptgammar13_draw");
  h3= (TH1D *)hptgammar32->Clone("hptgammar32_draw");
  h4= (TH1D *)hptgammar14->Clone("hptgammar14_draw");
  h5= (TH1D *)hptgammar15->Clone("hptgammar15_draw");
  h6= (TH1D *)hptgammar16->Clone("hptgammar16_draw");
  h7= (TH1D *)hptgammar17->Clone("hptgammar17_draw");
  h8= (TH1D *)hptgammar62->Clone("hptgammar62_draw");
  h9= (TH1D *)hptgammar63->Clone("hptgammar63_draw");  
  h1->GetXaxis()->SetTitle("ptgamma");
  h1->SetMarkerStyle(20);
  h1->SetMarkerColor(kRed);
  h1->SetLineColor(kRed);
  // h2->Scale(1./N2);
  h2->GetXaxis()->SetTitle("ptgamma");
  h2->SetMarkerStyle(21);
  h2->SetMarkerColor(kGreen);
  h2->SetLineColor(kGreen);
  // h3->Scale(1./N3);
  h3->GetXaxis()->SetTitle("ptgamma");
  h3->SetMarkerStyle(25);
  h3->SetMarkerColor(kOrange);
  h3->SetLineColor(kOrange);
  // h4->Scale(1./N4);
  h4->GetXaxis()->SetTitle("ptgamma");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kOrange);
  h4->SetLineColor(kOrange);
  // h5->Scale(1./N5);
  h5->GetXaxis()->SetTitle("ptgamma");
  h5->SetMarkerStyle(4);
  h5->SetMarkerColor(kSpring);
  h5->SetLineColor(kSpring);
  // h6->Scale(1./N6);
  h6->GetXaxis()->SetTitle("ptgamma");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kBlue);
  h6->SetLineColor(kBlue);
  // h7->Scale(1./N7);
  h7->GetXaxis()->SetTitle("ptgamma");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kBlack);
  h7->SetLineColor(kBlack);

  h8->GetXaxis()->SetTitle("ptgamma");
  h8->SetMarkerStyle(20);
  h8->SetMarkerColor(kRed);
  h8->SetLineColor(kRed);

  h9->GetXaxis()->SetTitle("ptgamma");
  h9->SetMarkerStyle(21);
  h9->SetMarkerColor(kBlue);
  h9->SetLineColor(kBlue);
  
  h2->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h2->SetTitle("p_{T}(#gamma), Normalized ratios");
  h2->GetYaxis()->SetRangeUser(0.6,1.8);
  h2->Draw();
  h1->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  TLegend *Lptgammar = new TLegend(0.3,0.6,0.6,0.9);
  Lptgammar->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  Lptgammar->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  Lptgammar->AddEntry(h4,"AlpgenPythia/PowhegPythia8","PE");
  Lptgammar->AddEntry(h5,"AlpgenJimmy/PowhegPythia8","PE");
  Lptgammar->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  Lptgammar->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  Lptgammar->Draw();
  filestr=dirstr+"hptgammar.eps";
  if (Smakepics==1) cptgammar->Print(filestr.c_str());

  // All sim without alpgen vs PowhegPythia_Zcorr
  cptgammar->Clear();
  cptgammar->SetGridy();
  cptgammar->cd();
  h2->GetYaxis()->SetRangeUser(0.6,1.8);
  h2->SetTitle("p_{T}(#gamma), Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  Lptgammar->Clear();
  Lptgammar->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  Lptgammar->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  Lptgammar->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  Lptgammar->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  Lptgammar->Draw();
  filestr=dirstr+"hptgammar236v1.eps";
  if (Smakepics==1) cptgammar->Print(filestr.c_str());

  // All PowhegPythia_OneGamma vs PowhegPythia_Zcorr
  cptgammar->Clear();
  cptgammar->SetGridy();
  cptgammar->cd();
  h7->GetYaxis()->SetRangeUser(0.8,1.2);
  h7->SetTitle("p_{T}(#gamma), Normalized ratios");
  f7 = new TF1("fptgamma7","[0]", 0, 1);
  f7->SetParName(0,"C");
  h7->Fit(f7,"","",12.,46.);
  h7->Draw("ep");
  TLegend *Lptgammar7v1 = new TLegend(0.2,0.7,0.5,0.85);  
  Lptgammar7v1->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  Lptgammar7v1->Draw();
  filestr=dirstr+"hptgammar7v1.eps";
  if (Smakepics==1) cptgammar->Print(filestr.c_str());
  
  //Sherpa only sims
  cptgammar->Clear();
  cptgammar->SetGridy();
  cptgammar->cd();
  h8->GetYaxis()->SetRangeUser(0.6,1.8);
  h8->SetTitle("p_{T}(#gamma), Normalized ratios");
  h8->Draw();
  h9->Draw("same");
  Lptgammar->Clear();
  Lptgammar->AddEntry(h8,"Sherpa 1.4/Sherpa 2.24","PE");
  Lptgammar->AddEntry(h9,"Sherpa 2.21/Sherpa 2.24","PE");
  Lptgammar->Draw();
  filestr=dirstr+"hptgammar23v6.eps";
  if (Smakepics==1) cptgammar->Print(filestr.c_str());

  cptgammarw=(TCanvas *)gROOT->FindObject("cptgammarw");
  if (!cptgammarw)  cptgammarw=new TCanvas("cptgammarw","cptgammarw",1000.,600.);
  cptgammarw->Clear();
  cptgammarw->SetGridy();
  cptgammarw->cd();
  h1= (TH1D *)hptgammarw12->Clone("hptgammarw12_draw");
  h2= (TH1D *)hptgammarw13->Clone("hptgammarw13_draw");
  h3= (TH1D *)hptgammarw32->Clone("hptgammarw32_draw");
  h4= (TH1D *)hptgammarw14->Clone("hptgammarw14_draw");
  h5= (TH1D *)hptgammarw15->Clone("hptgammarw15_draw");
  h6= (TH1D *)hptgammarw16->Clone("hptgammarw16_draw");
  h7= (TH1D *)hptgammarw17->Clone("hptgammarw17_draw");
  h8= (TH1D *)hptgammarw62->Clone("hptgammarw62_draw");
  h9= (TH1D *)hptgammarw63->Clone("hptgammarw63_draw");  
  h1->GetXaxis()->SetTitle("ptgamma");
  h1->SetMarkerStyle(20);
  h1->SetMarkerColor(kRed);
  h1->SetLineColor(kRed);
  // h2->Scale(1./N2);
  h2->GetXaxis()->SetTitle("ptgamma");
  h2->SetMarkerStyle(21);
  h2->SetMarkerColor(kGreen);
  h2->SetLineColor(kGreen);
  // h3->Scale(1./N3);
  h3->GetXaxis()->SetTitle("ptgamma");
  h3->SetMarkerStyle(25);
  h3->SetMarkerColor(kOrange);
  h3->SetLineColor(kOrange);
  // h4->Scale(1./N4);
  h4->GetXaxis()->SetTitle("ptgamma");
  h4->SetMarkerStyle(23);
  h4->SetMarkerColor(kOrange);
  h4->SetLineColor(kOrange);
  // h5->Scale(1./N5);
  h5->GetXaxis()->SetTitle("ptgamma");
  h5->SetMarkerStyle(4);
  h5->SetMarkerColor(kSpring);
  h5->SetLineColor(kSpring);
  // h6->Scale(1./N6);
  h6->GetXaxis()->SetTitle("ptgamma");
  h6->SetMarkerStyle(34);
  h6->SetMarkerColor(kBlue);
  h6->SetLineColor(kBlue);
  // h7->Scale(1./N7);
  h7->GetXaxis()->SetTitle("ptgamma");
  h7->SetMarkerStyle(4);
  h7->SetMarkerColor(kBlack);
  h7->SetLineColor(kBlack);

  h8->GetXaxis()->SetTitle("ptgamma");
  h8->SetMarkerStyle(20);
  h8->SetMarkerColor(kRed);
  h8->SetLineColor(kRed);

  h9->GetXaxis()->SetTitle("ptgamma");
  h9->SetMarkerStyle(21);
  h9->SetMarkerColor(kBlue);
  h9->SetLineColor(kBlue);
  
  h2->GetXaxis()->SetTitle("p_{T}(#gamma), GeV");
  h2->SetTitle("p_{T}(#gamma), Normalized ratios");
  h2->GetYaxis()->SetRangeUser(0.6,1.8);
  h2->Draw();
  h1->Draw("same");
  h4->Draw("same");
  h5->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  TLegend *Lptgammarw = new TLegend(0.3,0.6,0.6,0.9);
  Lptgammarw->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  Lptgammarw->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  Lptgammarw->AddEntry(h4,"AlpgenPythia/PowhegPythia8","PE");
  Lptgammarw->AddEntry(h5,"AlpgenJimmy/PowhegPythia8","PE");
  Lptgammarw->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  Lptgammarw->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  Lptgammarw->Draw();
  filestr=dirstr+"hptgammarw.eps";
  if (Smakepics==1) cptgammarw->Print(filestr.c_str());

  // All sim without alpgen vs PowhegPythia_Zcorr
  cptgammarw->Clear();
  cptgammarw->SetGridy();
  cptgammarw->cd();
  h2->GetYaxis()->SetRangeUser(0.6,1.8);
  h2->SetTitle("p_{T}(#gamma), Normalized ratios");
  h2->Draw();
  h1->Draw("same");
  h6->Draw("same");
  //  h7->Draw("same");
  Lptgammarw->Clear();
  Lptgammarw->AddEntry(h1,"Sherpa 1.4/PowhegPythia8","PE");
  Lptgammarw->AddEntry(h2,"Sherpa 2.21/PowhegPythia8","PE");
  Lptgammarw->AddEntry(h6,"Sherpa 2.24/PowhegPythia8","PE");
  //  Lptgammarw->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  Lptgammarw->Draw();
  filestr=dirstr+"hptgammarw236v1.eps";
  if (Smakepics==1) cptgammarw->Print(filestr.c_str());

  // All PowhegPythia_OneGamma vs PowhegPythia_Zcorr
  cptgammarw->Clear();
  cptgammarw->SetGridy();
  cptgammarw->cd();
  h7->GetYaxis()->SetRangeUser(0.8,1.2);
  h7->SetTitle("p_{T}(#gamma), Normalized ratios");
  f7 = new TF1("fptgammaw7","[0]", 0, 1);
  f7->SetParName(0,"C");
  h7->Fit(f7,"","",12.,46.);
  h7->Draw("ep");
  TLegend *Lptgammarw7v1 = new TLegend(0.2,0.7,0.5,0.85);  
  Lptgammarw->AddEntry(h7,"PowhegPythia8 (One #gamma)/PowhegPythia8","PE");
  Lptgammarw->Draw();
  filestr=dirstr+"hptgammarw7v1.eps";
  if (Smakepics==1) cptgammarw->Print(filestr.c_str());
  
  //Sherpa only sims
  cptgammarw->Clear();
  cptgammarw->SetGridy();
  cptgammarw->cd();
  h8->GetYaxis()->SetRangeUser(0.6,1.8);
  h8->SetTitle("p_{T}(#gamma), Normalized ratios");
  h8->Draw();
  h9->Draw("same");
  Lptgammarw->Clear();
  Lptgammarw->AddEntry(h8,"Sherpa 1.4/Sherpa 2.24","PE");
  Lptgammarw->AddEntry(h9,"Sherpa 2.21/Sherpa 2.24","PE");
  Lptgammarw->Draw();
  filestr=dirstr+"hptgammarw23v6.eps";
  if (Smakepics==1) cptgammarw->Print(filestr.c_str());
  
  if (Smakepics==1) cptgammarw->Print(filestr.c_str());
      

  filestr=dirstr+"rivet-hist.root";
  if (Smakehist==1) {
    TFile *fileout=new TFile(filestr.c_str(),"RECREATE");
    fileout->cd();     
    hdR1->Write();
    hdR2->Write();
    hdR3->Write();
    hdR4->Write();
    hdR5->Write();
    hdR6->Write();
    hdR7->Write();
    hdRw1->Write();
    hdRw2->Write();
    hdRw3->Write();
    hdRw4->Write();
    hdRw5->Write();
    hdRw6->Write();
    hdRw7->Write();
    hQp1->Write();
    hQp2->Write();
    hQp3->Write();
    hQp4->Write();
    hQp5->Write();
    hQp6->Write();
    hQp7->Write();
    hQpw1->Write();
    hQpw2->Write();
    hQpw3->Write();
    hQpw4->Write();
    hQpw5->Write();
    hQpw6->Write();
    hQpw7->Write();

    hptZ0->Write();
    hptZ1->Write();
    hptZ2->Write();
    hptZ3->Write();
    hptZ4->Write();
    hptZ5->Write();
    hptZ6->Write();
    hptZ7->Write();

    hptZw1->Write();
    hptZw2->Write();
    hptZw3->Write();
    hptZw4->Write();
    hptZw5->Write();
    hptZw6->Write();
    hptZw7->Write();

    hptgamma1->Write();
    hptgamma2->Write();
    hptgamma3->Write();
    hptgamma4->Write();
    hptgamma5->Write();
    hptgamma6->Write();
    hptgamma7->Write();

    hptgammaw1->Write();
    hptgammaw2->Write();
    hptgammaw3->Write();
    hptgammaw4->Write();
    hptgammaw5->Write();
    hptgammaw6->Write();
    hptgammaw7->Write();
    
    fileout->Close();
  }
  
  
  return 1;
}

// from addPtZ_weight.C
// 
int calcPtZW(TH1D *ptZhist,Double_t ptZ, Double_t &W){
  if (ptZ<=0) {W=0.;return 0;} 
  Double_t binSize=0;
  Double_t binCo=0;
  Double_t N=0;
  for( int i=0; i<nbinsPtZ;i++){
    // binSize=p9030_d27x1y4_xerrminus[i]+p9030_d27x1y4_xerrplus[i];
    binCo=ptZhist->GetBinContent(i+1);
    N = N +binCo;
    //   std::cout<<i<<" "<<po[i]<<" "<<binSize<<" "<<binCo<<" "<<N<<std::endl;
  };
  // Double_t binEr=0;
  // Double_t Sum=0;
  for( int i=0; i<nbinsPtZ;i++){
    Double_t upper=p9030_d27x1y4_xval[i]+p9030_d27x1y4_xerrplus[i];
    if (ptZ<=upper) {
        binSize=p9030_d27x1y4_xerrminus[i]+p9030_d27x1y4_xerrplus[i];
        binCo=ptZhist->GetBinContent(i+1);
        // binEr=TMath::Sqrt(binCo)/(binSize*N);
        binCo=binCo/(binSize*N);        
        // Sum = Sum+binCo;
	// std::cout<<i<<" "<<binCo<<" "<<binEr<<" "<<p9030_d27x1y4_yval[i]<<std::endl;
	//	if(binCo>0) W[i]=(p9030_d27x1y4.GetY())[i+1]/binCo;
	if(binCo>0) W=p9030_d27x1y4_yval[i]/binCo;
      	else W=1.;
	return 1;
    };
  }
  W=0;
  return 0;
}


// find closest bin in histogram 
int findbin(TH1D *hist,Double_t X, int &bin) {
  Int_t n=hist->GetNbinsX();
  for (Int_t i=1;i<=hist->GetNbinsX();i++) {
    Double_t w = hist->GetXaxis()->GetBinWidth(i);
    Double_t xc=hist->GetXaxis()->GetBinCenter(i);
    Double_t xl = xc-w/2.;
    Double_t xu = xc+w/2.;
    if (X>xl&&X<=xu) {bin=i;return 1;}
  }
  return 0;
}

int histreweight() {
  TTree *tlocal1,*tlocal2,*tlocal6,*tlocal7;
  TChain *tlocal3,*tlocal4,*tlocal5;
  switch (DressSwitch) {
  case DRESSED:
    tlocal1=t1;
    tlocal2=t2;
    tlocal3=t3;
    tlocal4=t4;
    tlocal5=t5;
    tlocal6=t6;
    tlocal7=t7;
    break;
  case UNDRESSED:
    tlocal1=tundr1;
    tlocal2=tundr2;
    tlocal3=tundr3;
    tlocal4=tundr4;
    tlocal5=tundr5;
    tlocal6=tundr6;
    tlocal7=tundr7;
    break;
  }

  // hQpw1=(TH1D *)hQp1->Clone("hQpw1");
  // hdRw1=(TH1D *)hdR1->Clone("hdRw1");
  // hptZw1=(TH1D *)hptZ1->Clone("hptZw1");
  // hptgammaw1=(TH1D *)hptgamma1->Clone("hptgammaw1");
  Long64_t nentries = tlocal1->GetEntries();
  switch (PTZrewightSwitch) {
  case ExternalFunc:
    for (Long64_t i=0;i<nentries;i++) {
      tlocal1->GetEntry(i);
      // JetSwitch
      // 1 - no jet selection
      // 2 - njets==0
      // 3 - njets>0
      if (!((JetSwitch==1)||
	    (JetSwitch==2&&njets==0)||
	    (JetSwitch==3&&njets>0))) continue;
      // NPhotonSwitch
      // 0 - no photoncounter selection
      // n>0 - number of photons photoncounter==n
      if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
      Double_t wratio=-1;
      if (calcPtZW(hptZ1,ptZ,wratio)) {
	hQpw1->Fill(Qp,weight*wratio*Wfile);
	hdRw1->Fill(dR,weight*wratio*Wfile);
	hptZw1->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw1->Fill(ptgamma,weight*wratio*Wfile);
      }
    }
    break;
  case Sim2PowhegPythia8: 
    hQpw1=(TH1D *)hQp1->Clone("hQpw1");
    hdRw1=(TH1D *)hdR1->Clone("hdRw1");
    hptZw1=(TH1D *)hptZ1->Clone("hptZw1");
    hptgammaw1=(TH1D *)hptgamma1->Clone("hptgammaw1");
    break;
  case Sim2PowhegPythia8RecoReweighted: 
    for (Long64_t i=0;i<nentries;i++) {
      tlocal1->GetEntry(i);
      // JetSwitch
      // 1 - no jet selection
      // 2 - njets==0
      // 3 - njets>0
      if (!((JetSwitch==1)||
	    (JetSwitch==2&&njets==0)||
	    (JetSwitch==3&&njets>0))) continue;
      // NPhotonSwitch
      // 0 - no photoncounter selection
      // n>0 - number of photons photoncounter==n
      if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
      int ptbin=-1;
      if (findbin(hptZr01,ptZ,ptbin)) {
	Double_t wratio=hptZr01->GetBinContent(ptbin);
	Double_t wratio_er=hptZr01->GetBinError(ptbin);
	if (wratio<1E-8) wratio=1.; //just for to be sure for small statistics
	hQpw1->Fill(Qp,weight*Wfile/wratio);
	hdRw1->Fill(dR,weight*Wfile/wratio);
	hptZw1->Fill(ptZ,weight*Wfile/wratio);
	hptgammaw1->Fill(ptgamma,weight*Wfile/wratio);
      }
    }
    break;
  case Sim2Sherpa224: 
    for (Long64_t i=0;i<nentries;i++) {
      tlocal1->GetEntry(i);
      // JetSwitch
      // 1 - no jet selection
      // 2 - njets==0
      // 3 - njets>0
      if (!((JetSwitch==1)||
	    (JetSwitch==2&&njets==0)||
	    (JetSwitch==3&&njets>0))) continue;
      // NPhotonSwitch
      // 0 - no photoncounter selection
      // n>0 - number of photons photoncounter==n
      if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
      int ptbin=-1;
      if (findbin(hptZr61,ptZ,ptbin)) {
	Double_t wratio=hptZr61->GetBinContent(ptbin);
	Double_t wratio_er=hptZr61->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw1->Fill(Qp,weight*wratio*Wfile);
	hdRw1->Fill(dR,weight*wratio*Wfile);
	hptZw1->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw1->Fill(ptgamma,weight*wratio*Wfile);
      }
    }
    break;
  default:
    cout << "Unknown PTZrewightSwitch="<< PTZrewightSwitch << endl;
    exit(1);
  }      
  
  
  nentries = tlocal2->GetEntries();
  for (Long64_t i=0;i<nentries;i++) {
    tlocal2->GetEntry(i);
    // JetSwitch
    // 1 - no jet selection
    // 2 - njets==0
    // 3 - njets>0
    if (!((JetSwitch==1)||
	  (JetSwitch==2&&njets==0)||
	  (JetSwitch==3&&njets>0))) continue;
    // NPhotonSwitch
    // 0 - no photoncounter selection
    // n>0 - number of photons photoncounter==n
    if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
    Double_t wratio;
    switch (PTZrewightSwitch) {
    case ExternalFunc:
      if (calcPtZW(hptZ2,ptZ,wratio)) {
	hQpw2->Fill(Qp,weight*wratio*Wfile);
	hdRw2->Fill(dR,weight*wratio*Wfile);
	hptZw2->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw2->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8: 
      int ptbin=-1;
      if (findbin(hptZr12,ptZ,ptbin)) {
	Double_t wratio=hptZr12->GetBinContent(ptbin);
	Double_t wratio_er=hptZr12->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw2->Fill(Qp,weight*wratio*Wfile);
	hdRw2->Fill(dR,weight*wratio*Wfile);
	hptZw2->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw2->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8RecoReweighted: 
      int ptbin=-1;
      if (findbin(hptZr02,ptZ,ptbin)) {
	Double_t wratio=hptZr02->GetBinContent(ptbin);
	Double_t wratio_er=hptZr02->GetBinError(ptbin);
	if (wratio<1E-8) wratio=1.; //just for to be sure for small statistics
	hQpw2->Fill(Qp,weight*Wfile/wratio);
	hdRw2->Fill(dR,weight*Wfile/wratio);
	hptZw2->Fill(ptZ,weight*Wfile/wratio);
	hptgammaw2->Fill(ptgamma,weight*Wfile/wratio);
      }
      break;      
    case Sim2Sherpa224: 
      int ptbin=-1;
      if (findbin(hptZr62,ptZ,ptbin)) {
	Double_t wratio=hptZr62->GetBinContent(ptbin);
	Double_t wratio_er=hptZr62->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw2->Fill(Qp,weight*wratio*Wfile);
	hdRw2->Fill(dR,weight*wratio*Wfile);
	hptZw2->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw2->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    default:
      cout << "Unknown PTZrewightSwitch="<< PTZrewightSwitch << endl;
      exit(1);
    }
  }
  nentries = tlocal3->GetEntries();
  for (Long64_t i=0;i<nentries;i++) {
    tlocal3->GetEntry(i);
    // JetSwitch
    // 1 - no jet selection
    // 2 - njets==0
    // 3 - njets>0
    if (!((JetSwitch==1)||
	  (JetSwitch==2&&njets==0)||
	  (JetSwitch==3&&njets>0))) continue;
    // NPhotonSwitch
    // 0 - no photoncounter selection
    // n>0 - number of photons photoncounter==n
    if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
    Double_t wratio;
    switch (PTZrewightSwitch) {
    case ExternalFunc:
      if (calcPtZW(hptZ3,ptZ,wratio)) {
	hQpw3->Fill(Qp,weight*wratio*Wfile);
	hdRw3->Fill(dR,weight*wratio*Wfile);
	hptZw3->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw3->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8: 
      int ptbin=-1;
      if (findbin(hptZr13,ptZ,ptbin)) {
	Double_t wratio=hptZr13->GetBinContent(ptbin);
	Double_t wratio_er=hptZr13->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw3->Fill(Qp,weight*wratio*Wfile);
	hdRw3->Fill(dR,weight*wratio*Wfile);
	hptZw3->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw3->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8RecoReweighted: 
      int ptbin=-1;
      if (findbin(hptZr03,ptZ,ptbin)) {
	Double_t wratio=hptZr03->GetBinContent(ptbin);
	Double_t wratio_er=hptZr03->GetBinError(ptbin);
	if (wratio<1E-8) wratio=1.; //just for to be sure for small statistics
	hQpw3->Fill(Qp,weight*Wfile/wratio);
	hdRw3->Fill(dR,weight*Wfile/wratio);
	hptZw3->Fill(ptZ,weight*Wfile/wratio);
	hptgammaw3->Fill(ptgamma,weight*Wfile/wratio);
      }
      break;
    case Sim2Sherpa224: 
      int ptbin=-1;
      if (findbin(hptZr63,ptZ,ptbin)) {
	Double_t wratio=hptZr63->GetBinContent(ptbin);
	Double_t wratio_er=hptZr63->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw3->Fill(Qp,weight*wratio*Wfile);
	hdRw3->Fill(dR,weight*wratio*Wfile);
	hptZw3->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw3->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    default:
      cout << "Unknown PTZrewightSwitch="<< PTZrewightSwitch << endl;
      exit(1);
    }
  }

  nentries = tlocal4->GetEntries();
  for (Long64_t i=0;i<nentries;i++) {
    tlocal4->GetEntry(i);
    // JetSwitch
    // 1 - no jet selection
    // 2 - njets==0
    // 3 - njets>0
    if (!((JetSwitch==1)||
	  (JetSwitch==2&&njets==0)||
	  (JetSwitch==3&&njets>0))) continue;
    // NPhotonSwitch
    // 0 - no photoncounter selection
    // n>0 - number of photons photoncounter==n
    if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
    Double_t wratio;
    switch (PTZrewightSwitch) {
    case ExternalFunc:
      if (calcPtZW(hptZ4,ptZ,wratio)) {
	hQpw4->Fill(Qp,weight*wratio*Wfile);
	hdRw4->Fill(dR,weight*wratio*Wfile);
	hptZw4->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw4->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8: 
      int ptbin=-1;
      if (findbin(hptZr14,ptZ,ptbin)) {
	Double_t wratio=hptZr14->GetBinContent(ptbin);
	Double_t wratio_er=hptZr14->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw4->Fill(Qp,weight*wratio*Wfile);
	hdRw4->Fill(dR,weight*wratio*Wfile);
	hptZw4->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw4->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8RecoReweighted: 
      int ptbin=-1;
      if (findbin(hptZr04,ptZ,ptbin)) {
	Double_t wratio=hptZr04->GetBinContent(ptbin);
	Double_t wratio_er=hptZr04->GetBinError(ptbin);
	if (wratio<1E-8) wratio=1.; //just for to be sure for small statistics
	hQpw4->Fill(Qp,weight*Wfile/wratio);
	hdRw4->Fill(dR,weight*Wfile/wratio);
	hptZw4->Fill(ptZ,weight*Wfile/wratio);
	hptgammaw4->Fill(ptgamma,weight*Wfile/wratio);
      }
      break;
    case Sim2Sherpa224: 
      int ptbin=-1;
      if (findbin(hptZr64,ptZ,ptbin)) {
	Double_t wratio=hptZr64->GetBinContent(ptbin);
	Double_t wratio_er=hptZr64->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw4->Fill(Qp,weight*wratio*Wfile);
	hdRw4->Fill(dR,weight*wratio*Wfile);
	hptZw4->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw4->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    default:
      cout << "Unknown PTZrewightSwitch="<< PTZrewightSwitch << endl;
      exit(1);
    }
  }

  nentries = tlocal5->GetEntries();
  for (Long64_t i=0;i<nentries;i++) {
    tlocal5->GetEntry(i);
    // JetSwitch
    // 1 - no jet selection
    // 2 - njets==0
    // 3 - njets>0
    if (!((JetSwitch==1)||
	  (JetSwitch==2&&njets==0)||
	  (JetSwitch==3&&njets>0))) continue;
    // NPhotonSwitch
    // 0 - no photoncounter selection
    // n>0 - number of photons photoncounter==n
    if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
    Double_t wratio;
    switch (PTZrewightSwitch) {
    case ExternalFunc:
      if (calcPtZW(hptZ5,ptZ,wratio)) {
	hQpw5->Fill(Qp,weight*wratio*Wfile);
	hdRw5->Fill(dR,weight*wratio*Wfile);
	hptZw5->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw5->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8: 
      int ptbin=-1;
      if (findbin(hptZr15,ptZ,ptbin)) {
	Double_t wratio=hptZr15->GetBinContent(ptbin);
	Double_t wratio_er=hptZr15->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw5->Fill(Qp,weight*wratio*Wfile);
	hdRw5->Fill(dR,weight*wratio*Wfile);
	hptZw5->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw5->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8RecoReweighted: 
      int ptbin=-1;
      if (findbin(hptZr05,ptZ,ptbin)) {
	Double_t wratio=hptZr05->GetBinContent(ptbin);
	Double_t wratio_er=hptZr05->GetBinError(ptbin);
	if (wratio<1E-8) wratio=1.; //just for to be sure for small statistics
	hQpw5->Fill(Qp,weight*Wfile/wratio);
	hdRw5->Fill(dR,weight*Wfile/wratio);
	hptZw5->Fill(ptZ,weight*Wfile/wratio);
	hptgammaw5->Fill(ptgamma,weight*Wfile/wratio);
      }
      break;
    case Sim2Sherpa224: 
      int ptbin=-1;
      if (findbin(hptZr65,ptZ,ptbin)) {
	Double_t wratio=hptZr65->GetBinContent(ptbin);
	Double_t wratio_er=hptZr65->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw5->Fill(Qp,weight*wratio*Wfile);
	hdRw5->Fill(dR,weight*wratio*Wfile);
	hptZw5->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw5->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    default:
      cout << "Unknown PTZrewightSwitch="<< PTZrewightSwitch << endl;
      exit(1);
    }
  }

  nentries = tlocal6->GetEntries();
  switch (PTZrewightSwitch) {
  case ExternalFunc:
    for (Long64_t i=0;i<nentries;i++) {
      tlocal6->GetEntry(i);
      // JetSwitch
      // 1 - no jet selection
      // 2 - njets==0
      // 3 - njets>0
      if (!((JetSwitch==1)||
	    (JetSwitch==2&&njets==0)||
	    (JetSwitch==3&&njets>0))) continue;
      // NPhotonSwitch
      // 0 - no photoncounter selection
      // n>0 - number of photons photoncounter==n
      if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
      Double_t wratio;
      if (calcPtZW(hptZ6,ptZ,wratio)) {
	hQpw6->Fill(Qp,weight*wratio*Wfile);
	hdRw6->Fill(dR,weight*wratio*Wfile);
	hptZw6->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw6->Fill(ptgamma,weight*wratio*Wfile);
      }
    }
    break;
  case Sim2PowhegPythia8: 
    for (Long64_t i=0;i<nentries;i++) {
      tlocal6->GetEntry(i);
      // JetSwitch
      // 1 - no jet selection
      // 2 - njets==0
      // 3 - njets>0
      if (!((JetSwitch==1)||
	    (JetSwitch==2&&njets==0)||
	    (JetSwitch==3&&njets>0))) continue;
      // NPhotonSwitch
      // 0 - no photoncounter selection
      // n>0 - number of photons photoncounter==n
      if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
      int ptbin=-1;
      if (findbin(hptZr16,ptZ,ptbin)) {
	Double_t wratio=hptZr16->GetBinContent(ptbin);
	Double_t wratio_er=hptZr16->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw6->Fill(Qp,weight*wratio*Wfile);
	hdRw6->Fill(dR,weight*wratio*Wfile);
	hptZw6->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw6->Fill(ptgamma,weight*wratio*Wfile);
      }
    }
    break;
  case Sim2PowhegPythia8RecoReweighted: 
    for (Long64_t i=0;i<nentries;i++) {
      tlocal6->GetEntry(i);
      // JetSwitch
      // 1 - no jet selection
      // 2 - njets==0
      // 3 - njets>0
      if (!((JetSwitch==1)||
	    (JetSwitch==2&&njets==0)||
	    (JetSwitch==3&&njets>0))) continue;
      // NPhotonSwitch
      // 0 - no photoncounter selection
      // n>0 - number of photons photoncounter==n
      if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
      int ptbin=-1;
      if (findbin(hptZr06,ptZ,ptbin)) {
	Double_t wratio=hptZr06->GetBinContent(ptbin);
	Double_t wratio_er=hptZr06->GetBinError(ptbin);
	if (wratio<1E-8) wratio=1.; //just for to be sure for small statistics
	hQpw6->Fill(Qp,weight*Wfile/wratio);
	hdRw6->Fill(dR,weight*Wfile/wratio);
	hptZw6->Fill(ptZ,weight*Wfile/wratio);
	hptgammaw6->Fill(ptgamma,weight*Wfile/wratio);
      }
    }
    break;
  case Sim2Sherpa224:
    hQpw6=(TH1D *)hQp6->Clone("hQpw6");
    hdRw6=(TH1D *)hdR6->Clone("hdRw6");
    hptZw6=(TH1D *)hptZ6->Clone("hptZw6");
    hptgammaw6=(TH1D *)hptgamma6->Clone("hptgammaw6");
    break;
  default:
    cout << "Unknown PTZrewightSwitch="<< PTZrewightSwitch << endl;
    exit(1);
  }      

  nentries = tlocal7->GetEntries();
  for (Long64_t i=0;i<nentries;i++) {
    tlocal7->GetEntry(i);
    // JetSwitch
    // 1 - no jet selection
    // 2 - njets==0
    // 3 - njets>0
    if (!((JetSwitch==1)||
	  (JetSwitch==2&&njets==0)||
	  (JetSwitch==3&&njets>0))) continue;
    // NPhotonSwitch
    // 0 - no photoncounter selection
    // n>0 - number of photons photoncounter==n
    if (NPhotonSwitch>0&&photoncounter!=NPhotonSwitch) continue;
    Double_t wratio;
    switch (PTZrewightSwitch) {
    case ExternalFunc:
      if (calcPtZW(hptZ7,ptZ,wratio)) {
	hQpw7->Fill(Qp,weight*wratio*Wfile);
	hdRw7->Fill(dR,weight*wratio*Wfile);
	hptZw7->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw7->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8: 
      int ptbin=-1;
      if (findbin(hptZr17,ptZ,ptbin)) {
	Double_t wratio=hptZr17->GetBinContent(ptbin);
	Double_t wratio_er=hptZr17->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw7->Fill(Qp,weight*wratio*Wfile);
	hdRw7->Fill(dR,weight*wratio*Wfile);
	hptZw7->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw7->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    case Sim2PowhegPythia8RecoReweighted: 
      int ptbin=-1;
      if (findbin(hptZr07,ptZ,ptbin)) {
	Double_t wratio=hptZr07->GetBinContent(ptbin);
	Double_t wratio_er=hptZr07->GetBinError(ptbin);
	if (wratio<1E-8) wratio=1.; //just for to be sure for small statistics
	hQpw7->Fill(Qp,weight*Wfile/wratio);
	hdRw7->Fill(dR,weight*Wfile/wratio);
	hptZw7->Fill(ptZ,weight*Wfile/wratio);
	hptgammaw7->Fill(ptgamma,weight*Wfile/wratio);
      }
      break;
    case Sim2Sherpa224: 
      int ptbin=-1;
      if (findbin(hptZr67,ptZ,ptbin)) {
	Double_t wratio=hptZr67->GetBinContent(ptbin);
	Double_t wratio_er=hptZr67->GetBinError(ptbin);
	if (wratio<1E-8||wratio_er>0.5*wratio) wratio=1.; //just for to be sure for small statistics
	hQpw7->Fill(Qp,weight*wratio*Wfile);
	hdRw7->Fill(dR,weight*wratio*Wfile);
	hptZw7->Fill(ptZ,weight*wratio*Wfile);
	hptgammaw7->Fill(ptgamma,weight*wratio*Wfile);
      }
      break;
    default:
      cout << "Unknown PTZrewightSwitch="<< PTZrewightSwitch << endl;
      exit(1);
    }      
  }
  
  hQpBvsR1=(TH1D *)hQp1->Clone("hQpBvsR1");
  hQpBvsR1->Divide(hQpw1);
  LowerErrorsBySqrt2(hQpBvsR1);
  hQpBvsR2=(TH1D *)hQp2->Clone("hQpBvsR2");
  hQpBvsR2->Divide(hQpw2);
  LowerErrorsBySqrt2(hQpBvsR2);
  hQpBvsR3=(TH1D *)hQp3->Clone("hQpBvsR3");
  hQpBvsR3->Divide(hQpw3);
  LowerErrorsBySqrt2(hQpBvsR3);  
  hQpBvsR4=(TH1D *)hQp4->Clone("hQpBvsR4");
  hQpBvsR4->Divide(hQpw4);
  LowerErrorsBySqrt2(hQpBvsR4);
  hQpBvsR5=(TH1D *)hQp5->Clone("hQpBvsR5");
  hQpBvsR5->Divide(hQpw5);
  LowerErrorsBySqrt2(hQpBvsR5);
  hQpBvsR6=(TH1D *)hQp6->Clone("hQpBvsR6");
  hQpBvsR6->Divide(hQpw6);
  LowerErrorsBySqrt2(hQpBvsR6);
  hQpBvsR7=(TH1D *)hQp7->Clone("hQpBvsR7");
  hQpBvsR7->Divide(hQpw7);
  LowerErrorsBySqrt2(hQpBvsR7);

  hdRBvsR1=(TH1D *)hdR1->Clone("hdRBvsR1");
  hdRBvsR1->Divide(hdRw1);
  LowerErrorsBySqrt2(hdRBvsR1);
  hdRBvsR2=(TH1D *)hdR2->Clone("hdRBvsR2");
  hdRBvsR2->Divide(hdRw2);
  LowerErrorsBySqrt2(hdRBvsR2);
  hdRBvsR3=(TH1D *)hdR3->Clone("hdRBvsR3");
  hdRBvsR3->Divide(hdRw3);
  LowerErrorsBySqrt2(hdRBvsR3);  
  hdRBvsR4=(TH1D *)hdR4->Clone("hdRBvsR4");
  hdRBvsR4->Divide(hdRw4);
  LowerErrorsBySqrt2(hdRBvsR4);  
  hdRBvsR5=(TH1D *)hdR5->Clone("hdRBvsR5");
  hdRBvsR5->Divide(hdRw5);
  LowerErrorsBySqrt2(hdRBvsR5);  
  hdRBvsR6=(TH1D *)hdR6->Clone("hdRBvsR6");
  hdRBvsR6->Divide(hdRw6);
  LowerErrorsBySqrt2(hdRBvsR6);  
  hdRBvsR7=(TH1D *)hdR7->Clone("hdRBvsR7");
  hdRBvsR7->Divide(hdRw7);
  LowerErrorsBySqrt2(hdRBvsR7);

  hptgammaBvsR1=(TH1D *)hptgamma1->Clone("hptgammaBvsR1");
  hptgammaBvsR1->Divide(hptgammaw1);
  LowerErrorsBySqrt2(hptgammaBvsR1);
  hptgammaBvsR2=(TH1D *)hptgamma2->Clone("hptgammaBvsR2");
  hptgammaBvsR2->Divide(hptgammaw2);
  LowerErrorsBySqrt2(hptgammaBvsR2);
  hptgammaBvsR3=(TH1D *)hptgamma3->Clone("hptgammaBvsR3");
  hptgammaBvsR3->Divide(hptgammaw3);
  LowerErrorsBySqrt2(hptgammaBvsR3);  
  hptgammaBvsR4=(TH1D *)hptgamma4->Clone("hptgammaBvsR4");
  hptgammaBvsR4->Divide(hptgammaw4);
  LowerErrorsBySqrt2(hptgammaBvsR4);
  hptgammaBvsR5=(TH1D *)hptgamma5->Clone("hptgammaBvsR5");
  hptgammaBvsR5->Divide(hptgammaw5);
  LowerErrorsBySqrt2(hptgammaBvsR5);
  hptgammaBvsR6=(TH1D *)hptgamma6->Clone("hptgammaBvsR6");
  hptgammaBvsR6->Divide(hptgammaw6);
  LowerErrorsBySqrt2(hptgammaBvsR6);
  hptgammaBvsR7=(TH1D *)hptgamma7->Clone("hptgammaBvsR7");
  hptgammaBvsR7->Divide(hptgammaw7);
  LowerErrorsBySqrt2(hptgammaBvsR7);

  hptZBvsR1=(TH1D *)hptZ1->Clone("hptZBvsR1");
  hptZBvsR1->Divide(hptZw1);
  LowerErrorsBySqrt2(hptZBvsR1);
  hptZBvsR2=(TH1D *)hptZ2->Clone("hptZBvsR2");
  hptZBvsR2->Divide(hptZw2);
  LowerErrorsBySqrt2(hptZBvsR2);
  hptZBvsR3=(TH1D *)hptZ3->Clone("hptZBvsR3");
  hptZBvsR3->Divide(hptZw3);
  LowerErrorsBySqrt2(hptZBvsR3);  
  hptZBvsR4=(TH1D *)hptZ4->Clone("hptZBvsR4");
  hptZBvsR4->Divide(hptZw4);
  LowerErrorsBySqrt2(hptZBvsR4);
  hptZBvsR5=(TH1D *)hptZ5->Clone("hptZBvsR5");
  hptZBvsR5->Divide(hptZw5);
  LowerErrorsBySqrt2(hptZBvsR5);
  hptZBvsR6=(TH1D *)hptZ6->Clone("hptZBvsR6");
  hptZBvsR6->Divide(hptZw6);
  LowerErrorsBySqrt2(hptZBvsR6);
  hptZBvsR7=(TH1D *)hptZ7->Clone("hptZBvsR7");
  hptZBvsR7->Divide(hptZw7);
  LowerErrorsBySqrt2(hptZBvsR7);
  
  
  hQprw12=(TH1D *)hQpw2->Clone("hQprw12");
  hQprw12->Divide(hQpw1);
  hQprw12->Scale((1./N2)/(1./N1));
  hQprw13=(TH1D *)hQpw3->Clone("hQprw13");
  hQprw13->Divide(hQpw1);
  hQprw13->Scale((1./N3)/(1./N1));
  hQprw32=(TH1D *)hQpw2->Clone("hQprw32");
  hQprw32->Divide(hQpw3);
  hQprw32->Scale((1./N2)/(1./N3));
  hQprw14=(TH1D *)hQpw4->Clone("hQprw14");
  hQprw14->Divide(hQpw1);
  hQprw14->Scale((1./N4)/(1./N1));  
  hQprw15=(TH1D *)hQpw5->Clone("hQprw15");
  hQprw15->Divide(hQpw1);
  hQprw15->Scale((1./N5)/(1./N1));  
  hQprw16=(TH1D *)hQpw6->Clone("hQprw16");
  hQprw16->Divide(hQpw1);
  hQprw16->Scale((1./N6)/(1./N1));
  hQprw17=(TH1D *)hQpw7->Clone("hQprw17");
  hQprw17->Divide(hQpw1);
  hQprw17->Scale((1./N7)/(1./N1));
  hQprw62=(TH1D *)hQpw2->Clone("hQprw62");
  hQprw62->Divide(hQpw6);
  hQprw62->Scale((1./N2)/(1./N6));
  hQprw63=(TH1D *)hQpw3->Clone("hQprw63");
  hQprw63->Divide(hQpw6);
  hQprw63->Scale((1./N3)/(1./N6));
  
  hdRrw12=(TH1D *)hdRw2->Clone("hdRrw12");
  hdRrw12->Divide(hdRw1);
  hdRrw12->Scale((1./N2)/(1./N1));
  hdRrw13=(TH1D *)hdRw3->Clone("hdRrw13");
  hdRrw13->Divide(hdRw1);
  hdRrw13->Scale((1./N3)/(1./N1));
  hdRrw32=(TH1D *)hdRw2->Clone("hdRrw32");
  hdRrw32->Divide(hdRw3);
  hdRrw32->Scale((1./N2)/(1./N3));
  hdRrw14=(TH1D *)hdRw4->Clone("hdRrw14");
  hdRrw14->Divide(hdRw1);
  hdRrw14->Scale((1./N4)/(1./N1));
  hdRrw15=(TH1D *)hdRw5->Clone("hdRrw15");
  hdRrw15->Divide(hdRw1);
  hdRrw15->Scale((1./N5)/(1./N1));
  hdRrw16=(TH1D *)hdRw6->Clone("hdRrw16");
  hdRrw16->Divide(hdRw1);
  hdRrw16->Scale((1./N6)/(1./N1));
  hdRrw17=(TH1D *)hdRw7->Clone("hdRrw17");
  hdRrw17->Divide(hdRw1);
  hdRrw17->Scale((1./N7)/(1./N1));
  hdRrw62=(TH1D *)hdRw2->Clone("hdRrw62");
  hdRrw62->Divide(hdRw6);
  hdRrw62->Scale((1./N2)/(1./N6));
  hdRrw63=(TH1D *)hdRw3->Clone("hdRrw63");
  hdRrw63->Divide(hdRw6);
  hdRrw63->Scale((1./N3)/(1./N6));

  hptgammarw12=(TH1D *)hptgammaw2->Clone("hptgammarw12");
  hptgammarw12->Divide(hptgammaw1);
  hptgammarw12->Scale((1./N2)/(1./N1));
  hptgammarw13=(TH1D *)hptgammaw3->Clone("hptgammarw13");
  hptgammarw13->Divide(hptgammaw1);
  hptgammarw13->Scale((1./N3)/(1./N1));
  hptgammarw32=(TH1D *)hptgammaw2->Clone("hptgammarw32");
  hptgammarw32->Divide(hptgammaw3);
  hptgammarw32->Scale((1./N2)/(1./N3));
  hptgammarw14=(TH1D *)hptgammaw4->Clone("hptgammarw14");
  hptgammarw14->Divide(hptgammaw1);
  hptgammarw14->Scale((1./N4)/(1./N1));
  hptgammarw15=(TH1D *)hptgammaw5->Clone("hptgammarw15");
  hptgammarw15->Divide(hptgammaw1);
  hptgammarw15->Scale((1./N5)/(1./N1));
  hptgammarw16=(TH1D *)hptgammaw6->Clone("hptgammarw16");
  hptgammarw16->Divide(hptgammaw1);
  hptgammarw16->Scale((1./N6)/(1./N1));
  hptgammarw17=(TH1D *)hptgammaw7->Clone("hptgammarw17");
  hptgammarw17->Divide(hptgammaw1);
  hptgammarw17->Scale((1./N7)/(1./N1));
  hptgammarw62=(TH1D *)hptgammaw2->Clone("hptgammarw62");
  hptgammarw62->Divide(hptgammaw6);
  hptgammarw62->Scale((1./N2)/(1./N6));
  hptgammarw63=(TH1D *)hptgammaw3->Clone("hptgammarw63");
  hptgammarw63->Divide(hptgammaw6);
  hptgammarw63->Scale((1./N3)/(1./N6));
  
  return 1;
}

// compare with Kharlamov results
int Kdiff() {
  int result1=1,result2=1;
  int result3=1,result4=1;
  //  DressSwitch=UNDRESSED;
  //result3=Kdiff(Z2MUMUG);
  //result2=Kdiff(Z2MUMUG);
  //return 1;
  DressSwitch=DRESSED;
  result1=Kdiff(Z2EEG);
  result2=Kdiff(Z2MUMUG);
  DressSwitch=UNDRESSED;
  result3=Kdiff(Z2EEG);
  result4=Kdiff(Z2MUMUG);
  return result1*result2*result3*result4;
}

// compare with Kharlamov results
int Kdiff(int decaytype) {
  // close_files();
  init();
  switch (decaytype){
  case Z2EEG:
    open_files_eeg();
    // KUcompare(Z2EEG);
    Kcompare(Z2EEG);
    break;
  case Z2MUMUG:
    open_files_mmg();
    // KUcompare(Z2MUMUG);
    Kcompare(Z2MUMUG);
    break;
  default:
    return 0;
  }
  return 1;
}

// Compare with kharlamov MC histogram 
// run after
//  init();
// open_files_mmg(); ||  open_files_eeg();
int Kcompare(int decaytype) {
  string histnamestr,unfoldingdir,unfoldingdirold,PowhegFilestr,SherpaFilestr;
  Double_t  PowhegFilesScale, SherpaFilesScale; //due to number of downloaded files
  Double_t  iSherpaReverse; //due to reverse number of events
  TFile *ftt1,*ftt2;
  TTree *tt1,*tt2;
  //Kharlamov input dir (should changed)
  Keefilestr="unfolding/180724/histElec.root";
  Kmmfilestr="unfolding/180724/histMu.root";
  unfoldingdirold="unfolding/simV5tree/";
  unfoldingdir="unfolding/simV9tree/";
  TTree *tPP,*tSherpa;
  // general settings
  switch (decaytype){
  case Z2EEG:
    //    dirstr="pics/Kdiff/ee/";
    dirstr="pics/Kdiff/data/";
    break;
  case Z2MUMUG:
    //    dirstr="pics/Kdiff/mumu/";
    dirstr="pics/Kdiff/data/";
    break;
  default:
    return 0;
  }
  cout << "DressSwitch="<<DressSwitch << endl;
  switch (DressSwitch){
  case DRESSED:
    dresspostfix="Dress";
    dresspicprefix="";
    tPP=t1;
    tSherpa=t2;
    switch (decaytype){
    case Z2EEG:
      PowhegFilestr=unfoldingdir+"user.tkharlam.mc12_8TeV.207320.PowhegPythia8_AU2CT10_Zee.merge.NTUP_SMWZ.e5239_s1773_r4485_r4540_p1328.dr1V9_ntuple.root";
      PowhegFilesScale=1;
      SherpaFilestr=unfoldingdir+"user.tkharlam.mc12_8TeV.145161.Sherpa_CT10_eegammaPt10.merge.NTUP_SMWZ.e1434_s1499_s1504_r3658_r3549_p1328.dr1V9_ntuple.root";
      SherpaFilesScale=1;
      break;
    case Z2MUMUG:
      PowhegFilestr=unfoldingdir+"user.tkharlam.mc12_8TeV.207321.PowhegPythia8_AU2CT10_Zmumu.merge.NTUP_SMWZ.e5239_s1773_r4485_r4540_p1328.dr1V9_ntuple.root";
      PowhegFilesScale=1;
      SherpaFilestr=unfoldingdir+"user.tkharlam.mc12_8TeV.145162.Sherpa_CT10_mumugammaPt10.merge.NTUP_SMWZ.e1434_s1499_s1504_r3658_r3549_p1328.dr1V9_ntuple.root";
      SherpaFilesScale=1;
      break;
    }
    break;
  case UNDRESSED:
    dresspostfix="Bare";
    dresspicprefix="undr_";
    tPP=tundr1;
    tSherpa=tundr2;
    switch (decaytype){
    case Z2EEG:
      PowhegFilestr=unfoldingdir+"user.tkharlam.mc12_8TeV.207320.PowhegPythia8_AU2CT10_Zee.merge.NTUP_SMWZ.e5239_s1773_r4485_r4540_p1328.drnoV9_ntuple.root";
      PowhegFilesScale=1;
      SherpaFilestr=unfoldingdir+"user.tkharlam.mc12_8TeV.145161.Sherpa_CT10_eegammaPt10.merge.NTUP_SMWZ.e1434_s1499_s1504_r3658_r3549_p1328.drnoV9_ntuple.root";
      SherpaFilesScale=1./(684./685.); //downloaded 684 out of 685 files
      break;
    case Z2MUMUG:
      PowhegFilestr=unfoldingdir+"user.tkharlam.mc12_8TeV.207321.PowhegPythia8_AU2CT10_Zmumu.merge.NTUP_SMWZ.e5239_s1773_r4485_r4540_p1328.drnoV9_ntuple.root";
      PowhegFilesScale=1./(969./972.); //downloaded 969 out of 672 files
      SherpaFilestr=unfoldingdir+"user.tkharlam.mc12_8TeV.145162.Sherpa_CT10_mumugammaPt10.merge.NTUP_SMWZ.e1434_s1499_s1504_r3658_r3549_p1328.drnoV9_ntuple.root";
      SherpaFilesScale=1.;
      break;
    }
    break;
  default:
    return 0;
  }

  // general settings
  int Smakepics=1;
  int Smakehist=1;
  string fileoutstr;
  fileoutstr=dirstr+dresspicprefix+"rivet-hist-"+str(decaytype)+".root";
  TFile *fileout;
  if (Smakehist==1) fileout=new TFile(fileoutstr.c_str(),"RECREATE");

  //extracting File Weight
  Double_t Wfile;
  tPP->SetBranchAddress("Wfile",&Wfile);
  tPP->GetEntry(0);    
  //  Double_t WfilePP=Wfile;
  Double_t WfilePP=1.;
  cout << "Wfile=" << Wfile << endl;
  // different number of events in datasets (EVNT vs SMWZ)
  switch (decaytype){
  case Z2EEG:
    WfilePP=WfilePP/4979985.*5000000.*PowhegFilesScale;
    break;
  case Z2MUMUG:
    WfilePP=WfilePP/4859190.*4995000.*PowhegFilesScale; // 
    break;
  }
  cout << "WfilePP=" << WfilePP << endl;


  tSherpa->SetBranchAddress("Wfile",&Wfile);
  cout << "Wfile=" << Wfile << endl;
  tSherpa->GetEntry(0);
  //  Double_t WfileSherpa=Wfile; 
  Double_t WfileSherpa=1; 
  // different number of events in datasets (EVNT vs SMWZ)
  switch (decaytype){
  case Z2EEG:
    WfileSherpa=WfileSherpa/(8844673./885.*685.)*13500000.*SherpaFilesScale;
    //WfileSherpa=WfileSherpa/8844673.*13500000.;
    break;
  case Z2MUMUG:
    WfileSherpa=WfileSherpa/9188579.*8500000.*SherpaFilesScale;
    //WfileSherpa=WfileSherpa/9188579.*8500000.;
    break;
  }
  cout << "WfileSherpa=" << WfileSherpa << endl;


  switch (decaytype){
  case Z2EEG:
    filestr=Keefilestr;
    DecayTypePostfix="EE";
    break;
  case Z2MUMUG:
    filestr=Kmmfilestr;
    DecayTypePostfix="MM";
    break;
  }
  cQp=(TCanvas *)gROOT->FindObject("cQp");
  if (!cQp)  cQp=new TCanvas("cQp","cQp",800.,600.);
  cQp->cd();

  //histograms
  histnamestr="QpPowHeg"+string(DecayTypePostfix)+string(dresspostfix);
  cout << "histnamestr=\"" << histnamestr<< "\""<<endl;
  TFile *FQp =  new TFile(filestr.c_str(),"FQp");
  TH1D *h1=(TH1D *)FQp->Get(histnamestr.c_str())->Clone("h1");

  histnamestr="QpSherpa"+string(DecayTypePostfix)+string(dresspostfix);
  cout << "histnamestr=\"" << histnamestr<< "\""<<endl;
  TH1D* h2=(TH1D *)FQp->Get(histnamestr.c_str())->Clone("h2");

  
  ftt1 = new TFile(PowhegFilestr.c_str(),"Kh-Powheg-file-tree","");
  hQpt1=(TH1D *)h1->Clone("hQpt1");
  hQpt1->Reset();
  hQpt1->SetTitle("PowhegPythia8 (analysis)");
  hQpt1->Sumw2();
  cout << "filenamein=\"" << PowhegFilestr << "\""<<endl;
  tt1=(TTree *)ftt1->Get("tree2l1g");
  tt1->Draw("M_D_Plus_t[0]/1.E3>>hQpt1");




  ftt2 = new TFile(SherpaFilestr.c_str(),"Kh-Sherpa-file-tree","");
  hQpt2=(TH1D *)h2->Clone("hQpt2");
  hQpt2->Reset();
  hQpt2->SetTitle("Sherpa 1.4 (analysis)");
  hQpt2->Sumw2();
  cout << "filenamein=\"" << SherpaFilestr << "\""<<endl;
  tt2=(TTree *)ftt2->Get("tree2l1g");
  tt2->Draw("M_D_Plus_t[0]/1.E3>>hQpt2");
  //hQpt2->Scale(WfileSherpa);
  


  nQp=hQpt1->GetNbinsX();
  
  //PowhegPythia8
  hQp1=(TH1D *)hQpt1->Clone("hQp1");
  hQp1->SetTitle("PowhegPythia8 (rivet)");
  hQp1->Reset();
  //  t1->Draw("Qp>>hQp1","W*Wfile");
  tPP->Draw("Qp>>hQp1");
  N1=hQp1->Integral(1,nQp);
  cout << "N1=" << N1 <<endl;
  Nt1=hQpt1->Integral(1,nQp);
  cout << "Nt1=" << Nt1 <<endl;
  hQptr1=(TH1D *)hQp1->Clone("hQptr1");
  hQptr1->Divide(hQpt1);
  setDiffErrors(hQptr1,hQp1,hQpt1);
  hQp1->GetXaxis()->SetTitle("Qp, GeV");
  hQp1->SetMarkerStyle(22);
  hQp1->SetMarkerColor(kBlack);
  hQp1->SetLineColor(kBlack);
  hQp1->Draw();
  hQpt1->SetMarkerStyle(1);
  hQpt1->SetMarkerColor(kBlue);
  hQpt1->Draw("same HIST");


  TLegend *LQp1 = new TLegend(0.67,0.7,0.88,0.85);
  LQp1->AddEntry(hQp1,"PowhegPythia8 (rivet)","PE");
  LQp1->AddEntry(hQpt1,"PowhegPythia8 (analysis)","PE");
  LQp1->Draw();
  filestr=string(dirstr)+string(dresspicprefix)+"Qp1-"+str(decaytype)+".eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());

  // difference
  hQptd1=(TH1D *)hQp1->Clone("hQptd1");
  // hQptd1->Add(hQpt1,-1);
  SubstractWithDNErrors(hQptd1,hQpt1);
  hQptd1->Draw();
  hQptr1->Draw();
  DivideHistWithoutErrorsChanges(hQptd1,hQp1,WfilePP);
  hQptd1->Draw();
  return 1;

  //Sherpa
  hQp2=(TH1D *)hQpt2->Clone("hQp2");
  hQp2->SetTitle("PowhegPythia8 (rivet)");
  hQp2->Reset();
  //  t2->Draw("Qp>>hQp2","W*Wfile");
  tSherpa->Draw("Qp>>hQp2");
  N2=hQp2->Integral(1,nQp);
  hQptr2=(TH1D *)hQp2->Clone("hQptr2");
  hQptr2->Divide(hQpt2);
  setDiffErrors(hQptr2,hQp2,hQpt2);
  hQp2->GetXaxis()->SetTitle("Qp, GeV");
  hQp2->SetMarkerStyle(22);
  hQp2->SetMarkerColor(kBlack);
  hQp2->SetLineColor(kBlack);
  hQp2->Draw();
  hQpt2->SetMarkerStyle(2);
  hQpt2->SetMarkerColor(kBlue);
  hQpt2->Draw("same HIST");


  TLegend *LQp2 = new TLegend(0.67,0.7,0.88,0.85);
  LQp2->AddEntry(hQp2,"Sherpa 1.4 (rivet)","PE");
  LQp2->AddEntry(hQpt2,"Sherpa 1.4 (analysis)","PE");
  LQp2->Draw();
  filestr=string(dirstr)+string(dresspicprefix)+"Qp2-"+str(decaytype)+".eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());

  // difference
  hQptd2=(TH1D *)hQp2->Clone("hQptd2");
  hQptd2->Draw();
  // hQptd2->Add(hQpt2,-1);
  int ireverse=0;
  if (decaytype==Z2MUMUG) ireverse=2;
  SubstractWithDNErrors(hQptd2,hQpt2,ireverse);
  hQptd2->Draw();
  hQptr2->Draw();
  if (decaytype==Z2EEG) {
    DivideHistWithoutErrorsChanges(hQptd2,hQp2,WfileSherpa);
  }
  else {
    DivideHistWithoutErrorsChanges(hQptd2,hQpt2,1./WfileSherpa);
  }
  hQptd2->Draw();
  return 1;
  
  //PowhegPythia8 rivet/analysis
  hQptr1->GetXaxis()->SetTitle("Qp, GeV");
  hQptr1->SetMarkerStyle(22);
  hQptr1->GetYaxis()->SetRangeUser(0.85,1.15);
  hQptr1->Draw();
  hQptr1->Draw("same");

  TF1 *lQptr1 = new TF1("lQptr1","[0]", 0, 1);
  lQptr1->SetParName(0,"CQptr1");
  hQptr1->Fit(lQptr1,"","",0.,90.);
  Double_t CQptr1=lQptr1->GetParameter(0);
  Double_t CQptr1_er=lQptr1->GetParError(0);
  CQptr1=int(CQptr1*1000+0.5)/1000.;
  CQptr1_er=int(CQptr1_er*1000+0.5)/1000.;
  Float_t C_chi2_Qptr1=int(lQptr1->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qptr1=lQptr1->GetNDF();

  TPaveText *PQptr1 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQptr1->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQptr1->SetTextAlign(12); 
  curstr="PowhegPythia8 (Rivet/analysis)";
  PQptr1->AddText(curstr.c_str());
  curstr="C="+str(CQptr1)+"#pm"+str(CQptr1_er)+", #chi^{2}/ndf="+str(C_chi2_Qptr1)+"/"+str(C_ndf_Qptr1)+"    ";
  PQptr1->AddText(curstr.c_str());
  PQptr1->Draw();
  TLegend *LQptr1 = new TLegend(0.53,0.15,0.75,0.35);
  LQptr1->AddEntry(hQptr1,"#frac{rivet}{analysis} (sim)","PE");
  LQptr1->Draw();
  
  filestr=string(dirstr)+string(dresspicprefix)+"Qpr1-"+str(decaytype)+".eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
  //Sherpa 
  hQp2=(TH1D *)hQp1->Clone("hQp2");
  hQp2->SetTitle("Sherpa 1.4 (rivet)");
  hQp2->Reset();
  tSherpa->Draw("Qp>>hQp2","W");
  N2=hQp2->Integral(1,nQp);
  hQptr2=(TH1D *)hQp2->Clone("hQptr2");
  hQptr2->Divide(hQpt2);
  setDiffErrors(hQptr2,hQp2,hQpt2);

  //Sherpa rivet/analysis
  hQptr2->GetXaxis()->SetTitle("Qp, GeV");
  hQptr2->SetMarkerStyle(22);
  hQptr2->GetYaxis()->SetRangeUser(0.85,2.);
  hQptr2->Draw();
  hQptr2->Draw("same");

  TF1 *lQptr2 = new TF1("lQptr2","[0]", 0, 1);
  lQptr2->SetParName(0,"CQptr2");
  hQptr2->Fit(lQptr2,"","",0.,90.);
  Double_t CQptr2=lQptr2->GetParameter(0);
  Double_t CQptr2_er=lQptr2->GetParError(0);
  CQptr2=int(CQptr2*1000+0.5)/1000.;
  CQptr2_er=int(CQptr2_er*1000+0.5)/1000.;
  Float_t C_chi2_Qptr2=int(lQptr2->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qptr2=lQptr2->GetNDF();

  TPaveText *PQptr2 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQptr2->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQptr2->SetTextAlign(12); 
  curstr="Sherpa (Rivet/analysis)";
  PQptr2->AddText(curstr.c_str());
  curstr="C="+str(CQptr2)+"#pm"+str(CQptr2_er)+", #chi^{2}/ndf="+str(C_chi2_Qptr2)+"/"+str(C_ndf_Qptr2)+"    ";
  PQptr2->AddText(curstr.c_str());
  PQptr2->Draw();
  TLegend *LQptr2 = new TLegend(0.53,0.15,0.75,0.35);
  LQptr2->AddEntry(hQptr2,"#frac{rivet}{analysis} (sim)","PE");
  LQptr2->Draw();
  
  filestr=string(dirstr)+string(dresspicprefix)+"Qpr2-"+str(decaytype)+".eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());

  if (Smakehist==1) {
    fileout->cd();
    cout << "Writing Qp hist in "<< fileoutstr << endl;
    hQpt1->Write();
    hQpt2->Write();
    hQp1->Write();
    hQp2->Write();
  }
   
  if (Smakehist==1) {
    fileout->Write();
    fileout->Close();
  }

  return 1;
}



// Compare with Kharlamov MC histogram for unfolded results
// run after
//  init();
// open_files_mmg(); ||  open_files_eeg();
int KUcompare(int decaytype) {
  // general settings
  switch (decaytype){
  case Z2EEG:
    dirstr="pics/Kdiff/ee/";
    break;
  case Z2MUMUG:
    dirstr="pics/Kdiff/mumu/";
    break;
  default:
    return 0;
  }
  int Smakepics=1;
  int Smakehist=1;
  filestr=dirstr+"rivet-hist.root";
  TFile *fileout;
  if (Smakehist==1) fileout=new TFile(filestr.c_str(),"RECREATE");

  //extracting File Weight
  Double_t Wfile;
  t1->SetBranchAddress("Wfile",&Wfile);
  t1->GetEntry(0);    
  Double_t WfilePP=Wfile;
  // different number of events in datasets (EVNT vs SMWZ)
  switch (decaytype){
  case Z2EEG:
    WfilePP=WfilePP/4979985.*4995000.;
    break;
  case Z2MUMUG:
    WfilePP=WfilePP/4994190.*5000000.; // 4859190
    break;
  }  


  t2->SetBranchAddress("Wfile",&Wfile);
  t2->GetEntry(0);
  Double_t WfileSherpa=Wfile; 
  // different number of events in datasets (EVNT vs SMWZ)
  switch (decaytype){
  case Z2EEG:
    WfileSherpa=WfileSherpa/8844673.*13500000.;
    break;
  case Z2MUMUG:
    WfileSherpa=WfileSherpa/9188579.*8500000.;
    break;
  }  


  // Unfolding tree
  // switch (decaytype){
  // case Z2EEG:
  //   FTS1 = new TFile("unfolding/PowHeg_Zee_new4.root","PowHeg_Zee_Unfolding","");
  //   ts1=(TTree *)FTS1->Get("sim2l1g_Qp");
  //   FTS2 =  new TFile("unfolding/Sherpa_Zee_new4.root","Sherpa_Zee_Unfolding","");
  //   ts2=(TTree *)FTS2->Get("sim2l1g_Qp");
  //   break;
  // case Z2MUMUG:
  //   FTS1 = new TFile("unfolding/PowHeg_Zmumu_new4.root","PowHeg_Zmumu_Unfolding","");
  //   ts1=(TTree *)FTS1->Get("sim2l1g_Qp");
  //   FTS2 =  new TFile("unfolding/Sherpa_Zmumu_new4.root","Sherpa_Zmumu_Unfolding","");
  //   ts2=(TTree *)FTS2->Get("sim2l1g_Qp");
  //   break;
  // }
  
#define BEGIN_Unfolding_Qp 1
#ifdef BEGIN_Unfolding_Qp 
  switch (decaytype){
  case Z2EEG:
    // filestr="unfolding/PowHeg_EE_Qp_New3/Unfolding.Powheg_Qp_EE_New3.root";
    filestr="unfolding/unfolding5/PowHeg_EE_Qp_New5/Unfolding.Test.root";
    break;
  case Z2MUMUG:
    // filestr="unfolding/PowHeg_MM_Qp_New3/Unfolding.Powheg_Qp_MM_New3.root";
    filestr="unfolding/unfolding5/PowHeg_MM_Qp_New5/Unfolding.Test.root";
    break;
  }
  
  TFile *FQp0 =  new TFile(filestr.c_str(),"FQp0");
  TDirectory *DQp0=(TDirectory *)FQp0->Get("Final");
  hQp0=(TH1D *)DQp0->Get("Final_Nominal_Unfolded")->Clone("hQp0");
  hQp0->SetTitle("Exp. Unfolded");
  hQpEr1=(TH1D *)DQp0->Get("Final_Statistical_Uncertainty");
  hQpEr2=(TH1D *)DQp0->Get("Final_SystematicUncertaintyCombined");
  TDirectory *DQpt0=(TDirectory *)FQp0->Get("Nominal");
  hQpt1=(TH1D *)DQpt0->Get("Nominal_MCTruth")->Clone("hQpt1");
  hQpt1->SetTitle("PowhegPythia8 (analysis)");
  // hQpt1->Reset();
  // hQpt1->Sumw2();
  // ts1->Draw("MCTruthValue>>hQpt1","(MCTruthIsFiducial>0)*MCTruthWeight");
  hQpt1->Scale(WfilePP); 

  switch (decaytype){
  case Z2EEG:
    //filestr="unfolding/Sherpa_EE_Qp_New3/Unfolding.Sherpa_Qp_EE_New3.root";
    filestr="unfolding/unfolding5/Sherpa_EE_Qp_New5/Unfolding.Test.root";
    break;
  case Z2MUMUG:
    //filestr="unfolding/Sherpa_MM_Qp_New3/Unfolding.Sherpa_Qp_MM_New3.root";
    filestr="unfolding/unfolding5/Sherpa_MM_Qp_New5/Unfolding.Test.root";
    break;
  }
  TFile *FQpS0 =  new TFile(filestr.c_str(),"FQpS");
  TDirectory *DQpS0=(TDirectory *)FQpS0->Get("Nominal");

  hQpt2=(TH1D *)DQpS0->Get("Nominal_MCTruth")->Clone("hQpt2"); 
  hQpt2->SetTitle("Sherpa 1.4 (analysis)");
  hQpt2->Sumw2();
  hQpt2->Scale(WfileSherpa);
  cQp=(TCanvas *)gROOT->FindObject("cQp");

  if (!cQp)  cQp=new TCanvas("cQp","cQp",800.,600.);
  cQp->cd();
  nQp=hQp0->GetNbinsX();
  N0=hQp0->Integral(1,nQp);
  
  for (int i=1;i<=nQp;i++) {
    Double_t Err1=hQpEr1->GetBinContent(i);
    Double_t Err2=hQpEr2->GetBinContent(i);
    Double_t Err=TMath::Sqrt(Err1*Err1+Err2*Err2)*N0;
    hQp0->SetBinError(i,Err);
  }

  
  //PowhegPythia8
  hQp1=(TH1D *)hQp0->Clone("hQp1");
  hQp1->SetTitle("PowhegPythia8 (rivet)");
  hQp1->Reset();
  t1->Draw("Qp>>hQp1","W*Wfile");
  N1=hQp1->Integral(1,nQp);
  hQpr1=(TH1D *)hQp1->Clone("hQpr1");
  hQpr1->Divide(hQp0);
  hQptr1=(TH1D *)hQp1->Clone("hQptr1");
  hQptr1->Divide(hQpt1);

  hQp1->GetXaxis()->SetTitle("Qp, GeV");
  hQp1->SetMarkerStyle(22);
  hQp1->SetMarkerColor(kBlack);
  hQp1->SetLineColor(kBlack);
  hQp1->Draw();
  hQp0->Draw("same");
  hQp0->SetMarkerStyle(23);
  hQp0->SetMarkerColor(kRed);
  hQpt1->SetMarkerStyle(1);
  hQpt1->SetMarkerColor(kBlue);
  hQpt1->Draw("same HIST");

  TLegend *LQp1 = new TLegend(0.67,0.7,0.88,0.85);
  LQp1->AddEntry(hQp1,"PowhegPythia8 (rivet)","PE");
  LQp1->AddEntry(hQp0,"Exp. Unfolded","PE");
  LQp1->AddEntry(hQpt1,"PowhegPythia8 (analysis)","PE");
  LQp1->Draw();
  filestr=dirstr+"hQp1.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
  
  hQpr1->GetXaxis()->SetTitle("Qp, GeV");
  hQpr1->SetMarkerStyle(23);
  hQpr1->SetMarkerColor(kRed);
  hQpr1->SetLineColor(kRed);
  hQpr1->GetYaxis()->SetRangeUser(0.4,1.6);
  hQpr1->Draw();
  hQptr1->Draw("same");

  TF1 *lQpr1 = new TF1("lQpr1","[0]", 0, 1);
  lQpr1->SetParName(0,"CQpr1");
  hQpr1->Fit(lQpr1,"","",10.,80.);
  Double_t CQpr1=lQpr1->GetParameter(0);
  Double_t CQpr1_er=lQpr1->GetParError(0);
  CQpr1=int(CQpr1*1000+0.5)/1000.;
  CQpr1_er=int(CQpr1_er*1000+0.5)/1000.;
  Float_t C_chi2_Qpr1=int(lQpr1->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qpr1=lQpr1->GetNDF();

  TPaveText *PQpr1 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQpr1->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQpr1->SetTextAlign(12); 
  curstr="PowhegPythia8/Unfolded exp.";
  PQpr1->AddText(curstr.c_str());
  curstr="C="+str(CQpr1)+"#pm"+str(CQpr1_er)+", #chi^{2}/ndf="+str(C_chi2_Qpr1)+"/"+str(C_ndf_Qpr1)+"    ";
  PQpr1->AddText(curstr.c_str());
  PQpr1->Draw();
  TLegend *LQpr1 = new TLegend(0.53,0.15,0.75,0.35);
  LQpr1->AddEntry(hQpr1,"#frac{rivet}{Exp. Unfolded} ","PE");
  LQpr1->AddEntry(hQptr1,"#frac{rivet}{analysis} (sim)","PE");
  LQpr1->Draw();
  
  filestr=dirstr+"hQpr1.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
   
  //Sherpa old
  hQp2=(TH1D *)hQp0->Clone("hQp2");
  hQp2->SetTitle("Sherpa 1.4 (rivet)");
  hQp2->Reset();
  t2->Draw("Qp>>hQp2","W*Wfile");
  N2=hQp2->Integral(1,nQp);
  hQpr2=(TH1D *)hQp2->Clone("hQpr2");
  hQpr2->Divide(hQp0);
  hQptr2=(TH1D *)hQp2->Clone("hQptr2");
  hQptr2->Divide(hQpt2);

  //simulation comparision
  //PowhegPythia8 rivet/analysis
  hQptr1->GetXaxis()->SetTitle("Qp, GeV");
  hQptr1->SetMarkerStyle(22);
  hQptr1->SetMarkerColor(kBlack);
  hQptr1->SetLineColor(kBlack);
  hQptr1->GetYaxis()->SetRangeUser(0.85,1.15);
  hQptr1->Draw();

  TF1 *lQptr1 = new TF1("lQptr1","[0]", 0, 1);
  lQptr1->SetParName(0,"CQptr1");
  hQptr1->Fit(lQptr1);//,"","",10.,80.
  Double_t CQptr1=lQptr1->GetParameter(0);
  Double_t CQptr1_er=lQptr1->GetParError(0);
  CQptr1=int(CQptr1*1000+0.5)/1000.;
  CQptr1_er=int(CQptr1_er*1000+0.5)/1000.;
  Float_t C_chi2_Qptr1=int(lQptr1->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qptr1=lQptr1->GetNDF();

  TPaveText *PQptr1 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQptr1->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQptr1->SetTextAlign(12); 
  curstr="PowhegPythia8 (Rivet/analysis)";
  PQptr1->AddText(curstr.c_str());
  curstr="C="+str(CQptr1)+"#pm"+str(CQptr1_er)+", #chi^{2}/ndf="+str(C_chi2_Qptr1)+"/"+str(C_ndf_Qptr1)+"    ";
  PQptr1->AddText(curstr.c_str());
  PQptr1->Draw();
  filestr=dirstr+"hQptr1.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
   
  //PowhegPythia vs Sherpa
  hQptr12=(TH1D *)hQptr1->Clone("hQptr12");
  hQptr12->Divide(hQptr2);
  hQptr12->SetLineColor(kBlack);
  hQptr12->GetXaxis()->SetTitle("Qp, GeV");
  hQptr12->GetYaxis()->SetRangeUser(0.9,1.2);
  hQptr12->Draw();
  TF1 *lQptr12 = new TF1("lQptr12","[0]", 0, 1);
  lQptr12->SetParName(0,"CQptr12");
  hQptr12->Fit(lQptr12);
  Double_t CQptr12=lQptr12->GetParameter(0);
  Double_t CQptr12_er=lQptr12->GetParError(0);
  CQptr12=int(CQptr12*1000+0.5)/1000.;
  CQptr12_er=int(CQptr12_er*1000+0.5)/1000.;
  Float_t C_chi2_Qptr12=int(lQptr12->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qptr12=lQptr12->GetNDF();

  TPaveText *PQptr12 = new TPaveText(0.3,0.62,0.65,0.85,"NDC");
  PQptr12->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQptr12->SetTextAlign(12); 
  curstr="#frac{PowhegPythia8 (Rivet/analysis)}{Sherpa 1.4 (Rivet/analysis)}";
  PQptr12->AddText(curstr.c_str());
  curstr="C="+str(CQptr12)+"#pm"+str(CQptr12_er)+", #chi^{2}/ndf="+str(C_chi2_Qptr12)+"/"+str(C_ndf_Qptr12)+"    ";
  PQptr12->AddText(curstr.c_str());
  PQptr12->Draw();
  filestr=dirstr+"hQptr12.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());

   
  //Sherpa old comparision
  hQp2->GetXaxis()->SetTitle("Qp, GeV");
  hQp2->SetMarkerStyle(22);
  hQp2->SetMarkerColor(kBlack);
  hQp2->SetLineColor(kBlack);
  hQp2->Draw();
  hQp0->Draw("same");
  hQp0->SetMarkerStyle(23);
  hQp0->SetMarkerColor(kRed);
  // hQpt2->SetMarkerStyle(1);
  // hQpt2->SetMarkerColor(kBlue);   
  // hQpt2->Draw("same");
  TLegend *LQp2 = new TLegend(0.67,0.7,0.88,0.85);
  LQp2->AddEntry(hQp2,"Sherpa 1.4 (rivet)","PE");
  LQp2->AddEntry(hQp0,"Exp. Unfolded","PE");
  //LQp2->AddEntry(hQpt2,"Sherpa 1.4 (analysis)","PE");
  LQp2->Draw();
  filestr=dirstr+"hQp2.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());


  hQpr2->GetXaxis()->SetTitle("Qp, GeV");
  hQpr2->SetMarkerStyle(22);
  hQpr2->SetMarkerColor(kBlack);
  hQpr2->SetLineColor(kBlack);
  hQpr2->GetYaxis()->SetRangeUser(0.6,1.8);
  hQpr2->Draw();
  // hQptr2->SetMarkerStyle(1);
  // hQptr2->SetMarkerColor(kBlue);   
  // hQptr2->Draw("same");
   
  TF1 *lQpr2 = new TF1("lQpr2","[0]", 0, 1);
  lQpr2->SetParName(0,"CQpr2");
  hQpr2->Fit(lQpr2,"","",10.,80.);
  Double_t CQpr2=lQpr2->GetParameter(0);
  Double_t CQpr2_er=lQpr2->GetParError(0);
  CQpr2=int(CQpr2*1000+0.5)/1000.;
  CQpr2_er=int(CQpr2_er*1000+0.5)/1000.;
  Float_t C_chi2_Qpr2=int(lQpr2->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qpr2=lQpr2->GetNDF();

  TPaveText *PQpr2 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQpr2->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQpr2->SetTextAlign(12); 
  curstr="Sherpa 1.4/Unfolded exp.";
  PQpr2->AddText(curstr.c_str());
  curstr="C="+str(CQpr2)+"#pm"+str(CQpr2_er)+", #chi^{2}/ndf="+str(C_chi2_Qpr2)+"/"+str(C_ndf_Qpr2)+"    ";
  PQpr2->AddText(curstr.c_str());
  PQpr2->Draw();
  filestr=dirstr+"hQpr2.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
   
  //Sherpa new
  hQp3=(TH1D *)hQp0->Clone("hQp3");
  hQp3->SetTitle("Sherpa 2.21 (rivet)");
  hQp3->Reset();
  t3->Draw("Qp>>hQp3","W*Wfile");
  N3=hQp3->Integral(1,nQp);
  hQpr3=(TH1D *)hQp3->Clone("hQpr3");
  hQpr3->Divide(hQp0);

  hQp0->GetXaxis()->SetTitle("Qp, GeV");
  hQp3->SetMarkerStyle(22);
  hQp3->SetMarkerColor(kBlack);
  hQp3->SetLineColor(kBlack);
  hQp0->Draw("");
  hQp3->Draw("same");
  hQp0->SetMarkerStyle(23);
  hQp0->SetMarkerColor(kRed);
  TLegend *LQp3 = new TLegend(0.67,0.7,0.88,0.85);
  LQp3->AddEntry(hQp3,"Sherpa 2.21 (rivet)","PE");
  LQp3->AddEntry(hQp0,"Exp. Unfolded","PE");
  LQp3->Draw();
  filestr=dirstr+"hQp3.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());

  hQpr3->GetXaxis()->SetTitle("Qp, GeV");
  hQpr3->SetMarkerStyle(22);
  hQpr3->SetMarkerColor(kBlack);
  hQpr3->SetLineColor(kBlack);
  hQpr3->GetYaxis()->SetRangeUser(0.4,1.6);
  hQpr3->Draw();
   
  TF1 *lQpr3 = new TF1("lQpr3","[0]", 0, 1);
  lQpr3->SetParName(0,"CQpr3");
  hQpr3->Fit(lQpr3,"","",10.,80.);
  Double_t CQpr3=lQpr3->GetParameter(0);
  Double_t CQpr3_er=lQpr3->GetParError(0);
  CQpr3=int(CQpr3*1000+0.5)/1000.;
  CQpr3_er=int(CQpr3_er*1000+0.5)/1000.;
  Float_t C_chi2_Qpr3=int(lQpr3->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qpr3=lQpr3->GetNDF();

  TPaveText *PQpr3 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQpr3->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQpr3->SetTextAlign(12); 
  curstr="Sherpa 2.21/Unfolded exp.";
  PQpr3->AddText(curstr.c_str());
  curstr="C="+str(CQpr3)+"#pm"+str(CQpr3_er)+", #chi^{2}/ndf="+str(C_chi2_Qpr3)+"/"+str(C_ndf_Qpr3)+"    ";
  PQpr3->AddText(curstr.c_str());
  PQpr3->Draw();
  filestr=dirstr+"hQpr3.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
 
  //Alpgen Pythia
  hQp4=(TH1D *)hQp0->Clone("hQp4");
  hQp4->SetTitle("AlpgenPythia (rivet)");
  hQp4->Reset();
  t4->Draw("Qp>>hQp4","W*Wfile");
  N4=hQp4->Integral(1,nQp);
  hQpr4=(TH1D *)hQp4->Clone("hQpr4");
  hQpr4->Divide(hQp0);

  hQp0->GetXaxis()->SetTitle("Qp, GeV");
  hQp4->SetMarkerStyle(22);
  hQp4->SetMarkerColor(kBlack);
  hQp4->SetLineColor(kBlack);
  hQp0->Draw("");
  hQp4->Draw("same");
  hQp0->SetMarkerStyle(23);
  hQp0->SetMarkerColor(kRed);
  TLegend *LQp4 = new TLegend(0.67,0.7,0.88,0.85);
  LQp4->AddEntry(hQp4,"AlpgenPythia (rivet)","PE");
  LQp4->AddEntry(hQp0,"Exp. Unfolded","PE");
  LQp4->Draw();
  filestr=dirstr+"hQp4.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
  hQpr4->GetXaxis()->SetTitle("Qp, GeV");
  hQpr4->SetMarkerStyle(22);
  hQpr4->SetMarkerColor(kBlack);
  hQpr4->SetLineColor(kBlack);
  hQpr4->GetYaxis()->SetRangeUser(0.6,1.8);
  hQpr4->Draw();
 
  TF1 *lQpr4 = new TF1("lQpr4","[0]", 0, 1);
  lQpr4->SetParName(0,"CQpr4");
  hQpr4->Fit(lQpr4,"","",10.,80.);
  Double_t CQpr4=lQpr4->GetParameter(0);
  Double_t CQpr4_er=lQpr4->GetParError(0);
  CQpr4=int(CQpr4*1000+0.5)/1000.;
  CQpr4_er=int(CQpr4_er*1000+0.5)/1000.;
  Float_t C_chi2_Qpr4=int(lQpr4->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qpr4=lQpr4->GetNDF();

  TPaveText *PQpr4 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQpr4->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQpr4->SetTextAlign(12); 
  curstr="AlpgenPythia/Unfolded exp.";
  PQpr4->AddText(curstr.c_str());
  curstr="C="+str(CQpr4)+"#pm"+str(CQpr4_er)+", #chi^{2}/ndf="+str(C_chi2_Qpr4)+"/"+str(C_ndf_Qpr4)+"    ";
  PQpr4->AddText(curstr.c_str());
  PQpr4->Draw();
  filestr=dirstr+"hQpr4.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str()); 
   
  //Alpgen Jimmy
  hQp5=(TH1D *)hQp0->Clone("hQp5");
  hQp5->SetTitle("AlpgenJimmy (rivet)");
  hQp5->Reset();
  t5->Draw("Qp>>hQp5","W*Wfile");
  N5=hQp5->Integral(1,nQp);
  hQpr5=(TH1D *)hQp5->Clone("hQpr5");
  hQpr5->Divide(hQp0);
   
  hQp0->GetXaxis()->SetTitle("Qp, GeV");
  hQp5->SetMarkerStyle(22);
  hQp5->SetMarkerColor(kBlack);
  hQp5->SetLineColor(kBlack);
  hQp0->Draw("");
  hQp5->Draw("same");
  hQp0->SetMarkerStyle(23);
  hQp0->SetMarkerColor(kRed);
  TLegend *LQp5 = new TLegend(0.67,0.7,0.88,0.85);
  LQp5->AddEntry(hQp5,"AlpgenJimmy (rivet)","PE");
  LQp5->AddEntry(hQp0,"Exp. Unfolded","PE");
  LQp5->Draw();
  filestr=dirstr+"hQp5.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
  hQpr5->GetXaxis()->SetTitle("Qp, GeV");
  hQpr5->SetMarkerStyle(22);
  hQpr5->SetMarkerColor(kBlack);
  hQpr5->SetLineColor(kBlack);
  hQpr5->GetYaxis()->SetRangeUser(0.4,1.6);
  hQpr5->Draw();
 
  TF1 *lQpr5 = new TF1("lQpr5","[0]", 0, 1);
  lQpr5->SetParName(0,"CQpr5");
  hQpr5->Fit(lQpr5,"","",10.,80.);
  Double_t CQpr5=lQpr5->GetParameter(0);
  Double_t CQpr5_er=lQpr5->GetParError(0);
  CQpr5=int(CQpr5*1000+0.5)/1000.;
  CQpr5_er=int(CQpr5_er*1000+0.5)/1000.;
  Float_t C_chi2_Qpr5=int(lQpr5->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qpr5=lQpr5->GetNDF();

  TPaveText *PQpr5 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQpr5->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQpr5->SetTextAlign(12); 
  curstr="AlpgenJimmy/Unfolded exp.";
  PQpr5->AddText(curstr.c_str());
  curstr="C="+str(CQpr5)+"#pm"+str(CQpr5_er)+", #chi^{2}/ndf="+str(C_chi2_Qpr5)+"/"+str(C_ndf_Qpr5)+"    ";
  PQpr5->AddText(curstr.c_str());
  PQpr5->Draw();
  filestr=dirstr+"hQpr5.eps";

  //Sherpa 2.24
  hQp6=(TH1D *)hQp0->Clone("hQp6");
  hQp6->SetTitle("Sherpa 2.24 (rivet)");
  hQp6->Reset();
  t6->Draw("Qp>>hQp6","W*Wfile");
  N6=hQp6->Integral(1,nQp);
  hQpr6=(TH1D *)hQp6->Clone("hQpr6");
  hQpr6->Divide(hQp0);
   
  hQp0->GetXaxis()->SetTitle("Qp, GeV");
  hQp6->SetMarkerStyle(22);
  hQp6->SetMarkerColor(kBlack);
  hQp6->SetLineColor(kBlack);
  hQp0->Draw("");
  hQp6->Draw("same");
  hQp0->SetMarkerStyle(23);
  hQp0->SetMarkerColor(kRed);
  TLegend *LQp6 = new TLegend(0.67,0.7,0.88,0.85);
  LQp6->AddEntry(hQp6,"Sherpa 2.24 (rivet)","PE");
  LQp6->AddEntry(hQp0,"Exp. Unfolded","PE");
  LQp6->Draw();
  filestr=dirstr+"hQp6.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
  hQpr6->GetXaxis()->SetTitle("Qp, GeV");
  hQpr6->SetMarkerStyle(22);
  hQpr6->SetMarkerColor(kBlack);
  hQpr6->SetLineColor(kBlack);
  hQpr6->GetYaxis()->SetRangeUser(0.4,1.6);
  hQpr6->Draw();
 
  TF1 *lQpr6 = new TF1("lQpr6","[0]", 0, 1);
  lQpr6->SetParName(0,"CQpr6");
  hQpr6->Fit(lQpr6,"","",10.,80.);
  Double_t CQpr6=lQpr6->GetParameter(0);
  Double_t CQpr6_er=lQpr6->GetParError(0);
  CQpr6=int(CQpr6*1000+0.5)/1000.;
  CQpr6_er=int(CQpr6_er*1000+0.5)/1000.;
  Float_t C_chi2_Qpr6=int(lQpr6->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qpr6=lQpr6->GetNDF();

  TPaveText *PQpr6 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQpr6->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQpr6->SetTextAlign(12); 
  curstr="Sherpa 2.24/Unfolded exp.";
  PQpr6->AddText(curstr.c_str());
  curstr="C="+str(CQpr6)+"#pm"+str(CQpr6_er)+", #chi^{2}/ndf="+str(C_chi2_Qpr6)+"/"+str(C_ndf_Qpr6)+"    ";
  PQpr6->AddText(curstr.c_str());
  PQpr6->Draw();
  filestr=dirstr+"hQpr6.eps";

  //PowhegPythia8 (One #gamma)
  hQp7=(TH1D *)hQp0->Clone("hQp7");
  hQp7->SetTitle("PowhegPythia8 (One #gamma) (rivet)");
  hQp7->Reset();
  t7->Draw("Qp>>hQp7","W*Wfile");
  N7=hQp7->Integral(1,nQp);
  hQpr7=(TH1D *)hQp7->Clone("hQpr7");
  hQpr7->Divide(hQp0);
  
  hQp0->GetXaxis()->SetTitle("Qp, GeV");
  hQp7->SetMarkerStyle(22);
  hQp7->SetMarkerColor(kBlack);
  hQp7->SetLineColor(kBlack);
  hQp0->Draw("");
  hQp7->Draw("same");
  hQp0->SetMarkerStyle(23);
  hQp0->SetMarkerColor(kRed);
  TLegend *LQp7 = new TLegend(0.67,0.7,0.88,0.85);
  LQp7->AddEntry(hQp7,"PowhegPythia8 (One #gamma) (rivet)","PE");
  LQp7->AddEntry(hQp0,"Exp. Unfolded","PE");
  LQp7->Draw();
  filestr=dirstr+"hQp7.eps";
  if (Smakepics==1) cQp->Print(filestr.c_str());
  hQpr7->GetXaxis()->SetTitle("Qp, GeV");
  hQpr7->SetMarkerStyle(22);
  hQpr7->SetMarkerColor(kBlack);
  hQpr7->SetLineColor(kBlack);
  hQpr7->GetYaxis()->SetRangeUser(0.4,1.6);
  hQpr7->Draw();
    
  TF1 *lQpr7 = new TF1("lQpr7","[0]", 0, 1);
  lQpr7->SetParName(0,"CQpr7");
  hQpr7->Fit(lQpr7,"","",10.,80.);
  Double_t CQpr7=lQpr7->GetParameter(0);
  Double_t CQpr7_er=lQpr7->GetParError(0);
  CQpr7=int(CQpr7*1000+0.5)/1000.;
  CQpr7_er=int(CQpr7_er*1000+0.5)/1000.;
  Float_t C_chi2_Qpr7=int(lQpr7->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qpr7=lQpr7->GetNDF();
    
  TPaveText *PQpr7 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQpr7->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQpr7->SetTextAlign(12); 
  curstr="PowhegPythia8 (One #gamma)/Unfolded exp.";
  PQpr7->AddText(curstr.c_str());
  curstr="C="+str(CQpr7)+"#pm"+str(CQpr7_er)+", #chi^{2}/ndf="+str(C_chi2_Qpr7)+"/"+str(C_ndf_Qpr7)+"    ";
  PQpr7->AddText(curstr.c_str());
  PQpr7->Draw();
  filestr=dirstr+"hQpr7.eps";
  
  if (Smakepics==1) cQp->Print(filestr.c_str());
  if (Smakehist==1) {
    fileout->cd();     
    hQpt1->Write();
    hQpt2->Write();
    hQp0->Write();
    hQp1->Write();
    hQp2->Write();
    hQp3->Write();
    hQp4->Write();
    hQp5->Write();
    hQp6->Write();
    hQp7->Write();
  }

   
#endif //BEGIN_Unfolding_Qp 

#define BEGIN_Unfolding_dR 1
#ifdef BEGIN_Unfolding_dR 
  switch (decaytype){
  case Z2EEG:
    filestr="unfolding/unfolding5/PowHeg_EE_dR_New5/Unfolding.Test.root";
    break;
  case Z2MUMUG:
    filestr="unfolding/unfolding5/PowHeg_MM_dR_New5/Unfolding.Test.root";
    break;
  }

  TFile *FdR0 =  new TFile(filestr.c_str(),"FdR0");
  TDirectory *DdR0=(TDirectory *)FdR0->Get("Final");
  hdR0=(TH1D *)DdR0->Get("Final_Nominal_Unfolded")->Clone("hdR0");
  hdR0->SetTitle("Exp. Unfolded");
  hdREr1=(TH1D *)DdR0->Get("Final_Statistical_Uncertainty");
  hdREr2=(TH1D *)DdR0->Get("Final_SystematicUncertaintyCombined");
  TDirectory *DdRt0=(TDirectory *)FdR0->Get("Nominal");
  hdRt1=(TH1D *)DdRt0->Get("Nominal_MCTruth")->Clone("hdRt1");
  hdRt1->SetTitle("PowhegPythia8 (analysis)");
  hdRt1->Sumw2();
  hdRt1->Scale(WfilePP); 

  switch (decaytype){
  case Z2EEG:
    filestr="unfolding/unfolding5/Sherpa_EE_dR_New5/Unfolding.Test.root";
    break;
  case Z2MUMUG:
    filestr="unfolding/unfolding5/Sherpa_MM_dR_New5/Unfolding.Test.root";
    break;
  }
  
  TFile *FdRS0 =  new TFile(filestr.c_str(),"FdRS");
  TDirectory *DdRS0=(TDirectory *)FdRS0->Get("Nominal");
  hdRt2=(TH1D *)DdRS0->Get("Nominal_MCTruth")->Clone("hdRt2"); 
  hdRt2->SetTitle("Sherpa 1.4 (analysis)");
  hdRt2->Sumw2();
  hdRt2->Scale(WfileSherpa); 
   
  cdR=(TCanvas *)gROOT->FindObject("cdR");

  if (!cdR)  cdR=new TCanvas("cdR","cdR",800.,600.);
  cdR->cd();
  //   hdR0->Draw();
  ndR=hdR0->GetNbinsX();
  N0=hdR0->Integral(1,ndR);

  for (int i=1;i<=ndR;i++) {
    Double_t Err1=hdREr1->GetBinContent(i);
    Double_t Err2=hdREr2->GetBinContent(i);
    Double_t Err=TMath::Sqrt(Err1*Err1+Err2*Err2)*N0;
    hdR0->SetBinError(i,Err);
  }

  
  //PowhegPythia8
  hdR1=(TH1D *)hdR0->Clone("hdR1");
  hdR1->SetTitle("PowhegPythia8 (rivet)");
  hdR1->Reset();
  t1->Draw("dR>>hdR1","W*Wfile");
  N1=hdR1->Integral(1,ndR);
  hdRr1=(TH1D *)hdR1->Clone("hdRr1");
  hdRr1->Divide(hdR0);
  hdRtr1=(TH1D *)hdR1->Clone("hdRtr1");
  hdRtr1->Divide(hdRt1);

  hdR1->GetXaxis()->SetTitle("dR");
  hdR0->GetXaxis()->SetTitle("dR");
  hdR1->SetMarkerStyle(22);
  hdR1->SetMarkerColor(kBlack);
  hdR1->SetLineColor(kBlack);
  hdR0->Draw();
  hdR1->Draw("same");
  hdR0->SetMarkerStyle(23);
  hdR0->SetMarkerColor(kRed);
  hdRt1->SetMarkerStyle(1);
  hdRt1->SetMarkerColor(kBlue);
  hdRt1->Draw("same HIST");
   
  TLegend *LdR1 = new TLegend(0.67,0.7,0.88,0.85);
  LdR1->AddEntry(hdR1,"PowhegPythia8 (rivet)","PE");
  LdR1->AddEntry(hdR0,"Exp. Unfolded","PE");
  LdR1->AddEntry(hdRt1,"PowhegPythia8 (analysis)","PE");
  LdR1->Draw();
  filestr=dirstr+"hdR1.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
   
  hdRr1->GetXaxis()->SetTitle("dR");
  hdRr1->SetMarkerStyle(23);
  hdRr1->SetMarkerColor(kRed);
  hdRr1->SetLineColor(kRed);
  hdRr1->GetYaxis()->SetRangeUser(0.4,1.6);
  hdRr1->Draw();
  hdRtr1->Draw("same");

   
  TF1 *ldRr1 = new TF1("ldRr1","[0]", 0, 1);
  ldRr1->SetParName(0,"CdRr1");
  hdRr1->Fit(ldRr1);
  Double_t CdRr1=ldRr1->GetParameter(0);
  Double_t CdRr1_er=ldRr1->GetParError(0);
  CdRr1=int(CdRr1*1000+0.5)/1000.;
  CdRr1_er=int(CdRr1_er*1000+0.5)/1000.;
  Float_t C_chi2_dRr1=int(ldRr1->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRr1=ldRr1->GetNDF();

  TPaveText *PdRr1 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PdRr1->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRr1->SetTextAlign(12); 
  curstr="PowhegPythia8/Unfolded exp.";
  PdRr1->AddText(curstr.c_str());
  curstr="C="+str(CdRr1)+"#pm"+str(CdRr1_er)+", #chi^{2}/ndf="+str(C_chi2_dRr1)+"/"+str(C_ndf_dRr1)+"    ";
  PdRr1->AddText(curstr.c_str());
  PdRr1->Draw();
  TLegend *LdRr1 = new TLegend(0.53,0.15,0.75,0.35);
  LdRr1->AddEntry(hdRr1,"#frac{rivet}{Exp. Unfolded} ","PE");
  LdRr1->AddEntry(hdRtr1,"#frac{rivet}{analysis} (sim)","PE");
  LdRr1->Draw();

  filestr=dirstr+"hdRr1.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());

   //Sherpa old
  hdR2=(TH1D *)hdR0->Clone("hdR2");
  hdR2->SetTitle("Sherpa 1.4 (rivet)");
  hdR2->Reset();
  t2->Draw("dR>>hdR2","W*Wfile");
  N2=hdR2->Integral(1,ndR);
  hdRr2=(TH1D *)hdR2->Clone("hdRr2");
  hdRr2->Divide(hdR0);
  hdRtr2=(TH1D *)hdR2->Clone("hdRtr2");
  hdRtr2->Divide(hdRt2);

  //simulation comparision
  //PowhegPythia8 rivet/analysis
  hdRtr1->GetXaxis()->SetTitle("dR");
  hdRtr1->SetMarkerStyle(22);
  hdRtr1->SetMarkerColor(kBlack);
  hdRtr1->SetLineColor(kBlack);
  hdRtr1->GetYaxis()->SetRangeUser(0.9,1.1);
  hdRtr1->Draw();
  
  TF1 *ldRtr1 = new TF1("ldRtr1","[0]", 0, 1);
  ldRtr1->SetParName(0,"CdRtr1");
  hdRtr1->Fit(ldRtr1);//,"","",10.,80.
  Double_t CdRtr1=ldRtr1->GetParameter(0);
  Double_t CdRtr1_er=ldRtr1->GetParError(0);
  CdRtr1=int(CdRtr1*1000+0.5)/1000.;
  CdRtr1_er=int(CdRtr1_er*1000+0.5)/1000.;
  Float_t C_chi2_dRtr1=int(ldRtr1->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRtr1=ldRtr1->GetNDF();

  TPaveText *PdRtr1 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PdRtr1->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRtr1->SetTextAlign(12); 
  curstr="PowhegPythia8 (Rivet/analysis)";
  PdRtr1->AddText(curstr.c_str());
  curstr="C="+str(CdRtr1)+"#pm"+str(CdRtr1_er)+", #chi^{2}/ndf="+str(C_chi2_dRtr1)+"/"+str(C_ndf_dRtr1)+"    ";
  PdRtr1->AddText(curstr.c_str());
  PdRtr1->Draw();
  filestr=dirstr+"hdRtr1.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
   
  //PowhegPythia vs Sherpa
  hdRtr12=(TH1D *)hdRtr1->Clone("hdRtr12");
  hdRtr12->Divide(hdRtr2);
  hdRtr12->SetLineColor(kBlack);
  hdRtr12->GetXaxis()->SetTitle("dR");
  hdRtr12->GetYaxis()->SetRangeUser(0.85,1.2);
  hdRtr12->Draw();
  TF1 *ldRtr12 = new TF1("ldRtr12","[0]", 0, 1);
  ldRtr12->SetParName(0,"CdRtr12");
  hdRtr12->Fit(ldRtr12);
  Double_t CdRtr12=ldRtr12->GetParameter(0);
  Double_t CdRtr12_er=ldRtr12->GetParError(0);
  CdRtr12=int(CdRtr12*1000+0.5)/1000.;
  CdRtr12_er=int(CdRtr12_er*1000+0.5)/1000.;
  Float_t C_chi2_dRtr12=int(ldRtr12->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRtr12=ldRtr12->GetNDF();

  TPaveText *PdRtr12 = new TPaveText(0.3,0.62,0.65,0.85,"NDC");
  PdRtr12->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRtr12->SetTextAlign(12); 
  curstr="#frac{PowhegPythia8 (Rivet/analysis)}{Sherpa 1.4 (Rivet/analysis)}";
  PdRtr12->AddText(curstr.c_str());
  curstr="C="+str(CdRtr12)+"#pm"+str(CdRtr12_er)+", #chi^{2}/ndf="+str(C_chi2_dRtr12)+"/"+str(C_ndf_dRtr12)+"    ";
  PdRtr12->AddText(curstr.c_str());
  PdRtr12->Draw();
  filestr=dirstr+"hdRtr12.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
  
  //Sherpa old comparision
  hdR2->GetXaxis()->SetTitle("dR");
  hdR2->SetMarkerStyle(22);
  hdR2->SetMarkerColor(kBlack);
  hdR2->SetLineColor(kBlack);
  hdR2->Draw();
  hdR0->Draw("same");
  hdR0->SetMarkerStyle(23);
  hdR0->SetMarkerColor(kRed);
  TLegend *LdR2 = new TLegend(0.67,0.7,0.88,0.85);
  LdR2->AddEntry(hdR2,"Sherpa 1.4 (rivet)","PE");
  LdR2->AddEntry(hdR0,"Exp. Unfolded","PE");
  LdR2->Draw();
  filestr=dirstr+"hdR2.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());

  hdRr2->GetXaxis()->SetTitle("dR");
  hdRr2->SetMarkerStyle(22);
  hdRr2->SetMarkerColor(kBlack);
  hdRr2->SetLineColor(kBlack);
  hdRr2->GetYaxis()->SetRangeUser(0.6,1.8);
  hdRr2->Draw();
   
  TF1 *ldRr2 = new TF1("ldRr2","[0]", 0, 1);
  ldRr2->SetParName(0,"CdRr2");
  hdRr2->Fit(ldRr2);
  Double_t CdRr2=ldRr2->GetParameter(0);
  Double_t CdRr2_er=ldRr2->GetParError(0);
  CdRr2=int(CdRr2*1000+0.5)/1000.;
  CdRr2_er=int(CdRr2_er*1000+0.5)/1000.;
  Float_t C_chi2_dRr2=int(ldRr2->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRr2=ldRr2->GetNDF();

  TPaveText *PdRr2 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PdRr2->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRr2->SetTextAlign(12); 
  curstr="Sherpa 1.4/Unfolded exp.";
  PdRr2->AddText(curstr.c_str());
  curstr="C="+str(CdRr2)+"#pm"+str(CdRr2_er)+", #chi^{2}/ndf="+str(C_chi2_dRr2)+"/"+str(C_ndf_dRr2)+"    ";
  PdRr2->AddText(curstr.c_str());
  PdRr2->Draw();
  filestr=dirstr+"hdRr2.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
   
  //Sherpa new
  hdR3=(TH1D *)hdR0->Clone("hdR3");
  hdR3->SetTitle("Sherpa 2.21 (rivet)");
  hdR3->Reset();
  t3->Draw("dR>>hdR3","W*Wfile");
  N3=hdR3->Integral(1,ndR);
  hdRr3=(TH1D *)hdR3->Clone("hdRr3");
  hdRr3->Divide(hdR0);
  
  hdR0->GetXaxis()->SetTitle("dR");
  hdR3->SetMarkerStyle(22);
  hdR3->SetMarkerColor(kBlack);
  hdR3->SetLineColor(kBlack);
  hdR0->Draw("");
  hdR3->Draw("same");
  hdR0->SetMarkerStyle(23);
  hdR0->SetMarkerColor(kRed);
  TLegend *LdR3 = new TLegend(0.67,0.7,0.88,0.85);
  LdR3->AddEntry(hdR3,"Sherpa 2.21 (rivet)","PE");
  LdR3->AddEntry(hdR0,"Exp. Unfolded","PE");
  LdR3->Draw();
  filestr=dirstr+"hdR3.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());

  hdRr3->GetXaxis()->SetTitle("dR");
  hdRr3->SetMarkerStyle(22);
  hdRr3->SetMarkerColor(kBlack);
  hdRr3->SetLineColor(kBlack);
  hdRr3->GetYaxis()->SetRangeUser(0.4,1.6);
  hdRr3->Draw();
   
  TF1 *ldRr3 = new TF1("ldRr3","[0]", 0, 1);
  ldRr3->SetParName(0,"CdRr3");
  hdRr3->Fit(ldRr3);
  Double_t CdRr3=ldRr3->GetParameter(0);
  Double_t CdRr3_er=ldRr3->GetParError(0);
  CdRr3=int(CdRr3*1000+0.5)/1000.;
  CdRr3_er=int(CdRr3_er*1000+0.5)/1000.;
  Float_t C_chi2_dRr3=int(ldRr3->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRr3=ldRr3->GetNDF();
  
  TPaveText *PdRr3 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PdRr3->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRr3->SetTextAlign(12); 
  curstr="Sherpa 2.21/Unfolded exp.";
  PdRr3->AddText(curstr.c_str());
  curstr="C="+str(CdRr3)+"#pm"+str(CdRr3_er)+", #chi^{2}/ndf="+str(C_chi2_dRr3)+"/"+str(C_ndf_dRr3)+"    ";
  PdRr3->AddText(curstr.c_str());
  PdRr3->Draw();
  filestr=dirstr+"hdRr3.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str()); 

  //Alpgen Pythia
  hdR4=(TH1D *)hdR0->Clone("hdR4");
  hdR4->SetTitle("AlpgenPythia (rivet)");
  hdR4->Reset();
  t4->Draw("dR>>hdR4","W*Wfile");
  N4=hdR4->Integral(1,ndR);
  hdRr4=(TH1D *)hdR4->Clone("hdRr4");
  hdRr4->Divide(hdR0);

  hdR0->GetXaxis()->SetTitle("dR");
  hdR4->SetMarkerStyle(22);
  hdR4->SetMarkerColor(kBlack);
  hdR4->SetLineColor(kBlack);
  hdR0->Draw("");
  hdR4->Draw("same");
  hdR0->SetMarkerStyle(23);
  hdR0->SetMarkerColor(kRed);
  TLegend *LdR4 = new TLegend(0.67,0.7,0.88,0.85);
  LdR4->AddEntry(hdR4,"AlpgenPythia (rivet)","PE");
  LdR4->AddEntry(hdR0,"Exp. Unfolded","PE");
  LdR4->Draw();
  filestr=dirstr+"hdR4.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
  hdRr4->GetXaxis()->SetTitle("dR");
  hdRr4->SetMarkerStyle(22);
  hdRr4->SetMarkerColor(kBlack);
  hdRr4->SetLineColor(kBlack);
  hdRr4->GetYaxis()->SetRangeUser(0.6,1.8);
  hdRr4->Draw();
 
  TF1 *ldRr4 = new TF1("ldRr4","[0]", 0, 1);
  ldRr4->SetParName(0,"CdRr4");
  hdRr4->Fit(ldRr4);
  Double_t CdRr4=ldRr4->GetParameter(0);
  Double_t CdRr4_er=ldRr4->GetParError(0);
  CdRr4=int(CdRr4*1000+0.5)/1000.;
  CdRr4_er=int(CdRr4_er*1000+0.5)/1000.;
  Float_t C_chi2_dRr4=int(ldRr4->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRr4=ldRr4->GetNDF();

  TPaveText *PdRr4 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PdRr4->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRr4->SetTextAlign(12); 
  curstr="AlpgenPythia/Unfolded exp.";
  PdRr4->AddText(curstr.c_str());
  curstr="C="+str(CdRr4)+"#pm"+str(CdRr4_er)+", #chi^{2}/ndf="+str(C_chi2_dRr4)+"/"+str(C_ndf_dRr4)+"    ";
  PdRr4->AddText(curstr.c_str());
  PdRr4->Draw();
  filestr=dirstr+"hdRr4.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
    
  //Alpgen Jimmy
  hdR5=(TH1D *)hdR0->Clone("hdR5");
  hdR5->SetTitle("AlpgenJimmy (rivet)");
  hdR5->Reset();
  t5->Draw("dR>>hdR5","W*Wfile");
  N5=hdR5->Integral(1,ndR);
  hdRr5=(TH1D *)hdR5->Clone("hdRr5");
  hdRr5->Divide(hdR0);
   
  hdR0->GetXaxis()->SetTitle("dR");
  hdR5->SetMarkerStyle(22);
  hdR5->SetMarkerColor(kBlack);
  hdR5->SetLineColor(kBlack);
  hdR0->Draw("");
  hdR5->Draw("same");
  hdR0->SetMarkerStyle(23);
  hdR0->SetMarkerColor(kRed);
  TLegend *LdR5 = new TLegend(0.67,0.7,0.88,0.85);
  LdR5->AddEntry(hdR5,"AlpgenJimmy (rivet)","PE");
  LdR5->AddEntry(hdR0,"Exp. Unfolded","PE");
  LdR5->Draw();
  filestr=dirstr+"hdR5.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
  hdRr5->GetXaxis()->SetTitle("dR");
  hdRr5->SetMarkerStyle(22);
  hdRr5->SetMarkerColor(kBlack);
  hdRr5->SetLineColor(kBlack);
  hdRr5->GetYaxis()->SetRangeUser(0.4,1.6);
  hdRr5->Draw();
 
  TF1 *ldRr5 = new TF1("ldRr5","[0]", 0, 1);
  ldRr5->SetParName(0,"CdRr5");
  hdRr5->Fit(ldRr5);
  Double_t CdRr5=ldRr5->GetParameter(0);
  Double_t CdRr5_er=ldRr5->GetParError(0);
  CdRr5=int(CdRr5*1000+0.5)/1000.;
  CdRr5_er=int(CdRr5_er*1000+0.5)/1000.;
  Float_t C_chi2_dRr5=int(ldRr5->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRr5=ldRr5->GetNDF();

  TPaveText *PdRr5 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PdRr5->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRr5->SetTextAlign(12); 
  curstr="AlpgenJimmy/Unfolded exp.";
  PdRr5->AddText(curstr.c_str());
  curstr="C="+str(CdRr5)+"#pm"+str(CdRr5_er)+", #chi^{2}/ndf="+str(C_chi2_dRr5)+"/"+str(C_ndf_dRr5)+"    ";
  PdRr5->AddText(curstr.c_str());
  PdRr5->Draw();
  filestr=dirstr+"hdRr5.eps";

  //Sherpa 2.24
  hdR6=(TH1D *)hdR0->Clone("hdR6");
  hdR6->SetTitle("Sherpa 2.24 (rivet)");
  hdR6->Reset();
  t6->Draw("dR>>hdR6","W*Wfile");
  N6=hdR6->Integral(1,ndR);
  hdRr6=(TH1D *)hdR6->Clone("hdRr6");
  hdRr6->Divide(hdR0);
   
  hdR0->GetXaxis()->SetTitle("dR");
  hdR6->SetMarkerStyle(22);
  hdR6->SetMarkerColor(kBlack);
  hdR6->SetLineColor(kBlack);
  hdR0->Draw("");
  hdR6->Draw("same");
  hdR0->SetMarkerStyle(23);
  hdR0->SetMarkerColor(kRed);
  TLegend *LdR6 = new TLegend(0.67,0.7,0.88,0.85);
  LdR6->AddEntry(hdR6,"Sherpa 2.24 (rivet)","PE");
  LdR6->AddEntry(hdR0,"Exp. Unfolded","PE");
  LdR6->Draw();
  filestr=dirstr+"hdR6.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
  hdRr6->GetXaxis()->SetTitle("dR");
  hdRr6->SetMarkerStyle(22);
  hdRr6->SetMarkerColor(kBlack);
  hdRr6->SetLineColor(kBlack);
  hdRr6->GetYaxis()->SetRangeUser(0.4,1.6);
  hdRr6->Draw();
 
  TF1 *ldRr6 = new TF1("ldRr6","[0]", 0, 1);
  ldRr6->SetParName(0,"CdRr6");
  hdRr6->Fit(ldRr6);
  Double_t CdRr6=ldRr6->GetParameter(0);
  Double_t CdRr6_er=ldRr6->GetParError(0);
  CdRr6=int(CdRr6*1000+0.5)/1000.;
  CdRr6_er=int(CdRr6_er*1000+0.5)/1000.;
  Float_t C_chi2_dRr6=int(ldRr6->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRr6=ldRr6->GetNDF();

  TPaveText *PdRr6 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PdRr6->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRr6->SetTextAlign(12); 
  curstr="Sherpa 2.24/Unfolded exp.";
  PdRr6->AddText(curstr.c_str());
  curstr="C="+str(CdRr6)+"#pm"+str(CdRr6_er)+", #chi^{2}/ndf="+str(C_chi2_dRr6)+"/"+str(C_ndf_dRr6)+"    ";
  PdRr6->AddText(curstr.c_str());
  PdRr6->Draw();
  filestr=dirstr+"hdRr6.eps";

  //PowhegPythia8 (One #gamma)
  hdR7=(TH1D *)hdR0->Clone("hdR7");
  hdR7->SetTitle("PowhegPythia8 (One #gamma) (rivet)");
  hdR7->Reset();
  t7->Draw("dR>>hdR7","W*Wfile");
  N7=hdR7->Integral(1,ndR);
  hdRr7=(TH1D *)hdR7->Clone("hdRr7");
  hdRr7->Divide(hdR0);
  
  hdR0->GetXaxis()->SetTitle("dR");
  hdR7->SetMarkerStyle(22);
  hdR7->SetMarkerColor(kBlack);
  hdR7->SetLineColor(kBlack);
  hdR0->Draw("");
  hdR7->Draw("same");
  hdR0->SetMarkerStyle(23);
  hdR0->SetMarkerColor(kRed);
  TLegend *LdR7 = new TLegend(0.67,0.7,0.88,0.85);
  LdR7->AddEntry(hdR7,"PowhegPythia8 (One #gamma) (rivet)","PE");
  LdR7->AddEntry(hdR0,"Exp. Unfolded","PE");
  LdR7->Draw();
  filestr=dirstr+"hdR7.eps";
  if (Smakepics==1) cdR->Print(filestr.c_str());
  hdRr7->GetXaxis()->SetTitle("dR");
  hdRr7->SetMarkerStyle(22);
  hdRr7->SetMarkerColor(kBlack);
  hdRr7->SetLineColor(kBlack);
  hdRr7->GetYaxis()->SetRangeUser(0.4,1.6);
  hdRr7->Draw();
 
  TF1 *ldRr7 = new TF1("ldRr7","[0]", 0, 1);
  ldRr7->SetParName(0,"CdRr7");
  hdRr7->Fit(ldRr7);
  Double_t CdRr7=ldRr7->GetParameter(0);
  Double_t CdRr7_er=ldRr7->GetParError(0);
  CdRr7=int(CdRr7*1000+0.5)/1000.;
  CdRr7_er=int(CdRr7_er*1000+0.5)/1000.;
  Float_t C_chi2_dRr7=int(ldRr7->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_dRr7=ldRr7->GetNDF();

  TPaveText *PdRr7 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PdRr7->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PdRr7->SetTextAlign(12); 
  curstr="PowhegPythia8 (One #gamma)/Unfolded exp.";
  PdRr7->AddText(curstr.c_str());
  curstr="C="+str(CdRr7)+"#pm"+str(CdRr7_er)+", #chi^{2}/ndf="+str(C_chi2_dRr7)+"/"+str(C_ndf_dRr7)+"    ";
  PdRr7->AddText(curstr.c_str());
  PdRr7->Draw();
  filestr=dirstr+"hdRr7.eps";
  
  if (Smakepics==1) cdR->Print(filestr.c_str());
  if (Smakehist==1) {
    fileout->cd();     
    hdRt1->Write();
    hdRt2->Write();
    hdR0->Write();
    hdR1->Write();
    hdR2->Write();
    hdR3->Write();
    hdR4->Write();
    hdR5->Write();
    hdR6->Write();
    hdR7->Write();
  }


#endif  //BEGIN_Unfolding_dR

#define BEGIN_Unfolding_Qcl 1
#ifdef BEGIN_Unfolding_Qcl 
  switch (decaytype){
  case Z2EEG:
    filestr="unfolding/unfolding5/PowHeg_EE_Qcl_New5/Unfolding.Test.root";
    break;
  case Z2MUMUG:
    filestr="unfolding/unfolding5/PowHeg_MM_Qcl_New5/Unfolding.Test.root";
    break;
  }
  TFile *FQcl0 =  new TFile(filestr.c_str(),"FQcl0");
  TDirectory *DQcl0=(TDirectory *)FQcl0->Get("Final");
  hQcl0=(TH1D *)DQcl0->Get("Final_Nominal_Unfolded")->Clone("hQcl0");
  hQcl0->SetTitle("Exp. Unfolded");
  hQclEr1=(TH1D *)DQcl0->Get("Final_Statistical_Uncertainty");
  hQclEr2=(TH1D *)DQcl0->Get("Final_SystematicUncertaintyCombined");
  TDirectory *DQclt0=(TDirectory *)FQcl0->Get("Nominal");
  hQclt1=(TH1D *)DQclt0->Get("Nominal_MCTruth")->Clone("hQclt1");
  hQclt1->SetTitle("PowhegPythia8 (analysis)");
  hQclt1->Sumw2();
  hQclt1->Scale(WfilePP); 

  switch (decaytype){
  case Z2EEG:
    filestr="unfolding/unfolding5/Sherpa_EE_Qcl_New5/Unfolding.Test.root";
    break;
  case Z2MUMUG:
    filestr="unfolding/unfolding5/Sherpa_MM_Qcl_New5/Unfolding.Test.root";
    break;
  }
  TFile *FQclS0 =  new TFile(filestr.c_str(),"FQclS");
  TDirectory *DQclS0=(TDirectory *)FQclS0->Get("Nominal");
  hQclt2=(TH1D *)DQclS0->Get("Nominal_MCTruth")->Clone("hQclt2"); 
  hQclt2->SetTitle("Sherpa 1.4 (analysis)");
  hQclt2->Sumw2();
  hQclt2->Scale(WfileSherpa); 

  cQcl=(TCanvas *)gROOT->FindObject("cQcl");

  if (!cQcl)  cQcl=new TCanvas("cQcl","cQcl",800.,600.);
  cQcl->cd();
  nQcl=hQcl0->GetNbinsX();
  N0=hQcl0->Integral(1,nQcl);
  cout << "N0=" << N0 << endl;

  for (int i=1;i<=nQcl;i++) {
    Double_t Err1=hQclEr1->GetBinContent(i);
    Double_t Err2=hQclEr2->GetBinContent(i);
    Double_t Err=TMath::Sqrt(Err1*Err1+Err2*Err2)*N0;
    hQcl0->SetBinError(i,Err);
  }
  
  //PowhegPythia8
  hQcl1=(TH1D *)hQcl0->Clone("hQcl1");
  hQcl1->SetTitle("PowhegPythia8 (rivet)");
  hQcl1->Reset();
  t1->Draw("Qcl>>hQcl1","W*Wfile");
  N1=hQcl1->Integral(1,nQcl);
  cout << "N1=" << N1 << endl;
  hQclr1=(TH1D *)hQcl1->Clone("hQclr1");
  hQclr1->Divide(hQcl0);
  hQcltr1=(TH1D *)hQcl1->Clone("hQcltr1");
  hQcltr1->Divide(hQclt1);

  hQcl1->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcl1->SetMarkerStyle(22);
  hQcl1->SetMarkerColor(kBlack);
  hQcl1->SetLineColor(kBlack);
  hQcl1->Draw();
  hQcl0->Draw("same");
  hQcl0->SetMarkerStyle(23);
  hQcl0->SetMarkerColor(kRed);
  hQclt1->SetMarkerStyle(1);
  hQclt1->SetMarkerColor(kBlue);
  hQclt1->Draw("same HIST");

  TLegend *LQcl1 = new TLegend(0.67,0.7,0.88,0.85);
  LQcl1->AddEntry(hQcl1,"PowhegPythia8 (rivet)","PE");
  LQcl1->AddEntry(hQcl0,"Exp. Unfolded","PE");
  LQcl1->AddEntry(hQclt1,"PowhegPythia8 (analysis)","PE");
  LQcl1->Draw();
  filestr=dirstr+"hQcl1.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());

  hQclr1->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQclr1->SetMarkerStyle(23);
  hQclr1->SetMarkerColor(kRed);
  hQclr1->SetLineColor(kRed);
  hQclr1->GetYaxis()->SetRangeUser(0.4,1.6);
  hQclr1->Draw();
  hQcltr1->Draw("same");

  TF1 *lQclr1 = new TF1("lQclr1","[0]", 0, 1);
  lQclr1->SetParName(0,"CQclr1");
  hQclr1->Fit(lQclr1,"","",0.,60.);
  Double_t CQclr1=lQclr1->GetParameter(0);
  Double_t CQclr1_er=lQclr1->GetParError(0);
  CQclr1=int(CQclr1*1000+0.5)/1000.;
  CQclr1_er=int(CQclr1_er*1000+0.5)/1000.;
  Float_t C_chi2_Qclr1=int(lQclr1->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qclr1=lQclr1->GetNDF();

  TPaveText *PQclr1 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQclr1->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQclr1->SetTextAlign(12); 
  curstr="PowhegPythia8/Unfolded exp.";
  PQclr1->AddText(curstr.c_str());
  curstr="C="+str(CQclr1)+"#pm"+str(CQclr1_er)+", #chi^{2}/ndf="+str(C_chi2_Qclr1)+"/"+str(C_ndf_Qclr1)+"    ";
  PQclr1->AddText(curstr.c_str());
  PQclr1->Draw();
  TLegend *LQclr1 = new TLegend(0.33,0.15,0.55,0.35);
  LQclr1->AddEntry(hQclr1,"#frac{rivet}{Exp. Unfolded} ","PE");
  LQclr1->AddEntry(hQcltr1,"#frac{rivet}{analysis} (sim)","PE");
  LQclr1->Draw();

  filestr=dirstr+"hQclr1.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
  
  //Sherpa old
  hQcl2=(TH1D *)hQcl0->Clone("hQcl2");
  hQcl2->SetTitle("Sherpa 1.4 (rivet)");
  hQcl2->Reset();
  t2->Draw("Qcl>>hQcl2","W*Wfile");
  N2=hQcl2->Integral(1,nQcl);
  cout << "N2=" << N2 << endl;
  hQclr2=(TH1D *)hQcl2->Clone("hQclr2");
  hQclr2->Divide(hQcl0);
  hQcltr2=(TH1D *)hQcl2->Clone("hQcltr2");
  hQcltr2->Divide(hQclt2);

  //simulation comparision
  //PowhegPythia8 rivet/analysis
  
  hQcltr1->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcltr1->SetMarkerStyle(22);
  hQcltr1->SetMarkerColor(kBlack);
  hQcltr1->SetLineColor(kBlack);
  hQcltr1->GetYaxis()->SetRangeUser(0.9,1.1);
  hQcltr1->Draw();

  TF1 *lQcltr1 = new TF1("lQcltr1","[0]", 0, 1);
  lQcltr1->SetParName(0,"CQcltr1");
  hQcltr1->Fit(lQcltr1,"","",0.,60.);
  Double_t CQcltr1=lQcltr1->GetParameter(0);
  Double_t CQcltr1_er=lQcltr1->GetParError(0);
  CQcltr1=int(CQcltr1*1000+0.5)/1000.;
  CQcltr1_er=int(CQcltr1_er*1000+0.5)/1000.;
  Float_t C_chi2_Qcltr1=int(lQcltr1->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qcltr1=lQcltr1->GetNDF();

  TPaveText *PQcltr1 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQcltr1->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQcltr1->SetTextAlign(12); 
  curstr="PowhegPythia8 (Rivet/analysis)";
  PQcltr1->AddText(curstr.c_str());
  curstr="C="+str(CQcltr1)+"#pm"+str(CQcltr1_er)+", #chi^{2}/ndf="+str(C_chi2_Qcltr1)+"/"+str(C_ndf_Qcltr1)+"    ";
  PQcltr1->AddText(curstr.c_str());
  PQcltr1->Draw();
  filestr=dirstr+"hQcltr1.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
   
  //PowhegPythia vs Sherpa
  hQcltr12=(TH1D *)hQcltr1->Clone("hQcltr12");
  hQcltr12->Divide(hQcltr2);
  hQcltr12->SetLineColor(kBlack);
  hQcltr12->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcltr12->GetYaxis()->SetRangeUser(0.8,1.2);
  hQcltr12->Draw();
  TF1 *lQcltr12 = new TF1("lQcltr12","[0]", 0, 1);
  lQcltr12->SetParName(0,"CQcltr12");
  hQcltr12->Fit(lQcltr12);//,"","",5.,50.);
  Double_t CQcltr12=lQcltr12->GetParameter(0);
  Double_t CQcltr12_er=lQcltr12->GetParError(0);
  CQcltr12=int(CQcltr12*1000+0.5)/1000.;
  CQcltr12_er=int(CQcltr12_er*1000+0.5)/1000.;
  Float_t C_chi2_Qcltr12=int(lQcltr12->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qcltr12=lQcltr12->GetNDF();

  TPaveText *PQcltr12 = new TPaveText(0.15,0.65,0.5,0.88,"NDC");
  PQcltr12->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQcltr12->SetTextAlign(12); 
  curstr="#frac{PowhegPythia8 (Rivet/analysis)}{Sherpa 1.4 (Rivet/analysis)}";
  PQcltr12->AddText(curstr.c_str());
  curstr="C="+str(CQcltr12)+"#pm"+str(CQcltr12_er)+", #chi^{2}/ndf="+str(C_chi2_Qcltr12)+"/"+str(C_ndf_Qcltr12)+"    ";
  PQcltr12->AddText(curstr.c_str());
  PQcltr12->Draw();
  filestr=dirstr+"hQcltr12.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
 
  //Sherpa old comparision
  hQcl2->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcl2->SetMarkerStyle(22);
  hQcl2->SetMarkerColor(kBlack);
  hQcl2->SetLineColor(kBlack);
  hQcl2->Draw();
  hQcl0->Draw("same");
  hQcl0->SetMarkerStyle(23);
  hQcl0->SetMarkerColor(kRed);
  TLegend *LQcl2 = new TLegend(0.67,0.7,0.88,0.85);
  LQcl2->AddEntry(hQcl2,"Sherpa 1.4 (rivet)","PE");
  LQcl2->AddEntry(hQcl0,"Exp. Unfolded","PE");
  LQcl2->Draw();
  filestr=dirstr+"hQcl2.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());

  hQclr2->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQclr2->SetMarkerStyle(22);
  hQclr2->SetMarkerColor(kBlack);
  hQclr2->SetLineColor(kBlack);
  hQclr2->GetYaxis()->SetRangeUser(0.6,1.8);
  hQclr2->Draw();
 
  TF1 *lQclr2 = new TF1("lQclr2","[0]", 0, 1);
  lQclr2->SetParName(0,"CQclr2");
  hQclr2->Fit(lQclr2,"","",0.,60.);
  Double_t CQclr2=lQclr2->GetParameter(0);
  Double_t CQclr2_er=lQclr2->GetParError(0);
  CQclr2=int(CQclr2*1000+0.5)/1000.;
  CQclr2_er=int(CQclr2_er*1000+0.5)/1000.;
  Float_t C_chi2_Qclr2=int(lQclr2->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qclr2=lQclr2->GetNDF();

  TPaveText *PQclr2 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQclr2->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQclr2->SetTextAlign(12); 
  curstr="Sherpa 1.4/Unfolded exp.";
  PQclr2->AddText(curstr.c_str());
  curstr="C="+str(CQclr2)+"#pm"+str(CQclr2_er)+", #chi^{2}/ndf="+str(C_chi2_Qclr2)+"/"+str(C_ndf_Qclr2)+"    ";
  PQclr2->AddText(curstr.c_str());
  PQclr2->Draw();
  filestr=dirstr+"hQclr2.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
   
  //Sherpa new
  hQcl3=(TH1D *)hQcl0->Clone("hQcl3");
  hQcl3->SetTitle("Sherpa 2.21 (rivet)");
  hQcl3->Reset();
  t3->Draw("Qcl>>hQcl3","W*Wfile");
  N3=hQcl3->Integral(1,nQcl);
  cout << "N3=" << N3 << endl;
  hQclr3=(TH1D *)hQcl3->Clone("hQclr3");
  hQclr3->Divide(hQcl0);

  hQcl0->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcl3->SetMarkerStyle(22);
  hQcl3->SetMarkerColor(kBlack);
  hQcl3->SetLineColor(kBlack);
  hQcl0->Draw("");
  hQcl3->Draw("same");
  hQcl0->SetMarkerStyle(23);
  hQcl0->SetMarkerColor(kRed);
  TLegend *LQcl3 = new TLegend(0.67,0.7,0.88,0.85);
  LQcl3->AddEntry(hQcl3,"Sherpa 2.21 (rivet)","PE");
  LQcl3->AddEntry(hQcl0,"Exp. Unfolded","PE");
  LQcl3->Draw();
  filestr=dirstr+"hQcl3.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());

  hQclr3->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQclr3->SetMarkerStyle(22);
  hQclr3->SetMarkerColor(kBlack);
  hQclr3->SetLineColor(kBlack);
  hQclr3->GetYaxis()->SetRangeUser(0.4,1.6);
  hQclr3->Draw();

   
  TF1 *lQclr3 = new TF1("lQclr3","[0]", 0, 1);
  lQclr3->SetParName(0,"CQclr3");
  hQclr3->Fit(lQclr3,"","",0.,60.);
  Double_t CQclr3=lQclr3->GetParameter(0);
  Double_t CQclr3_er=lQclr3->GetParError(0);
  CQclr3=int(CQclr3*1000+0.5)/1000.;
  CQclr3_er=int(CQclr3_er*1000+0.5)/1000.;
  Float_t C_chi2_Qclr3=int(lQclr3->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qclr3=lQclr3->GetNDF();

  TPaveText *PQclr3 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQclr3->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQclr3->SetTextAlign(12); 
  curstr="Sherpa 2.21/Unfolded exp.";
  PQclr3->AddText(curstr.c_str());
  curstr="C="+str(CQclr3)+"#pm"+str(CQclr3_er)+", #chi^{2}/ndf="+str(C_chi2_Qclr3)+"/"+str(C_ndf_Qclr3)+"    ";
  PQclr3->AddText(curstr.c_str());
  PQclr3->Draw();
  filestr=dirstr+"hQclr3.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str()); 

  //Alpgen Pythia
  hQcl4=(TH1D *)hQcl0->Clone("hQcl4");
  hQcl4->SetTitle("AlpgenPythia (rivet)");
  hQcl4->Reset();
  t4->Draw("Qcl>>hQcl4","W*Wfile");
  N4=hQcl4->Integral(1,nQcl);
  cout << "N4=" << N4 << endl;
  hQclr4=(TH1D *)hQcl4->Clone("hQclr4");
  hQclr4->Divide(hQcl0);

  hQcl0->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcl4->SetMarkerStyle(22);
  hQcl4->SetMarkerColor(kBlack);
  hQcl4->SetLineColor(kBlack);
  hQcl0->Draw("");
  hQcl4->Draw("same");
  hQcl0->SetMarkerStyle(23);
  hQcl0->SetMarkerColor(kRed);
  TLegend *LQcl4 = new TLegend(0.67,0.7,0.88,0.85);
  LQcl4->AddEntry(hQcl4,"AlpgenPythia (rivet)","PE");
  LQcl4->AddEntry(hQcl0,"Exp. Unfolded","PE");
  LQcl4->Draw();
  filestr=dirstr+"hQcl4.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
  hQclr4->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQclr4->SetMarkerStyle(22);
  hQclr4->SetMarkerColor(kBlack);
  hQclr4->SetLineColor(kBlack);
  hQclr4->GetYaxis()->SetRangeUser(0.6,1.8);
  hQclr4->Draw();
 
  TF1 *lQclr4 = new TF1("lQclr4","[0]", 0, 1);
  lQclr4->SetParName(0,"CQclr4");
  hQclr4->Fit(lQclr4,"","",0.,60.);
  Double_t CQclr4=lQclr4->GetParameter(0);
  Double_t CQclr4_er=lQclr4->GetParError(0);
  CQclr4=int(CQclr4*1000+0.5)/1000.;
  CQclr4_er=int(CQclr4_er*1000+0.5)/1000.;
  Float_t C_chi2_Qclr4=int(lQclr4->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qclr4=lQclr4->GetNDF();

  TPaveText *PQclr4 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQclr4->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQclr4->SetTextAlign(12); 
  curstr="AlpgenPythia/Unfolded exp.";
  PQclr4->AddText(curstr.c_str());
  curstr="C="+str(CQclr4)+"#pm"+str(CQclr4_er)+", #chi^{2}/ndf="+str(C_chi2_Qclr4)+"/"+str(C_ndf_Qclr4)+"    ";
  PQclr4->AddText(curstr.c_str());
  PQclr4->Draw();
  filestr=dirstr+"hQclr4.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
    
  //Alpgen Jimmy
  hQcl5=(TH1D *)hQcl0->Clone("hQcl5");
  hQcl5->SetTitle("AlpgenJimmy (rivet)");
  hQcl5->Reset();
  t5->Draw("Qcl>>hQcl5","W*Wfile");
  N5=hQcl5->Integral(1,nQcl);
  cout << "N5=" << N5 << endl;
  hQclr5=(TH1D *)hQcl5->Clone("hQclr5");
  hQclr5->Divide(hQcl0);
   
  hQcl0->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcl5->SetMarkerStyle(22);
  hQcl5->SetMarkerColor(kBlack);
  hQcl5->SetLineColor(kBlack);
  hQcl0->Draw("");
  hQcl5->Draw("same");
  hQcl0->SetMarkerStyle(23);
  hQcl0->SetMarkerColor(kRed);
  TLegend *LQcl5 = new TLegend(0.67,0.7,0.88,0.85);
  LQcl5->AddEntry(hQcl5,"AlpgenJimmy (rivet)","PE");
  LQcl5->AddEntry(hQcl0,"Exp. Unfolded","PE");
  LQcl5->Draw();
  filestr=dirstr+"hQcl5.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
  hQclr5->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQclr5->SetMarkerStyle(22);
  hQclr5->SetMarkerColor(kBlack);
  hQclr5->SetLineColor(kBlack);
  hQclr5->GetYaxis()->SetRangeUser(0.4,1.6);
  hQclr5->Draw();
 
  TF1 *lQclr5 = new TF1("lQclr5","[0]", 0, 1);
  lQclr5->SetParName(0,"CQclr5");
  hQclr5->Fit(lQclr5,"","",0.,60.);
  Double_t CQclr5=lQclr5->GetParameter(0);
  Double_t CQclr5_er=lQclr5->GetParError(0);
  CQclr5=int(CQclr5*1000+0.5)/1000.;
  CQclr5_er=int(CQclr5_er*1000+0.5)/1000.;
  Float_t C_chi2_Qclr5=int(lQclr5->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qclr5=lQclr5->GetNDF();

  TPaveText *PQclr5 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQclr5->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQclr5->SetTextAlign(12); 
  curstr="AlpgenJimmy/Unfolded exp.";
  PQclr5->AddText(curstr.c_str());
  curstr="C="+str(CQclr5)+"#pm"+str(CQclr5_er)+", #chi^{2}/ndf="+str(C_chi2_Qclr5)+"/"+str(C_ndf_Qclr5)+"    ";
  PQclr5->AddText(curstr.c_str());
  PQclr5->Draw();
  filestr=dirstr+"hQclr5.eps";

  //Sherpa 2.24
  hQcl6=(TH1D *)hQcl0->Clone("hQcl6");
  hQcl6->SetTitle("Sherpa 2.24 (rivet)");
  hQcl6->Reset();
  t6->Draw("Qcl>>hQcl6","W*Wfile");
  N6=hQcl6->Integral(1,nQcl);
  cout << "N6=" << N6 << endl;
  hQclr6=(TH1D *)hQcl6->Clone("hQclr6");
  hQclr6->Divide(hQcl0);
   
  hQcl0->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcl6->SetMarkerStyle(22);
  hQcl6->SetMarkerColor(kBlack);
  hQcl6->SetLineColor(kBlack);
  hQcl0->Draw("");
  hQcl6->Draw("same");
  hQcl0->SetMarkerStyle(23);
  hQcl0->SetMarkerColor(kRed);
  TLegend *LQcl6 = new TLegend(0.67,0.7,0.88,0.85);
  LQcl6->AddEntry(hQcl6,"Sherpa 2.24 (rivet)","PE");
  LQcl6->AddEntry(hQcl0,"Exp. Unfolded","PE");
  LQcl6->Draw();
  filestr=dirstr+"hQcl6.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
  hQclr6->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQclr6->SetMarkerStyle(22);
  hQclr6->SetMarkerColor(kBlack);
  hQclr6->SetLineColor(kBlack);
  hQclr6->GetYaxis()->SetRangeUser(0.4,1.6);
  hQclr6->Draw();
 
  TF1 *lQclr6 = new TF1("lQclr6","[0]", 0, 1);
  lQclr6->SetParName(0,"CQclr6");
  hQclr6->Fit(lQclr6,"","",0.,60.);
  Double_t CQclr6=lQclr6->GetParameter(0);
  Double_t CQclr6_er=lQclr6->GetParError(0);
  CQclr6=int(CQclr6*1000+0.5)/1000.;
  CQclr6_er=int(CQclr6_er*1000+0.5)/1000.;
  Float_t C_chi2_Qclr6=int(lQclr6->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qclr6=lQclr6->GetNDF();

  TPaveText *PQclr6 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQclr6->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQclr6->SetTextAlign(12); 
  curstr="Sherpa 2.24/Unfolded exp.";
  PQclr6->AddText(curstr.c_str());
  curstr="C="+str(CQclr6)+"#pm"+str(CQclr6_er)+", #chi^{2}/ndf="+str(C_chi2_Qclr6)+"/"+str(C_ndf_Qclr6)+"    ";
  PQclr6->AddText(curstr.c_str());
  PQclr6->Draw();
  filestr=dirstr+"hQclr6.eps";

  //PowhegPythia8 (One #gamma)
  hQcl7=(TH1D *)hQcl0->Clone("hQcl7");
  hQcl7->SetTitle("PowhegPythia8 (One #gamma) (rivet)");
  hQcl7->Reset();
  t7->Draw("Qcl>>hQcl7","W*Wfile");
  N7=hQcl7->Integral(1,nQcl);
  cout << "N7=" << N7 << endl;
  hQclr7=(TH1D *)hQcl7->Clone("hQclr7");
  hQclr7->Divide(hQcl0);
   
  hQcl0->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQcl7->SetMarkerStyle(22);
  hQcl7->SetMarkerColor(kBlack);
  hQcl7->SetLineColor(kBlack);
  hQcl0->Draw("");
  hQcl7->Draw("same");
  hQcl0->SetMarkerStyle(23);
  hQcl0->SetMarkerColor(kRed);
  TLegend *LQcl7 = new TLegend(0.67,0.7,0.88,0.85);
  LQcl7->AddEntry(hQcl7,"PowhegPythia8 (One #gamma) (rivet)","PE");
  LQcl7->AddEntry(hQcl0,"Exp. Unfolded","PE");
  LQcl7->Draw();
  filestr=dirstr+"hQcl7.eps";
  if (Smakepics==1) cQcl->Print(filestr.c_str());
  hQclr7->GetXaxis()->SetTitle("Q_{close}, GeV");
  hQclr7->SetMarkerStyle(22);
  hQclr7->SetMarkerColor(kBlack);
  hQclr7->SetLineColor(kBlack);
  hQclr7->GetYaxis()->SetRangeUser(0.4,1.6);
  hQclr7->Draw();
    
  TF1 *lQclr7 = new TF1("lQclr7","[0]", 0, 1);
  lQclr7->SetParName(0,"CQclr7");
  hQclr7->Fit(lQclr7,"","",0.,60.);
  Double_t CQclr7=lQclr7->GetParameter(0);
  Double_t CQclr7_er=lQclr7->GetParError(0);
  CQclr7=int(CQclr7*1000+0.5)/1000.;
  CQclr7_er=int(CQclr7_er*1000+0.5)/1000.;
  Float_t C_chi2_Qclr7=int(lQclr7->GetChisquare()*10+0.5)/10.;
  Int_t C_ndf_Qclr7=lQclr7->GetNDF();
  
  TPaveText *PQclr7 = new TPaveText(0.32,0.72,0.63,0.85,"NDC");
  PQclr7->SetFillColor(0);
  //1=left adjusted, 2=centered, 3=right adjusted
  //1=bottom adjusted, 2=centered, 3=top adjusted
  PQclr7->SetTextAlign(12); 
  curstr="PowhegPythia8 (One #gamma)/Unfolded exp.";
  PQclr7->AddText(curstr.c_str());
  curstr="C="+str(CQclr7)+"#pm"+str(CQclr7_er)+", #chi^{2}/ndf="+str(C_chi2_Qclr7)+"/"+str(C_ndf_Qclr7)+"    ";
  PQclr7->AddText(curstr.c_str());
  PQclr7->Draw();
  filestr=dirstr+"hQclr7.eps";
  
  if (Smakepics==1) cQcl->Print(filestr.c_str());
  if (Smakehist==1) {
    fileout->cd();     
    hQclt1->Write();
    hQclt2->Write();
    hQcl0->Write();
    hQcl1->Write();
    hQcl2->Write();
    hQcl3->Write();
    hQcl4->Write();
    hQcl5->Write();
    hQcl6->Write();
    hQcl7->Write();
  }


#endif  //BEGIN_Unfolding_Qcl

  if (Smakehist==1) {
    fileout->Write();
    fileout->Close();
  }

  return 1;
}

// divide error by \sqrt{2} -- for correlated histograms ratio
int LowerErrorsBySqrt2(TH1D* hist) {
  Int_t  n=hist->GetNbinsX();
  // divide error by \sqrt{2}
  for (int i=1;i<=n;i++) {
    Double_t Err=hist->GetBinError(i)/1.41421356237;
    hist->SetBinError(i,Err);
  }
  return 1;
}

// set sqr2 errors
int SubstractWithDNErrors(TH1D* hist1,TH1D* hist2) {
  return SubstractWithDNErrors(hist1,hist2,0);
}
int SubstractWithDNErrors(TH1D* hist1,TH1D* hist2,int ireverse) {
  Int_t  n=hist1->GetNbinsX();
  for (int i=1;i<=n;i++) {
    Double_t N1=hist1->GetBinContent(i);
    Double_t N2=hist2->GetBinContent(i);
    //cout << N1 << " " << N2 << endl;
    Double_t delta,deltaErr;
    if (ireverse==0) delta=N1-N2;
    else delta=N2-N1;
    if (delta>0) {
      hist1->SetBinContent(i,delta);      
      if (ireverse==2)
	deltaErr=TMath::Sqrt(N1+N2);
      else
	deltaErr=TMath::Sqrt(delta);
      hist1->SetBinError(i,deltaErr);
    }
    else {
      hist1->SetBinContent(i,0);
      hist1->SetBinError(i,0);      
    }
  }
  return 1;
}

int DivideHistWithoutErrorsChanges(TH1D* hist,TH1D* dhist,Double_t ScaleFactor) {
  Int_t  n=hist->GetNbinsX();
  for (int i=1;i<=n;i++) {
    Double_t N=hist->GetBinContent(i);
    Double_t Nerr=hist->GetBinError(i);
    Double_t dN=dhist->GetBinContent(i)*(1.-1./ScaleFactor);
    //    cout << N << " " << dN << " " << ScaleFactor << endl;
    if (dN>0) {
      hist->SetBinContent(i,N/dN);
      hist->SetBinError(i,Nerr/dN);
    }
    else {
      hist->SetBinContent(i,0);
      hist->SetBinError(i,0);
    }
  }
  return 1;

}


// histDiv=hist1/hist2 -> if hist1 and hist2 are strongly correlated then Errors could be estimate as sqrt(|hist1-hist2|)/max(hist1,hist2)
int setDiffErrors(TH1D* histDiv,TH1D* hist1,TH1D* hist2) {
  Int_t  n=histDiv->GetNbinsX();
  Double_t N1,N2,EstErr;
  for (int i=1;i<=n;i++) {
    N1=hist1->GetBinContent(i);
    N2=hist2->GetBinContent(i);
    if (N1>0||N2>0)
      EstErr=TMath::Sqrt(TMath::Abs(N1-N2))/TMath::Max(N1,N2);
    else EstErr=1;
    histDiv->SetBinError(i,EstErr);
  }
  return 1;
}
