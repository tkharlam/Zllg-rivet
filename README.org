** description
*** init
> source setuprivet_llyFF.sh
> source setupRivet.sh
*** compiling
make bin or source buildPlugin_llyFF.sh
*** running
> athena -l WARNING runRivetllyFF.py

examples of scripting trough runlocal.py could be found in runlocal.sh

*** ntuple merge
> source ~/bin/setupAtlas.sh
or setupATLAS 
> localSetupROOT 6.14.04-x86_64-slc6-gcc62-opt

examples of scripting trough treeupdate.py could be found in
treeupdate.sh

*** unfolded ntuple merge


> source ~/bin/setupAtlas.sh
or setupATLAS 
> localSetupROOT

examples of scripting trough treemerge.py could be found in
treemerge.sh

*** making pics
> root
root [0] .L reweight.cxx 
root [1] diff()

All sims for dressed/undressed leptons
root [0] .L reweight.cxx 
root [1] L=1;i=1;for (int j=1;j<=((i==1)?7:6);j++) {DressSwitch=L;diffForSim(i,j);}
root [2] .q // due to memory leak
root [0] .L reweight.cxx 
root [1] L=1;i=2;for (int j=1;j<=((i==1)?7:6);j++) {DressSwitch=L;diffForSim(i,j);}
root [2] .q 
root [0] .L reweight.cxx 
root [1] L=2;i=1;for (int j=1;j<=((i==1)?7:6);j++) {DressSwitch=L;diffForSim(i,j);}
root [2] .q
root [0] .L reweight.cxx 
root [1] L=2;i=2;for (int j=1;j<=((i==1)?7:6);j++) {DressSwitch=L;diffForSim(i,j);}
root [2] .q

Dressed with All photons and Prompt photons only comparison
root [0] .L cmp.cxx 
root [1] for (int j=1;j<=2;j++){for (int i=1;i<=2;i++){cmp(i,j);}}

All photons and Prompt photons only comparison
root [0] .L cmp-prompt.cxx 
root [1] for (int j=1;j<=2;j++){for (int i=1;i<=2;i++){cmp(i,j);}}

photoncounter==1
root [0] .L cmp-prompt-onephoton.cxx 
root [1] for (int j=1;j<=2;j++){for (int i=1;i<=2;i++){cmp(i,j);}}

Default vs Wrong Mass leptons typo comparison 
root [0] .L cmp-WrongMass.cxx 
root [1] for (int j=1;j<=2;j++){for (int i=1;i<=2;i++){cmp(i,j);}}

**** Comparing with MCTruth

root [0] .L reweight.cxx 
root [1] Kdiff()

*** mails
Tatiana Kharlamova
	
пн, 6 мая, 20:31 (15 часов назад)
	
кому: Evgenii
Евгений, здравствуйте!

Информация для перевзвешивания. Цифры ниже получены путем перевзвешивания моделирования Повхег+Фотос на трус уровне для достижения согласия на уровне реконструированного распределению по импульсу Z (итерациями).

20 бинов от 0 до 100 ГэВ
бины по Pt(Z) truth следующие:
0 , 5 , 10 , 15 , 20 , 25 , 30 , 35 , 40 , 45 , 50 , 55 , 60 , 65 , 70 , 75 , 80 , 85 , 90 , 95 ,

Для случая с дрессингом (без дрессинга пока не делала, но могу завтра сделать)

Pt(Z) truth (отобранное в наших условиях по трус переменным) для мюонов
0 , 117786 , 126868 , 80026 , 55902.5 , 40118.6 , 28856.2 , 22517.7 , 16657.5 , 12573.9 , 10619.6 , 9194.49 , 7254.19 , 5576.39 , 4321.51 , 3823.78 , 3026.77 , 2911.29 , 2238.43 , 1882
с нормировкой на полное число событий в гистограмме
0 , 0.21274 , 0.229142 , 0.144539 , 0.100968 , 0.0724603 , 0.0521187 , 0.0406703 , 0.0300859 , 0.0227103 , 0.0191806 , 0.0166066 , 0.0131021 , 0.0100718 , 0.00780529 , 0.00690632 , 0.00546679 , 0.00525823 , 0.00404294 , 0.00339917 

Pt(Z) truth для электронов
0 , 61840.1 , 67742.2 , 43573.8 , 29807.4 , 21337.8 , 15153.5 , 11135.6 , 8983.95 , 7235.85 , 5658.81 , 4498.52 , 3456.49 , 3044.35 , 2673.62 , 2026.55 , 1803.41 , 1722.67 , 1072.1 , 1012
нормир
0 , 0.209888 , 0.22992 , 0.147891 , 0.101168 , 0.0724216 , 0.0514318 , 0.0377946 , 0.0304919 , 0.0245588 , 0.0192063 , 0.0152682 , 0.0117315 , 0.0103327 , 0.00907439 , 0.00687822 , 0.00612086 , 0.0058468 , 0.00363874 , 0.00343477

Если будет удобнее, могу просто веса к моделированию Повхег напечатать.

Нужно перевзвесить следующие переменные:
Qp              = 0
 Qm              = 0
 ptZ             = 0
 dR              = 0 //min
 ptg             = 0 //фотон

Всего хорошего,
Татьяна

*** files
README.org
**** making pics
AtlasStyle.C 
AtlasStyle.h
num2str.C
reweight.cxx
**** running rivert
Makefile
buildPlugin_llyFF.sh
llyFFAnalysis.cc
runRivetllyFF-mc15electrons.py
runRivetllyFF-mc15muons.py
runRivetllyFF.py
setuprivet_llyFF.sh
** rivet
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/RivetForAtlas
** mails
*** mails from Kristin
Hi Evgenii,

probably it would be good to prepare a rivet analysis (would be also
very useful for the publication of the paper).

Rivet is described here: https://rivet.hepforge.org/

But to get you started, I put up a very basic Rivet Analysis here:

/afs/cern.ch/user/k/kristin/public/ForEvgenii

the steps to do are:

1) setup the code:
source setuprivet_llyFF.sh

(setup the Analysis release / athena version)

2) compile:

source buildPlugin_llyFF.sh

3) run:

athena runRivetllyFF.py

(you need to put in some EVNT files... that you can access.)

4) the actual code is here:

llyFFAnalysis.cc

it has a basic event selection (for WW), so this needs to be adapted,
but photons, electrons and muons are already setup.

and some histograms are filled.

(what is filled you need to adapt too, but since you have the basic
4-vectors should be ok).

Output is a root file (also some yoda file, which is the generic rivet
format, but to start with, Root is more convenient).

Let me know, if there are questions.

Cheers
Kristin


Hi,
thanks Frank!
Also, Evgenii, just to give a plan of action that would be easiest and fastest:

you have the truth level plots for some sample already using your
code, right?  The idea would be to run the rivet analysis with the
*same* binning and *same* selection on the EVNT files of that specific
data set (that you have already NTUPLES from) to validate the rivet
routine.  Afterwards, you can apply the rivet routine to the MC15 EVNT
new samples.

Best
Kristin

*** Tatiana Kharlamova
	
ср, 14 авг., 14:43 (23 часа назад)
	
кому: Евгений
Нужны такие моделирования с условием M(2l)>20 ГэВ:

1) Шерпа 2.2.4
/home/baldin/data/sample/EVNT/mc15_8TeV.364388.Sh_224_NN30NNLO_mumugamma.evgen.EVNT.e6994_tid15777202_00/
/home/baldin/data/sample/EVNT/mc15_8TeV.364387.Sh_224_NN30NNLO_eegamma.evgen.EVNT.e6994_tid15777197_00/

2) Повхег
/home/baldin/data/sample/EVNT/mc12_8TeV.207320.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon_ZCorr.evgen.EVNT.e5239/
/home/baldin/data/sample/EVNT/mc12_8TeV.207321.PowhegPythia8_AU2CT10_Zmumu_2Lepton1Photon_ZCorr.evgen.EVNT.e5239/

3) Новый фотос сингл
/net/nfs4/exports/spool/atlas/tkharlam/PowHegSingleNew/user.tkharlam.mc12_8TeV.129250.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon.ZCorr.Mod.test6_EXT1

** samples
*** Sherpa [2/2]
  * [X] mc12_8TeV.145161.Sherpa_CT10_eegammaPt10.evgen.EVNT.e1434 (265 GB)
  * [X] mc12_8TeV.145162.Sherpa_CT10_mumugammaPt10.evgen.EVNT.e1434 (167 GB)
*** PowhegPythia8 [3/6]
  * [X] mc12_8TeV.207320.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon_ZCorr.evgen.EVNT.e5239 (168 GB)
  * [X] mc12_8TeV.207321.PowhegPythia8_AU2CT10_Zmumu_2Lepton1Photon_ZCorr.evgen.EVNT.e5239 (169 GB)
  * [X] mc12_8TeV.129250.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon.evgen.EVNT.e1444
  * [ ] mc12_8TeV.129681.PowhegPythia8_AU2CT10_Zmumu_DiLeptonFilter.evgen.EVNT.e2095
  * [ ] mc12_8TeV.129686.PowhegPythia8_AU2CT10_Zmumu_Exactly1LeptonFilter.evgen.EVNT.e2095
  * [ ] mc12_8TeV.129697.PowhegPythia8_AU2CT10_Zmumu_LeptonVeto.evgen.EVNT.e2095

*** Sherpa mc15 [28/28]
  * [X] 'mc15_8TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.evgen.EVNT.e5345'
  * [X] 'mc15_8TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5345'
  * [X] 'mc15_8TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.evgen.EVNT.e5345'
  * [X] 'mc15_8TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.evgen.EVNT.e5375'
  * [X] 'mc15_8TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.evgen.EVNT.e5320'
  * [X] 'mc15_8TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.evgen.EVNT.e5320'

*** AlpgenPythia [12/12]

  * [X] mc12_8TeV.147105.AlpgenPythia_Auto_P2011C_ZeeNp0.evgen.EVNT.e1879
  * [X] mc12_8TeV.147106.AlpgenPythia_Auto_P2011C_ZeeNp1.evgen.EVNT.e1879
  * [X] mc12_8TeV.147107.AlpgenPythia_Auto_P2011C_ZeeNp2.evgen.EVNT.e1879
  * [X] mc12_8TeV.147108.AlpgenPythia_Auto_P2011C_ZeeNp3.evgen.EVNT.e1879
  * [X] mc12_8TeV.147109.AlpgenPythia_Auto_P2011C_ZeeNp4.evgen.EVNT.e1879
  * [X] mc12_8TeV.147110.AlpgenPythia_Auto_P2011C_ZeeNp5incl.evgen.EVNT.e1879

  * [X] mc12_8TeV.147113.AlpgenPythia_Auto_P2011C_ZmumuNp0.evgen.EVNT.e1880
  * [X] mc12_8TeV.147114.AlpgenPythia_Auto_P2011C_ZmumuNp1.evgen.EVNT.e1880
  * [X] mc12_8TeV.147115.AlpgenPythia_Auto_P2011C_ZmumuNp2.evgen.EVNT.e1880
  * [X] mc12_8TeV.147116.AlpgenPythia_Auto_P2011C_ZmumuNp3.evgen.EVNT.e1880
  * [X] mc12_8TeV.147117.AlpgenPythia_Auto_P2011C_ZmumuNp4.evgen.EVNT.e1880
  * [X] mc12_8TeV.147118.AlpgenPythia_Auto_P2011C_ZmumuNp5incl.evgen.EVNT.e1880

*** AlpgenJimmy [12/12] 

  * [X] mc12_8TeV.107650.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp0.evgen.EVNT.e1571
  * [X] mc12_8TeV.107651.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp1.evgen.EVNT.e1571
  * [X] mc12_8TeV.107652.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp2.evgen.EVNT.e1571
  * [X] mc12_8TeV.107653.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp3.evgen.EVNT.e1571
  * [X] mc12_8TeV.107654.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp4.evgen.EVNT.e1571
  * [X] mc12_8TeV.107655.AlpgenJimmy_AUET2CTEQ6L1_ZeeNp5.evgen.EVNT.e1571

  * [X] mc12_8TeV.107660.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp0.evgen.EVNT.e1571
  * [X] mc12_8TeV.107661.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp1.evgen.EVNT.e1571
  * [X] mc12_8TeV.107662.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp2.evgen.EVNT.e1571
  * [X] mc12_8TeV.107663.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp3.evgen.EVNT.e1571
  * [X] mc12_8TeV.107664.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp4.evgen.EVNT.e1571
  * [X] mc12_8TeV.107665.AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp5.evgen.EVNT.e1571

** git

https://gitlab.cern.ch/ebaldin/Zllg-Rivet

 Command line instructions
Git global setup

git config --global user.name "Evgenii Baldin"
git config --global user.email "evgenii.baldin@cern.ch"

Create a new repository

ssh: ssh://git@gitlab.cern.ch:7999/ebaldin/Zllg-Rivet.git
https: https://:@gitlab.cern.ch:8443/ebaldin/Zllg-Rivet.git

git clone ssh://git@gitlab.cern.ch:7999/ebaldin/Zllg-Rivet.git
cd Z2lg-analysis
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin https://:@gitlab.cern.ch:8443/ebaldin/Zllg-Rivet.git
git add .
git commit
git push -u origin master

** TODO
 * [X] add var for CMS
 * [X] add dR for CMS
 * [ ] add number of event (is it unique?)
 * [X] add new observables 
   + min(Qp, Qm)
   + angle(gamma,closest lepton)
   + dR
   + dRll
   + phi
   + Eg 
   + inv mass ee\gamma
   + inv(\ell\ell)
 * [ ] Add information about Z vertex
 * [ ] Select undressed by dressed (adding a marker 2
   dressed/undressed tries)
 * [ ] Check the differences between different dressing

** EWUnfolding (external)

https://twiki.cern.ch/twiki/bin/view/Main/AUThAtlasGroupPage#EWUnfolding
https://svnweb.cern.ch/trac/atlasphys-sm/browser/Physics/StandardModel/ElectroWeak/Analyses/EWUnfolding?order=name

#+BEGIN_SRC bash
svn co svn+ssh://ebaldin@svn.cern.ch/reps/atlasphys-sm/Physics/StandardModel/ElectroWeak/Analyses/EWUnfolding
# next steps only for compiling branch EWUnfolding-00-00-01-branch SLC6
# gcc 4.6
source /afs/cern.ch/sw/lcg/external/gcc/4.6.3/x86_64-slc6-gcc46-opt/setup.sh
# ROOT 5.34.03
cd /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.03/x86_64-slc6-gcc46-opt/root/
source bin/thisroot.sh
cd ~
cd EWUnfolding/branches/EWUnfolding-00-00-01-branch/Code/

# Compile RooUnfold:
cd RooUnfold
source setupRooUnfold.sh

# Set up RootCore:
cd external/RootCore
./configure

# Compile BootstrapGenerator using RootCore:
cd ..
source RootCore/scripts/setup.sh
RootCore/scripts/find_packages.sh  #do not source
RootCore/scripts/compile.sh        #do not source

# Compile EWUnfold:
cd ..
make

# Set LD_LIBRARY_PATH to use BOOTSTRAP
source external/RootCore/scripts/setup.sh
#+END_SRC
** comparison
*** Number of events



|                     | PowhegPythia8 Zeeg | PowhegPythia8 Zmumug | Sherpa Zeeg | Sherpa Zmumug |
|---------------------+--------------------+----------------------+-------------+---------------|
| analysis (ami EVNT) |            5000000 |              5000000 |     8844673 |       9188579 |
| *analysis (grid)*   |            4979985 |            *4859190* |     8844673 |       9188579 |
| *analysis (calc)*   |            4979985 |            *4994190* |     8844673 |       9188579 |
| analysis (files)    |            4995000 |              5000000 |     8850000 |       9190000 |
| rivet (ami)         |            4995000 |              5000000 |    13500000 |       8500000 |
| rivet (files)       |            4995000 |              5000000 |    13500000 |       8500000 |
| *rivet (calc)*      |            4995000 |              5000000 |    13500000 |       8500000 |

 * grid -- information from grid (Tatyana Kharlamova)
 * files -- number of files * number of event in one file (expected
   that in all files equal number of event)
 * ami -- information from ami
 * calc -- calculating number of event in 

The problem: PowhegPythia8 Zmumug *analysis (grid)* disagrees with
*analysis (calc)* so number of events could depend from the specific
iteration.

*** unfolding with and without background taking into account
PowHeg_EE_Qp_New4 vs PowHeg_EE_Qp_New4_nobkg
Sherpa_EE_Qp_New4 vs Sherpa_EE_Qp_New4_nobkg

$Z\to e^+e^-\gamma$

| PowhegPythia8 rivet/unfolded Exp. | chi^2/ndf | C             |
|-----------------------------------+-----------+---------------|
| with bkg. taking into account     | 107.4/34  | 0.93 ± 0.005  |
| without bkg. taking into account  | 90.7/34   | 0.909 ± 0.005 |


Иными словами если учесть подложку, то chi^2 ухудшается (91->107) и
абсолютный сдвиг отношения для ee\gamma уменьшается на 2% (-9% ->
-7%), но всё равно остаётся значительным.

** reweighting


Статья Даниэля

http://hepdata.cedar.ac.uk/view/ins1408516/all/short

Бины
dR	<Binning> 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.8, 3.2 </Binning> 

Qp <Binning> 4 , 6 , 8 , 10 , 12 , 14 , 16 , 18 , 20 , 22 , 24 , 26 , 28 , 30 , 32 , 34 , 36 , 38 , 40 , 42 , 44 , 46 , 48 , 50 , 52 , 54 , 56 , 58 , 60 , 62 , 64 , 66 , 68 , 70 , 72 , 74 , 76 , 78 , 80 , 82 , 84 , 90</Binning> 

PtG <Binning> 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40,  42, 44, 46 </Binning>

Нтаплы, полученные в анализе
/net/nfs5/exports/w2r6/atlas/tkharlam/simV10

Новое моделирование PhotosSingle в формате EVNT
mc12_8TeV.129252.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon_ZCorr_Mod.evgen.EVNT.e7027
Скачивается сюда
/net/nfs5/exports/w2r6/atlas/tkharlam/PowHegSingle

Директория на атласе:
/home/tkharlam/Z2l1g/PtZrew/

addPtZ_weight.C - программа по вычислению весов
PtZall.C - данные из статьи

P.S. Скопированы в Zllg-Rivet/docs/reweight/

Инструкция по превзвешиванию:
Основанна на приложенном файле.

1) включить файл с данными 2L (PtZall.C) например:
gROOT->ProcessLine(".x PtZall.C");


2) Создать гистограмму как в эксперименте 2L

double po[44];

    for( int i=0; i<43;i++){
        po[i]=p9030_d27x1y4_xval[i]-p9030_d27x1y4_xerrminus[i];
   };
    po[43]=900;

TH1F* q3 = new TH1F("q3","PtZ",43,po);

3) Заполнить q3 значениями PtZ в ГэВ!!!
Z2mu1g->Draw("PtZ/1000.0>>q3",

4) Вычислить интеграл от своей МС по бинам эксперимента:
  double Integral=0;
    for( int i=0; i<43;i++){

        binSize=p9030_d27x1y4_xerrminus[i]+p9030_d27x1y4_xerrplus[i];
        binCo=q3->GetBinContent(i+1);
        Integral = Integral +binCo;//*binSize;
};

5) Вычислить размер бина в 2L

binSize=p9030_d27x1y4_xerrminus[i]+p9030_d27x1y4_xerrplus[i];
i от 0 до 43

6) Вычислить значение вероятности в твоем МС
NewBinCo[i]=binCo[i]/(binSize*Integral);


7)  Сделать TGraph из массива NewBinCo и значений по х: p9030_d27x1y4_xval
TGraph* MyTGraph = TGraph(43, p9030_d27x1y4_xval, NewBinCo);

8) Сделать сплайн TSpline3* TSPtZmuMC = new TSpline3("#mu spline MC",&MyTGraph);

9)  Получить веса W = TSPtZmu->Eval(PtZ в ГэВ)/ TSPtZmuMC->Eval(PtZ в ГэВ);
TSPtZmu имеется в в файле который инклюдится

TSPtZmuMC нужно сделать по своей МС гистограмме PtZ.

Для электронов TSPtZe для него нужен свой TSPtZeMC.

С уважением, Харламов Алексей.


Статья Даниэля

http://hepdata.cedar.ac.uk/view/ins1408516/all/short

Бины
dR	<Binning> 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.8, 3.2 </Binning> 

Qp <Binning> 4 , 6 , 8 , 10 , 12 , 14 , 16 , 18 , 20 , 22 , 24 , 26 , 28 , 30 , 32 , 34 , 36 , 38 , 40 , 42 , 44 , 46 , 48 , 50 , 52 , 54 , 56 , 58 , 60 , 62 , 64 , 66 , 68 , 70 , 72 , 74 , 76 , 78 , 80 , 82 , 84 , 90</Binning> 

PtG <Binning> 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40,  42, 44, 46 </Binning>

Нтаплы, полученные в анализе
/net/nfs5/exports/w2r6/atlas/tkharlam/simV10

Новое моделирование PhotosSingle в формате EVNT
mc12_8TeV.129252.PowhegPythia8_AU2CT10_Zee_2Lepton1Photon_ZCorr_Mod.evgen.EVNT.e7027
Скачивается сюда
/net/nfs5/exports/w2r6/atlas/tkharlam/PowHegSingle

** Kharlamov analysis.c
*** trigger

#+BEGIN_SRC C++
    //tanya
    if (isDATA&& !mutrigger && !etrigger && !dimutrigger && !dietrigger && !muetrigger)  continue;
#+END_SRC

#+BEGIN_SRC C++
      int stat2 = doTr2lgNoDr(&dilepton, isDATA);
        stat = doTr2lg(&dilepton, isDATA);//truth
       if (stat>2) isSq=1;
       if (stat2>2) isSq2=1;

        ClearVariables_2(); //isSelected_t is not cleared here
        ClearVariables(); //isSelected_t is not cleared here
        
        treecurrent=tree2l1g;
       stat = do2mu1g(&dilepton, isDATA);
       if (stat==13) {
           doTr=1;
           //std::cout<<" isSel "<<isSelected_t<<" "<<isSq<<std::endl;
          doTr2lg(&dilepton, isDATA); 
	  treecurrent->Fill();
       };
        ClearVariables_2(); //isSelected_t is not cleared here
       ClearVariables(); //isSelected_t is not cleared here
       treecurrent=tree2l1g;
       stat = do2e1g(&dilepton, isDATA);
       if (stat==13){
           doTr=2;
	  doTr2lg(&dilepton, isDATA);
           treecurrent->Fill();
       };
       ClearVariables_2(); //isSelected_t is not cleared here
       ClearVariables(); //isSelected_t is not cleared here
        
        treecurrent=tree2l1g;
        stat2 = doTr2lgNoDr(&dilepton, isDATA);
        stat = doTr2lg(&dilepton, isDATA);
        if (stat==12 && doTr==0){
            treecurrent->Fill();
        };
        if (stat2==12 && doTr2==0 &&(stat != 12)){
            treecurrent->Fill();
        };
#+END_SRC

*** doTr2lg
#+BEGIN_SRC C++
int doTr2lg(DiLeptonClass *dilepton, bool isDATA)
{
/*  
 X_Weight_Event = 1.0;
  X_Weight_Event2 = 1.0;
  X_Weight_Eff = 1.0;
  X_Weight_EffP = 1.0;
  X_Weight_EffM = 1.0;
  X_Weight_MuPlus = 1.0;
  X_Weight_MuMinus = 1.0;
  X_Weight_Photon_1 = 1.0;
  X_Weight_Photon_2 = 1.0;
  X_Weight_UncMuPlus = 1.0;
  X_Weight_UncMuMinus = 1.0;
  X_Weight_UncPhoton_1 = 1.0;
  X_Weight_UncPhoton_2 = 1.0;
  X_Weight_Trig = 1.0;
  X_Weight_TrigUnc = 1.0;
  X_Weight_Trig2=1.0;
  X_Weight_TrigUnc2=1.0;
*/
    isSelected_t = 0;
    
    double maxPt11=0;
    double maxPt12=0;
    double maxPt21=0;
    double maxPt22=0;
    int tte1=0;
    int tte2=0;
    int ttm1=0;
    int ttm2=0;
    
    int ttPhot = 0;
    double PtPhot=0;
    
  for(int ttt=0;ttt<dilepton->mc_n; ttt++){  
  
      if(dilepton->mc_barcode->at(ttt) > 199999.0) continue;
      double Pt_cur = dilepton->mc_pt->at(ttt);
      double eta_cur = dilepton->mc_eta->at(ttt);
      if(dilepton->mc_status->at(ttt) != 1) continue;
      if(Pt_cur<4000) continue;
      
      if( (dilepton->mc_pdgId->at(ttt) == 22)&&(abs(eta_cur)<2.37)){
     
          if((PtPhot<Pt_cur)&&(!(eta_cur<1.52 && eta_cur>1.37))){
              ttPhot=ttt;
              PtPhot=Pt_cur;
          };
         
      };
      
     // double maxPt11,maxPt12,maxPt21,maxPt22;
      
      
      if((dilepton->mc_pdgId->at(ttt)==-11) &&(abs(eta_cur)<2.47)){
        if(maxPt11<Pt_cur){
            tte1=ttt;
            maxPt11=Pt_cur;
        };
      };
      if((dilepton->mc_pdgId->at(ttt)==11)&&(abs(eta_cur)<2.47)){
          if(maxPt12<Pt_cur){
              tte2=ttt;
              maxPt12=Pt_cur;
          };
          
      
      };
      
      if((dilepton->mc_pdgId->at(ttt)==-13)&&(abs(eta_cur)<2.7)){
      
          if(maxPt21<Pt_cur){
              ttm1=ttt;
              maxPt21=Pt_cur;
           };
          
      };
      if((dilepton->mc_pdgId->at(ttt)==13)&&(abs(eta_cur)<2.7)){
          if(maxPt22<Pt_cur){
              ttm2=ttt;
              maxPt22=Pt_cur;
          };
          
      };
      
      
  };
 
    int is = 0;
    if((tte1>0)&&(tte2>0)) is=1;
    
    if((ttm1>0)&&(ttm2>0)) is=2; //mmgamma
           
    
    if (is == 0) return 1;
    if(ttPhot==0) return 1;
    //if((tte1>0)&&(tte2>0)&&(ttm1>0)&&(ttm2>0)) std::cout<<"<<<<  2 pairs in event <<<<<<"<<std::endl;
    
   //dressing
    int m1_id=0;
    int m2_id=0;
    
    if(is == 1){
        m1_id = tte1;
        m2_id = tte2;
    };
    
    if(is == 2){
        m1_id = ttm1;
        m2_id = ttm2;
    };
    
    etaL_t[0]= dilepton->mc_eta->at(m1_id);
    etaL_t[1]= dilepton->mc_eta->at(m2_id);
    phiL_t[0]= dilepton->mc_phi->at(m1_id);
    phiL_t[1]= dilepton->mc_phi->at(m2_id);
    ptL_t[0]=  dilepton->mc_pt->at(m1_id);
    ptL_t[1]=  dilepton->mc_pt->at(m2_id);
    etaG_t[0]= dilepton->mc_eta->at(ttPhot);
          //etaG[1]= ;
    phiG_t[0]= dilepton->mc_phi->at(ttPhot);
          //phiG[1]= ;
    ptG_t[0]=  dilepton->mc_pt->at(ttPhot);
 /*   if(abs(etaG_t[0])>2.37) return 2;
    if((etaG_t[0]>1.37)&&(etaG_t[0]<1.52)) return 2;
    if(is==1){
    if(abs(etaL_t[0])>2.47) return 2;
    }else{
        
    };
   */ 
    
          etaL_t2[0]=etaL_t[0];
          etaL_t2[1]=etaL_t[1];
    //std::cout<<" here "<<etaL_t2[0]<<" "<<etaL_t[1]<<std::endl;
          phiL_t2[0]=phiL_t[0];
          phiL_t2[1]=phiL_t[1];
          ptL_t2[0]= ptL_t[0];
          ptL_t2[1]= ptL_t[1];
          etaG_t2[0]=etaG_t[0];
          //etaG[1]=//etaG[1];
          phiG_t2[0]=phiG_t[0];
          //phiG[1]=//phiG[1];
          ptG_t2[0]= ptG_t[0];
    
	  motherG_t[0]=0;// dilepton->ph_truth_mothertype->at(ttPhot);
          TLorentzVector* pi1 = new TLorentzVector();
          TLorentzVector* pi2 = new TLorentzVector();
          TLorentzVector* gam1 = new TLorentzVector();
    
          pi1->SetPtEtaPhiM(ptL_t[0],etaL_t[0],phiL_t[0],mumass);
          pi2->SetPtEtaPhiM(ptL_t[1],etaL_t[1],phiL_t[1],mumass);
          gam1->SetPtEtaPhiM(ptG_t[0],etaG_t[0],phiG_t[0],0.);
    
         TLorentzVector pi1d(1,1,1,1);
          TLorentzVector pi2d(1,1,1,1);
          TLorentzVector gam1d(1,1,1,1);
    
          pi1d.SetPtEtaPhiM(ptL_t[0],etaL_t[0],phiL_t[0],0.);
          pi2d.SetPtEtaPhiM(ptL_t[1],etaL_t[1],phiL_t[1], 0.);
          gam1d.SetPtEtaPhiM(ptG_t[0],etaG_t[0],phiG_t[0],0.);
   
    
      TLorentzVector* deltaI = new TLorentzVector();
      for(int ttt=0;ttt<dilepton->mc_n; ttt++){
     
          if(ttt==ttPhot) continue;
          if(dilepton->mc_status->at(ttt) != 1) continue;
          if(dilepton->mc_pdgId->at(ttt) != 22) continue;
          if(dilepton->mc_barcode->at(ttt) > 199999.0) continue;
          
          double iDPhi_1 = fabs(dilepton->mc_phi->at(ttt)-phiL_t[0]);
          if (iDPhi_1>pi) iDPhi_1=2*pi-iDPhi_1;
          double iDEta_1 = fabs(dilepton->mc_eta->at(ttt)-etaL_t[0]);
          
          double iDPhi_2 = fabs(dilepton->mc_phi->at(ttt)-phiL_t[1]);
          if (iDPhi_2>pi) iDPhi_2=2*pi-iDPhi_2;
          double iDEta_2 = fabs(dilepton->mc_eta->at(ttt)-etaL_t[1]);

       //   double iDPhi_3 = fabs(dilepton->mc_phi->at(ttt)-phiG_t[0]);
       //   if (iDPhi_3>pi) iDPhi_3=2*pi-iDPhi_3;
       //   double iDEta_3 = fabs(dilepton->mc_eta->at(ttt)-etaG_t[0]);
          
          
          double dRR1= sqrt(iDPhi_1*iDPhi_1+iDEta_1*iDEta_1);
          double dRR2= sqrt(iDPhi_2*iDPhi_2+iDEta_2*iDEta_2);
     //     double dRR3= sqrt(iDPhi_3*iDPhi_3+iDEta_3*iDEta_3);
          
          if(dRR1<0.1){
              deltaI->SetPtEtaPhiM(dilepton->mc_pt->at(ttt),dilepton->mc_eta->at(ttt),dilepton->mc_phi->at(ttt),0.);
              pi1d = pi1d+*deltaI;
          };
          if(dRR2<0.1){
              deltaI->SetPtEtaPhiM(dilepton->mc_pt->at(ttt),dilepton->mc_eta->at(ttt),dilepton->mc_phi->at(ttt),0.);
              pi2d = pi2d+*deltaI;
          };
          /*if(dRR3<0.1){
              deltaI->SetPtEtaPhiM(dilepton->mc_pt->at(ttt),dilepton->mc_eta->at(ttt),dilepton->mc_phi->at(ttt),0.);
              gam1d = gam1d+*deltaI;
          };*/
          
          
      };
      
    delete deltaI;
    deltaI=0;
        double DPhi_1 = fabs(phiL_t2[0]-phiG_t2[0]);
      if (DPhi_1>pi) DPhi_1=2*pi-DPhi_1;
      double DEta_1 = fabs(etaL_t2[0]-etaG_t2[0]);
      double DPhi_2 = fabs(phiL_t2[1]-phiG_t2[0]);
      if (DPhi_2>pi) DPhi_2=2*pi-DPhi_2;
      double DEta_2 = fabs(etaL_t2[1]-etaG_t2[0]);

      dR_D_Plus_t2[0] = sqrt(DPhi_1*DPhi_1+DEta_1*DEta_1);
      dR_D_Minus_t2[0] =sqrt(DPhi_2*DPhi_2+DEta_2*DEta_2) ;
      //      dR_D_Plus[0] = TMath::Sqrt((Py3-dilepton->mc_eta->at(m1_id))*(Py3-dilepton->mc_eta->at(m1_id))+(Pz3-dilepton->mc_phi->at(m1_id))*(Pz3-dilepton->mc_phi->at(m1_id)));
      // dR_D_Minus[0] = TMath::Sqrt((Py3-dilepton->mc_eta->at(m2_id))*(Py3-dilepton->mc_eta->at(m2_id))+(Pz3-dilepton->mc_phi->at(m2_id))*(Pz3-dilepton->mc_phi->at(m2_id)));
     
     //  pi1d.SetPtEtaPhiM(ptL_t[0],etaL_t[0],phiL_t[0],0.);
     // pi1d.SetPtEtaPhiM(ptL_t[1],etaL_t[1],phiL_t[1], 0.);
     // gam1d.SetPtEtaPhiM(ptG_t[0],etaG_t[0],phiG_t[0],0.);
     
    ptL_t[0]=pi1d.Pt();
      etaL_t[0]=pi1d.Eta();
      phiL_t[0]=pi1d.Phi();
      ptL_t[1]=pi2d.Pt();
      etaL_t[1]=pi2d.Eta();
      phiL_t[1]=pi2d.Phi();
   //   ptG_t[0]=gam1d.Pt();
   //   etaG_t[0]=gam1d.Eta();
   //   phiG_t[0]=gam1d.Phi();
    //std::cout<<" here2"<<etaL_t[0]<<" "<<etaL_t[1]<<std::endl;
    
    if(ptG_t[0]<15000){ 
        delete pi2;
        delete pi1;
        delete gam1;
        isSelected_t =2;
        return 4;
    };
    if(ptL_t[0]<10000){ 
        delete pi2;
        delete pi1;
        delete gam1;
        isSelected_t =3;
        return 5;
    };
    if(ptL_t[1]<10000){ 
        delete pi2;
        delete pi1;
        delete gam1;
        isSelected_t =4;
        return 5;
    };
    if(max(ptL_t[0],ptL_t[1])<25000){ 
        delete pi2;
        delete pi1;
        delete gam1;
        isSelected_t = 5;
        return 5;
        
    };
   // std::cout<<" herePtSelected "<<ptL_t[0]<<" "<<ptL_t[1]<<" "<<ptG_t[0]<<std::endl;
 //  if(abs(etaG_t[0])>2.37) return 6;
 //   if((etaG_t[0]>1.37)&&(etaG_t[0]<1.52)) return 6;
   
    
    DPhi_1 = fabs(phiL_t[0]-phiG_t[0]);
    if (DPhi_1>pi) DPhi_1=2*pi-DPhi_1;
    DEta_1 = fabs(etaL_t[0]-etaG_t[0]);
    
    DPhi_2 = fabs(phiL_t[1]-phiG_t[0]);
    if (DPhi_2>pi) DPhi_2=2*pi-DPhi_2;
    DEta_2 = fabs(etaL_t[1]-etaG_t[0]);

      dR_D_Plus_t[0] = sqrt(DPhi_1*DPhi_1+DEta_1*DEta_1);
      dR_D_Minus_t[0] =sqrt(DPhi_2*DPhi_2+DEta_2*DEta_2) ;
      //      dR_D_Plus_t[0] = TMath::Sqrt((etaG_t[0]-etaL_t[0])*(etaG_t[0]-etaL_t[0])+(phiG_t[0]-phiL_t[0])*(phiG_t[0]-phiL_t[0]));
      // dR_D_Minus_t[0] = TMath::Sqrt((etaG_t[0]-etaL_t[1])*(etaG_t[0]-etaL_t[1])+(phiG_t[0]-phiL_t[1])*(phiG_t[0]-phiL_t[1]));
      
      
      TLorentzVector Aq1(1,1,1,1);
      TLorentzVector Aq2(1,1,1,1);
      TLorentzVector M2l(1,1,1,1);
      TLorentzVector Zz(1,1,1,1);
      
      Zz=*pi2+*pi1+*gam1;
      M2l=*pi2+*pi1;
      Aq1=*pi1+*gam1;
      Aq2=*pi2+*gam1;
      
      M_Tr_t2[0]=Zz.M();
      M_2L_t2=M2l.M();
      M_D_Plus_t2[0] = Aq1.M();
      M_D_Minus_t2[0] = Aq2.M();
      mindRL_G_t2[0]= min(dR_D_Plus_t2[0],dR_D_Minus_t2[0]);
      
      
            Zz=pi2d+pi1d+gam1d;
            M2l=pi2d+pi1d;
            Aq1=pi1d+gam1d;
            Aq2=pi2d+gam1d;
      
      
      M_Tr_t[0]=Zz.M();
      M_2L_t=M2l.M();
      M_D_Plus_t[0] = Aq1.M();
      M_D_Minus_t[0] = Aq2.M();
      mindRL_G_t[0]= min(dR_D_Plus_t[0],dR_D_Minus_t[0]);
      
      delete pi2;
      delete pi1;
      delete gam1;
    
   // std::cout<<" hereM0 "<<M_2L_t<<" "<<M_Tr_t[0]<<std::endl;
    
    isSelected_t =10;
    if( (M_2L_t>80000.0) || (M_2L_t<45000.0)) return 10;
    if((M_Tr_t[0]<80000.0) || (M_Tr_t[0]>100000.0)) return 10;
    //std::cout<<" hereM "<<M_2L_t<<" "<<M_Tr_t[0]<<std::endl;
    isSelected_t = 1;
  return 12;  
    
}

#+END_SRC

** Dressing
Good example:
https://rivet.hepforge.org/trac/browser/analyses/pluginATLAS/ATLAS_2017_I1609448.cc

prompt photon for prompt lepton dressing.
** jets counting

examples of jet usage

*** ATLAS_2015_I1394679
Multijets at 8 TeV

    p + p -> j j j j + X

Differential cross sections for the production of at least four jets
have been measured in proton-proton collisions at $\sqrt{s}=8$\,TeV at
the Large Hadron Collider using the ATLAS detector. The dataset
corresponds to an integrated luminosity of 20.3\,fb^{−1}. The cross
sections are corrected for detector effects and presented as a
function of the jet momenta, invariant masses, minimum and maximum
opening angles and other kinematic variables.

https://rivet.hepforge.org/analyses#ATLAS_2015_I1394679
https://rivet.hepforge.org/code/dev/ATLAS__2015__I1394679_8cc_source.html
*** ATLAS_2015_CONF_2015_041
Z+jets at 13 TeV
Experiment: ATLAS (LHC)
Inspire ID: None
Status: PRELIMINARY
Authors:

    Christian Gutschow

References:

    ATLAS-CONF-2015-041

Beams: p+ p+
Beam energies: (6500.0, 6500.0) GeV
Run details:

    inclusive Z production in the electron channel

Preliminary measurements of the cross section for the production of a
$Z$ boson in association with jets in pp collisions at $\sqrt{s} =
13$\,TeV are presented, using data corresponding to an integrated
luminosity of $85\,\text{pb}^{-1}$ collected by the ATLAS experiment
at the Large Hadron Collider. The cross sections are measured for
events containing a $Z$ boson decaying to electrons or muons and
produced in association with up to four jets in the kinematical range
of $p_\text{T} > 30$\,GeV and $|y| < 2.5$. NB--Use the plugin names
ATLAS_2015_CONF_2015_041_EL or ATLAS_2015_CONF_2015_041_MU to specify
the lepton channel.

https://rivet.hepforge.org/analyses#ATLAS_2015_CONF_2015_041
https://rivet.hepforge.org/code/dev/ATLAS__2015__CONF__2015__041_8cc_source.html

*** ATLAS_2014_I1279489
Measurements of electroweak production of dijets + $Z$ boson, and
distributions sensitive to vector boson fusion

Experiment: ATLAS (LHC)
Inspire ID: 1279489
Status: VALIDATED
Authors:

    Kiran Joshi

References:

    JHEP 1404 (2014) 031
    arXiv: 1401.7610

Beams: p+ p+
Beam energies: (4000.0, 4000.0) GeV
Run details:

    Generate $Z$+jets events at 8 TeV, with $Z$ decaying to muons or electrons and $pT(j1)>55$ GeV, $pT(j2)>45$ GeV.

Measurements differential distributions for inclusive
$Z$-boson-plus-dijet production are performed in five fiducial
regions, each with different sensitivity to the electroweak
contribution. Measured distributions include the differential cross
section as a function of the dijet invariant mass, the differential
cross section and a function of the dijet rapidity separation, the
differential cross section as a function of the number of jets in the
rapidity interval bounded by the two leading jets. Other measurements
include the jet veto effiency as a function of the dijet invariant
mass and rapdity separation, the normalized transverse momentum
balance cut efficiency, and the average number of jets falling into
the rapidity interval boundd by the two leading jets, as a function of
dijet invariant mass and dijet rapidity separation.
ATLAS_2014_I1282441

https://rivet.hepforge.org/analyses#ATLAS_2014_I1279489
http://rivet.hepforge.org/code/2.2.1/a00549_source.html
*** Kristina mail
Hi Evgenii,

yes the following code is fine.

#+BEGIN_SRC C++
const Jets& jets = applyProjection<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::absrap < 4.4);
#+END_SRC 

and remove jets which overlap with leptons


---------

Some other helpful code could be the one below... (but it is basically equivalent)

#+BEGIN_SRC C++
       const vector<DressedLepton>& leptons = applyProjection<DressedLeptons>(event, "leptons").dressedLeptons();
       // Project jets
       FastJets jets(FS, FastJets::ANTIKT, 0.4);
       jets.useInvisibles(JetAlg::NO_INVISIBLES);
       jets.useMuons(JetAlg::NO_MUONS);
       addProjection(jets, "jets");

     int njets_selected = 0;
     Jets jets_selected;
    int njets_selected_centraleta = 0;
     Jets jets_selected_centraleta;
     foreach (const Jet &j, jets) {
       if( j.abseta() > 4.5 && j.pT()<=25*GeV ) continue;
       bool keep = true;
       foreach(DressedLepton el, good_el) {
         keep &= deltaR(j, el) >= 0.3;
       }
       if (keep)  {
         jets_selected += j;
         njets_selected++;
         if( j.abseta() <2.5) {
         jets_selected_centraleta += j;
         njets_selected_centraleta++;
         }
         }
     }
#+END_SRC 

Cheers
Kristin


Hi,

oh - indeed you are right!!

Bad mistake...

Luckily, it does not affect the code too much, because I forgot one 
snippet of code:

#+BEGIN_SRC C++
        // Project jets
        FastJets jets(FS, FastJets::ANTIKT, 0.4);
        jets.useInvisibles(JetAlg::NO_INVISIBLES);
        jets.useMuons(JetAlg::NO_MUONS);
        addProjection(jets, "jets");


      const Jets& jets = applyProjection<FastJets>(event, "jets").jetsByPt(Cuts::pT>25*GeV && Cuts::abseta < 4.5);**

before the overlapp is done.
#+END_SRC 

I put the code on lxplus: ~kristin/public/ForEvgenii/

Cheers
Kristin
