#include <TH1F.h>
// -*- C++ -*-
#include <TFile.h>
#include <TTree.h>


//#include "ROOT/TFile.h"

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Projections/FastJets.hh"

//#include "Rivet/Projections/StableFinalState.hh"
//#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"

#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/VisibleFinalState.hh"

#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "HepMC/SimpleVector.h"
#include "HepMC/WeightContainer.h"



namespace Rivet {
  
  class llyFFAnalysis : public Analysis {
  public:

    /// Constructor
    llyFFAnalysis()
      : Analysis("llyFFAnalysis")
    {
    }
    

    /// @name Analysis methods
    //@{

    /// Book histograms and initialize projections before the run
    void init() {
      cout<<" initialize "<<endl;
      // -------------------------------------
      //      configurable data members
      // 1 -- default, 2 -- Kharlamov (1l trg), 3 -- Kharlamov (2l trg), 4 -- Kharlamov (1l trg || 2l trg)
      // 5 -- Kharlamov \eta distorted
      cut_type=2;
      // 1 -- default, 2 -- using _check functions
      precut_type=1;
      // 1 -- all photons, 2 -- only one photon with max p_T (default)
      photon_selection=2;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T (default)
      lepton_selection=2;
      // 1 -- default, 2 -- add dressed info into undressed tree (comparison only)
      DressUndressMix=1;
      // -------------------------------------
      countA=0;

      pi = 3.1415926535897932384;
      Zmass = 91.1876*GeV;
      event_counter=0;
      event_counter_W=0.;
     
      /// @todo Initialise and register projections here
      // All particles within |eta| < 5.0
      const FinalState FS(Cuts::abseta < 5.0);

      // Project photons for dressing (for prompt leptons prompt photons)
#define CorrectDressing
#ifdef CorrectDressing      
      IdentifiedFinalState all_photon_id(FS);
      all_photon_id.acceptId(PID::PHOTON);
      PromptFinalState photon_id(all_photon_id);
#else      
      IdentifiedFinalState photon_id(FS);
      photon_id.acceptId(PID::PHOTON);
#endif      
      // Project signal photons  with pT > 15 GeV and |eta| < 2.37  and |eta| notin (1.37,1.52)
      Cut photon_cuts = (Cuts::abseta < 2.37) && ( (Cuts::abseta <= 1.37) || (Cuts::abseta >= 1.52) ) && (Cuts::pT > 4*GeV);
      //Cut photon_cuts = (Cuts::abseta < 2.37) && (Cuts::pT > 15*GeV);
      /* FinalState FS_photon(photon_cuts);
      IdentifiedFinalState photon_signal(FS_photon);
      // IdentifiedFinalState photon_signal(FinalState(-2.47, 2.47, 15.0*GeV));
      photon_signal.acceptId(PID::PHOTON);
      addProjection(photon_signal, "Photon_Signal");*/
      
      //Cut photon_cuts = (Cuts::abseta < 2.37) && (Cuts::pT > 15*GeV);
      FinalState FS_photon(photon_cuts);
      IdentifiedFinalState photon_presignal(FS_photon);
      // IdentifiedFinalState photon_signal(FinalState(-2.47, 2.47, 15.0*GeV));
      photon_presignal.acceptId(PID::PHOTON);
      PromptFinalState photon_signal(photon_presignal);
      declare(photon_signal, "Photon_Signal");
         


      // Project dressed electrons with pT > 10 GeV and |eta| < 2.47
      IdentifiedFinalState el_id(FS);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState el_bare(el_id);
      // Cut cuts = (Cuts::abseta < 2.37) && ( (Cuts::abseta <= 1.37) || (Cuts::abseta >= 1.52) ) && (Cuts::pT > 10*GeV);
      // Cut cuts = (Cuts::abseta < 2.37) && (Cuts::pT > 10*GeV);
      Cut cuts_el = (Cuts::abseta < 2.47) && (Cuts::pT > 4*GeV);
        precut_eta_el=2.47;
      DressedLeptons el_dressed_FS(photon_id, el_bare, 0.1, cuts_el, true, true);
      declare(el_dressed_FS,"EL_DRESSED_FS");

      FinalState FS_el(cuts_el);
      IdentifiedFinalState el_signal(FS_el);
      el_signal.acceptIdPair(PID::ELECTRON);
      declare(el_signal, "Electron_Signal");

      // Project dressed muons with pT > 10 GeV and |eta| < 2.5
      IdentifiedFinalState mu_id(FS);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState mu_bare(mu_id);
      Cut cuts_mu = (Cuts::abseta < 2.7) && (Cuts::pT > 4*GeV);
       precut_eta_mu=2.7;
      DressedLeptons mu_dressed_FS(photon_id, mu_bare, 0.1, cuts_mu, true, true);
      declare(mu_dressed_FS,"MU_DRESSED_FS");

      FinalState FS_mu(cuts_mu);
      IdentifiedFinalState mu_signal(FS_mu);
      mu_signal.acceptIdPair(PID::MUON);
      declare(mu_signal, "Muon_Signal");

      // jets --- just for counting them
      FastJets jets(FS, FastJets::ANTIKT, 0.4);
      declare(jets, "Jets");
      
      

      // Book histograms
     MSG_INFO("Booking histograms");

     // total cross sections
     book(_h_tot_em, "xsec_em", 1, 0, 1 );
     book(_h_tot_all, "xsec", 1, 0, 1 );
     
     // user histogram
     // Z->e^+e^-\gamma
     book(_h_Qp_e, "Qp_e",50,0.*GeV,100.*GeV);
     book(_h_Qm_e, "Qm_e",50,0.*GeV,100.*GeV);
     book(_h_dR_e, "dR_e",30,0.,3.);
     // Z->\mu^+\mu^-\gamma
     book(_h_Qp_m, "Qp_m",50,0.*GeV,100.*GeV);
     book(_h_Qm_m , "Qm_m",50,0.*GeV,100.*GeV);
     book(_h_dR_m, "dR_m",30,0.,3.);
     // Z
     book(_h_pT_Z_e, "pT_Z_e",100,0.*GeV,100.*GeV);
     book(_h_pT_Z_m, "pT_Z_m",100,0.*GeV,100.*GeV);
     // invarinat mass
     book(_h_M2e, "M2e",99,10.*GeV,100.*GeV);
     book(_h_M2m, "M2m",99,10.*GeV,100.*GeV);
     book(_h_M2e1g, "M2e1g",160,70.*GeV,120.*GeV);
     book(_h_M2m1g , "M2m1g",160,70.*GeV,120.*GeV);
     // eta distributions
     book(_h_etaZ_e, "etaZ_e",50,-8.,8.);
     book(_h_etaG_e, "etaG_e",60,-3.,3.);
     book(_h_etaLplus_e, "etaLplus_e",60,-3.,3.);
     book(_h_etaLminus_e, "etaLminus_e",60,-3.,3.);
     book(_h_etaZ_m, "etaZ_m",50,-8.,8.);
     book(_h_etaG_m, "etaG_m",60,-3.,3.);
     book(_h_etaLplus_m,"etaLplus_m",60,-3.,3.);
     book(_h_etaLminus_m, "etaLminus_m",60,-3.,3.);


     // Z->e^+e^-\gamma (undressed leptons)
     book(_h_Qp_eundr, "Qp_eundr",50,0.*GeV,100.*GeV);
     book(_h_Qm_eundr,"Qm_eundr",50,0.*GeV,100.*GeV);
     book(_h_dR_eundr, "dR_eundr",30,0.,3.);
     // Z->\mu^+\mu^-\gamma  (undressed leptons)
     book(_h_Qp_mundr, "Qp_mundr",50,0.*GeV,100.*GeV);
     book(_h_Qm_mundr, "Qm_mundr",50,0.*GeV,100.*GeV);
     book(_h_dR_mundr,"dR_mundr",30,0.,3.);
     // Z
     book(_h_pT_Z_eundr, "pT_Z_eundr",100,0.*GeV,100.*GeV);
     book(_h_pT_Z_mundr, "pT_Z_mundr",100,0.*GeV,100.*GeV);

     //root
     fout=new TFile("Rivet-tree.root","recreate");
     fout->cd();
     teeg=new TTree("teeg","Rivet Z->eeg tree");
     teeg->SetDirectory(fout); 
     fill_tree_withvars(teeg);

     tmmg=new TTree("tmmg","Rivet Z->mmg tree");
     tmmg->SetDirectory(fout); 
     fill_tree_withvars(tmmg);


     teegundr=new TTree("teegundr","Rivet Z->eegundr tree");
     teegundr->SetDirectory(fout); 
     fill_tree_withvars(teegundr);

     tmmgundr=new TTree("tmmgundr","Rivet Z->mmgundr tree");
     tmmgundr->SetDirectory(fout); 
     fill_tree_withvars(tmmgundr);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      cout<<"analyze "<<endl;
      int ileptonplusbest=-1;
      int jleptonminusbest=-1;
     
      double dPhi_1, dEta_1, dPhi_2,dEta_2,dR1,dR2;
	double pTplusmax=0;
	double pTminusmax=0;
	double pTmax=0;

      // get weight!!!
      weight = event.weight();
      event_counter++;
      event_counter_W+=weight;

      Particles photons = applyProjection<FinalState>(event, "Photon_Signal").particles();
      const vector<DressedLepton>& good_mu = applyProjection<DressedLeptons>(event, "MU_DRESSED_FS").dressedLeptons();
      const vector<DressedLepton>& good_el = applyProjection<DressedLeptons>(event, "EL_DRESSED_FS").dressedLeptons();

      Particles electrons = applyProjection<FinalState>(event, "Electron_Signal").particles();
      Particles muons = applyProjection<FinalState>(event, "Muon_Signal").particles();

      // Do lepton-jet overlap removal: (from http://rivet.hepforge.org/code/2.2.1/a00549_source.html)
      vector<const Jet*> good_jets;
      const Jets& jets = applyProjection<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::absrap < 4.4);
      //foreach(const Jet& j, jets) {
      for (const Jet& j : jets) {	
      bool nearby_lepton = false;
      for (const Particle& m : good_mu) {	
      //	foreach (const Particle& m, good_mu)
	  if (deltaR(j, m) < 0.3) nearby_lepton = true;
      }
      //	foreach (const Particle& e, good_el)
      for (const Particle& e : good_el) {	
          if (deltaR(j, e) < 0.3) nearby_lepton = true;
      }
        if (!nearby_lepton)
	  good_jets.push_back(&j);
      }
      // fill ntuple var
      njets=good_jets.size();
      
      if (event_counter%10==0) cout << "Event N " << event_counter << "(" << event_counter_W << ")" << endl;
      isSelected=0;

      int kphotonbest=-1;
      photoncounter=photons.size();
      pTmax=0;
      for (unsigned int k=0;k<photons.size();k++) {	
	double pT=photons[k].momentum().pT();
	//double etaP=TMath::Abs(photons[k].eta());
	if (photon_selection==2) {// 1 -- all photons, 2 -- only one photon with max p_T
	  if (kphotonbest==-1 || pT>pTmax) {
	    kphotonbest=k;
	    pTmax=pT;
	  }
	}
      }
      
      //**************************************************************
      cout<<"selecting Z to mumugamma (bare) "<<endl;

      ileptonplusbest=-1;
      jleptonminusbest=-1;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
      if (lepton_selection==2) {

	for (unsigned int i=0;i<muons.size();i++) {
	  if (muons[i].pid()==PID::MUON) {
	    double pT=muons[i].momentum().pT();
	    if (ileptonplusbest==-1 || pT>pTplusmax) {
	      ileptonplusbest=i;
	      pTplusmax=pT;
	    }
	  }
	  if (muons[i].pid()==PID::ANTIMUON) {
	    double pT=muons[i].momentum().pT();
	    if (jleptonminusbest==-1 || pT>pTminusmax) {
	      jleptonminusbest=i;
	      pTminusmax=pT;
	    }
	  }
	}
      }
              
      error=0;
      tripletcounter=0;
      Mbest=-1.;Qpbest=-1.;Qmbest=-1.;Qclbest=-1.;etaZbest=-100.;ptZbest=-1.;dRbest=-1;M2Lbest=-1;MLGminbest=-1;
      ptplusbest=-1.;ptminusbest=-1.;ptgammabest=-1.;
      etaplusbest=-1.;etaminusbest=-1.;etagammabest=-1.;
      
      if(muons.size()>1&&photons.size()>0&&ileptonplusbest>=0&&jleptonminusbest>=0){
      tlvminus=muons[ileptonplusbest].momentum();
      etaminus=tlvminus.eta();
      ptminus=tlvminus.pT();
    
      tlvplus=muons[jleptonminusbest].momentum();
      etaplus=tlvplus.eta();
      ptplus=tlvplus.pT();
      
      tlv2l=tlvminus+tlvplus;
      M2L=tlv2l.mass();

      tlvphoton=photons[kphotonbest].momentum();
      ptgamma=tlvphoton.pT();
      etagamma=tlvphoton.eta();
      tlv2l1g=tlv2l+tlvphoton;
      M=tlv2l1g.mass();

       // calculating dR
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton.eta());
      dR1=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1<dR2) {
	dR=dR1;
	Qcl=(tlvminus+tlvphoton).mass();
      }
      else {
	dR=dR2;
	Qcl=(tlvplus+tlvphoton).mass();
      }
      isSelected=0;
      if(pTmax<15*GeV) isSelected=2;
      if(pTminusmax<10*GeV) isSelected=3;
      if(pTplusmax<10*GeV) isSelected=4;
      if(TMath::Max(pTplusmax,pTminusmax)<25*GeV) isSelected=5;
      if(dR<0.4) isSelected=6;
      if(M2L<45*GeV||M2L>80*GeV) isSelected=7;
      if(M<80*GeV||M>100*GeV) isSelected=8;

      if(pTmax>15*GeV&&pTminusmax>10*GeV&&pTplusmax>10*GeV&&TMath::Max(pTplusmax,pTminusmax)>25*GeV&&dR>0.4&&M2L>45*GeV&&M2L<80*GeV&&M>80*GeV&&M<100*GeV) isSelected=1;

      ptplusbest=ptplus;ptminusbest=ptminus;ptgammabest=ptgamma;
      etaplusbest=etaplus;etaminusbest=etaminus;etagammabest=etagamma;
      Mbest=M;M2Lbest=M2L;MLGminbest=MLGmin;
      ptZbest=tlv2l1g.pT();
      etaZbest=tlv2l1g.eta();
      dRbest=dR;
      Qpbest=(tlvplus+tlvphoton).mass();
      Qmbest=(tlvminus+tlvphoton).mass();
      Qclbest=Qcl;
      tlv2l1gbest=tlv2l1g;
      tlvplusbest=tlvplus;
      tlvminusbest=tlvminus;
      tlvphotonbest=tlvphoton;
      
      isundressed=1;
      if (isundressed==1) {
	_h_Qp_mundr->fill(Qpbest,weight);
	_h_Qm_mundr->fill(Qmbest,weight);
	_h_dR_mundr->fill(dRbest,weight);
	_h_pT_Z_mundr->fill(ptZbest,weight);
	if (!fill_ntuplevars()) error=1;
	//	tmmgundr->Fill();
      }
      }
	tmmgundr->Fill();
   //**************************************************************
      cout<<"selecting Z to mumugamma (dressed) "<<endl;
      ileptonplusbest=-1;
      jleptonminusbest=-1;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
      if (lepton_selection==2) {
	pTplusmax=0;
	pTminusmax=0;
	for (unsigned int i=0;i<good_mu.size();i++) {
	  if (good_mu[i].pid()==PID::MUON) {
	    double pT=good_mu[i].momentum().pT();
	    if (ileptonplusbest==-1 || pT>pTplusmax) {
	      ileptonplusbest=i;
	      pTplusmax=pT;
	    }
	  }
	  if (good_mu[i].pid()==PID::ANTIMUON) {
	    double pT=good_mu[i].momentum().pT();
	    if (jleptonminusbest==-1 || pT>pTminusmax) {
	      jleptonminusbest=i;
	      pTminusmax=pT;
	    }
	  }
	}
      }
      // Z->\mu^+\mu^-\gamma selection (dressed)
      error=0;
      tripletcounter=0;
      Mbest=-1.;Qpbest=-1.;Qmbest=-1.;Qclbest=-1.;ptZbest=-1.;etaZbest=-100.;dRbest=-1;M2Lbest=-1;MLGminbest=-1.;
      ptplusbest=-1.;ptminusbest=-1.;ptgammabest=-1.;
      etaplusbest=-1.;etaminusbest=-1.;etagammabest=-1.;
      
       if(good_mu.size()>1&&photons.size()>0&&ileptonplusbest>=0&&jleptonminusbest>=0){
      tlvminus=good_mu[ileptonplusbest].momentum();
      etaminus=tlvminus.eta();
      ptminus=tlvminus.pT();
    
      tlvplus=good_mu[jleptonminusbest].momentum();
      etaplus=tlvplus.eta();
      ptplus=tlvplus.pT();
      
      tlv2l=tlvminus+tlvplus;
      M2L=tlv2l.mass();

      tlvphoton=photons[kphotonbest].momentum();
      ptgamma=tlvphoton.pT();
      etagamma=tlvphoton.eta();
      tlv2l1g=tlv2l+tlvphoton;
      M=tlv2l1g.mass();

       // calculating dR
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton.eta());
      dR1=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1<dR2) {
	dR=dR1;
	Qcl=(tlvminus+tlvphoton).mass();
      }
      else {
	dR=dR2;
	Qcl=(tlvplus+tlvphoton).mass();
      }
      isSelected=0;
      if(pTmax<15*GeV) isSelected=2;
      if(pTminusmax<10*GeV) isSelected=3;
      if(pTplusmax<10*GeV) isSelected=4;
      if(TMath::Max(pTplusmax,pTminusmax)<25*GeV) isSelected=5;
      if(dR<0.4) isSelected=6;
      if(M2L<45*GeV||M2L>80*GeV) isSelected=7;
      if(M<80*GeV||M>100*GeV) isSelected=8;

      if(pTmax>15*GeV&&pTminusmax>10*GeV&&pTplusmax>10*GeV&&TMath::Max(pTplusmax,pTminusmax)>25*GeV&&dR>0.4&&M2L>45*GeV&&M2L<80*GeV&&M>80*GeV&&M<100*GeV) isSelected=1;

      ptplusbest=ptplus;ptminusbest=ptminus;ptgammabest=ptgamma;
      etaplusbest=etaplus;etaminusbest=etaminus;etagammabest=etagamma;
      Mbest=M;M2Lbest=M2L;MLGminbest=MLGmin;
      ptZbest=tlv2l1g.pT();
      etaZbest=tlv2l1g.eta();
      dRbest=dR;
      Qpbest=(tlvplus+tlvphoton).mass();
      Qmbest=(tlvminus+tlvphoton).mass();
      Qclbest=Qcl;
      tlv2l1gbest=tlv2l1g;
      tlvplusbest=tlvplus;
      tlvminusbest=tlvminus;
      tlvphotonbest=tlvphoton;

      isdressed=1;
      if (isdressed==1) {
	_h_Qp_m->fill(Qpbest,weight);
	_h_Qm_m->fill(Qmbest,weight);
	_h_dR_m->fill(dRbest,weight);
	_h_pT_Z_m->fill(ptZbest,weight);
	_h_M2m->fill(M2Lbest,weight);
	_h_M2m1g->fill(Mbest,weight);
	// eta distributions
	_h_etaZ_m->fill(etaZbest,weight);
	_h_etaG_m->fill(etagammabest,weight);
	_h_etaLplus_m->fill(etaplusbest,weight);
	_h_etaLminus_m->fill(etaminusbest,weight);
	if (!fill_ntuplevars()) error=1;
	isdressed=1;
	//	tmmg->Fill();
      }
       }
	tmmg->Fill();
      //*********************************************************
       cout<<"selecting Z to eegamma (bare) "<<endl;
      ileptonplusbest=-1;
      jleptonminusbest=-1;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
      if (lepton_selection==2) {
	pTplusmax=0;
	pTminusmax=0;
	for (unsigned int i=0;i<electrons.size();i++) {
	  if (electrons[i].pid()==PID::ELECTRON) {
	    double pT=electrons[i].momentum().pT();
	    if (ileptonplusbest==-1 || pT>pTplusmax) {
	      ileptonplusbest=i;
	      pTplusmax=pT;
	    }
	  }
	  if (electrons[i].pid()==PID::POSITRON) {
	    double pT=electrons[i].momentum().pT();
	    if (jleptonminusbest==-1 || pT>pTminusmax) {
	      jleptonminusbest=i;
	      pTminusmax=pT;
	    }
	  }
	}
      }
      // Z->e^+e^-\gamma selection (undressed leptons)
      error=0;
      tripletcounter=0;
      Mbest=-1.;Qpbest=-1.;Qmbest=-1.;Qclbest=-1.;etaZbest=-100.;ptZbest=-1.;dRbest=-1;M2Lbest=-1;MLGminbest=-1;
      ptplusbest=-1.;ptminusbest=-1.;ptgammabest=-1.;
      etaplusbest=-1.;etaminusbest=-1.;etagammabest=-1.;
     
      if(electrons.size()>1&&photons.size()>0&&ileptonplusbest>=0&&jleptonminusbest>=0){
      tlvminus=electrons[ileptonplusbest].momentum();
      etaminus=tlvminus.eta();
      ptminus=tlvminus.pT();
    
      tlvplus=electrons[jleptonminusbest].momentum();
      etaplus=tlvplus.eta();
      ptplus=tlvplus.pT();
      
      tlv2l=tlvminus+tlvplus;
      M2L=tlv2l.mass();

      tlvphoton=photons[kphotonbest].momentum();
      ptgamma=tlvphoton.pT();
      etagamma=tlvphoton.eta();
      tlv2l1g=tlv2l+tlvphoton;
      M=tlv2l1g.mass();

       // calculating dR
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton.eta());
      dR1=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1<dR2) {
	dR=dR1;
	Qcl=(tlvminus+tlvphoton).mass();
      }
      else {
	dR=dR2;
	Qcl=(tlvplus+tlvphoton).mass();
      }
      isSelected=0;
      if(pTmax<15*GeV) isSelected=2;
      if(pTminusmax<10*GeV) isSelected=3;
      if(pTplusmax<10*GeV) isSelected=4;
      if(TMath::Max(pTplusmax,pTminusmax)<25*GeV) isSelected=5;
      if(dR<0.4) isSelected=6;
      if(M2L<45*GeV||M2L>80*GeV) isSelected=7;
      if(M<80*GeV||M>100*GeV) isSelected=8;

      if(pTmax>15*GeV&&pTminusmax>10*GeV&&pTplusmax>10*GeV&&TMath::Max(pTplusmax,pTminusmax)>25*GeV&&dR>0.4&&M2L>45*GeV&&M2L<80*GeV&&M>80*GeV&&M<100*GeV) isSelected=1;

      ptplusbest=ptplus;ptminusbest=ptminus;ptgammabest=ptgamma;
      etaplusbest=etaplus;etaminusbest=etaminus;etagammabest=etagamma;
      Mbest=M;M2Lbest=M2L;MLGminbest=MLGmin;
      ptZbest=tlv2l1g.pT();
      etaZbest=tlv2l1g.eta();
      dRbest=dR;
      Qpbest=(tlvplus+tlvphoton).mass();
      Qmbest=(tlvminus+tlvphoton).mass();
      Qclbest=Qcl;
      tlv2l1gbest=tlv2l1g;
      tlvplusbest=tlvplus;
      tlvminusbest=tlvminus;
      tlvphotonbest=tlvphoton;

      isundressed=1;
      if (isundressed==1) {
	_h_Qp_eundr->fill(Qpbest,weight);
	_h_Qm_eundr->fill(Qmbest,weight);
	_h_dR_eundr->fill(dRbest,weight);
	_h_pT_Z_eundr->fill(ptZbest,weight);
	if (!fill_ntuplevars()) error=1;
	//	teegundr->Fill();
      }
      }
      teegundr->Fill();
      //*****************************************************
      cout<<"selecting Z to eegamma (dressed) "<<endl;
      ileptonplusbest=-1;
      jleptonminusbest=-1;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
      if (lepton_selection==2) {
	pTplusmax=0;
	pTminusmax=0;
	for (unsigned int i=0;i<good_el.size();i++) {
	  if (good_el[i].pid()==PID::ELECTRON) {
	    double pT=good_el[i].momentum().pT();
	    if (ileptonplusbest==-1 || pT>pTplusmax) {
	      ileptonplusbest=i;
	      pTplusmax=pT;
	    }
	  }
	  if (good_el[i].pid()==PID::POSITRON) {
	    double pT=good_el[i].momentum().pT();
	    if (jleptonminusbest==-1 || pT>pTminusmax) {
	      jleptonminusbest=i;
	      pTminusmax=pT;
	    }
	  }
	}
      }
      cout<<"el.size "<<good_el.size()<<"ph.size "<<photons.size()<<endl;
     if(good_el.size()>1&&photons.size()>0&&ileptonplusbest>=0&&jleptonminusbest>=0){ 
      tlvminus=good_el[ileptonplusbest].momentum();
      etaminus=tlvminus.eta();
      ptminus=tlvminus.pT();
    
      tlvplus=good_el[jleptonminusbest].momentum();
      etaplus=tlvplus.eta();
      ptplus=tlvplus.pT();
      
      tlv2l=tlvminus+tlvplus;
      M2L=tlv2l.mass();

      tlvphoton=photons[kphotonbest].momentum();
      ptgamma=tlvphoton.pT();
      etagamma=tlvphoton.eta();
      tlv2l1g=tlv2l+tlvphoton;
      M=tlv2l1g.mass();
      cout<<"M "<<M<<endl;

       // calculating dR
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton.eta());
      dR1=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1<dR2) {
	dR=dR1;
	Qcl=(tlvminus+tlvphoton).mass();
      }
      else {
	dR=dR2;
	Qcl=(tlvplus+tlvphoton).mass();
      }
      isSelected=0;
      if(pTmax<15*GeV) isSelected=2;
      if(pTminusmax<10*GeV) isSelected=3;
      if(pTplusmax<10*GeV) isSelected=4;
      if(TMath::Max(pTplusmax,pTminusmax)<25*GeV) isSelected=5;
      if(dR<0.4) isSelected=6;
      if(M2L<45*GeV||M2L>80*GeV) isSelected=7;
      if(M<80*GeV||M>100*GeV) isSelected=8;

      if(pTmax>15*GeV&&pTminusmax>10*GeV&&pTplusmax>10*GeV&&TMath::Max(pTplusmax,pTminusmax)>25*GeV&&dR>0.4&&M2L>45*GeV&&M2L<80*GeV&&M>80*GeV&&M<100*GeV) isSelected=1;

      ptplusbest=ptplus;ptminusbest=ptminus;ptgammabest=ptgamma;
      etaplusbest=etaplus;etaminusbest=etaminus;etagammabest=etagamma;
      Mbest=M;M2Lbest=M2L;MLGminbest=MLGmin;
      ptZbest=tlv2l1g.pT();
      etaZbest=tlv2l1g.eta();
      dRbest=dR;
      Qpbest=(tlvplus+tlvphoton).mass();
      Qmbest=(tlvminus+tlvphoton).mass();
      Qclbest=Qcl;
      tlv2l1gbest=tlv2l1g;
      tlvplusbest=tlvplus;
      tlvminusbest=tlvminus;
      tlvphotonbest=tlvphoton;

      isdressed=1;
      if (isdressed==1) {
	_h_Qp_e->fill(Qpbest,weight);
	_h_Qm_e->fill(Qmbest,weight);
	_h_dR_e->fill(dRbest,weight);
	_h_pT_Z_e->fill(ptZbest,weight);
	_h_M2e->fill(M2Lbest,weight);
	_h_M2e1g->fill(Mbest,weight);
	// eta distributions
	_h_etaZ_e->fill(etaZbest,weight);
	_h_etaG_e->fill(etagammabest,weight);
	_h_etaLplus_e->fill(etaplusbest,weight);
	_h_etaLminus_e->fill(etaminusbest,weight);
	if (!fill_ntuplevars()) error=1;
	//	teeg->Fill();
      }
     }
	teeg->Fill();
	cout<<"end events loop"<<endl;
    }//end events loop

    // Check \mu^+\mu^-\gamma triplets 
    bool mmg_check(int typeTest) {
      double MTest,dRTest,M2LTest,MLGminTest;
      double ptplusTest,ptminusTest,ptgammaTest;
      double etaplusTest,etaminusTest,etagammaTest;
      switch(typeTest) {
      case BEST:
	MTest=Mbest;
	dRTest=dRbest;
	M2LTest=M2Lbest;
	MLGminTest=MLGminbest;
	ptplusTest=ptplusbest;
	ptminusTest=ptminusbest;
	ptgammaTest=ptgammabest;
	etaplusTest=etaplusbest;
	etaminusTest=etaminusbest;
	etagammaTest=etagammabest;
	break;
      case CURRENT:
	MTest=M;
	dRTest=dR;
	M2LTest=M2L;
	MLGminTest=MLGmin;
	ptplusTest=ptplus;
	ptminusTest=ptminus;
	ptgammaTest=ptgamma;
	etaplusTest=etaplus;
	etaminusTest=etaminus;
	etagammaTest=etagamma;
	break;
      default:
	return false;
      }
            
      switch(cut_type) {// 1 -- default, 2 -- Kharlamov (1l trg), 3 -- Kharlamov (2l trg), 4 -- Kharlamov (1l trg || 2l trg)
      case 1: //default
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.7&&abs(etaminusTest)<2.7)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (abs(etagammaTest)<1.37||abs(etagammaTest)>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  (ptplusTest>25*GeV||ptminusTest>25*GeV|| // one lepton trigger
	   (ptplusTest>14*GeV&&ptminusTest>14*GeV))&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  (dRTest>0.4); // dR condition
	break;
      case 2: //Kharlamov one lepton trigger
	// 	|eta_mu|<2.7 for muons
	// 	|eta_gamma|<2.37  |eta_gamma| \notin 1.37-1.52
	// Pt_l_min > 10 GeV && Pt_l_max > 25 GeV (EF_mu24i_tight),
	// Pt_gamma > 15 GeV
	// 45 < M2L < 80 GeV
	// 80 < M < 100 GeV
	// MLg > 1 GeV
	// dR > 0.4 	
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.7&&abs(etaminusTest)<2.7)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (abs(etagammaTest)<1.37||abs(etagammaTest)>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  (ptplusTest>25*GeV||ptminusTest>25*GeV)&& // one lepton trigger
	  //	  || (ptplusTest>14*GeV&&ptminusTest>14*GeV))&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  // (MLGminTest>1*GeV)&& // minimum invariant lepton-photon mass
	  (dRTest>0.4); // dR condition
	break;
      case 3: //Kharlamov two lepton trigger
	// 	|eta_mu|<2.7 for muons
	// 	|eta_gamma|<2.37  |eta_gamma| \notin 1.37-1.52
	// Pt_l_min > 14 GeV (2l trg EF_2mu13_matching)
	// Pt_gamma > 15 GeV
	// 45 < M2L < 80 GeV
	// 80 < M < 100 GeV
	// MLg > 1 GeV
	// dR > 0.4 	
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.7&&abs(etaminusTest)<2.7)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (abs(etagammaTest)<1.37||abs(etagammaTest)>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  // (ptplusTest>25*GeV||ptminusTest>25*GeV)&& // one lepton trigger
	  (ptplusTest>14*GeV&&ptminusTest>14*GeV)&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  (MLGminTest>1*GeV)&& // minimum invariant lepton-photon mass
	  (dRTest>0.4); // dR condition
	break;
      case 4: //Kharlamov (one lepton trigger || two lepton trigger
	// 	|eta_mu|<2.7 for muons
	// 	|eta_gamma|<2.37  |eta_gamma| \notin 1.37-1.52
	// Pt_l_min > 10 GeV &&
	// ( Pt_l_max > 25 GeV (EF_mu24i_tight) || Pt_l_min > 14 GeV (2l trg EF_2mu13_matching) )
	// Pt_gamma > 15 GeV
	// 45 < M2L < 80 GeV
	// 80 < M < 100 GeV
	// MLg > 1 GeV
	// dR > 0.4 	
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.7&&abs(etaminusTest)<2.7)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (abs(etagammaTest)<1.37||abs(etagammaTest)>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  ((ptplusTest>25*GeV||ptminusTest>25*GeV) // one lepton trigger
	   || (ptplusTest>14*GeV&&ptminusTest>14*GeV))&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  (MLGminTest>1*GeV)&& // minimum invariant lepton-photon mass
	  (dRTest>0.4); // dR condition
	break;
      case 5: //Kharlamov \eta distorted
	// 	|eta_mu|<2.7 for muons
	// 	|eta_gamma|<2.37  eta_gamma \notin 1.37-1.52
	// Pt_l_min > 10 GeV && Pt_l_max > 25 GeV (EF_mu24i_tight),
	// Pt_gamma > 15 GeV
	// 45 < M2L < 80 GeV
	// 80 < M < 100 GeV
	// MLg > 1 GeV
	// dR > 0.4 	
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.7&&abs(etaminusTest)<2.7)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (etagammaTest<1.37||etagammaTest>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  (ptplusTest>25*GeV||ptminusTest>25*GeV)&& // one lepton trigger
	  //	  || (ptplusTest>14*GeV&&ptminusTest>14*GeV))&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  (MLGminTest>1*GeV)&& // minimum invariant lepton-photon mass
	  (dRTest>0.4); // dR condition
	break;
      }
      return false;
    }

    // Check e^+e^-\gamma triplets 
    bool eeg_check(int typeTest) {
      double MTest,dRTest,M2LTest,MLGminTest;
      double ptplusTest,ptminusTest,ptgammaTest;
      double etaplusTest,etaminusTest,etagammaTest;
      switch(typeTest) {
      case BEST:
	MTest=Mbest;
	dRTest=dRbest;
	M2LTest=M2Lbest;
	MLGminTest=MLGminbest;
	ptplusTest=ptplusbest;
	ptminusTest=ptminusbest;
	ptgammaTest=ptgammabest;
	etaplusTest=etaplusbest;
	etaminusTest=etaminusbest;
	etagammaTest=etagammabest;
	break;
      case CURRENT:
	MTest=M;
	dRTest=dR;
	M2LTest=M2L;
	MLGminTest=MLGmin;
	ptplusTest=ptplus;
	ptminusTest=ptminus;
	ptgammaTest=ptgamma;
	etaplusTest=etaplus;
	etaminusTest=etaminus;
	etagammaTest=etagamma;
	break;
      default:
	return false;
      }
      
      switch(cut_type) {// 1 -- default, 2 -- Kharlamov (1l trg), 3 -- Kharlamov (2l trg), 4 -- Kharlamov (1l trg || 2l trg)
      case 1: //default
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.47&&abs(etaminusTest)<2.47)&& // eta electron condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for electron
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (abs(etagammaTest)<1.37||abs(etagammaTest)>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  (ptplusTest>27*GeV||ptminusTest>27*GeV||  // one lepton trigger
	   (ptplusTest>16*GeV&&ptminusTest>16*GeV))&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  (dRTest>0.4); // dR condition
      case 2: //Kharlamov 1ltrg
	// 	|eta_e|<2.47 for electrons
	// 	|eta_gamma|<2.37  |eta_gamma| \notin 1.37-1.52
	// Pt_l_min > 10 GeV && Pt_l_max > 25 GeV (EF_e24vhi_medium1),
	// Pt_gamma > 15 GeV
	// 45 < M2L < 80 GeV
	// 80 < M < 100 GeV
	// MLg > 1 GeV
	// dR > 0.4 	
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.47&&abs(etaminusTest)<2.47)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (abs(etagammaTest)<1.37||abs(etagammaTest)>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  (ptplusTest>25*GeV||ptminusTest>25*GeV)&& // one lepton trigger
	  //	  || (ptplusTest>14*GeV&&ptminusTest>14*GeV))&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  // (MLGminTest>1*GeV)&& // minimum invariant lepton-photon mass
	  (dRTest>0.4); // dR condition
	break;
      case 3: //Kharlamov 2ltrg
	// 	|eta_e|<2.47 for electrons
	// 	|eta_gamma|<2.37  |eta_gamma| \notin 1.37-1.52
	// Pt_l_min > 14 GeV (EF_2e12Tvh_loose1) 
	// Pt_gamma > 15 GeV
	// 45 < M2L < 80 GeV
	// 80 < M < 100 GeV
	// MLg > 1 GeV
	// dR > 0.4 	
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.47&&abs(etaminusTest)<2.47)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (abs(etagammaTest)<1.37||abs(etagammaTest)>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  // (ptplusTest>25*GeV||ptminusTest>25*GeV)&& // one lepton trigger
	  (ptplusTest>14*GeV&&ptminusTest>14*GeV)&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  (MLGminTest>1*GeV)&& // minimum invariant lepton-photon mass
	  (dRTest>0.4); // dR condition
	break;
      case 4: //Kharlamov 1ltrg || 2ltrg
	// 	|eta_e|<2.47 for electrons
	// 	|eta_gamma|<2.37  |eta_gamma| \notin 1.37-1.52
	// Pt_l_min > 10 GeV &&
	// (Pt_l_max > 25 GeV (EF_e24vhi_medium1) || Pt_l_min > 14 GeV (EF_2e12Tvh_loose1) )
	// Pt_gamma > 15 GeV
	// 45 < M2L < 80 GeV
	// 80 < M < 100 GeV
	// MLg > 1 GeV
	// dR > 0.4 	
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.47&&abs(etaminusTest)<2.47)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (abs(etagammaTest)<1.37||abs(etagammaTest)>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  ((ptplusTest>25*GeV||ptminusTest>25*GeV) // one lepton trigger
	   || (ptplusTest>14*GeV&&ptminusTest>14*GeV))&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  (MLGminTest>1*GeV)&& // minimum invariant lepton-photon mass
	  (dRTest>0.4); // dR condition
	break;	
      case 5: //Kharlamov 1ltrg
	// 	|eta_e|<2.47 for electrons
	// 	|eta_gamma|<2.37  eta_gamma \notin 1.37-1.52
	// Pt_l_min > 10 GeV && Pt_l_max > 25 GeV (EF_e24vhi_medium1),
	// Pt_gamma > 15 GeV
	// 45 < M2L < 80 GeV
	// 80 < M < 100 GeV
	// MLg > 1 GeV
	// dR > 0.4 	
	return MTest>=0&& // found at least one triplet
	  (abs(etaplusTest)<2.47&&abs(etaminusTest)<2.47)&& // eta muon condition
	  ptplusTest>10*GeV&&ptminusTest>10*GeV&& // pt_min for muons
	  abs(etagammaTest)<2.37&& // eta \gamma condition
	  (etagammaTest<1.37||etagammaTest>1.52)&& // removing transitional eta area
	  ptgammaTest>15*GeV&& // pt_min for gamma
	  (ptplusTest>25*GeV||ptminusTest>25*GeV)&& // one lepton trigger
	  //	  || (ptplusTest>14*GeV&&ptminusTest>14*GeV))&& // two leptons trigger
	  (M2LTest>45*GeV&&M2LTest<80*GeV)&& // lepton invariant mass condition
	  (MTest>80*GeV&&MTest<100*GeV)&& // triplet invariant mass condition
	  (MLGminTest>1*GeV)&& // minimum invariant lepton-photon mass
	  (dRTest>0.4); // dR condition
	break;
      }
      return false;
    }


    
    int fill_tree_withvars(TTree *intree) {
      // if something went wrong
      intree->Branch("error",&error,"error/I");
      // triplet counters
      intree->Branch("tripletcounter",&tripletcounter,"tripletcounter/I");
      // photon counters
      intree->Branch("photoncounter",&photoncounter,"photoncounter/I");
      // base var
      intree->Branch("isSelected",&isSelected,"isSelected/I");
      intree->Branch("M",&Mbest,"M/D");
      intree->Branch("MLGmin",&MLGminbest,"MLGmin/D");      
      intree->Branch("Qp",&Qpbest,"Qp/D");
      intree->Branch("Qm",&Qmbest,"Qm/D");
      intree->Branch("Qcl",&Qclbest,"Qcl/D");
      intree->Branch("ptZ",&ptZbest,"ptZ/D");
      intree->Branch("etaZ",&etaZbest,"etaZ/D");
      intree->Branch("dR",&dRbest,"dR/D");
      intree->Branch("M2L",&M2Lbest,"M2L/D");
      intree->Branch("ptp",&ptplusbest,"ptp/D");
      intree->Branch("ptm",&ptminusbest,"ptm/D");
      intree->Branch("ptg",&ptgammabest,"ptg/D");
      intree->Branch("etap",&etaplusbest,"etap/D");
      intree->Branch("etam",&etaminusbest,"etam/D");
      intree->Branch("etag",&etagammabest,"etag/D");
      intree->Branch("W",&weight,"W/D");
      // additional vars
      intree->Branch("dRll",&dRll,"dRll/D");
      intree->Branch("Eg",&Eg,"Eg/D");
      intree->Branch("njets",&njets,"njets/I");
      // CMS center of mass system (cms prefix)
      intree->Branch("cms_dR",&cms_dR,"cms_dR/D"); // CMS dR
      intree->Branch("cms_Eg",&cms_Eg,"cms_Eg/D"); // photon Energy 
      intree->Branch("cms_Em",&cms_Em,"cms_Em/D"); // electron Energy
      intree->Branch("cms_Ep",&cms_Ep,"cms_Ep/D"); // positron Energy
      intree->Branch("cms_phi_pm",&cms_phi_pm,"cms_phi_pm/D"); // angle between positron and electron 
      intree->Branch("cms_phi_pg",&cms_phi_pg,"cms_phi_pg/D"); // angle between positron and photon
      intree->Branch("cms_phi_mg",&cms_phi_mg,"cms_phi_mg/D"); // angle between electron and photon
      intree->Branch("cms_Ptg",&cms_Ptg,"cms_Ptg/D"); // photon Pt
      intree->Branch("cms_Ptm",&cms_Ptm,"cms_Ptm/D"); // electron Pt
      intree->Branch("cms_Ptp",&cms_Ptp,"cms_Ptp/D"); // positron Pt
      // Collins-Soper frame (CSf prefix) 
      // CSf positiv lepton vars (p suffix)
      intree->Branch("CSf_pxp",&CSf_pxp,"CSf_pxp/D");
      intree->Branch("CSf_pyp",&CSf_pyp,"CSf_pyp/D");
      intree->Branch("CSf_pzp",&CSf_pzp,"CSf_pzp/D");
      // CSf negativ lepton vars (m suffix)
      intree->Branch("CSf_pxm",&CSf_pxm,"CSf_pxm/D");
      intree->Branch("CSf_pym",&CSf_pym,"CSf_pym/D");
      intree->Branch("CSf_pzm",&CSf_pzm,"CSf_pzm/D");
      // CSf gamma vars (g suffix)
      intree->Branch("CSf_pxg",&CSf_pxg,"CSf_pxg/D");
      intree->Branch("CSf_pyg",&CSf_pyg,"CSf_pyg/D");
      intree->Branch("CSf_pzg",&CSf_pzg,"CSf_pzg/D");
      // CSf normolized gamma x l^+ vector product 
      intree->Branch("CSf_gpx",&CSf_gpx,"CSf_gpx/D");
      intree->Branch("CSf_gpy",&CSf_gpy,"CSf_gpy/D");
      intree->Branch("CSf_gpz",&CSf_gpz,"CSf_gpz/D");
      // CSf normolized gamma x l^- vector product 
      intree->Branch("CSf_gmx",&CSf_gmx,"CSf_gmx/D");
      intree->Branch("CSf_gmy",&CSf_gmy,"CSf_gmy/D");
      intree->Branch("CSf_gmz",&CSf_gmz,"CSf_gmz/D");
      // Switches
      intree->Branch("isdressed",&isdressed,"isdressed/I");
      intree->Branch("isundressed",&isundressed,"isundressed/I");      
      return 1;
    }
    

    // Collins-Soper frame variables filling
    // return 1 if O'k, 0 if error
    int fill_ntuplevars() {
      // additional vars
      FourMomentum v_p,v_m,v_g;
      v_p=tlvplusbest;
      v_m=tlvminusbest;
      v_g=tlvphotonbest;
      Eg=v_g.E(); // photon Energy
      
      double dPhi = abs(v_m.phi()-v_p.phi());
      if (dPhi>pi) dPhi=2*pi-dPhi;
      double dEta = abs(v_m.eta()-v_p.eta());
      dRll=sqrt(dPhi*dPhi+dEta*dEta); // dRll

      // CMS center of mass system (cms prefix)
      FourMomentum cms_v_p,cms_v_m,cms_v_g,CMS;
      CMS=tlv2l1gbest;
      cms_v_p=tlvplusbest;
      cms_v_m=tlvminusbest;
      cms_v_g=tlvphotonbest;

      LorentzTransform CMSboost;
      // boostVector -> betaVec (in new rivet release!!!!)
      // setBoost -> setBetaVec (in new rivet release!!!!)
      // There is an assert(boost.mod2() < 1); in  
      // LorentzTrans.hh  LorentzTransform& setBoost(const Vector3& boost) 
      //Vector3 vCMSboost(-CMS.boostVector());
      Vector3 vCMSboost(-CMS.betaVec());
      if (abs(vCMSboost.mod2())>=1) return 0;
      //CMSboost.setBoost(vCMSboost);
      CMSboost.setBetaVec(vCMSboost);
      cms_v_p=CMSboost.transform(cms_v_p);
      cms_v_m=CMSboost.transform(cms_v_m);
      cms_v_g=CMSboost.transform(cms_v_g);

      double dPhi_1 = abs(cms_v_m.phi()-cms_v_g.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      double dEta_1 = abs(cms_v_m.eta()-cms_v_g.eta());
      double dPhi_2 = abs(cms_v_p.phi()-cms_v_g.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      double dEta_2 = abs(cms_v_p.eta()-cms_v_g.eta());
      double dRmin=min(dPhi_1*dPhi_1+dEta_1*dEta_1,dPhi_2*dPhi_2+dEta_2*dEta_2); 
      cms_dR=sqrt(dRmin); // dR in CMS

      cms_Eg=cms_v_g.E(); // photon Energy 
      cms_Em=cms_v_m.E(); // electron Energy
      cms_Ep=cms_v_p.E(); // positron Energy
      cms_phi_pm=cms_v_p.angle(cms_v_m); // angle between positron and electron 
      cms_phi_pg=cms_v_p.angle(cms_v_g); // angle between positron and photon
      cms_phi_mg=cms_v_m.angle(cms_v_g); // angle between electron and photon
      cms_Ptg=cms_v_g.pT(); // photon Pt
      cms_Ptm=cms_v_m.pT(); // electron Pt
      cms_Ptp=cms_v_p.pT(); // positron Pt

      // Collins-Soper frame (CSf prefix) 
      // CSf positiv lepton vars (p suffix)
      FourMomentum CSf_v_p,CSf_v_m,CSf_v_g,CSf;
      // Z boost removing
      CSf=tlv2l1gbest;
      double rapZ = CSf.rapidity(); // rapidity for Z
      CSf_v_p=tlvplusbest;
      CSf_v_m=tlvminusbest;
      CSf_v_g=tlvphotonbest;
      LorentzTransform CMSZboost;
      // boostVector -> betaVec (in new rivet release!!!!)
      // setBoost -> setBetaVec (in new rivet release!!!!)
      // There is an assert(boost.mod2() < 1); in  
      // LorentzTrans.hh  LorentzTransform& setBoost(const Vector3& boost) 
      //      Vector3 vCMSZboost(0,0,-CSf.boostVector().z());
      Vector3 vCMSZboost(0,0,-CSf.betaVec().z());
      if (abs(vCMSZboost.mod2())>=1) return 0;
      //      CMSZboost.setBoost(vCMSZboost);
      CMSZboost.setBetaVec(vCMSZboost);
      CSf_v_p=CMSZboost.transform(CSf_v_p);
      CSf_v_m=CMSZboost.transform(CSf_v_m);
      CSf_v_g=CMSZboost.transform(CSf_v_g);
      CSf=CMSZboost.transform(CSf);
      // xy rotate till all P directed along x
      LorentzTransform CSfZPhiRotate;
      CSfZPhiRotate.preMult(Matrix3(Vector3::mkZ(), -CSf.phi()));
      CSf_v_p=CSfZPhiRotate.transform(CSf_v_p);
      CSf_v_m=CSfZPhiRotate.transform(CSf_v_m);
      CSf_v_g=CSfZPhiRotate.transform(CSf_v_g);
      CSf=CSfZPhiRotate.transform(CSf);
      // the remaning boost removing (only Pt exist)
      LorentzTransform CSfboost;
      // boostVector -> betaVec (in new rivet release!!!!)
      // setBoost -> setBetaVec (in new rivet release!!!!)
      // There is an assert(boost.mod2() < 1); in  
      // LorentzTrans.hh  LorentzTransform& setBoost(const Vector3& boost) 
      //      if (abs(CSf.boostVector().mod2())>=1) return 0;
      if (abs(CSf.betaVec().mod2())>=1) return 0;
      //CSfboost.setBoost(-CSf.boostVector());
      CSfboost.setBetaVec(-CSf.betaVec());
      // cout << "1 " << CSf.x() << " " << CSf.y()  << " " << CSf.z() << " " << CSf.phi()<< endl;
      // CSf=CSfboost.transform(CSf);
      // cout << "2 " << CSf.x() << " " << CSf.y()  << " " << CSf.z() << endl;
      CSf_v_p=CSfboost.transform(CSf_v_p);
      CSf_v_m=CSfboost.transform(CSf_v_m);
      CSf_v_g=CSfboost.transform(CSf_v_g);	
      // eta simmetry removing
      if (rapZ<0) {
	LorentzTransform CSfZPiRotate;
	CSfZPiRotate.preMult(Matrix3(Vector3::mkZ(), pi));
	CSf_v_p=CSfZPiRotate.transform(CSf_v_p);
	CSf_v_m=CSfZPiRotate.transform(CSf_v_m);
	CSf_v_g=CSfZPiRotate.transform(CSf_v_g);	
      }
      // positiv lepton vars (p suffix)
      CSf_pxp=CSf_v_p.px(); // x momentum component for positive lepton
      CSf_pyp=CSf_v_p.py(); // y momentum component for positive lepton
      CSf_pzp=CSf_v_p.pz(); // z momentum component for positive lepton
      // negativ lepton vars (m suffix)
      CSf_pxm=CSf_v_m.px(); // x momentum component for negative lepton
      CSf_pym=CSf_v_m.py(); // y momentum component for negative lepton
      CSf_pzm=CSf_v_m.pz(); // z momentum component for negative lepton
      // gamma vars (g suffix)
      CSf_pxg=CSf_v_g.px(); // x momentum component for photon
      CSf_pyg=CSf_v_g.py(); // y momentum component for photon
      CSf_pzg=CSf_v_g.pz(); // z momentum component for photon
      // CSf normolized gamma x l^+ vector product 
      // vector3();
      Vector3 v3gp;
      v3gp=CSf_v_g.vector3().cross(CSf_v_p.vector3()).unit();
      CSf_gpx=v3gp.x();
      CSf_gpy=v3gp.y();
      CSf_gpz=v3gp.z(); 
      // CSf normolized gamma x l^- vector product 
      Vector3 v3gm;
      v3gp=CSf_v_g.vector3().cross(CSf_v_m.vector3()).unit();
      CSf_gmx=v3gm.x();
      CSf_gmy=v3gm.y();
      CSf_gmz=v3gm.z(); 
	
      return 1;
    }

    /// Normalise histograms etc., after the run
    void finalize() {
      cout <<"finalize: "<< event_counter << " " << event_counter_W << endl;
      _h_tot_all->fill(0.5,crossSection());
      cout<<" finalize-1  "<<endl;
      const double xs = crossSectionPerEvent()/femtobarn;
      
      scale(_h_tot_em, xs);

      if(countA==0){
      cout<<" finalize-2  "<<endl;
      //root
      fout->cd();
      cout<<" finalize-2-1  "<< endl;
      cout<<" Nteeg "<<teeg->GetEntries()<<" "<<endl;
      teeg->Write();
      cout<<" finalize-2-2"<< endl;
      cout<<"Ntmmg "<<tmmg->GetEntries()<<endl;
      tmmg->Write();
      cout<<" finalize-3  "<<endl;
      teegundr->Write();
      tmmgundr->Write();
      cout<<" finalize-4  "<<endl;
      fout->Write();
      fout->Close();
      countA++;
      }
      cout<<"end finalize "<<endl;

    } //end finalize
    //@}

  private:

    //@{
    //d01-x01-

    Histo1DPtr _h_tot_em;
    Histo1DPtr _h_tot_all;

    Histo1DPtr _h_Qp_e;
    Histo1DPtr _h_Qm_e;
    Histo1DPtr _h_dR_e;
    Histo1DPtr _h_Qp_m;
    Histo1DPtr _h_Qm_m;
    Histo1DPtr _h_dR_m;
    Histo1DPtr _h_pT_Z_e;
    Histo1DPtr _h_pT_Z_m;
    Histo1DPtr _h_M2e;
    Histo1DPtr _h_M2m;
    Histo1DPtr _h_M2e1g;
    Histo1DPtr _h_M2m1g;

    // eta distributions
    Histo1DPtr _h_etaZ_e;
    Histo1DPtr _h_etaG_e;
    Histo1DPtr _h_etaLplus_e;
    Histo1DPtr _h_etaLminus_e;
    Histo1DPtr _h_etaZ_m;
    Histo1DPtr _h_etaG_m;
    Histo1DPtr _h_etaLplus_m;
    Histo1DPtr _h_etaLminus_m;
    
    //undressed histograms
    Histo1DPtr _h_Qp_eundr;
    Histo1DPtr _h_Qm_eundr;
    Histo1DPtr _h_dR_eundr;
    Histo1DPtr _h_Qp_mundr;
    Histo1DPtr _h_Qm_mundr;
    Histo1DPtr _h_dR_mundr;
    Histo1DPtr _h_pT_Z_eundr;
    Histo1DPtr _h_pT_Z_mundr;
  

    //@}
    double pi;// = 3.1415926535897;
    double Zmass;
    int event_counter;
    double event_counter_W;

    // root
    TFile *fout;
    TTree *teeg;
    TTree *tmmg;
    TTree *teegundr;
    TTree *tmmgundr;

    FourMomentum tlvplus,tlvminus,tlvphoton,tlv2l,tlv2l1g,tlvlpg,tlvlmg;
    FourMomentum tlvplusbest,tlvminusbest,tlvphotonbest;
    FourMomentum tlv2l1gbest;

    // ttree variables
    int error; // due to bugs? 0 is O'k
    int tripletcounter;
    int photoncounter;
    int isdressed,isundressed;
    double Mbest,Qpbest,Qmbest,Qclbest,ptZbest,etaZbest,dRbest,M2Lbest,MLGminbest;
    double ptplusbest,ptminusbest,ptgammabest;
    double etaplusbest,etaminusbest,etagammabest;
    double weight;
    // current vars
    double M,Qp,Qm,Qcl,ptZ,etaZ,dR,M2L,MLGmin;
    double ptplus,ptminus,ptgamma;
    double etaplus,etaminus,etagamma;
    // enums
    enum {BEST,CURRENT};
    
    // additional vars
    double dRll;
    double Eg;
    int njets;

    int countA;
    
    // CMS center of mass system (cms prefix)
    double cms_dR; // cms dR
    double cms_Eg; // photon Energy 
    double cms_Em; // electron Energy
    double cms_Ep; // positron Energy
    double cms_phi_pm; // angle between positron and electron 
    double cms_phi_pg; // angle between positron and photon
    double cms_phi_mg; // angle between electron and photon
    double cms_Ptg; // photon Pt
    double cms_Ptm; // electron Pt
    double cms_Ptp; // positron Pt
    
    // Collins-Soper frame (CSf prefix) 
    // CSf positiv lepton vars (p suffix)
    double CSf_pxp;
    double CSf_pyp;
    double CSf_pzp;
    // CSf negativ lepton vars (m suffix)
    double CSf_pxm;
    double CSf_pym;
    double CSf_pzm;
    // CSf gamma vars (g suffix)
    double CSf_pxg;
    double CSf_pyg;
    double CSf_pzg;
    // CSf normolized gamma x l^+ vector product 
    double CSf_gpx;
    double CSf_gpy;
    double CSf_gpz;
    // CSf normolized gamma x l^- vector product 
    double CSf_gmx;
    double CSf_gmy;
    double CSf_gmz;

    //      configurable data members
    //------------------------------------------------------------------------
    // cut_type: 1 -- default, 2 -- Kharlamov (1l trg), 3 -- Kharlamov (2l trg), 4 -- Kharlamov (1l trg || 2l trg)
    int cut_type;     
    // precut_type: 1 -- default, 2 -- using _check functions
    int precut_type;
    // 1 -- all photons, 2 -- only one photon with max p_T
    int photon_selection;
    // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
    int lepton_selection;
    // 1 -- default, 2 -- add dressed info into undressed tree (comparison only)
    int DressUndressMix;
    //for cutflow
    int isSelected;
    // precut valuse
    double precut_eta_mu;
    double precut_eta_el;
    //tmp variables
   
  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(llyFFAnalysis);


}
