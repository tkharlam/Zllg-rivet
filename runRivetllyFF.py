theApp.EvtMax = 1000 #-1
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool

# garou
datapath="/net/nfs1/exports/atlas1/data/baldin/sample/EVNT/mc15_8TeV.364387.Sh_224_NN30NNLO_eegamma.evgen.EVNT.e6994_tid15777197_00/"
# CERN lxplus
#datapath="/net/nfs1/exports/atlas1/atlas/data/akharlam/Sherpa2_2_4/mc15_8TeV.364387.Sh_224_NN30NNLO_eegamma.evgen.EVNT.e6994/"

from os import listdir
from os.path import isfile, join
files = sorted([join(datapath,f) for f in listdir(datapath) if isfile(join(datapath, f))])
svcMgr.EventSelector.InputCollections =  files[0:100] # files[0:100]# files[0:2500] #[0:2]



from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']

#from GaudiSvc.GaudiSvcConf import THistSvc
#ServiceMgr += THistSvc()
#ServiceMgr.THistSvc.Output = ["Rivet DATAFILE='Rivet.root' OPT='RECREATE'"]
rivet.HistoFile = 'MyOutput.yoda.gz'
rivet.DoRootHistos = True

rivet.Analyses += [ 'llyFFAnalysis' ]
rivet.RunName = ""
job += rivet
