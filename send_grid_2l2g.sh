lsetup panda

pathena --extOutFile=Rivet-2l2g-tree.root \
--inDS=mc15_8TeV.364387.Sh_224_NN30NNLO_eegamma.evgen.EVNT.e6994 \
--outDS=user.tkharlam.RivetOnTheGrid.2l2g.v1 \
--extFile=RivetllyyFFAnalysis.so \
runRivetllyy-grid.py

pathena --extOutFile=Rivet-2l2g-tree.root \
--inDS=mc15_8TeV.364388.Sh_224_NN30NNLO_mumugamma.evgen.EVNT.e6994 \
--outDS=user.tkharlam.RivetOnTheGrid.2mu2g.v1 \
--extFile=RivetllyyFFAnalysis.so \
runRivetllyy-grid.py
