SHELL = /bin/bash
#	source buildPlugin_llyFF.sh
all:
	@echo "run: source setuprivet_llyFF.sh"
	@echo "make bin, clean, logclean"
	@echo "make tbin  (Tatyana modified bin)"

bin: RivetAnalysis.so

RivetAnalysis.so: llyFFAnalysis.cc
	rivet-buildplugin -r RivetAnalysis.so  llyFFAnalysis.cc
#	rivet-buildplugin RivetAnalysis.so --with-root llyFFAnalysis.cc

tbin:
	rivet-buildplugin RivetAnalysis.so --with-root llyFFAnalysis-tkharlam.cc -I`root-config --incdir`

clean:
	rm RivetAnalysis.so

logclean:
	rm *-log.*

#init source setuprivet_llyFF.sh
#run athena -l WARNING runRivetllyFF.py
