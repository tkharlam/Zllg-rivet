#include <TH1F.h>
// -*- C++ -*-
#include <TFile.h>
#include <TTree.h>


//#include "ROOT/TFile.h"

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Projections/FastJets.hh"

//#include "Rivet/Projections/StableFinalState.hh"
//#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"

#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/VisibleFinalState.hh"

#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "HepMC/SimpleVector.h"
#include "HepMC/WeightContainer.h"



namespace Rivet {
  
  class llyyFFAnalysis : public Analysis {
  public:

    /// Constructor
    llyyFFAnalysis()
      : Analysis("llyyFFAnalysis")
    {
    }
    

    /// @name Analysis methods
    //@{

    /// Book histograms and initialize projections before the run
    void init() {
      cout<<" initialize "<<endl;
      // -------------------------------------
      //      configurable data members
      // 1 -- default, 2 -- Kharlamov (1l trg), 3 -- Kharlamov (2l trg), 4 -- Kharlamov (1l trg || 2l trg)
      // 5 -- Kharlamov \eta distorted
      cut_type=2;
      // 1 -- default, 2 -- using _check functions
      precut_type=1;
      // 1 -- all photons, 2 -- only one photon with max p_T (default)
      photon_selection=2;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T (default)
      lepton_selection=2;
      // 1 -- default, 2 -- add dressed info into undressed tree (comparison only)
      DressUndressMix=1;
      // -------------------------------------
      countA=0;

      pi = 3.1415926535897932384;
      Zmass = 91.1876*GeV;
      event_counter=0;
      event_counter_W=0.;
     
      /// @todo Initialise and register projections here
      // All particles within |eta| < 5.0
      const FinalState FS(Cuts::abseta < 5.0);

      // Project photons for dressing (for prompt leptons prompt photons)
#define CorrectDressing
#ifdef CorrectDressing      
      IdentifiedFinalState all_photon_id(FS);
      all_photon_id.acceptId(PID::PHOTON);
      PromptFinalState photon_id(all_photon_id);
#else      
      IdentifiedFinalState photon_id(FS);
      photon_id.acceptId(PID::PHOTON);
#endif      
      // Project signal photons  with pT > 15 GeV and |eta| < 2.37  and |eta| notin (1.37,1.52)
      Cut photon_cuts = (Cuts::abseta < 2.37) && ( (Cuts::abseta <= 1.37) || (Cuts::abseta >= 1.52) ) && (Cuts::pT > 4*GeV);
      //Cut photon_cuts = (Cuts::abseta < 2.37) && (Cuts::pT > 15*GeV);
      /* FinalState FS_photon(photon_cuts);
      IdentifiedFinalState photon_signal(FS_photon);
      // IdentifiedFinalState photon_signal(FinalState(-2.47, 2.47, 15.0*GeV));
      photon_signal.acceptId(PID::PHOTON);
      addProjection(photon_signal, "Photon_Signal");*/
      
      //Cut photon_cuts = (Cuts::abseta < 2.37) && (Cuts::pT > 15*GeV);
      FinalState FS_photon(photon_cuts);
      IdentifiedFinalState photon_presignal(FS_photon);
      // IdentifiedFinalState photon_signal(FinalState(-2.47, 2.47, 15.0*GeV));
      photon_presignal.acceptId(PID::PHOTON);
      PromptFinalState photon_signal(photon_presignal);
      declare(photon_signal, "Photon_Signal");
         


      // Project dressed electrons with pT > 10 GeV and |eta| < 2.47
      IdentifiedFinalState el_id(FS);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState el_bare(el_id);
      // Cut cuts = (Cuts::abseta < 2.37) && ( (Cuts::abseta <= 1.37) || (Cuts::abseta >= 1.52) ) && (Cuts::pT > 10*GeV);
      // Cut cuts = (Cuts::abseta < 2.37) && (Cuts::pT > 10*GeV);
      Cut cuts_el = (Cuts::abseta < 2.47) && (Cuts::pT > 4*GeV);
        precut_eta_el=2.47;
      DressedLeptons el_dressed_FS(photon_id, el_bare, 0.1, cuts_el, true, true);
      declare(el_dressed_FS,"EL_DRESSED_FS");

      FinalState FS_el(cuts_el);
      IdentifiedFinalState el_signal(FS_el);
      el_signal.acceptIdPair(PID::ELECTRON);
      declare(el_signal, "Electron_Signal");

      // Project dressed muons with pT > 10 GeV and |eta| < 2.5
      IdentifiedFinalState mu_id(FS);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState mu_bare(mu_id);
      Cut cuts_mu = (Cuts::abseta < 2.7) && (Cuts::pT > 4*GeV);
       precut_eta_mu=2.7;
      DressedLeptons mu_dressed_FS(photon_id, mu_bare, 0.1, cuts_mu, true, true);
      declare(mu_dressed_FS,"MU_DRESSED_FS");

      FinalState FS_mu(cuts_mu);
      IdentifiedFinalState mu_signal(FS_mu);
      mu_signal.acceptIdPair(PID::MUON);
      declare(mu_signal, "Muon_Signal");

      // jets --- just for counting them
      FastJets jets(FS, FastJets::ANTIKT, 0.4);
      declare(jets, "Jets");
      
      

      // Book histograms
     MSG_INFO("Booking histograms");

     // total cross sections
     book(_h_tot_em, "xsec_em", 1, 0, 1 );
     book(_h_tot_all, "xsec", 1, 0, 1 );
     
     // user histogram
     // Z->e^+e^-\gamma
     book(_h_Qp_e, "Qp_e",50,0.*GeV,100.*GeV);
     book(_h_Qm_e, "Qm_e",50,0.*GeV,100.*GeV);
     book(_h_dR1_e, "dR1_e",30,0.,3.);
     book(_h_dR2_e, "dR2_e",30,0.,3.);
     book(_h_dRgg_e, "dRgg_e",30,0.,3.);
     // Z->\mu^+\mu^-\gamma
     book(_h_Qp_m, "Qp_m",50,0.*GeV,100.*GeV);
     book(_h_Qm_m , "Qm_m",50,0.*GeV,100.*GeV);
     book(_h_dR1_m, "dR1_m",30,0.,3.);
     book(_h_dR2_m, "dR2_m",30,0.,3.);
     book(_h_dRgg_m, "dRgg_m",30,0.,3.);
     // Z
     book(_h_pT_Z_e, "pT_Z_e",100,0.*GeV,100.*GeV);
     book(_h_pT_Z_m, "pT_Z_m",100,0.*GeV,100.*GeV);
     // invarinat mass
     book(_h_M2e, "M2e",99,10.*GeV,100.*GeV);
     book(_h_M2m, "M2m",99,10.*GeV,100.*GeV);
     book(_h_M2e1g, "M2e1g",160,70.*GeV,120.*GeV);
     book(_h_M2m1g , "M2m1g",160,70.*GeV,120.*GeV);
     // eta distributions
     book(_h_etaZ_e, "etaZ_e",50,-8.,8.);
     book(_h_etaG1_e, "etaG1_e",60,-3.,3.);
     book(_h_etaG2_e, "etaG2_e",60,-3.,3.);
     book(_h_etaLplus_e, "etaLplus_e",60,-3.,3.);
     book(_h_etaLminus_e, "etaLminus_e",60,-3.,3.);
     book(_h_etaZ_m, "etaZ_m",50,-8.,8.);
     book(_h_etaG1_m, "etaG1_m",60,-3.,3.);
     book(_h_etaG2_m, "etaG2_m",60,-3.,3.);
     book(_h_etaLplus_m,"etaLplus_m",60,-3.,3.);
     book(_h_etaLminus_m, "etaLminus_m",60,-3.,3.);

     // Z->e^+e^-\gamma (undressed leptons)
     book(_h_Qp_eundr, "Qp_eundr",50,0.*GeV,100.*GeV);
     book(_h_Qm_eundr,"Qm_eundr",50,0.*GeV,100.*GeV);
     book(_h_dR1_eundr, "dR1_eundr",30,0.,3.);
     book(_h_dR2_eundr, "dR2_eundr",30,0.,3.);
     book(_h_dRgg_eundr, "dRgg_eundr",30,0.,3.);
     // Z->\mu^+\mu^-\gamma  (undressed leptons)
     book(_h_Qp_mundr, "Qp_mundr",50,0.*GeV,100.*GeV);
     book(_h_Qm_mundr, "Qm_mundr",50,0.*GeV,100.*GeV);
     book(_h_dR1_mundr,"dR1_mundr",30,0.,3.);
     book(_h_dR2_mundr,"dR2_mundr",30,0.,3.);
     book(_h_dRgg_mundr,"dRgg_mundr",30,0.,3.);
     // Z
     book(_h_pT_Z_eundr, "pT_Z_eundr",100,0.*GeV,100.*GeV);
     book(_h_pT_Z_mundr, "pT_Z_mundr",100,0.*GeV,100.*GeV);

     //root
     fout=new TFile("Rivet-2l2g-tree.root","recreate");
     fout->cd();
     teegg=new TTree("teegg","Rivet Z->eegg tree");
     teegg->SetDirectory(fout); 
     fill_tree_withvars(teegg);

     tmmgg=new TTree("tmmgg","Rivet Z->mmgg tree");
     tmmgg->SetDirectory(fout); 
     fill_tree_withvars(tmmgg);


     teeggundr=new TTree("teeggundr","Rivet Z->eeggundr tree");
     teeggundr->SetDirectory(fout); 
     fill_tree_withvars(teeggundr);

     tmmggundr=new TTree("tmmggundr","Rivet Z->mmggundr tree");
     tmmggundr->SetDirectory(fout); 
     fill_tree_withvars(tmmggundr);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      cout<<"analyze "<<endl;
      int ileptonplusbest=-1;
      int jleptonminusbest=-1;
     
      double dPhi_1, dEta_1, dPhi_2,dEta_2,dR1,dR2;
	double pTplusmax=0.;
	double pTminusmax=0.;
	double pTmax1=0.,pTmax2=0.;

      // get weight!!!
      weight = event.weight();
      event_counter++;
      event_counter_W+=weight;

      Particles photons = applyProjection<FinalState>(event, "Photon_Signal").particles();
      const vector<DressedLepton>& good_mu = applyProjection<DressedLeptons>(event, "MU_DRESSED_FS").dressedLeptons();
      const vector<DressedLepton>& good_el = applyProjection<DressedLeptons>(event, "EL_DRESSED_FS").dressedLeptons();

      Particles electrons = applyProjection<FinalState>(event, "Electron_Signal").particles();
      Particles muons = applyProjection<FinalState>(event, "Muon_Signal").particles();

      // Do lepton-jet overlap removal: (from http://rivet.hepforge.org/code/2.2.1/a00549_source.html)
      vector<const Jet*> good_jets;
      const Jets& jets = applyProjection<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::absrap < 4.4);
      //foreach(const Jet& j, jets) {
      for (const Jet& j : jets) {	
      bool nearby_lepton = false;
      for (const Particle& m : good_mu) {	
      //	foreach (const Particle& m, good_mu)
	  if (deltaR(j, m) < 0.3) nearby_lepton = true;
      }
      //	foreach (const Particle& e, good_el)
      for (const Particle& e : good_el) {	
          if (deltaR(j, e) < 0.3) nearby_lepton = true;
      }
        if (!nearby_lepton)
	  good_jets.push_back(&j);
      }
      // fill ntuple var
      njets=good_jets.size();
      
      if (event_counter%10==0) cout << "Event N " << event_counter << "(" << event_counter_W << ")" << endl;
      isSelected=0;

      int kphoton1best=-1, kphoton2best=-1;
      photoncounter=photons.size();
      pTmax1=0,pTmax2=0;
      for (unsigned int k=0;k<photons.size();k++) {	
	double pT=photons[k].momentum().pT();
	//double etaP=TMath::Abs(photons[k].eta());
	if (photon_selection==2) {// 1 -- all photons, 2 -- two photons with max p_T
	  if (kphoton1best==-1 || pT>pTmax1) {
	    kphoton1best=k;
	    pTmax1=pT;
	  }
	}
      }
      for (unsigned int k=0;k<photons.size();k++) {
        double pT=photons[k].momentum().pT();
        //double etaP=TMath::Abs(photons[k].eta());                                    
        if (photon_selection==2&&(kphoton1best>=0&&k!=(unsigned)kphoton1best)) {// 1 -- all photons, 2 -- two photons with max p_T   
          if (kphoton2best==-1 || pT>pTmax2) {
            kphoton2best=k;
            pTmax2=pT;
          }
        }
      }

      //**************************************************************
      cout<<"selecting Z to mumugamma (bare) "<<endl;

      ileptonplusbest=-1;
      jleptonminusbest=-1;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
      if (lepton_selection==2) {

	for (unsigned int i=0;i<muons.size();i++) {
	  if (muons[i].pid()==PID::MUON) {
	    double pT=muons[i].momentum().pT();
	    if (ileptonplusbest==-1 || pT>pTplusmax) {
	      ileptonplusbest=i;
	      pTplusmax=pT;
	    }
	  }
	  if (muons[i].pid()==PID::ANTIMUON) {
	    double pT=muons[i].momentum().pT();
	    if (jleptonminusbest==-1 || pT>pTminusmax) {
	      jleptonminusbest=i;
	      pTminusmax=pT;
	    }
	  }
	}
      }
              
      error=0;
      tripletcounter=0;
      Mbest=-1.;M2Lgamma1best=-1.;M2Lgamma2best=-1.;Qpbest=-1.;Qmbest=-1.;Qcl1best=-1.;Qcl2best=-1.;etaZbest=-100.;ptZbest=-1.;dR1best=-1;dR2best=-1.;dRggbest=-1.;M2Lbest=-1;
      ptplusbest=-1.;ptminusbest=-1.;ptgamma1best=-1.,ptgamma2best=-1.;
      etaplusbest=-1.;etaminusbest=-1.;etagamma1best=-1.,etagamma2best=-1.;
      
      if(muons.size()>1&&photons.size()>1&&ileptonplusbest>=0&&jleptonminusbest>=0){
      tlvminus=muons[ileptonplusbest].momentum();
      etaminus=tlvminus.eta();
      ptminus=tlvminus.pT();
    
      tlvplus=muons[jleptonminusbest].momentum();
      etaplus=tlvplus.eta();
      ptplus=tlvplus.pT();
      
      tlv2l=tlvminus+tlvplus;
      M2L=tlv2l.mass();

      tlvphoton1=photons[kphoton1best].momentum();
      ptgamma1=tlvphoton1.pT();
      etagamma1=tlvphoton1.eta();

      tlvphoton2=photons[kphoton2best].momentum();
      ptgamma2=tlvphoton2.pT();
      etagamma2=tlvphoton2.eta();

      tlv2l2g=tlv2l+tlvphoton1+tlvphoton2;
      M=tlv2l2g.mass();

      M2Lgamma1=(tlv2l+tlvphoton1).mass();
      M2Lgamma2=(tlv2l+tlvphoton2).mass();

       // calculating dR1
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton1.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton1.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton1.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton1.eta());
      dR1t=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2t=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1t<dR2t) {
	dR1=dR1t;
	Qcl1=(tlvminus+tlvphoton1).mass();
      }
      else {
	dR1=dR2t;
	Qcl1=(tlvplus+tlvphoton1).mass();
      }

      // calculating dR2                                                                                   
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton2.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton2.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton2.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton2.eta());
      dR1t=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2t=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1t<dR2t) {
	dR2=dR1t;
        Qcl2=(tlvminus+tlvphoton2).mass();
      }
      else {
        dR2=dR2t;
        Qcl2=(tlvplus+tlvphoton2).mass();
      }

      // calculating dRgg                                                                                  
      dPhi_1 = fabs(tlvphoton1.phi()-tlvphoton2.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvphoton1.eta()-tlvphoton2.eta());
      dRgg=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
     
      isSelected=0;
      if(pTmax1<10*GeV) isSelected=2;
      if(pTmax2<10*GeV) isSelected=3;
      if(pTminusmax<15*GeV) isSelected=4;
      if(pTplusmax<15*GeV) isSelected=5;
      if(TMath::Max(pTplusmax,pTminusmax)<25*GeV) isSelected=6;
      if(dR1<0.4) isSelected=7;
      if(dR2<0.4) isSelected=8;
      if(M2Lgamma1>80*GeV) isSelected=9;
      if(M2Lgamma2>80*GeV) isSelected=10;

      if(pTmax1>15*GeV&&pTmax2>10*GeV&&pTminusmax>15*GeV&&pTplusmax>15*GeV&&TMath::Max(pTplusmax,pTminusmax)>25*GeV&&dR1>0.4&&dR2>0.4&&M2Lgamma1<80*GeV&&M2Lgamma2<80*GeV) isSelected=1;

      ptplusbest=ptplus;ptminusbest=ptminus;ptgamma1best=ptgamma1;ptgamma2best=ptgamma2;
      etaplusbest=etaplus;etaminusbest=etaminus;etagamma1best=etagamma1;etagamma2best=etagamma2;
      Mbest=M;M2Lbest=M2L;M2Lgamma1best=M2Lgamma1;M2Lgamma2best=M2Lgamma2;
      ptZbest=tlv2l2g.pT();
      etaZbest=tlv2l2g.eta();
      dR1best=dR1;dR2best=dR2;dRggbest=dRgg;
      Qpbest=(tlvplus+tlvphoton1+tlvphoton2).mass();
      Qmbest=(tlvminus+tlvphoton1+tlvphoton2).mass();
      Qcl1best=Qcl1;Qcl2best=Qcl2;
      tlv2l2gbest=tlv2l2g;
      tlvplusbest=tlvplus;
      tlvminusbest=tlvminus;
      tlvphoton1best=tlvphoton1;
      tlvphoton2best=tlvphoton2;
      
      isundressed=1;
      if (isundressed==1) {
	_h_Qp_mundr->fill(Qpbest,weight);
	_h_Qm_mundr->fill(Qmbest,weight);
	_h_dR1_mundr->fill(dR1best,weight);
	_h_dR2_mundr->fill(dR2best,weight);
	_h_dRgg_mundr->fill(dRggbest,weight);
	_h_pT_Z_mundr->fill(ptZbest,weight);
	//	tmmggundr->Fill();
      }
      }
	tmmggundr->Fill();
   //**************************************************************
      cout<<"selecting Z to mumugamma (dressed) "<<endl;
      ileptonplusbest=-1;
      jleptonminusbest=-1;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
      if (lepton_selection==2) {
	pTplusmax=0;
	pTminusmax=0;
	for (unsigned int i=0;i<good_mu.size();i++) {
	  if (good_mu[i].pid()==PID::MUON) {
	    double pT=good_mu[i].momentum().pT();
	    if (ileptonplusbest==-1 || pT>pTplusmax) {
	      ileptonplusbest=i;
	      pTplusmax=pT;
	    }
	  }
	  if (good_mu[i].pid()==PID::ANTIMUON) {
	    double pT=good_mu[i].momentum().pT();
	    if (jleptonminusbest==-1 || pT>pTminusmax) {
	      jleptonminusbest=i;
	      pTminusmax=pT;
	    }
	  }
	}
      }
      // Z->\mu^+\mu^-\gamma selection (dressed)
      error=0;
      tripletcounter=0;
      Mbest=-1.;M2Lgamma1best=-1.;M2Lgamma2best=-1.;Qpbest=-1.;Qmbest=-1.;Qcl1best=-1.;Qcl2best=-1.;etaZbest=-100.;ptZbest=-1.;dR1best=-1;dR2best=-1.;dRggbest=-1.;M2Lbest=-1;
      ptplusbest=-1.;ptminusbest=-1.;ptgamma1best=-1.,ptgamma2best=-1.;
      etaplusbest=-1.;etaminusbest=-1.;etagamma1best=-1.,etagamma2best=-1.;
      
      //cout<<"mu.size "<<good_mu.size()<<"ph.size "<<photons.size()<<endl;
      if(good_mu.size()>1&&photons.size()>1&&ileptonplusbest>=0&&jleptonminusbest>=0){
      tlvminus=good_mu[ileptonplusbest].momentum();
      etaminus=tlvminus.eta();
      ptminus=tlvminus.pT();
    
      tlvplus=good_mu[jleptonminusbest].momentum();
      etaplus=tlvplus.eta();
      ptplus=tlvplus.pT();
      
      tlv2l=tlvminus+tlvplus;
      M2L=tlv2l.mass();

      tlvphoton1=photons[kphoton1best].momentum();
      ptgamma1=tlvphoton1.pT();
      etagamma1=tlvphoton1.eta();

      tlvphoton2=photons[kphoton2best].momentum();
      ptgamma2=tlvphoton2.pT();
      etagamma2=tlvphoton2.eta();

      tlv2l2g=tlv2l+tlvphoton1+tlvphoton2;
      M=tlv2l2g.mass();

      M2Lgamma1=(tlv2l+tlvphoton1).mass();
      M2Lgamma2=(tlv2l+tlvphoton2).mass();

      // calculating dR1                                                                     
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton1.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton1.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton1.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton1.eta());
      dR1t=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2t=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1t<dR2t) {
        dR1=dR1t;
        Qcl1=(tlvminus+tlvphoton1).mass();
      }
      else {
        dR1=dR2t;
        Qcl1=(tlvplus+tlvphoton1).mass();
      }

      // calculating dR2
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton2.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton2.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton2.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton2.eta());
      dR1t=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2t=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1t<dR2t) {
        dR2=dR1t;
        Qcl2=(tlvminus+tlvphoton2).mass();
      }
      else {
        dR2=dR2t;
        Qcl2=(tlvplus+tlvphoton2).mass();
      }

      // calculating dRgg
      dPhi_1 = fabs(tlvphoton1.phi()-tlvphoton2.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvphoton1.eta()-tlvphoton2.eta());
      dRgg=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);

      isSelected=0;
      if(pTmax1<10*GeV) isSelected=2;
      if(pTmax2<10*GeV) isSelected=3;
      if(pTminusmax<15*GeV) isSelected=4;
      if(pTplusmax<15*GeV) isSelected=5;
      if(TMath::Max(pTplusmax,pTminusmax)<25*GeV) isSelected=6;
      if(dR1<0.4) isSelected=7;
      if(dR2<0.4) isSelected=8;
      if(M2Lgamma1>80*GeV) isSelected=9;
      if(M2Lgamma2>80*GeV) isSelected=10;
      if(pTmax1>15*GeV&&pTmax2>10*GeV&&pTminusmax>15*GeV&&pTplusmax>15*GeV&&TMath::Max(pTplusmax,pTminusmax)>25*GeV&&dR1>0.4&&dR2>0.4&&M2Lgamma1<80*GeV&&M2Lgamma2<80*GeV) isSelected=1;
      //cout<<"isSelected "<<isSelected<<endl;
      ptplusbest=ptplus;ptminusbest=ptminus;ptgamma1best=ptgamma1;ptgamma2best=ptgamma2;
      etaplusbest=etaplus;etaminusbest=etaminus;etagamma1best=etagamma1;etagamma2best=etagamma2;
      Mbest=M;M2Lbest=M2L;M2Lgamma1best=M2Lgamma1;M2Lgamma2best=M2Lgamma2;
      ptZbest=tlv2l2g.pT();
      etaZbest=tlv2l2g.eta();
      dR1best=dR1;dR2best=dR2;dRggbest=dRgg;
      Qpbest=(tlvplus+tlvphoton1+tlvphoton2).mass();
      Qmbest=(tlvminus+tlvphoton1+tlvphoton2).mass();
      Qcl1best=Qcl1;Qcl2best=Qcl2;
      tlv2l2gbest=tlv2l2g;
      tlvplusbest=tlvplus;
      tlvminusbest=tlvminus;
      tlvphoton1best=tlvphoton1;
      tlvphoton2best=tlvphoton2;
      isdressed=1;
      if (isdressed==1) {
	_h_Qp_m->fill(Qpbest,weight);
	_h_Qm_m->fill(Qmbest,weight);
	_h_dR1_m->fill(dR1best,weight);
	_h_dR2_m->fill(dR2best,weight);
	_h_dRgg_m->fill(dRggbest,weight);
	_h_pT_Z_m->fill(ptZbest,weight);
	_h_M2m->fill(M2Lbest,weight);
	// eta distributions
	_h_etaZ_m->fill(etaZbest,weight);
	_h_etaG1_m->fill(etagamma1best,weight);
	_h_etaG2_m->fill(etagamma2best,weight);
	_h_etaLplus_m->fill(etaplusbest,weight);
	_h_etaLminus_m->fill(etaminusbest,weight);
	isdressed=1;
	//	tmmgg->Fill();
      }
       }
	tmmgg->Fill();
      //*********************************************************
       cout<<"selecting Z to eegamma (bare) "<<endl;
      ileptonplusbest=-1;
      jleptonminusbest=-1;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
      if (lepton_selection==2) {
	pTplusmax=0;
	pTminusmax=0;
	for (unsigned int i=0;i<electrons.size();i++) {
	  if (electrons[i].pid()==PID::ELECTRON) {
	    double pT=electrons[i].momentum().pT();
	    if (ileptonplusbest==-1 || pT>pTplusmax) {
	      ileptonplusbest=i;
	      pTplusmax=pT;
	    }
	  }
	  if (electrons[i].pid()==PID::POSITRON) {
	    double pT=electrons[i].momentum().pT();
	    if (jleptonminusbest==-1 || pT>pTminusmax) {
	      jleptonminusbest=i;
	      pTminusmax=pT;
	    }
	  }
	}
      }
      // Z->e^+e^-\gamma selection (undressed leptons)
      error=0;
      tripletcounter=0;
      Mbest=-1.;M2Lgamma1best=-1.;M2Lgamma2best=-1.;Qpbest=-1.;Qmbest=-1.;Qcl1best=-1.;Qcl2best=-1.;etaZbest=-100.;ptZbest=-1.;dR1best=-1;dR2best=-1.;dRggbest=-1.;M2Lbest=-1;
      ptplusbest=-1.;ptminusbest=-1.;ptgamma1best=-1.,ptgamma2best=-1.;
      etaplusbest=-1.;etaminusbest=-1.;etagamma1best=-1.,etagamma2best=-1.; 
     
      if(electrons.size()>1&&photons.size()>1&&ileptonplusbest>=0&&jleptonminusbest>=0){
      tlvminus=electrons[ileptonplusbest].momentum();
      etaminus=tlvminus.eta();
      ptminus=tlvminus.pT();
    
      tlvplus=electrons[jleptonminusbest].momentum();
      etaplus=tlvplus.eta();
      ptplus=tlvplus.pT();
      
      tlv2l=tlvminus+tlvplus;
      M2L=tlv2l.mass();

      tlvphoton1=photons[kphoton1best].momentum();
      ptgamma1=tlvphoton1.pT();
      etagamma1=tlvphoton1.eta();

      tlvphoton2=photons[kphoton2best].momentum();
      ptgamma2=tlvphoton2.pT();
      etagamma2=tlvphoton2.eta();

      tlv2l2g=tlv2l+tlvphoton1+tlvphoton2;
      M=tlv2l2g.mass();

      M2Lgamma1=(tlv2l+tlvphoton1).mass();
      M2Lgamma2=(tlv2l+tlvphoton2).mass();

      // calculating dR1                                                                     
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton1.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton1.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton1.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton1.eta());
      dR1t=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2t=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1t<dR2t) {
        dR1=dR1t;
        Qcl1=(tlvminus+tlvphoton1).mass();
      }
      else {
        dR1=dR2t;
        Qcl1=(tlvplus+tlvphoton1).mass();
      }

      // calculating dR2
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton2.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton2.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton2.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton2.eta());
      dR1t=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2t=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1t<dR2t) {
        dR2=dR1t;
        Qcl2=(tlvminus+tlvphoton2).mass();
      }
      else {
        dR2=dR2t;
        Qcl2=(tlvplus+tlvphoton2).mass();
      }

      // calculating dRgg
      dPhi_1 = fabs(tlvphoton1.phi()-tlvphoton2.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvphoton1.eta()-tlvphoton2.eta());
      dRgg=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);

      isSelected=0;
      if(pTmax1<10*GeV) isSelected=2;
      if(pTmax2<10*GeV) isSelected=3;
      if(pTminusmax<15*GeV) isSelected=4;
      if(pTplusmax<15*GeV) isSelected=5;
      if(TMath::Max(pTplusmax,pTminusmax)<25*GeV) isSelected=6;
      if(dR1<0.4) isSelected=7;
      if(dR2<0.4) isSelected=8;
      if(M2Lgamma1>80*GeV) isSelected=9;
      if(M2Lgamma2>80*GeV) isSelected=10;

      if(pTmax1>15*GeV&&pTmax2>10*GeV&&pTminusmax>15*GeV&&pTplusmax>15*GeV&&TMath::Max(pTplusmax,pTminusmax)>25*GeV&&dR1>0.4&&dR2>0.4&&M2Lgamma1<80*GeV&&M2Lgamma2<80*GeV) isSelected=1;

      ptplusbest=ptplus;ptminusbest=ptminus;ptgamma1best=ptgamma1;ptgamma2best=ptgamma2;
      etaplusbest=etaplus;etaminusbest=etaminus;etagamma1best=etagamma1;etagamma2best=etagamma2;
      Mbest=M;M2Lbest=M2L;M2Lgamma1best=M2Lgamma1;M2Lgamma2best=M2Lgamma2;
      ptZbest=tlv2l2g.pT();
      etaZbest=tlv2l2g.eta();
      dR1best=dR1;dR2best=dR2;dRggbest=dRgg;
      Qpbest=(tlvplus+tlvphoton1+tlvphoton2).mass();
      Qmbest=(tlvminus+tlvphoton1+tlvphoton2).mass();
      Qcl1best=Qcl1;Qcl2best=Qcl2;
      tlv2l2gbest=tlv2l2g;
      tlvplusbest=tlvplus;
      tlvminusbest=tlvminus;
      tlvphoton1best=tlvphoton1;
      tlvphoton2best=tlvphoton2;

      isundressed=1;
      if (isundressed==1) {
	_h_Qp_eundr->fill(Qpbest,weight);
	_h_Qm_eundr->fill(Qmbest,weight);
	_h_dR1_eundr->fill(dR1best,weight);
	_h_dR2_eundr->fill(dR2best,weight);
	_h_dRgg_eundr->fill(dRggbest,weight);
	_h_pT_Z_eundr->fill(ptZbest,weight);
	//	teeggundr->Fill();
      }
      }
      teeggundr->Fill();
      //*****************************************************
      cout<<"selecting Z to eegamma (dressed) "<<endl;
      ileptonplusbest=-1;
      jleptonminusbest=-1;
      // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
      if (lepton_selection==2) {
	pTplusmax=0;
	pTminusmax=0;
	for (unsigned int i=0;i<good_el.size();i++) {
	  if (good_el[i].pid()==PID::ELECTRON) {
	    double pT=good_el[i].momentum().pT();
	    if (ileptonplusbest==-1 || pT>pTplusmax) {
	      ileptonplusbest=i;
	      pTplusmax=pT;
	    }
	  }
	  if (good_el[i].pid()==PID::POSITRON) {
	    double pT=good_el[i].momentum().pT();
	    if (jleptonminusbest==-1 || pT>pTminusmax) {
	      jleptonminusbest=i;
	      pTminusmax=pT;
	    }
	  }
	}
      }

      error=0;
      tripletcounter=0;
      Mbest=-1.;M2Lgamma1best=-1.;M2Lgamma2best=-1.;Qpbest=-1.;Qmbest=-1.;Qcl1best=-1.;Qcl2best=-1.;etaZbest=-100.;ptZbest=-1.;dR1best=-1;dR2best=-1.;dRggbest=-1.;M2Lbest=-1;
      ptplusbest=-1.;ptminusbest=-1.;ptgamma1best=-1.,ptgamma2best=-1.;
      etaplusbest=-1.;etaminusbest=-1.;etagamma1best=-1.,etagamma2best=-1.;

      cout<<"el.size "<<good_el.size()<<"ph.size "<<photons.size()<<endl;
     if(good_el.size()>1&&photons.size()>1&&ileptonplusbest>=0&&jleptonminusbest>=0){ 
      tlvminus=good_el[ileptonplusbest].momentum();
      etaminus=tlvminus.eta();
      ptminus=tlvminus.pT();
    
      tlvplus=good_el[jleptonminusbest].momentum();
      etaplus=tlvplus.eta();
      ptplus=tlvplus.pT();
      
      tlv2l=tlvminus+tlvplus;
      M2L=tlv2l.mass();

      tlvphoton1=photons[kphoton1best].momentum();
      ptgamma1=tlvphoton1.pT();
      etagamma1=tlvphoton1.eta();

      tlvphoton2=photons[kphoton2best].momentum();
      ptgamma2=tlvphoton2.pT();
      etagamma2=tlvphoton2.eta();

      tlv2l2g=tlv2l+tlvphoton1+tlvphoton2;
      M=tlv2l2g.mass();
      cout<<"M "<<M<<endl;

      M2Lgamma1=(tlv2l+tlvphoton1).mass();
      M2Lgamma2=(tlv2l+tlvphoton2).mass();

      // calculating dR1                                                                     
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton1.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton1.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton1.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton1.eta());
      dR1t=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2t=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1t<dR2t) {
        dR1=dR1t;
        Qcl1=(tlvminus+tlvphoton1).mass();
      }
      else {
        dR1=dR2t;
        Qcl1=(tlvplus+tlvphoton1).mass();
      }

      // calculating dR2
      dPhi_1 = fabs(tlvminus.phi()-tlvphoton2.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvminus.eta()-tlvphoton2.eta());
      dPhi_2 = fabs(tlvplus.phi()-tlvphoton2.phi());
      if (dPhi_2>pi) dPhi_2=2*pi-dPhi_2;
      dEta_2 = fabs(tlvplus.eta()-tlvphoton2.eta());
      dR1t=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);
      dR2t=sqrt(dPhi_2*dPhi_2+dEta_2*dEta_2);
      if (dR1t<dR2t) {
        dR2=dR1t;
        Qcl2=(tlvminus+tlvphoton2).mass();
      }
      else {
        dR2=dR2t;
        Qcl2=(tlvplus+tlvphoton2).mass();
      }

      // calculating dRgg
      dPhi_1 = fabs(tlvphoton1.phi()-tlvphoton2.phi());
      if (dPhi_1>pi) dPhi_1=2*pi-dPhi_1;
      dEta_1 = fabs(tlvphoton1.eta()-tlvphoton2.eta());
      dRgg=sqrt(dPhi_1*dPhi_1+dEta_1*dEta_1);


      isSelected=0;
      if(pTmax1<10*GeV) isSelected=2;
      if(pTmax2<10*GeV) isSelected=3;
      if(pTminusmax<15*GeV) isSelected=4;
      if(pTplusmax<15*GeV) isSelected=5;
      if(TMath::Max(pTplusmax,pTminusmax)<25*GeV) isSelected=6;
      if(dR1<0.4) isSelected=7;
      if(dR2<0.4) isSelected=8;
      if(M2Lgamma1>80*GeV) isSelected=9;
      if(M2Lgamma2>80*GeV) isSelected=10;

      if(pTmax1>15*GeV&&pTmax2>10*GeV&&pTminusmax>15*GeV&&pTplusmax>15*GeV&&TMath::Max(pTplusmax,pTminusmax)>25*GeV&&dR1>0.4&&dR2>0.4&&M2Lgamma1<80*GeV&&M2Lgamma2<80*GeV) isSelected=1;

      ptplusbest=ptplus;ptminusbest=ptminus;ptgamma1best=ptgamma1;ptgamma2best=ptgamma2;
      etaplusbest=etaplus;etaminusbest=etaminus;etagamma1best=etagamma1;etagamma2best=etagamma2;
      Mbest=M;M2Lbest=M2L;M2Lgamma1best=M2Lgamma1;M2Lgamma2best=M2Lgamma2;
      ptZbest=tlv2l2g.pT();
      etaZbest=tlv2l2g.eta();
      dR1best=dR1;dR2best=dR2;dRggbest=dRgg;
      Qpbest=(tlvplus+tlvphoton1+tlvphoton2).mass();
      Qmbest=(tlvminus+tlvphoton1+tlvphoton2).mass();
      Qcl1best=Qcl1;Qcl2best=Qcl2;
      tlv2l2gbest=tlv2l2g;
      tlvplusbest=tlvplus;
      tlvminusbest=tlvminus;
      tlvphoton1best=tlvphoton1;
      tlvphoton2best=tlvphoton2;

      isdressed=1;
      if (isdressed==1) {
	_h_Qp_e->fill(Qpbest,weight);
	_h_Qm_e->fill(Qmbest,weight);
	_h_dR1_e->fill(dR1best,weight);
	_h_dR2_e->fill(dR2best,weight);
	_h_dRgg_e->fill(dRggbest,weight);
	_h_pT_Z_e->fill(ptZbest,weight);
	_h_M2e->fill(M2Lbest,weight);
	_h_M2e1g->fill(Mbest,weight);
	// eta distributions
	_h_etaZ_e->fill(etaZbest,weight);
	_h_etaG1_e->fill(etagamma1best,weight);
	_h_etaG2_e->fill(etagamma2best,weight);
	_h_etaLplus_e->fill(etaplusbest,weight);
	_h_etaLminus_e->fill(etaminusbest,weight);
	//	teegg->Fill();
      }
     }
	teegg->Fill();
	cout<<"end events loop"<<endl;
    }//end events loop

    
    int fill_tree_withvars(TTree *intree) {
      // if something went wrong
      intree->Branch("error",&error,"error/I");
      // triplet counters
      intree->Branch("tripletcounter",&tripletcounter,"tripletcounter/I");
      // photon counters
      intree->Branch("photoncounter",&photoncounter,"photoncounter/I");
      // base var
      intree->Branch("isSelected",&isSelected,"isSelected/I");
      intree->Branch("M",&Mbest,"M/D");
      intree->Branch("M2Lgamma1",&M2Lgamma1best,"M2Lgamma1/D");  
      intree->Branch("M2Lgamma2",&M2Lgamma2best,"M2Lgamma2/D");    
      intree->Branch("Qp",&Qpbest,"Qp/D");
      intree->Branch("Qm",&Qmbest,"Qm/D");
      intree->Branch("Qcl1",&Qcl1best,"Qcl1/D");
      intree->Branch("Qcl2",&Qcl2best,"Qcl2/D");
      intree->Branch("ptZ",&ptZbest,"ptZ/D");
      intree->Branch("etaZ",&etaZbest,"etaZ/D");
      intree->Branch("dR1",&dR1best,"dR1/D");
      intree->Branch("dR2",&dR2best,"dR2/D");
      intree->Branch("dRgg",&dRggbest,"dRgg/D");
      intree->Branch("M2L",&M2Lbest,"M2L/D");
      intree->Branch("ptp",&ptplusbest,"ptp/D");
      intree->Branch("ptm",&ptminusbest,"ptm/D");
      intree->Branch("ptg1",&ptgamma1best,"ptg1/D");
      intree->Branch("ptg2",&ptgamma2best,"ptg2/D");
      intree->Branch("etap",&etaplusbest,"etap/D");
      intree->Branch("etam",&etaminusbest,"etam/D");
      intree->Branch("etag1",&etagamma1best,"etag1/D");
      intree->Branch("etag2",&etagamma2best,"etag2/D");
      intree->Branch("W",&weight,"W/D");
      // additional vars
      intree->Branch("dRll",&dRll,"dRll/D");
      intree->Branch("Eg",&Eg,"Eg/D");
      intree->Branch("njets",&njets,"njets/I");
       // Switches
      intree->Branch("isdressed",&isdressed,"isdressed/I");
      intree->Branch("isundressed",&isundressed,"isundressed/I");      
      return 1;
    }
    

    /// Normalise histograms etc., after the run
    void finalize() {
      cout <<"finalize: "<< event_counter << " " << event_counter_W << endl;
      _h_tot_all->fill(0.5,crossSection());
      cout<<" finalize-1  "<<endl;
      const double xs = crossSectionPerEvent()/femtobarn;
      
      scale(_h_tot_em, xs);

      if(countA==0){
      cout<<" finalize-2  "<<endl;
      //root
      fout->cd();
      cout<<" finalize-2-1  "<< endl;
      cout<<" Nteegg "<<teegg->GetEntries()<<" "<<endl;
      teegg->Write();
      cout<<" finalize-2-2"<< endl;
      cout<<"Ntmmgg "<<tmmgg->GetEntries()<<endl;
      tmmgg->Write();
      cout<<" finalize-3  "<<endl;
      teeggundr->Write();
      tmmggundr->Write();
      cout<<" finalize-4  "<<endl;
      fout->Write();
      fout->Close();
      countA++;
      }
      cout<<"end finalize "<<endl;

    } //end finalize
    //@}

  private:

    //@{
    //d01-x01-

    Histo1DPtr _h_tot_em;
    Histo1DPtr _h_tot_all;

    Histo1DPtr _h_Qp_e;
    Histo1DPtr _h_Qm_e;
    Histo1DPtr _h_dR1_e;
    Histo1DPtr _h_dR2_e;
    Histo1DPtr _h_dRgg_e;
    Histo1DPtr _h_Qp_m;
    Histo1DPtr _h_Qm_m;
    Histo1DPtr _h_dR1_m;
    Histo1DPtr _h_dR2_m;
    Histo1DPtr _h_dRgg_m;
    Histo1DPtr _h_pT_Z_e;
    Histo1DPtr _h_pT_Z_m;
    Histo1DPtr _h_M2e;
    Histo1DPtr _h_M2m;
    Histo1DPtr _h_M2e1g;
    Histo1DPtr _h_M2m1g;
    Histo1DPtr _h_M2e2g;
    Histo1DPtr _h_M2m2g;

    // eta distributions
    Histo1DPtr _h_etaZ_e;
    Histo1DPtr _h_etaG1_e;
    Histo1DPtr _h_etaG2_e;
    Histo1DPtr _h_etaLplus_e;
    Histo1DPtr _h_etaLminus_e;
    Histo1DPtr _h_etaZ_m;
    Histo1DPtr _h_etaG1_m;
    Histo1DPtr _h_etaG2_m;
    Histo1DPtr _h_etaLplus_m;
    Histo1DPtr _h_etaLminus_m;
    
    //undressed histograms
    Histo1DPtr _h_Qp_eundr;
    Histo1DPtr _h_Qm_eundr;
    Histo1DPtr _h_dR1_eundr;
    Histo1DPtr _h_dR2_eundr;
    Histo1DPtr _h_dRgg_eundr;
    Histo1DPtr _h_Qp_mundr;
    Histo1DPtr _h_Qm_mundr;
    Histo1DPtr _h_dR1_mundr;
    Histo1DPtr _h_dR2_mundr;
    Histo1DPtr _h_dRgg_mundr;
    Histo1DPtr _h_pT_Z_eundr;
    Histo1DPtr _h_pT_Z_mundr;
  

    //@}
    double pi;// = 3.1415926535897;
    double Zmass;
    int event_counter;
    double event_counter_W;

    // root
    TFile *fout;
    TTree *teegg;
    TTree *tmmgg;
    TTree *teeggundr;
    TTree *tmmggundr;

    FourMomentum tlvplus,tlvminus,tlvphoton1,tlvphoton2,tlv2l,tlv2l2g,tlvlpg,tlvlmg;
    FourMomentum tlvplusbest,tlvminusbest,tlvphoton1best,tlvphoton2best;
    FourMomentum tlv2l2gbest;

    // ttree variables
    int error; // due to bugs? 0 is O'k
    int tripletcounter;
    int photoncounter;
    int isdressed,isundressed;
    double Mbest,Qpbest,Qmbest,Qcl1best,Qcl2best,ptZbest,etaZbest,dR1best,dR2best,dRggbest,M2Lbest,M2Lgamma1best,M2Lgamma2best;
    double ptplusbest,ptminusbest,ptgamma1best,ptgamma2best;
    double etaplusbest,etaminusbest,etagamma1best,etagamma2best;
    double weight;
    // current vars
    double M,Qp,Qm,Qcl1,Qcl2,ptZ,etaZ,dR1,dR2,dRgg,M2L,M2Lgamma1,M2Lgamma2,dR1t,dR2t;
    double ptplus,ptminus,ptgamma1,ptgamma2;
    double etaplus,etaminus,etagamma1,etagamma2;
    // enums
    enum {BEST,CURRENT};
    
    // additional vars
    double dRll;
    double Eg;
    int njets;

    int countA;
    
    //      configurable data members
    //------------------------------------------------------------------------
    // cut_type: 1 -- default, 2 -- Kharlamov (1l trg), 3 -- Kharlamov (2l trg), 4 -- Kharlamov (1l trg || 2l trg)
    int cut_type;     
    // precut_type: 1 -- default, 2 -- using _check functions
    int precut_type;
    // 1 -- all photons, 2 -- only one photon with max p_T
    int photon_selection;
    // 1 -- all leptons, 2 -- only one lepton for each sign with max p_T
    int lepton_selection;
    // 1 -- default, 2 -- add dressed info into undressed tree (comparison only)
    int DressUndressMix;
    //for cutflow
    int isSelected;
    // precut valuse
    double precut_eta_mu;
    double precut_eta_el;
    //tmp variables
   
  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(llyyFFAnalysis);


}
