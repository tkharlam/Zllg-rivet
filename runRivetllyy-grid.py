theApp.EvtMax = -1
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']

rivet.HistoFile = 'MyOutput_2l2g.yoda.gz'
rivet.DoRootHistos = True
rivet.CrossSection = 1.0
rivet.Analyses += [ 'llyyFFAnalysis' ]
rivet.RunName = ""

job += rivet
